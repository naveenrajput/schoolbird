var historyVal = 0;
var historyArray = [];
var innerChange = 0;
var outerChange = 0;
var massEditUrl = 0;
function showSearch()
{
$('#searchBox').animate({top:'0'},100);
}

function hideSearch()
{
$('#searchBox').animate({top:'-100px'},100);
}

function closeBigModal()
{
$("#myModalBig").modal("hide");
}

function toogleFormTable()
{
	if(document.getElementById('formDiv').style.display == 'none')
	{
		$('#formDiv').show();
		$('#tableDiv').hide();
	}
	else
	{
		$('#formDiv').hide();
		$('#tableDiv').show();		
	}
}

function toogleFormTableModalBig()
{
	if(document.getElementById('formModalBig').style.display == 'none')
	{
		$('#formModalBig').show();
		$('#tableModalBig').hide();
	}
	else
	{
		$('#formModalBig').hide();
		$('#tableModalBig').show();		
	}

}

function addTask()
{
var d = new Date();
var n = d.getTime();
var x= '<div class="checkbox taskList" style="display:none" id="'+n+'">';
x+= '<label><input type="checkbox" value="">'+document.getElementById("taskAdder").value+'</label>';
x+= '</div>';
//var y = document.getElementById('taskList').innerHTML;
document.getElementById('taskList').insertAdjacentHTML('afterBegin',x);
document.getElementById("taskAdder").value = '';
$('#'+n).slideDown('fast');
}

function callCalModal()
{
getModal('projects/calData.php','tableModalBig','formModalBig','loading')
}

function showNotifBar()
{
$('#notifContainer').fadeToggle('fast')
}

function hideNotifBar()
{
$('#notifContainer').fadeOut('fast')
}

function showError(errorMessage,elm,markRed)
{
	if(markRed == 1)
	{
					elm.className = 'w3-input input1bRed';
					elm.addEventListener('focus',function(){
						this.className = 'w3-input input1bdark';
					});		
	}
	toast(errorMessage,'toast bg-danger','2000');
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

var notifTimeout;
function showBottomNotification(type,message)
{
	clearTimeout(notifTimeout);
	$('#bottomNotification').html("");

	if(type == 'stop')
	{
		document.getElementById('bottomNotification').style.backgroundColor = "rgba(216,99,73,0.8)";
		$('#bottomNotification').animate({opacity:1,width:'330px',padding:'20px'},300,function(){
			$('#bottomNotification').animate({width:'300px'},100,function(){
			$('#bottomNotification').html(message);
			});

		});
	notifTimeout = setTimeout(function(){
		hideBottomNotification();
	},8000);

	}


	if(type == 'successdel')
	{
		document.getElementById('bottomNotification').style.backgroundColor = "rgba(95,207,128,0.8)";
		$('#bottomNotification').animate({opacity:1,width:'330px',padding:'20px'},300,function(){
			$('#bottomNotification').animate({width:'300px'},100,function(){
			$('#bottomNotification').html(message);
			});

		});
	notifTimeout = setTimeout(function(){
		hideBottomNotification();
	},20000);

	}


	if(type == 'success')
	{
		document.getElementById('bottomNotification').style.backgroundColor = "rgba(95,207,128,0.8)";
		$('#bottomNotification').animate({opacity:1,width:'330px',padding:'20px'},300,function(){
			$('#bottomNotification').animate({width:'300px'},100,function(){
			$('#bottomNotification').html(message);
			});

		});
	notifTimeout = setTimeout(function(){
		hideBottomNotification();
	},8000);

	}

}


function hideBottomNotification()
{
	$('#bottomNotification').html("");
			$('#bottomNotification').animate({opacity:0.3},500,function(){
			$('#bottomNotification').animate({opacity:0,width:'0px',padding:'0px'},200);
		});
}

function tableSearch(event,url)
{

	if(event.keyCode == '13')
	{
		var varName = "";
		var varValue = "";
		var i =0;
		var itemSearch = $('.searchInput');
		searchList = $('#tableDiv').find(itemSearch).each(function(){
			if($(this).val() != '')
			{
			varName += $(this).attr('name')+",";				
			varValue += $(this).val()+"!NBCOMNB!";
			}


    });
		
		checkAlready = url.indexOf("varList=");
		if(checkAlready == -1)
		{
			checkQue = url.indexOf("?");
			if(checkQue == -1)
			{
				url = url+'?varList='+varName+'&varValues='+varValue;				
			}
			else
			{
				url = url+'&varList='+varName+'&varValues='+varValue;				
			}

		}
		else
		{
			temp = url.split("varList=");
			url = temp[0]+'varList='+varName+'&varValues='+varValue;
		}
		getModule(url,'tableDiv','fomrDiv','loading');
				
	}			
}

function checkAll(elm)
{
	var itemSearch = $('.checkInput');
	if(elm.checked == true)
	{
		searchList = $('#tableDiv').find(itemSearch).each(function(){
			//$(this).attr('checked') = 'checked';	
			$(this).attr("checked","checked");		
			this.checked = true;	
    });

	}
	else
	{
		searchList = $('#tableDiv').find(itemSearch).each(function(){
			//$(this).attr('checked') = 'checked';	
			$(this).removeAttr("checked");		
			this.checked = false;		
    });		
	}
}

function bulkAction()
{
	var action = document.getElementById('action').value;
	var tempAction = action.split(":");
	if(tempAction[0] == 'D')
	{
		crossCheckDelete(tempAction[1]);
	}
	else
	{
		massEdit();
		//showBottomNotification('stop','Mass edit feature not available yet, but will appear soon');
	}
}

function disableButton(id,loading)
{
	var orgClasName = document.getElementById(id).className;
	var tempClassName = orgClasName+" disabled";
	document.getElementById(id).className = tempClassName;
	if(loading != '')
	{
		document.getElementById(id).innerHTML = "<img src='images/circle-primary.GIF' alt='' style='height:12px;margin-bottom:2px;'/>&nbsp;&nbsp;"+loading;
	}
}

function enableButton(id,orgHTML)
{
	var tempClasName = document.getElementById(id).className;
	var orgClassName = tempClasName.replace(" disabled","");
	document.getElementById(id).className = orgClassName;
	if(orgHTML != '')
	{
		document.getElementById(id).innerHTML = orgHTML;
	}

}

function sortThis(url,tableDiv,formDiv,loading,orderParam)
{
	globalSendParam  = {
		orderby:orderParam
	}
	getModule(url,tableDiv,formDiv,loading);
}





var crtlKey = 0;
function checkMasterKey(e)
{
	if(e.keyCode == '17')
	{
		crtlKey = 1;
	}
	else if(e.keyCode =='77' && crtlKey == 1)
	{
		
		$('#masterKeyModal').modal();
		crtlKey = 0;
	}
	else
	{
		crtlKey = 0;
	}

}

function removeCrtl()
{
	crtlKey = 0;
}


function changeHashValue()
{
var hash = window.location.hash;
if(hash != '')
{
	if(innerChange == 0)
	{
		hash = hash.replace("#","");
		hashVals = hash.split("&rtl=");
		outerChange = 1;
		getModule(hashVals[1],hashVals[2],hashVals[3],hashVals[4])
	}
	else
	{
		innerChange = 0;
	}	
}
else
{
	//getModule('buses/index.php','tableDiv','formDiv','loading')
}

}


function toggleThings(show,hide,showprop,hideprop)
{

	if(hideprop == 'slide')
	{
		$('#'+hide).slideUp('fast');
	}
	else
	{
		$('#'+hide).fadeOut('fast');
	}

	if(showprop == 'slide')
	{
		$('#'+show).slideDown('fast');
	}
	else
	{
		$('#'+show).fadeIn('fast');
	}	



}

function checkInstallmentAmount(url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv)
{

	var currentAmount = parseInt(document.getElementById('mod2').value);
	var maxAmount = parseInt(document.getElementById('mod2').lang);

	if(maxAmount < currentAmount)
	{
		
		var thisurl = url.replace("savepayment","checkPending");
		genAjax(thisurl,'',function(response){
			var temp = parseInt(response);
			if(temp < currentAmount)
			{
				showError('The maximum outstanding amount for this customer is Rs.'+temp);
			}
			else
			{
				toggleThings('reallyPay','simplePay','','slide');
			}
		})				


	}
	else
	{
		savedata(url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv);
	}



//	
}

function checkEmail(email)
{
	document.getElementById('emailInp2').value = '';
	document.getElementById('emailInp2').placeholder = email;
	document.getElementById('loadResponse').innerHTML= '';
	document.getElementById('checkLoad').style.display = 'inline-block';
var url = "emails/checkEmail.php?email="+email;
		genAjax(url,'',function(response){
			document.getElementById('checkLoad').style.display = 'none';
			if(response == 0)
			{
				document.getElementById('emailInp2').value = email;
				document.getElementById('loadResponse').innerHTML = '<span class="glyphicon glyphicon-ok" style="color:green"></span>';
			}
			else
			{
				document.getElementById('emailInp2').placeholder = '';
				errorMessage = "Email account already exists";
				elm = document.getElementById('emailInp2');
				showError(errorMessage,elm,1);
				document.getElementById('loadResponse').innerHTML = '<span class="glyphicon glyphicon-alert" style="color:#b82121"></span>';
			}
		})	

}

setInterval(function(){
checkFrame();
},500);


function checkFrame()
{

	if(document.getElementById('genFrame'))
	{
		var x = window.innerWidth;
		x = x- (120+280);
		var lef = parseInt(x/2);
		document.getElementById('default2').style.left = lef +"px";
	}

}


function notWorking()
{
	$('#notWorking').modal();
}


var hiderTimeObj;


function toast(html,css,timeout)
{
	if(hiderTimeObj)
	{
		clearTimeout(hiderTimeObj);
	}
	document.getElementById('toastText').innerHTML = html;
	$('#toast').animate({bottom:'0px'});
	document.getElementById('toast').className = css;
	if(timeout != '0')
	{
	hiderTimeObj = setTimeout(function(){
hideToast()			
},timeout);
	}

}

function hideToast()
{
	$('#toast').animate({bottom:'-300px'});
}



function multiSelect(fromId, toId)
{
var x = '';
   var checkboxArray = document.getElementById(fromId);
        for (var i = 0; i < checkboxArray.length; i++) {
          if (checkboxArray.options[i].selected) {
          	if(fromId == 'route3' && toId == 'route4')
          	{
          	 var temp = checkboxArray[i].value;
          	 temp = temp.split(":::");
          	 x += temp[1]+",";
          	}
          	else
          	{
	          	x += checkboxArray[i].value+",";          		
          	}

          }
        }
        document.getElementById(toId).value = x;
}

var valString = '';

function collectStopValues(i,total,toid)
{
valString = '';
if(document.getElementById('li'+i).className == 'list-group-item')
{
	document.getElementById('li'+i).className = 'list-group-item li-selected';
	if(document.getElementById('not-selectedTab').className == 'active')
	{
		$('#li'+i).slideUp();
	}

}
else
{
	document.getElementById('li'+i).className = 'list-group-item';
	if(document.getElementById('selectedTab').className == 'active')
	{
		$('#li'+i).slideUp();
	}
}



for(j=0;j<total;j++)
{
	if(document.getElementById('li'+j))
	{
		if(document.getElementById('li'+j).className == 'list-group-item li-selected')
		{
			var x = document.getElementById('li'+j).lang;
			x = x.split(":::");
			valString += x[1]+",";
		}
	}
}
document.getElementById(toid).value = valString;

displayRoute();
}





function displayRoute()
{
	document.getElementById('targetFrame').contentWindow.generateRoute();
}


function toogleSelect(elm)
{
	if(elm.lang == '1')
	{
		elm.lang = '0';
		x = elm.className;
		x = x.replace(" selectedItem","");
		elm.className = x;

	}
	else
	{
		elm.lang = '1';
		x = elm.className;
		if(x.indexOf('selectedItem') == -1)
		{
		x = x + " selectedItem";			
		}

		elm.className = x;		
	}
}


function selectAll(selector)
{	
	if(selector.lang == '1')
	{
			todo = '1';
			selector.lang = '0';
	}
	else
	{
		todo = '0';
		selector.lang = '1';
	}

	var total = document.getElementById('totalBusCount').value;
	for(j=0;j<total;j++)
	{
		if(document.getElementById('buslistRow'+j))
		{
			elm = document.getElementById('buslistRow'+j);		
if(todo == '1')
{
		elm.lang = '0';
		x = elm.className;
		x = x.replace(" selectedItem","");
		elm.className = x;
}
else
{

		elm.lang = '1';
		x = elm.className;
		if(x.indexOf('selectedItem') == -1)
		{
		x = x + " selectedItem";			
		}

		elm.className = x;
	
}
		}
	}
}
var selectedBuses = '';
function selectBuses()
{
selectedBuses = '';
	var total = document.getElementById('totalBusCount').value;
	for(j=0;j<total;j++)
	{
		if(document.getElementById('buslistRow'+j))
		{
			if(document.getElementById('buslistRow'+j).lang == '1')
			{
				selectedBuses += document.getElementById('buslistRow'+j).getAttribute('data-value')+",";
			}
		}
	}
	//return "65";
	return selectedBuses;
}


function liveTrack()
{
selectedBuses = selectBuses();
if(selectedBuses == '')
{
	toast("Please select atleast one bus to display live tracking",'toast bg-danger','3000');
	return;
}
else
{
	window.open("tracking/trackMap.php?buslist="+selectedBuses,'_blank');
}
}

function selectOperation(id)
{
for(j=0;j<=6;j++)
{
	if(document.getElementById('opSel'+j))
	{
		if(j != id)
		{
			document.getElementById('opSel'+j).className = "";	
			$('#rep'+j).hide();	
		}
	}
}
document.getElementById('opSel'+id).className = "selectedred";
$('#rep'+id).show();

}


function drawSpeedChart(type)
{
	var speedBus = document.getElementById('speedBus').value;
	var speedDate = document.getElementById('speedDate').value;
	var speedFrom = document.getElementById('speedFrom').value;
	var speedTo = document.getElementById('speedTo').value;

	if(speedBus == '' || speedDate == '' || speedFrom == '' || speedTo == '')
	{
		toast("Please fill all the fields.","toast bg-danger","4000");
		return false;
	}
	if(type == 'in')
	{
	document.getElementById('speedResult').src = 'tracking/speedchartresult.php?bus='+speedBus+'&fordate='+speedDate+'&fromtime='+speedFrom+'&totime='+speedTo;
	showProcessing();
	$( "#tableDiv" ).scrollTop( 300 );		
	}
	else
	{
		window.open('tracking/speedchartresult.php?bus='+speedBus+'&fordate='+speedDate+'&fromtime='+speedFrom+'&totime='+speedTo,'_blank');
	}
}


function drawHaltChart(type)
{
	var haltBus = document.getElementById('haltBus').value;
	var fromDate = document.getElementById('haltFrom').value;
	var toDate = document.getElementById('haltTo').value;
	var minHalt = document.getElementById('minHalt').value;
	if(haltBus == '' || fromDate == '' || toDate == '' || minHalt == '')
	{
		toast("Please fill all the fields.","toast bg-danger","4000");
		return false;
	}
	if(type == 'in')
	{
	document.getElementById('haltResult').src = 'tracking/haltChart.php?bus='+haltBus+'&fromdate='+fromDate+'&todate='+toDate+'&minhalt='+minHalt;	
	showProcessing();
	$( "#tableDiv" ).scrollTop( 300 );		
	}
	else
	{
		window.open('tracking/haltChart.php?bus='+haltBus+'&fromdate='+fromDate+'&todate='+toDate+'&minhalt='+minHalt,'_blank');
	}
}


function drawActivityChart(type)
{
	var speedBus = document.getElementById('actBus').value;
	var speedDate = document.getElementById('actDate').value;
	var speedFrom = document.getElementById('actFrom').value;
	var speedTo = document.getElementById('actTo').value;

	if(speedBus == '' || speedDate == '' || speedFrom == '' || speedTo == '')
	{
		toast("Please fill all the fields.","toast bg-danger","4000");
		return false;
	}
	if(type == 'in')
	{
	document.getElementById('activityResult').src = 'tracking/activityResult.php?bus='+speedBus+'&fordate='+speedDate+'&fromtime='+speedFrom+'&totime='+speedTo;
	showProcessing();
	$( "#tableDiv" ).scrollTop( 300 );
	}
	else
	{
		window.open('tracking/activityResult.php?bus='+speedBus+'&fordate='+speedDate+'&fromtime='+speedFrom+'&totime='+speedTo,'_blank');
	}
}

function searchBusTable(value)
{
	value = value.toUpperCase();
var total = document.getElementById('totalBusCount').value;
if(value == '')
{
for(j=0;j<total;j++)
{

		$('#buslistRow'+j).show();

}
}
else
{
for(j=0;j<total;j++)
{
	x = document.getElementById('buslistTd'+j).innerHTML;
	if(x.indexOf(value) != -1)
	{
		$('#buslistRow'+j).show();
	}
	else
	{
		$('#buslistRow'+j).hide();
	}
}
}

}


function getAddress(elm)
{
elm.innerHTML = "<i class='fa fa-spin fa-spinner'></i>"
	var geoCodeUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+elm.lang+"&sensor=true";

	 $.ajax({
    type: 'POST',
    dataType:'json',
    url: geoCodeUrl,
    data: '', 
    success: function(responseData) {
    	j=0;
			 for (var key in responseData) {
			 	if(j ==0)
			 	{
			 		var html = "<a href='https://www.google.co.in/maps?q="+elm.lang+"' target='_blank'>"+responseData[key][0].formatted_address+"</a>"
			        elm.innerHTML = html; 
			        elm.className ='';		
			 	}
			j++;
			 	}
    
    },
    error: function() {
    	toast("Unable to get location",'toast bg-danger', '2000');
    }
});
}

function deleteData(id,table)
{
	var reply = confirm("Are you sure you want to delete this entry?")
	if(reply)
	{
		var url = "deleteData.php?table="+table+"&id="+id;
		genAjax(url,'',function(){
			if(table != 'busroutemap')
			{
			toogleFormTable();				
			}

			toast("Successfully Deleted.","toast bg-success","3000");
			document.getElementById('tableRow'+id).className = 'bg-danger';
			setTimeout(function(){
			$('#tableRow'+id).hide();
			},2000);

		})
	}
}


function rotateMenu(id)
{
for(j=0;j<6;j++)
{
	if(document.getElementById('menutr'+j))
	{
		if(j != id)
		{
			document.getElementById('menutr'+j).className = "";	
		}
	}
}
document.getElementById('menutr'+id).className = "active";
}


function filterRouteSelect(type)
{
	if(type == '')
	{
		if(document.getElementById('allTab').className == 'active')
		{
			type = 'all';
		}
		else if(document.getElementById('selectedTab').className == 'active')
		{
			type = 'selected';
		}
		else
		{
			type = 'not-selected';
		}
	}
	else
	{
		document.getElementById('allTab').className= '';
		document.getElementById('selectedTab').className= '';
		document.getElementById('not-selectedTab').className= '';
		document.getElementById(type+'Tab').className= 'active';
	}


var searchTerm = document.getElementById('stopSearcher').value;
searchTerm = searchTerm.toUpperCase();
var total = document.getElementById('totalStops').value;
if(type == 'all')
{
for(j=0;j<total;j++)
{
	if(searchTerm != '')
	{
			t = document.getElementById('li'+j).title;
			t = t.toUpperCase();
			y = t.indexOf(searchTerm);
			if(y != -1)
		{
			$('#li'+j).show();
		}
		else
		{
			$('#li'+j).hide();
		}
	}
	else
	{
		$('#li'+j).show();		
	}


}
}
else if(type == 'selected')
{
for(j=0;j<total;j++)
{
	x = document.getElementById('li'+j).className;
	if(x == 'list-group-item li-selected')
	{
		if(searchTerm != '')
		{
			t = document.getElementById('li'+j).title;
			t = t.toUpperCase();
			y = t.indexOf(searchTerm);
			if(y != -1)
			{
				$('#li'+j).show();
			}
			else
			{
				$('#li'+j).hide();
			}
		}
		else
		{
			$('#li'+j).show();		
		}
	}
	else
	{
		
		$('#li'+j).hide();
	}
}
}
else if(type == 'not-selected')
{
for(j=0;j<total;j++)
{
	x = document.getElementById('li'+j).className;
	if(x == 'list-group-item li-selected')
	{
		$('#li'+j).hide();
	}
	else
	{
		if(searchTerm != '')
		{
			t = document.getElementById('li'+j).title;
			t = t.toUpperCase();
			y = t.indexOf(searchTerm);
			if(y != -1)
			{
				$('#li'+j).show();
			}
			else
			{
				$('#li'+j).hide();
			}
		}
		else
		{
			$('#li'+j).show();		
		}
	}
}
}

}


function searchStudents()
{
	var term = document.getElementById('searchterm').value;
		var indata = document.getElementById('indata').value;
	if(term == '')
	{
		return false;
	}
	if(term.length < 2)
	{
		toast('Please enter atleast four letters','bg-danger','3000');
		return false;
	}
	
	if(document.getElementById('thisisleftindex'))
	{
    	getModule('students/leftindex.php?term='+term+'&indata='+indata,'tableDiv','formDiv','loading');
	}
	else
	{
	getModule('students/index.php?term='+term+'&indata='+indata,'tableDiv','formDiv','loading');
		    
	}

}



function searchParents()
{
	var term = document.getElementById('searchparent').value;
	if(term == '')
	{
		return false;
	}
	if(term.length < 4)
	{
		toast('Please enter atleast four letters','bg-danger','3000');
		return false;
	}
	getModule('students/merge-parents.php?term='+term,'formDiv','tableDiv','loading');
	
}



function sendMessageSingle(id)
{
	var msg = document.getElementById('msg').value;
	if(msg == '')
	{
		return false;
	}

var url = 'students/savemessage.php?id='+id;
var params = {
	message:msg
}
genAjax(url,params,function(result){
	document.getElementById('msg-container').insertAdjacentHTML('afterBegin',result);
	document.getElementById('msg').value = '';
});

}

function saveDiary(id)
{

var msg = document.getElementById('msg').value;
var heading = document.getElementById('heading').value;
	if(msg == '' || heading == '')
	{
		return false;
	}

var url = 'students/savediary.php?id='+id;
var params = {
	message:msg,
	heading:heading
}
genAjax(url,params,function(result){
	document.getElementById('msg-container').insertAdjacentHTML('afterBegin',result);
	document.getElementById('msg').value = '';
	document.getElementById('heading').value = '';
});

}


function getAttendance()
{
	var className = document.getElementById('class').value;
	var atdate = document.getElementById('atdate').value;
	if(className == '')
	{
		return false;
	}
	var params = {
		date:atdate,
		class:className
	}
	var url = 'attendance/view.php';
	genAjax(url,params,function(result){
		document.getElementById('resultCont').innerHTML= result;
	})
}


function getStudents()
{
	var className = document.getElementById('class').value;
	if(className == '')
	{
		return false;
	}
	var params = {
		class:className
	}
	var url = 'messages/getstudents.php';
	genAjax(url,params,function(result){
		document.getElementById('stucontainer').innerHTML= result;
	})

}

function sendMessage()
{
	var stuName = document.getElementById('student').value;
	var className = document.getElementById('class').value;
	var message = document.getElementById('message').value;
	if(className == '' || stuName == '' || message == '')
	{
		return false;
	}
	var params = {
		class:className,
		student:stuName,
		message:message
	}
	var url = 'messages/sendMessage.php';
	genAjax(url,params,function(result){
		document.getElementById('resultholder').innerHTML= result;
	})


}

function getTeacherAttendance()
{

	var atdate = document.getElementById('atdate').value;
	var params = {
		date:atdate
			}
	var url = 'teacherattendance/view.php';
	genAjax(url,params,function(result){
		document.getElementById('resultCont').innerHTML= result;
	})	
}


function getFees()
{
		var className = document.getElementById('class').value;
	if(className == '')
	{
		return false;
	}
	var params = {
		class:className
	}
	var url = 'fees/getfees.php';
	genAjax(url,params,function(result){
		document.getElementById('resultCont').innerHTML= result;
	})

}


function saveBulkRouteBuses()
{
	showProcessing();
	var checkedBuses= '';
	var tbus = document.getElementById('tbus').value;
	for(j=0;j<tbus;j++)
	{
		if(document.getElementById('tbus'+j))
		{
			if(document.getElementById('tbus'+j).checked == true)
			{
				checkedBuses += document.getElementById('tbus'+j).value+",";
			}
		}
	}


	var checkedDays= '';
	for(j=0;j<6;j++)
	{
		if(document.getElementById('day'+j))
		{
			if(document.getElementById('day'+j).checked == true)
			{
				checkedDays += document.getElementById('day'+j).value+",";
			}
		}
	}


var type = document.getElementById('routeType').value;
var route = document.getElementById('selRoute').value;
var fromTime = document.getElementById('routeFromTime').value;
var toTime = document.getElementById('routeToTime').value;

if(type == '' || route == '' || fromTime == '' || toTime == '' || checkedBuses == '' || checkedDays == '')
{
toast('Please fill all the fields.','toast bg-danger','5000');	
return false;
} 


var url = "buses/saveBulkData.php?type="+type+"&route="+route+"&fromtime="+fromTime+"&totime="+toTime+"&days="+checkedDays+"&checkedBuses="+checkedBuses;
		genAjax(url,'',function(response){
			hideProcessing();
			toast('Timing successfully updated.','toast bg-success','3000');
//			console.log(response);
//			alert(response);
		})


}


function selectStop(id)
{
	$('#li'+id).trigger('click');
}


function getBulkRouteStudents(id)
{

	var selClasses = '';
	var total = document.getElementById('totalClasses').value;
	for(j=0;j<total;j++)
	{
		if(document.getElementById('class'+j))
		{
			if(document.getElementById('class'+j).checked == true)
			{
				selClasses = selClasses+document.getElementById('class'+j).value+",";
			}
		}
	}
	


getModule('students/getData.php?route='+document.getElementById('selRoute').value+'&class='+selClasses,'stuResult','','loading')


}

function toggleCheck(id)
{
	if(document.getElementById(id).checked == true)
	{
		document.getElementById(id).checked = false;
	}
	else
	{
		document.getElementById(id).checked = true;
	}

if(id.indexOf('tosave') != -1)
{
	var total = document.getElementById('totalMatch').value;
	for(j=0;j<total;j++)
	{
		if(id != 'tosave'+j)
		{
			if(document.getElementById('tosave'+j))
			{
				document.getElementById('tosave'+j).checked = false;
			}

		}
	}


}

}


function saveBulkRoute()
{
	var route = document.getElementById('selRoute').value;

	var selStudents = '';
	var totalmatch = document.getElementById('totalMatch').value;
	var totalunmatch = document.getElementById('totalUnMatch').value;
	for(j=0;j<totalmatch;j++)
	{
		if(document.getElementById('match'+j))
		{
			if(document.getElementById('match'+j).checked == true)
			{
				selStudents = selStudents+document.getElementById('match'+j).value+",";
			}
		}
	}
	

	for(j=0;j<totalunmatch;j++)
	{
		if(document.getElementById('unmatch'+j))
		{
			if(document.getElementById('unmatch'+j).checked == true)
			{
				selStudents = selStudents+document.getElementById('unmatch'+j).value+",";
			}
		}
	}

	var routeType = document.getElementById('selRouteType').value;

	var url = 'students/savebulkroutemapping.php?students='+selStudents+'&type='+routeType+'&route='+route;
	var params = '';
	genAjax(url,params,function(response){
		//alert(response);
		toast('Data Successfully Saved','toast bg-success','3000');
	});
	}


	function saveLeaveData()
	{
		var date = document.getElementById('actDate').value;
		var selClasses = '';

totalClasses = document.getElementById('totalClasses').value;
for(j=0;j<totalClasses;j++)
	{
		if(document.getElementById('leave'+j))
		{
			if(document.getElementById('leave'+j).checked == true)
			{
				selClasses = selClasses+document.getElementById('leave'+j).value+",";
			}
		}
	}



	var url = 'buses/saveclassleave.php?date='+date+'&classes='+selClasses;
	var params = '';
	genAjax(url,params,function(response){
//		alert(response);
		toast('Data Successfully Saved','toast bg-success','3000');
	});
}





function mergeParents()
{

showProcessing();
	var mergeString= '';
	var saveParent = '';
	var totalMerge = document.getElementById('totalMatch').value;
	for(j=0;j<totalMerge;j++)
	{
		if(document.getElementById('tomerge'+j))
		{
			if(document.getElementById('tomerge'+j).checked == true)
			{
				mergeString = mergeString+document.getElementById('tomerge'+j).value+",";
			}
		}
	}
	

	for(j=0;j<totalMerge;j++)
	{
		if(document.getElementById('tosave'+j))
		{
			if(document.getElementById('tosave'+j).checked == true)
			{
				saveParent = document.getElementById('tosave'+j).value;
			}
		}
	}


	var url = 'students/saveMerge.php?merge='+mergeString+'&save='+saveParent;
	var params = '';
	genAjax(url,params,function(response){
		toast('Parents Merged','toast bg-success','3000');
		searchParents();
	});
	}

	function shiftStudents()
	{
		showProcessing();
		var fromclass = document.getElementById('blkClass').value;
		var toclass = document.getElementById('blkClassTo').value;
		var totalStudents= document.getElementById('totalStudents').value;
		var studentsSelected = '';
		for(j=0;j<totalStudents;j++)
		{
			if(document.getElementById('student'+j))
			{
				if(document.getElementById('student'+j).checked == true)
				{
					studentsSelected = studentsSelected+','+document.getElementById('student'+j).value					
				}
			}

		}

		var url = 'students/shiftStudents.php?from='+fromclass+'&to='+toclass+'&students='+studentsSelected;
		var params = '';
		genAjax(url,params,function(response){
			hideProcessing();
			toast('Students Successfully Shifted','toast bg-success',3000);
			$('#studentData').slideUp();

		})
	}



function checkStudentId()
{
showProcessing();
    var stid = document.getElementById('stop1').value;
    if(stid == '')
    {
    toast("Please enter valid student id","toast bg-danger","4000");
    $('#moduleSaveButtontopStudent').hide();
    hideProcessing();
    }
    else
    {
    var url = 'students/checkstid.php?stid='+stid;
    	genAjax(url,'',function(response){
    	hideProcessing();
    		if(response.indexOf('FALSE')  != -1)
    		{
			    toast("StudentId already exists.","toast bg-danger","4000");
			    $('#moduleSaveButtontopStudent').hide();
			    document.getElementById('stop1').value = '';
			
    		}
    		else
    		{
    			$('#moduleSaveButtontopStudent').show();
    		}
    	});
    }
    
}    

