var globalSendParam;
var sortingArray= [];
sortingArray[0] = 'students/getStudents.php';
sortingArray[1] = 'classes/index.php';
sortingArray[2] = 'employee/index.php';
sortingArray[3] = 'enquiry/index.php';
sortingArray[4] = 'subjects/index.php';
sortingArray[5] = 'lectures/index.php';
sortingArray[6] = 'house/index.php';
sortingArray[7] = 'academicyear/index.php';
sortingArray[8] = 'rooms/index.php';
sortingArray[9] = 'setup/general/index.php';
sortingArray[10] = 'supplier/index.php';
sortingArray[11] = 'books/index.php';
sortingArray[12] = 'library/issue-book.php';
sortingArray[13] = 'library/return-book.php';
sortingArray[14] = 'library/movement-log.php';
sortingArray[15] = 'library/bookdefaulters.php';
sortingArray[16] = 'library/daily-log.php';
sortingArray[17] = 'exams/gradingscale.php';
sortingArray[18] = 'exams/allotment.php';
sortingArray[19] = 'exams/timetable.php';
sortingArray[20] = 'employee-leaves/index.php';
sortingArray[21] = 'buses/index.php';
sortingArray[22] = 'routes/index.php';
sortingArray[23] = 'stops/index.php';
sortingArray[24] = 'fees/collection.php';
sortingArray[25] = 'fees/allocateddata.php';
sortingArray[26] = 'fees/getDefaulters.php';
sortingArray[27] = 'fees/getOutstanding.php';
sortingArray[28] = 'documents/index.php';
sortingArray[29] = 'interactionreport/index.php';
sortingArray[30] = 'studentadmission/view.php';
sortingArray[31] = 'frontdesk/employee-registration.php';
sortingArray[32] = 'visitorpass/index.php';
sortingArray[33] = 'student-gatepass/index.php';
sortingArray[34] = 'employee-gatepass/index.php';
sortingArray[35] = 'class-allotment/getStudents.php';
sortingArray[36] = 'no-dues/index.php';
sortingArray[37] = 'transfer-certificate/index.php';
sortingArray[38] = 'events/index.php';
sortingArray[39] = 'remarks/getstudents.php';
sortingArray[40] = 'student-leaves/index.php';
sortingArray[41] = 'fees/new-collection.php';

function getModule(url,showresponse,hideresponse,loading)
{ $("#bdaybtn").click(function() {
    document.getElementById('bdaySpot').style.display = 'none';
   });
   
$("#notifbtn").click(function() {
    document.getElementById('notifSpot').style.display = 'none';
   });
 $("#visitsbtn").click(function() {
    document.getElementById('visitorSpot').style.display = 'none';
   });
closeTopMenu();
if(showresponse == 'formDiv')
{
  document.getElementById('tableModalBig').innerHTML = '';
  document.getElementById('formModalBig').innerHTML = '';
}
  if(document.getElementById(showresponse))
  {
  var loadingVars = [];
  var checkLoadingType = loading.indexOf(":");
  if(outerChange != 1)
  {
    outerChange = 0;
    innerChange = 1;
    window.location.hash = "&rtl="+url+"&rtl="+showresponse+"&rtl="+hideresponse+"&rtl="+loading;
  }
  else
  {
   outerChange = 0; 
  }
  
  if(checkLoadingType == -1)
  {
  showProcessing();
  //  $('#'+loading).show();
  }
  else
  {
    loadingVars = loading.split(":");
    $('#'+loadingVars[0]).html = loadingVars[1];
  }

        $.post(url,
        {
        globalSendParam
        },
        function(data,status){
          globalSendParam = '';
          document.getElementById(showresponse).innerHTML = data;
//          dummyButtonActions();
            dummyAtMark();

            for(k=0;k<=sortingArray.length;k++)
            {
              if(sortingArray[k])
              {
                if(url.indexOf(sortingArray[k]) != -1)
                {
                              $("#dataTable").excelTableFilter();   
                }
              }
            }



          
          if(url.indexOf('eod/index.php') != -1)
          {
            setEodMech();
          }


          if(url.indexOf('trigger=') != -1)
          {
            temp = url.split('trigger=');
            $('#'+temp[1]).trigger('click');
          }
          

          if(document.getElementById('timeBox'))
          {
            clearInterval(decorateInterval);
            startDecoration();
          }

          setColumnSelectionRule();
          setJsColor();
          setCkeditor();
          checkHeightBoxes();

          if(showresponse != 'bottomDiv')
          {
            console.log('hereup');
            $('#'+showresponse).show();
            $( "#"+showresponse ).scrollTop( 0 );            
          }
          else
          {
            console.log('here');
            $('#bottomDivContainer').animate({top:'0px'});
          }

          if(hideresponse != '')
          {
            $('#'+hideresponse).hide();
          }
          
    
    if(checkLoadingType == -1)
      {
        hideProcessing();
        //$('#'+loading).hide();
      }
      else
      {
        loadingVars = loading.split(":");
        $('#'+loadingVars[0]).html = loadingVars[2];
      }

        });
        }
        else
        {
          showError('The system is modified since last update. Please <a href="default.php" style="color:#b82121;text-decoration:underline"><span class="glyphicon glyphicon-reload"></span>Reload</a> to continue')
        }
        
    }


    function genAjax(url,params,callback)
    {
      showProcessing();

$.ajax({
  type: "POST",
  url: url,
  data: params,
  success: function(data){
    hideProcessing();
    if(callback != 'nothing')
    {
       callback(data);      
    }

    
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    if(document.getElementById('toast').innerHTML.indexOf('Retrying') != -1)
    {
      setTimeout(function(){
   toast('Unable to connect to Server. Please check your network connection. Will retry in a few moments','toast bg-danger','0');
      },2000);
    }
    else
    {
         toast('Unable to connect to Server. Please check your network connection. Will retry in a few moments','toast bg-danger','0');
    }

   setTimeout(function(){
    toast('Retrying..','toast bg-info','0');
genAjax(url,params,callback);
   },5000);
  }
});

    }



var loadInterval;

    function showProcessing()
    {
      
      $('#loading').show();

      document.getElementById('loadbar').style.width = "0%";

$('#loadbar').animate({width:'85%'},{
                  duration:10000,
                  done:function(){
                    $('#stopLoad').fadeIn();
                  }
                }); 
    }


    function hideProcessing()
    {

    $('#loadbar').stop();
    
      $('#loadbar').animate({width:'100%'},300);
      setTimeout(function(){
      $('#stopLoad').fadeOut();
      $('#loading').hide();     
    },500);
    }

function getStudentData(item){
    
    alert(item);
}