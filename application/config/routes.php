<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['(:num)'] = "page/index/$1";
$route['translate_uri_dashes'] = FALSE;


//sidebar menu  rought for Dashboard

$route['profile']           = 'dashboard/dashboard/profile';
$route['sign-out']          = 'dashboard/dashboard/sign_out';
//$route['add-enquiry']       = 'front_desk/front_desk/Add_Enquiry';


//sidebar menu  rought for Front_desk

$route['docs']              = 'front_desk/front_desk/docs';

//IN Front_desk:- Enquery,Interaction,Admission,Employee,Visitors,Gatepass routes//

$route['enquiry']           = 'front_desk/front_desk/Enquiry';
$route['add-enquiry']       = 'front_desk/Front_desk/Add_Enquiry';
$route['insert-enquiry']           = 'front_desk/Front_desk/Insert_Enquiry';
$route['edit-enquiry']    = 'front_desk/Front_desk/Edit_Enquiry';
$route['edit-enquiry/(:any)']    = 'front_desk/Front_desk/Edit_Enquiry/$1';
$route['update-enquiry']    = 'front_desk/Front_desk/Update_Enquiry';

$route['reminder']       = 'front_desk/Front_desk/Reminder';
$route['add-reminder']       = 'front_desk/Front_desk/Add_Reminder';
$route['close-enquiry']    = 'front_desk/Front_desk/Close_Enquiry';
$route['hold-enquiry']    = 'front_desk/Front_desk/Hold_Enquiry';
$route['unhold-enquiry']    = 'front_desk/Front_desk/Unhold_Enquiry';
$route['status-change']    = 'front_desk/Front_desk/Status_change';
$route['interact']       = 'front_desk/Front_desk/View_interaction';
$route['send-interaction']       = 'front_desk/Front_desk/Send_Interaction';
$route['review']       = 'front_desk/Front_desk/Review';
$route['add-review']       = 'front_desk/Front_desk/Add_Review';
$route['dereview']       = 'front_desk/Front_desk/Detactnumber';

$route['Interaction']       = 'front_desk/front_desk/Reports';
$route['add-interaction']   = 'front_desk/front_desk/Add_Interaction';
$route['add-interaction/(:any)']= 'front_desk/Front_desk/Add_Interaction/$1';
$route['insert-interaction']= 'front_desk/Front_desk/Insert_Interaction';

$route['update-interaction']    = 'front_desk/Front_desk/Update_Interaction';
$route['admission']         = 'front_desk/front_desk/Admission';
$route['stud-admission']         = 'front_desk/front_desk/Stud_Admission';
$route['employee']          = 'front_desk/front_desk/Employee';
$route['add-emp']          = 'front_desk/front_desk/Insert_employee';
$route['add-employee']      = 'front_desk/front_desk/Add_Employee';
$route['edit-employee/(:any)']      = 'front_desk/front_desk/Edit_Employee/$1';
$route['update-emp']          = 'front_desk/front_desk/Update_employee';
$route['visitors']          = 'front_desk/front_desk/Visitors';
$route['add-visitor']       = 'front_desk/front_desk/Add_Visitor';
$route['insert-visitor']       = 'front_desk/front_desk/Insert_Visitor';
$route['gate-pass']         = 'front_desk/front_desk/Gate_Pass';
$route['add-gatepass']     = 'front_desk/front_desk/Add_Gate_Pass';
$route['stud-gatepass']     = 'front_desk/front_desk/Stud_GatePass';
$route['emp-gatepass']     = 'front_desk/front_desk/Emp_GatePass';
$route['get-dept']     = 'front_desk/front_desk/Get_departmentid';
$route['get-section']     = 'front_desk/front_desk/Get_section';

//END Front_desk:- Enquery,Interaction,Admission,Employee,Visitors,Gatepass routes//


//sidebar menu  rought for Student
$route['Master_List']       = 'student/student/Master_List';
$route['Fees']              = 'student/student/Fees';
$route['Attendance']        = 'student/student/Attendance';
$route['Remarks']           = 'student/student/Remarks';
$route['Leave']             = 'student/student/Leave';
$route['Parent']            = 'student/student/Parent';
$route['Transfer']          = 'student/student/Transfer';
$route['Category']          = 'student/student/Category';
$route['addcategory']       = 'student/student/AddCategory';
$route['addnecategory']     = 'student/student/SaveCategory';
$route['No_Dues']           = 'student/student/No_Dues';
$route['TC']                = 'student/student/TC';

//sidebar menu  rought for Academics
$route['academics_year']    = 'academics/Academics/Academics_year';
$route['academics_year/(:num)']    = 'academics/Academics/Academics_year/$1';
//$route['Classes']           = 'academics/Academics/Classes';
$route['Classes_Group']     = 'academics/Academics/Classes_Group';
$route['subjects']          = 'academics/Academics/Subjects';
$route['subjects/(:num)']          = 'academics/Academics/Subjects/$1';

$route['Lectures']          = 'academics/Academics/Lectures';
$route['Timetable']         = 'academics/Academics/Timetable';
$route['Substitute']        = 'academics/Academics/Substitute';
$route['Buildings']         = 'academics/Academics/Buildings';
//$route['Rooms']             = 'academics/Academics/Rooms';
//$route['Houses']            = 'academics/Academics/Houses';
$route['add_acadmic']       = 'academics/Academics/show_Acadmic_year';
$route['edit/(:num)']       = 'academics/Academics/ShowAcadmicYear/$1';
$route['status/(:num)']     = 'academics/Academics/StatusChange/$1';
$route['edit_subject/(:num)']= 'academics/Academics/EditSubjectForm/$1';
$route['update']            = 'academics/Academics/UpdateAcadmic';
$route['Add_Acadmic_year']  = 'academics/Academics/AddAcadmicyear';
$route['add_subject']       = 'academics/Academics/Add_Subject';
$route['Addnewsubject']     = 'academics/Academics/AddnewsubjectData';
$route['updateSubject']       = 'academics/Academics/updateSubject';

//$route['Add_Acadmic']       = 'academics/show_Acadmic_year';
//$route['Edit/(:num)']       = 'academics/ShowAcadmicYear/$1';
//$route['Edit_subject/(:num)']= 'academics/EditSubjectForm/$1';
$route['Edit_building/(:num)']= 'academics/EditbuildingForm/$1';
//$route['update']            = 'academics/UpdateAcadmic';
//$route['Add_Acadmic_year']  = 'academics/AddAcadmicyear';
//$route['Add_Subject']       = 'academics/Add_Subject';
$route['Add_building']      = 'academics/Add_building';
$route['Add_classgroup']    = 'academics/AddClassGroup';
$route['Add_Lecture']       = 'academics/AddLecture';
//$route['Addnewsubject']     = 'academics/AddnewsubjectData';
$route['Addnewbuilding']    = 'academics/AddnewbuildingData';
//$route['updateSubject']     = 'academics/updateSubject';
$route['updatebulding']     = 'academics/updatebulding';
$route['addnewgroup']       = 'academics/AddGroup';
$route['addLecture']        = 'academics/Addlecturetime';


//IN Acadmics:- classes,Rooms,Houses routes//
$route['classes']           = 'academics/Academics/classes';
$route['classes/(:any)']    = 'academics/Academics/classes/$1';
$route['addclass']          = 'academics/Academics/Add_Class';

$route['insertclass']       = 'academics/Academics/Insert_Class';
$route['deleteclass/(:num)']= 'academics/Academics/Class_delete/$1';
$route['classedit/(:num)']  = 'academics/Academics/Class_edit/$1';
$route['classupdate']       = 'academics/Academics/Class_update';
$route['rooms']             = 'academics/Academics/Rooms';
$route['rooms/(:num)']      = 'academics/Academics/Rooms/$1';
$route['addroom']           = 'academics/Academics/Add_Rooms';
$route['insertroom']        = 'academics/Academics/Insert_Rooms';
$route['roomsdelete/(:num)']= 'academics/Academics/Delete_Rooms/$1';
$route['roomsedit/(:num)']  = 'academics/Academics/Edit_Rooms/$1';
$route['roomsupdate']       = 'academics/Academics/Update_Rooms';
$route['houses']            = 'academics/Academics/Houses';
$route['houses/(:num)']     = 'academics/Academics/Houses/$1';
$route['addhouse']          = 'academics/Academics/Add_Houses';
$route['inserthouse']       = 'academics/Academics/Insert_Houses';
$route['housesdelete/(:num)']= 'academics/Academics/Delete_Houses/$1';
$route['housesedit/(:num)'] = 'academics/Academics/Edit_Houses/$1';
$route['housesupdate']      = 'academics/Academics/Update_Houses';
//END Acadmics:- classes,Rooms,Houses routes//

//sidebar menu  rought for Academics/mapping
$route['class_teacher']     = 'academics/Academics/class_teacher';
$route['class_room']        = 'academics/Academics/class_room';
$route['Class_Subject']     = 'academics/Academics/Class_Subject';
$route['Student_Subject']   = 'academics/Academics/Class_Subject';

$route['lesson_planning']   = 'academics/Academics/lesson_planning';
$route['homework']          = 'academics/Academics/homework';
$route['calendar']          = 'academics/Academics/calendar';
$route['addcalender']       = 'academics/Academics/Addcalendar';
$route['workload']          = 'academics/Academics/workload';
$route['Parents_sms']       = 'academics/Academics/Parents_sms';
$route['circular']          = 'academics/Academics/circular';
$route['Addnewcelender']    = 'academics/Academics/Addnewcelender';

//sidebar menu  rought for Library
$route['library']           = 'library/library/Library';
$route['compartment']       = 'library/library/compartment';
$route['racks']             = 'library/library/racks';
$route['category']          = 'library/library/category';
$route['sub_category']      = 'library/library/sub_category';
$route['genre']             = 'library/library/genre';
$route['books']             = 'library/library/books';

$route['issue_book']        = 'library/library/issue_book';
$route['return_book']       = 'library/library/return_book';
$route['book_movement']     = 'library/library/book_movement';
$route['book_defaulters']   = 'library/library/book_defaulters';
$route['daily_log']         = 'library/library/daily_log';

//sidebar menu  rought for Examination
$route['exam_type']         = 'Examination/Examination/exam_type';
$route['addexamtypes']      = 'Examination/Examination/AddExamTypes';
$route['saveexamtype']      = 'Examination/Examination/saveExamTypes';
$route['exam']              = 'Examination/Examination/exam';
$route['examedit/(:any)']   = 'Examination/Examination/examEdit/$1';
$route['examupdate']        = 'Examination/Examination/ExamUpdate';
$route['addgrading']        = 'Examination/Examination/AddGradingScale';
$route['savegrading']       = 'Examination/Examination/Savenewgrading';
$route['updategrading']     = 'Examination/Examination/Updategrading';
$route['gragingedit/(:any)']= 'Examination/Examination/GragingEdit/$1';
$route['examallotment']     = 'Examination/Examination/AddExamAllotment';

$route['grading_scale']     = 'Examination/Examination/grading_scale';
$route['exam_allotment']    = 'Examination/Examination/exam_allotment';
$route['time_table']        = 'Examination/Examination/time_table';
$route['update_marks']      = 'Examination/Examination/update_marks';

$route['exam_reports']      = 'Examination/Examination/exam_reports';
$route['classwise_report']  = 'Examination/Examination/classwise_report';
$route['export_report']     = 'Examination/Examination/export_report';


 //sidebar menu  rought for Hr_payroll
$route['employees']        = 'hr_payroll/hr_payroll/employees';
$route['employee_category']= 'hr_payroll/hr_payroll/employee_category';
$route['designations']     = 'hr_payroll/hr_payroll/designations';
$route['departments']      = 'hr_payroll/hr_payroll/departments';
$route['employee_attendance']= 'hr_payroll/hr_payroll/employee_attendance';
$route['leave_management'] = 'hr_payroll/hr_payroll/leave_management';
$route['leave_type']       = 'hr_payroll/hr_payroll/leave_type';
$route['payheads']         = 'hr_payroll/hr_payroll/payheads';
$route['employee_access']  = 'hr_payroll/hr_payroll/employee_access';
$route['dashboard_access'] = 'hr_payroll/hr_payroll/dashboard_access';

//sidebar menu  rought for Transport
$route['buses']           = 'transport/transport/buses';
$route['addbus']          = 'transport/transport/AddnewBus';
$route['savebus']         = 'transport/transport/SavenewBus';
$route['routes']          = 'transport/transport/routes';
$route['Add_routes']      = 'transport/transport/AddNewroutes';
$route['Add_new_routes']  = 'transport/transport/SavenewRouter';
$route['stops']           = 'transport/transport/stops';
$route['reporting']       = 'transport/transport/reporting';
$route['misreports']      = 'transport/transport/MisReports';
$route['live_tracking']   = 'transport/transport/live_tracking';
//$route['replay_tracking'] = 'transport/transport/replay_tracking';
$route['speed_chart']     = 'transport/transport/speed_chart';
$route['getspeed']        = 'transport/transport/getSpeedChart';
$route['halt_summary']    = 'transport/transport/halt_summary';
$route['activity_reports']= 'transport/transport/activity_reports';
$route['Bus_Route']       = 'transport/transport/Bus_Route';
$route['in_process']      = 'transport/transport/in_process';
$route['Add_Stop']        = 'transport/transport/AddStopForm';
$route['Savestop']        = 'transport/transport/SaveNewstop';
$route['stopview/(:num)'] = 'transport/transport/stopview/$1';
$route['UpdateStopsData'] = 'transport/transport/UpdateStopsData';
$route['replay_tracking'] = 'transport/transport/ReplayTracking';
$route['getchart']        = 'transport/transport/Getchart';
$route['getdirection']    = 'transport/transport/Getdirection';

 //sidebar menu  rought for Finance
$route['fee_type']        = 'finance/finance/fee_type';
$route['fee_collection']  = 'finance/finance/fee_collection';
$route['canceled_fee_receipts'] = 'finance/finance/canceled_fee_receipts';
$route['fee_settings']    = 'finance/finance/fee_settings';
$route['fee_defaulters']  = 'finance/finance/fee_defaulters';
$route['fee_outstanding'] = 'finance/finance/fee_outstanding';
$route['fee_reports']     = 'finance/finance/fee_reports';
$route['transaction_category'] = 'finance/finance/transaction_category';
$route['transactions']    = 'finance/finance/transactions';

 //sidebar menu  rought for Inventory
$route['store']           = 'inventory/inventory/store';
$route['supplier_type']   = 'inventory/inventory/supplier_type';
$route['supplier']        = 'inventory/inventory/supplier';
$route['item_category']   = 'inventory/inventory/item_category';
$route['items']           = 'inventory/inventory/items';
$route['new_stock_request']= 'inventory/inventory/new_stock_request';
$route['requests_list']   = 'inventory/inventory/requests_list';

//sidebar menu for Hostel Module
$route['hostel-type']     = 'hostel/hostel/hostel_type';
$route['add-hostype']     = 'hostel/hostel/Add_hosteltype';
$route['edit-hostype/(:any)']= 'hostel/hostel/Edit_hosteltype/$1';
$route['update-hostype']  = 'hostel/hostel/Update_hosteltype';
$route['insert-hostype']  = 'hostel/hostel/Insert_hosteltype';
$route['close-hostype']   = 'hostel/hostel/Closehostype';

$route['hostels']         = 'hostel/hostel/hostels';
$route['add-hostel']      = 'hostel/hostel/Add_hostel';
$route['insert-hostel']   = 'hostel/hostel/Insert_hostel';
$route['edit-hostel/(:any)']= 'hostel/hostel/Edit_hostel/$1';
$route['update-hostel']  = 'hostel/hostel/Update_hostel';
$route['close-hostel']   = 'hostel/hostel/Close_hostels';
$route['get-hname']      = 'hostel/hostel/Get_hostelname';

$route['room-type']      = 'hostel/hostel/room_type';
$route['add-roomtype']   = 'hostel/hostel/Add_roomtype';
$route['edit-roomtype/(:any)'] = 'hostel/hostel/Edit_roomtype/$1';
$route['update-roomtype']  = 'hostel/hostel/Update_roomtype';
$route['insert-roomtype']  = 'hostel/hostel/Insert_roomtype';
$route['close-roomtype']  = 'hostel/hostel/Closeroomtype';


$route['assigning-rooms'] = 'hostel/hostel/assigning_rooms';
$route['addassign-rooms'] = 'hostel/hostel/Addassigning_rooms';
$route['insert-rooms']    = 'hostel/hostel/Insert_rooms';
$route['close-rooms']     = 'hostel/hostel/Close_rooms';
$route['edit-rooms/(:any)']    = 'hostel/hostel/Edit_rooms/$1';
$route['update-rooms']    = 'hostel/hostel/Update_rooms';


$route['facilities']      = 'hostel/hostel/facilities';
$route['add-facility'] = 'hostel/hostel/Add_facility';
$route['insert-facility']    = 'hostel/hostel/Insert_facility';
$route['close-facility']     = 'hostel/hostel/Close_facility';
$route['edit-facility/(:any)']    = 'hostel/hostel/Edit_facility/$1';
$route['update-facility']    = 'hostel/hostel/Update_facility';


$route['warden-details']  = 'hostel/hostel/warden_details';
$route['Hostel-allot']    = 'hostel/hostel/Hostel_student_allotment';
$route['mess-type']       = 'hostel/hostel/mess_type';
$route['meal-type']       = 'hostel/hostel/meal_type';
$route['mess-details']    = 'hostel/hostel/mess_details';
$route['mess-allot']      = 'hostel/hostel/mess_student_allotment';
$route['mess-attendance'] = 'hostel/hostel/mess_attendance';
//End of sidebar menu for Hostel Module