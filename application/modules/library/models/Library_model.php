<?php  
class Library_model extends CI_Model  
{  
	function __construct()  
	{  
		parent::__construct();  
	}
	public function inventorybook()
	{
		$this->db->select('*');
		$this->db->from('bookinventory');
		$this->db->where('is_delete',1);
		return $query=$this->db->get()->result();
	    //print_r($this->db->last_query($query));die;
	}
	public function Insertinventory($data)
    {
    	$data['createdate']=date("Y-m-d");
		$data['is_delete']=1;
		$this->db->insert('bookinventory',$data);
    }
    public function getinvetory($id)
	{
		$this->db->select('inservice');
		$this->db->from('bookinventory');
		$this->db->where('id',$id);
		return $query=$this->db->get()->row();
	}
    public function inventorystatus($id,$data)
    {
    	$query=$this->db->where('id',$id)->update('bookinventory',$data);
    }
    public function closeinventory($id,$data)
    {
    	$query=$this->db->where('id',$id)->update('bookinventory',$data);
    }
    public function editinventory($id)
	{
		$this->db->select('*');
		$this->db->from('bookinventory');
		$this->db->where('id',$id);
		return $query=$this->db->get()->result();
	}
	public function updateinventory($id,$data)
	{
		$query=$this->db->where('id',$id)->update('bookinventory',$data);
	}
	public function Getissue_detail()
	{
		$this->db->select('*');
		$this->db->from('booksissue');
		$this->db->where('is_delete',1);
		return $query=$this->db->get()->result();
	}
	public function Getbooksdetail()
	{
		$this->db->select('id,book_title');
		$this->db->from('bookinventory');
		return $query=$this->db->get()->result();
	}
	public function GetAvailablebooks()
	{
		$this->db->select('id,book_title');
		$this->db->from('bookinventory');
		$this->db->where('total_qty > issued_qty');
		return $query=$this->db->get()->result();
	}
	public function Getstudents()
	{
		$this->db->select('id,name');
		$this->db->from('students');
		return $query=$this->db->get()->result();
	}
	public function Insertrecord($data)
	{
		$data['createdate']=date("Y-m-d");
		$data['is_delete']=1;
		$query=$this->db->insert('booksissue',$data);
		//print_r($this->db->last_query($query));die;
	}
	public function Getreturnstatus($id)
	{
		$this->db->select('is_return');
		$this->db->from('booksissue');
		$this->db->where('id',$id);
		return $query=$this->db->get()->row();
	}
	public function Getfinestatus($id)
	{
		$this->db->select('deposite_fine');
		$this->db->from('booksissue');
		$this->db->where('id',$id);
		return $query=$this->db->get()->row();
	}
	public function returnstatus($id,$data)
	{
		$data['modifieddate']=date("Y-m-d");
		$this->db->where('id',$id)->update('booksissue',$data);
	}
	public function finestatus($id,$data)
	{
		$this->db->where('id',$id)->update('booksissue',$data);
	}
	public function editissuerecord($id)
	{
		$this->db->select('*');
		$this->db->from('booksissue');
		$this->db->where('id',$id);
		return $query=$this->db->get()->result();
	}
	public function updateissuerecord($id,$data)
	{
		$query=$this->db->where('id',$id)->update('booksissue',$data);
	}
}  
?>  