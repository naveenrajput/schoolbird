<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Add Inventory Form
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Add Inventory</li>
  </ol>
</section>
<!-- Add Inventory content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Add Inventory</h3>
        </div>
        <!-- /.box-header --> 
        <!-- form start -->
        <form method="post" action="<?php echo base_url('insert-inventory'); ?>" data-toggle="validator" role="form">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Book Title</label>
                  <input type="text" name="book_title" class="form-control" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                 <label>Subject</label>
                 <input type="text" name="subject" class="form-control" required>
               </div>
             </div>
            <div class="form-group col-md-6" >
               <label>Class</label>
                  <select class="form-control select2" name="class" style="width: 100%;" data-placeholder="Select" required>
                    <option selected="selected" value="">select</option>
                    <option value="KG-1">KG-1</option>
                    <option value="KG-2">KG-2</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                  </select>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Edition</label>
                <input type="text" name="edition" class="form-control" required>
              </div> 
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Author</label>
                <input type="text" name="author" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>ISBN No.</label>
                <input type="text" name="ISBM" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Inservice</label>
                <select name="inservice" class="form-control" required>
                  <option value="" selected="selected">Select</option>
                  <option value="YES">YES</option>
                  <option value="NO">NO</option>
                </select>
        
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Total Qty</label>
                <input type="text" name="total_qty" class="form-control" required>
              </div>
            </div>


          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>	
        </div>

      </form>
    </div>

  </div>
</div>
</section>

</div>