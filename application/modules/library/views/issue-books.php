  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
     <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        LIBRARY
      </h1>
      <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Library</a></li>
        <li class="active">Issue Books</li>
      </ol>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;">
      <a  href="<?php echo base_url('issue-page');?>" class="btn btn-primary">&nbsp;Issue Book</a>
    </div>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">


       <div class="box">
        <div class="box-header with-border mr-top-20 text-center">
         <div class="form-group col-md-5">

          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right datepicker" placeholder="Start Date">
          </div>
          <!-- /.input group -->
        </div>
        <!-- /.form group -->

        <!-- Date range -->
        <div class="form-group col-md-5">

          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right datepicker"  placeholder="End Date">
          </div>
          <!-- /.input group -->
        </div>
        <div class="col-md-2" >
         <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>

       </div>



     </div>


     <div class="box-body table-responsive">

       <!-- <table id="example" class="display nowrap" style="width:100%">--->
        <table id="example" class="table table-bordered " >

          <thead>
            <tr>
              <th><input type="checkbox" id="selectall"> All</th>
              <th>S.no</th>
              <th>Student Name</th>
              <th>Book</th>
              <th>Issue Date</th>
              <th>Return Date</th>
              <th>Return</th>
              <th>Fine</th>
              <th>Fine Deposite</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              if (isset($issue_detail)) {
                $i=1; foreach ($issue_detail as $issue) { 
                  $book_title=explode(",", $issue->bookid);
            ?>

              <tr>
                <td>
                  <input type="checkbox" class="selectedId" name="selectedId" />
                </td>     
                <td><?php echo $i; ?></td>  
                <td><?php foreach ($students as $row) {
                  if ($row->id ==$issue->stdid) {
                   echo $row->name; } } ?>
                </td>
                <td><?php foreach ($books as $row) {
                       if (in_array($row->id,$book_title)) { echo $row->book_title." ";
                    } } ?>   
                </td>
                <td><?php echo $issue->issuedate; ?></td>
                <td><?php echo $issue->returndate; ?></td>
                <td>
                  <a href="" class="return_status" id="<?php echo $issue->id;?>">
                    <input type="hidden" class="<?php echo $issue->id;?>" value="<?php echo $issue->bookid; ?>">
                    <?php if($issue->is_return=='YES'){ ?>
                    YES
                    <?php } elseif($issue->is_return=='NO'){ ?>  
                    NO
                    <?php } ?>          
                  </a>
                </td>
                <td><?php $date1=date_create($issue->returndate);
                          $date2=date_create(date("Y-m-d"));
                          $diff=date_diff($date1,$date2);
                          $fine=$diff->format("%a")*5;
                        if ( $date1 < $date2 ) { 
                          if ($issue->fine == "") {
                            echo $fine;
                          }elseif($issue->fine !=""){
                            echo $issue->fine;
                          }
                        }else{
                          echo 0;
                        }
                    ?>      
                </td>
                <td>
                  <a href="" class="fine_status" id="<?php echo $issue->id;?>">
                    <input type="hidden" class="<?php echo $issue->id."1";?>" value="<?php if ( $date1 < $date2 ) { echo $fine; }else{ echo 0;} ?>">
                    <?php if($issue->deposite_fine=='YES'){ ?>
                    YES
                    <?php } elseif($issue->deposite_fine=='NO'){ ?>  
                    NO
                    <?php } ?>          
                  </a> 
                </td>
                <td><button type="button" class="btn btn-block btn-info btn-sm Returnbook" id="<?php echo $issue->id; ?>" data-toggle="modal" data-target="#Mymodal">Return</button>
                </td>
                <td>
                  <ul class="table-icons">
                    <li><a href="<?php echo base_url('edit-record/').$issue->id; ?>" class="table-icon" title="Edit"> 
                      <span class="glyphicon glyphicon-edit display-icon"></span>
                      </a>
                    </li>
                  </ul>
                </td>
              </tr>
              <?php  $i++; } } ?>
            </tbody>
            <tfoot>
              <tr>
                <th><input type="checkbox" id="selectall">All</th>
                <th>S.no</th>
                <th>Student Name</th>
                <th>Book</th>
                <th>Issue Date</th>
                <th>Return Date</th>
                <th>Is_Returned</th>
                <th>Fine</th>
                <th>Fine Deposite</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>

<!-- ALL MODALS VIEW -->
<div class="modal fade" id="Mymodal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="callout callout-info" >
        <h4>Return Form</h4>
      </div>
      <div class="modal-body" id="Mymodal_view">
        
        <div class="row">
          <div class="form-group col-md-offset-1 col-md-10">
            <label>Days Delay</label>
              <input type="text" class="form-control" name="" value="">
          </div>
          <div class="form-group col-md-offset-1 col-md-10">
            <label>Days Delay</label>
              <input type="text" class="form-control" name="" value="">
          </div>  
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
<!-- /.modal-dialog --> 
</div>

<script>
  $(document).ready(function(){
    var table = $('#example').DataTable( {
      lengthChange: true,
      autoWidth : true,
       
     } );
    $('.Returnbook').click(function(){  
     var returnid = $(this).attr("id");
     alert(returnid);
      $.ajax({  
            url:"<?php echo base_url('return-page'); ?>",  
            method:"post",  
            data:{returnid:returnid},  
            success:function(data){
              $('#Mymodal_view').html(data);
            }                             
       }); 
    });  

    $('.return_status').click(function(){debugger;
      var rtid= $(this).attr("id");
      var bookid=$('.'+rtid).val();
      alert(bookid);
      var x=confirm("Are you sure Books Returned ?")
          if (x) {
            $.ajax({  
                    url:"<?php echo base_url('return-status'); ?>", 
                    method:"post",  
                    data:{rtid:rtid,bookid:bookid},
                });
          }else {
            return false;
          } 
    });
    
    $('.fine_status').click(function(){debugger;
      var fnid= $(this).attr("id");
      var fine=$('.'+fnid+'1').val();
      alert(fine);
      var x=confirm("Are you sure Fine hasbeen Deposited?")
          if (x) {
            $.ajax({  
                    url:"<?php echo base_url('fine-status'); ?>",  
                    method:"post",  
                    data:{fnid:fnid,fine:fine},
                });
          }else {
            return false;
          } 
    });
  });
</script>
