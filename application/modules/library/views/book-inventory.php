  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
     <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        LIBRARY
      </h1>
      <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Library</a></li>
        <li class="active">Inventory</li>
      </ol>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;">

      <a  href="<?php echo base_url ('add-inventory'); ?>" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp;Inventory</a>

    </div>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">


       <div class="box">
        <div class="box-header with-border mr-top-20 text-center">
         <div class="form-group col-md-5">

          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right datepicker" placeholder="Start Date">
          </div>
          <!-- /.input group -->
        </div>
        <!-- /.form group -->

        <!-- Date range -->
        <div class="form-group col-md-5">

          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right datepicker"  placeholder="End Date">
          </div>
          <!-- /.input group -->
        </div>
        <div class="col-md-2" >
         <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>

       </div>



     </div>


     <div class="box-body table-responsive">

       <!-- <table id="example" class="display nowrap" style="width:100%">--->
        <table id="example" class="table table-bordered " >

          <thead>
            <tr>
              <th><input type="checkbox" id="selectall"> All</th>
              <th>S.no</th>
              <th>Title</th>
              <th>Subject</th>
              <th>Class</th>
              <th>Author</th>
              <th>Inservice</th>
              <th>Qty</th>
              <th>Create Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              if (isset($inventory)) {
                $i=1; foreach ($inventory as $inventory) { 
            ?>
              <tr>
                <td>
                  <input type="checkbox" class="selectedId" name="selectedId" />
                </td>     
                <td><?php echo $i; ?></td>
                <td><?php echo $inventory->book_title;?></td>
                <td><?php echo $inventory->subject; ?></td>
                <td><?php echo $inventory->class; ?></td>
                <td><?php echo $inventory->author; ?></td>
                <td>
                  <a href="" class="inventory_status" id="<?php echo $inventory->id;?>">
                    <?php if($inventory->inservice=='YES'){ ?>
                    YES
                    <?php } elseif($inventory->inservice=='NO'){ ?>  
                    NO
                    <?php } ?>          
                  </a> 
                </td>
                <td><?php echo $inventory->total_qty; ?></td>
                <td><?php echo $inventory->createdate; ?></td>
                <td>
                  <ul class="table-icons">
                    <li><a href="<?php echo base_url('edit-inventory/').$inventory->id; ?>" class="table-icon" title="Edit"> 
                      <span class="glyphicon glyphicon-edit display-icon"></span>
                      </a>
                    </li>
                    <li>
                      <a href="" class="table-icon" title="Delete" ><span class="glyphicon glyphicon-trash display-icon closeinventory" id="<?php echo $inventory->id; ?>"></span></a>
                    </li>
                  </ul>
                </td>
              </tr>
              <?php  $i++; } } ?>
            </tbody>
            <tfoot>
              <tr>
                <th><input type="checkbox" id="selectall">All</th>
                <th>S.no</th>
                <th>Title</th>
                <th>Subject</th>
                <th>Class</th>
                <th>Author</th>
                <th>Inservice</th>
                <th>Qty</th>
                <th>Create Date</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>

<script>
  $(document).ready(function() {
    var table = $('#example').DataTable( {
      lengthChange: true,
      autoWidth : true,
       // buttons: [   'csv', 'excel', 'pdf', 'print' ],
     } );

    $('#selectall').click(function () {
      $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function () {
      var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
      $('#selectall').prop("checked", check);
    });

    $('.inventory_status').click(function(){
      var Bid= $(this).attr("id");
      var x=confirm("Are you sure to change Status?")
          if (x) {
            $.ajax({  
                    url:"<?php echo base_url('inventory-status'); ?>",  
                    method:"post",  
                    data:{Bid:Bid},
                    success:function(data){ 
                      location.reload(data);
                    }  
                });
          }else {
            return false;
          } 
    });

    $('.closeinventory').click(function(){
         var Bid= $(this).attr("id");
         var x=confirm("Are you sure to delete record?")
          if (x) {
          $.ajax({
                    url:"<?php echo base_url('close-inventory'); ?>",  
                    method:"post",  
                    data:{Bid:Bid},  
                    success:function(data){ 
                          location.reload();
                    }  
                 }); 
        }else {
          return false;
        } 
    });
  });
</script>

