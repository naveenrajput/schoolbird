<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
     <h1>
       Edit Inventory Form
     </h1>
     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li><a href="#">Dashboard</a></li>
      <li class="active">Edit Inventory</li>
    </ol>
  </section>
<!-- Edit Inventory content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Inventory</h3>
        </div>
        <!-- /.box-header --> 
        <!-- form start -->
        <form method="post" action="<?php echo base_url('update-inventory'); ?>" data-toggle="validator" role="form">
          <?php if (isset($inv_info)) {
                foreach ($inv_info as $inventory) { ?>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="hidden" name="updateid" value="<?php echo $inventory->id;?>">
                  <label>Book Title</label>
                  <input type="text" name="book_title" class="form-control" value="<?php echo $inventory->book_title; ?>" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                 <label>Subject</label>
                 <input type="text" name="subject" class="form-control" value="<?php echo $inventory->subject; ?>" required>
               </div>
             </div>
            <div class="form-group col-md-6" >
               <label>Class</label>
                  <select class="form-control select2" name="class" style="width: 100%;" data-placeholder="Select" required>
                    <option selected="selected" value="<?php echo $inventory->class; ?>"><?php echo $inventory->class; ?></option>
                    <option value="KG-1">KG-1</option>
                    <option value="KG-2">KG-2</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                  </select>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Edition</label>
                <input type="text" name="edition" class="form-control" value="<?php echo $inventory->edition; ?>" required>
              </div> 
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Author</label>
                <input type="text" name="author" class="form-control" value="<?php echo $inventory->author; ?>" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>ISBN No.</label>
                <input type="text" name="ISBM" class="form-control" value="<?php echo $inventory->ISBM; ?>" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Inservice</label>
                <select name="inservice" class="form-control" required>
                  <option value="<?php echo $inventory->inservice; ?>" selected><?php echo $inventory->inservice; ?></option>
                    <option value="YES">YES</option>
                    <option value="NO">NO</option>
                </select>
        
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Total Qty</label>
                <input type="text" name="total_qty" class="form-control" value="<?php echo $inventory->total_qty; ?>" required>
              </div>
            </div>


          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>	
        </div>
        <?php } } ?>
      </form>
    </div>

  </div>
</div>
</section>

</div>