<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit Issue Record Form
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Edit Issue Record</li>
  </ol>
</section>
<!-- Edit Issue Record content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Issue Record</h3>
        </div>
        <!-- /.box-header --> 
        <!-- form start -->
        <form method="post" action="<?php echo base_url('update-record'); ?>" data-toggle="validator" role="form">
          <?php if (isset($record)) {
                foreach ($record as $isurecord) { 
                  $book_title=explode(",", $isurecord->bookid);
          ?>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="hidden" name="updateid" value="<?php echo $isurecord->id;?>">
                  <label>Name/ID</label>
                  <select name="stdid" class="form-control select2" required>
                    <option selected="selected" value="<?php echo $isurecord->stdid; ?>"><?php foreach ($students as $row) {
                        if ($row->id == $isurecord->stdid) {
                         echo $row->name; } } ?>
                    </option>
                    <?php foreach ($students as $stud) { ?>
                      <option value="<?php echo $stud->id; ?>"><?php echo $stud->name; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                <label>Book</label>
                  <select name="bookid[]" class="form-control select2" multiple="multiple" data-placeholder="Select Books" style="width: 100%;" >
                    <?php foreach ($books_Avl as $row) {
                       if (in_array($row->id,$book_title)) { ?>
                       <option selected="selected" value="<?php echo $row->id; ?>"><?php echo $row->book_title; ?></option>
                    <?php }elseif(!in_array($row->id,$book_title)){ ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->book_title; ?></option>
                   <?php } } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Issue Date</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="issuedate" class="form-control pull-right  datepicker-edit" value="<?php echo $isurecord->issuedate; ?>" required>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Return Date</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="returndate" class="form-control pull-right datepicker-edit" value="<?php echo $isurecord->returndate; ?>" required>
                  </div>
                </div>
              </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>	
        </div>
      <?php } } ?>
      </form>
    </div>

  </div>
</div>
</section>

</div>