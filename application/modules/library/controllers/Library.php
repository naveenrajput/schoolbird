<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Library extends MY_Controller {

	
	public function __construct(){
        parent::__construct();
        //$this->load->model('contact_model', 'contact');
        $this->load->helper('url');
      }

    // Controller for Library
	public function Library()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for compartment
	public function compartment()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for racks
	public function racks()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for category
	public function category()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for sub_category
	public function sub_category()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // Controller for genre
	public function genre()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for books
	public function books()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for issue_book
	public function issue_book()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for return_book
	public function return_book()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for book_movement
	public function book_movement()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for book_defaulters
	public function book_defaulters()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for daily_log
	public function daily_log()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	
	
}
