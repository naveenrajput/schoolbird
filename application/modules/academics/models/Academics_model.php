<?php  
   class Academics_model extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }
      //to count rows of classes table for pagination 
        public function Countrows_classes() 
        {
          $query = $this->db->where('delete', 0)->get('classes');
          return $query->num_rows();
        }
        //to set limited rows of classes table for pagination 
        public function Limitrows_classes($limit, $start) {
            $this->db->limit($limit, $start);
            $this->db->select('*');
            $this->db->from('classes');
            $this->db->where('delete',1);
            return $query=$this->db->get()->result();
        }
       // query to insert data in classes table
       public function Insertclass($data)
       {
          $cls=implode(",", $data['subject']);
          $data['subject']="[".$cls."]";
          $data['delete']='1';
          $data['createdate']=date('Y-m-d H:i:s');
          $query=$this->db->insert('classes',$data);

       $query= $this->db->last_query($query);
       }
       // query to delete data from classes table
       public function Classdelete($del_id)
       {
          $u['delete']='2';
          $query= $this->db->where('id',$del_id)->update('classes',$u);    
       }
       // query to select classes table data
       public function Classedit($edit_id)
       {
          $this->db->select('*');
         $this->db->from('classes');
         $this->db->where('id',$edit_id);
         return $query=$this->db->get()->row();
       }
       // query to update data of classes table
       public function Classupdate($data,$id)
       {
          $cls=implode(",", $data['subject']);
          $data['subject']="[".$cls."]";
          $data['modifieddate']=date('Y-m-d H:i:s');
          $this->db->where('id',$id)->update('classes',$data);  
       }
       //to count rows of rooms table for pagination 
        public function Countrows_rooms() 
        {
            $query = $this->db->where('delete', 1)->get('rooms');
            return $query->num_rows();
        }
        //to set limited rows of rooms table for pagination 
        public function Limitrows_rooms($limit, $start) {
            $this->db->limit($limit, $start);
            $this->db->select('*');
            $this->db->from('rooms');
            $this->db->where('delete',0);
            return $query=$this->db->get()->result();
        }
       // query to insert data in rooms table
       public function Insertrooms($data)
       {
          $data['updatedby']='0';
          $data['createdate']=date('Y-m-d H:i:s');
          $query=$this->db->insert('rooms',$data);
          //print_r($query);die;
       }
       // query to delete data from rooms table
       public function Deleterooms($del_id)
       {
          $u['delete']='2';
          $query= $this->db->where('id',$del_id)->update('rooms',$u);    
       }
       // query to select rooms table data
       public function Eeditrooms($edit_id)
       {
          $this->db->select('*');
         $this->db->from('rooms');
         $this->db->where('id',$edit_id);
         return $query=$this->db->get()->row();
       }
       // query to update data of rooms table
       public function Updaterooms($data,$u_id)
       {
           $this->db->where('id',$u_id)->update('rooms',$data);  
       }
       //to count rows of houses table for pagination 
        public function Countrows_houses() 
        {
            $query = $this->db->where('delete', 0)->get('houses');
            return $query->num_rows();
        }
        //to set limited rows of houses table for pagination 
        public function Limitrows_houses($limit, $start) {
            $this->db->limit($limit, $start);
            $this->db->select('*');
            $this->db->from('houses');
            $this->db->where('delete',1);
            return $query=$this->db->get()->result();
        }
       // query to insert data in houses table
       public function Inserthouses($data)
       {
          $data['updatedby']='0';
          $data['delete']='1';
          $data['createdate']=date('Y-m-d H:i:s');
          $query=$this->db->insert('houses',$data);
          //print_r($query);die;
       }
       // query to delete data from houses table
       public function Deletehouses($del_id)
       {
          $u['delete']='2';
          $query= $this->db->where('id',$del_id)->update('houses',$u);    
       }
       // query to select houses table data
       public function Eedithouses($edit_id)
       {
          $this->db->select('*');
         $this->db->from('houses');
         $this->db->where('id',$edit_id);
         return $query=$this->db->get()->row();
       }
       // query to update data of houses table
       public function Updatehouses($data,$u_id)
       {
           $this->db->where('id',$u_id)->update('houses',$data);  
       }  
       //to count rows of academicyear table for pagination 
        public function Countrows_academicyear() 
        {
            $query = $this->db->where('delete', 0)->get('academicyear');
            return $query->num_rows();
        }
       //to set limited rows of academicyear table for pagination 
        public function Limitrows_academicyear($limit, $start) {
            $this->db->limit($limit, $start);
            $this->db->select('*');
            $this->db->from('academicyear');
            $this->db->where('delete',0);
            return $query=$this->db->get()->result();
        }
      //to count rows of subjects table for pagination 
        public function Countrows_subjects() 
        {
            $query = $this->db->where('delete', 0)->get('subjects');
            return $query->num_rows();
        }
       //to set limited rows of subjects table for pagination 
        public function Limitrows_subjects($limit, $start) {
            $this->db->limit($limit, $start);
            $this->db->select('*');
            $this->db->from('subjects');
            $this->db->where('delete',0);
            return $query=$this->db->get()->result();
        }
     
     //Function To Fetch  academic year Record 
      public function AcademicYear()  {  
        $this->db->where('delete', 0); 
        $query = $this->db->get('academicyear'); 
         return $query;  
      }
      //Function To Fetch  Subject  Record
      public function getSubject()  {  
        $this->db->where('delete', 0); 
        $query = $this->db->get('subjects'); 
        return $query;  
      }

      //Function To Add new subject
      public function AddaNewSubject($data)  {  
        $this->db->insert('subjects', $data);  
      }

      //Function To Delet  Acadmic  Record
      public function deletacadmic($id,$data)  {  
         $this->db->where('id', $id);
         $this->db->update('academicyear', $data);
      }

      //Function To Delet  Subject  Record
      public function deletSubject($id,$data)  {  
         $this->db->where('id', $id);
         $this->db->update('subjects', $data);
      }

      // Function To Fetch Selected academic year Record for Edit
      public function show_academicyear(){
        $this->db->select('*');
        $this->db->from('academicyear');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
      }
      //Query to Check Status is Active or Deactive
      public function Check_status($id)
      {
        $this->db->select('status');
        $this->db->from('academicyear');
        $this->db->where('id', $id);
        $query=$this->db->get()->row();
        return $query->status;
      }
      //Query to change Status
      public function Change_status($id,$s)
      { 
        return $query= $this->db->where('id',$id)->update('academicyear',$s);
      }
      
      // Function To Fetch Edit Subject Record
      public function EditSubject($id){
        $this->db->select('*');
        $this->db->from('subjects');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
      }


      // Update Query For Selected Student
      public function Acadmic_update1($id,$data){
        $this->db->where('id', $id);
        $this->db->update('academicyear', $data);
      }

      // Update Query For Update subject 
       public function updateSubject($id,$data){
        $this->db->where('id', $id);
        $this->db->update('subjects', $data);
      }
      
        // Update Query For Add new Acadmic Session
      public function AddacadmicYear($data){
        $this->db->insert('academicyear', $data);
      }

      
             //Function To Fetch  calender  Record
      public function getcalendar()  {  
        $this->db->where('delete', 0); 
        $query = $this->db->get('calender'); 
        return $query;  
      } 
      //Function To Fetch  Subject  Record
      public function getBuilding()  {  
        $this->db->where('delete', 0); 
        $query = $this->db->get('building'); 
        return $query;  
      }

      //Function To Fetch  classgroup  Record
       public function classgroup()  {  
        $this->db->where('delete', 1); 
        $query = $this->db->get('classes'); 
        return $query;  
      }

      //Function To Fetch  classgroup  Record
       public function getLectures()  {  
        $this->db->where('delete', 0); 
        $query = $this->db->get('lecture'); 
        return $query;  
      }

       //Function To Fetch  classgroup  Record
       public function classname()  {  
        $query = $this->db->get('groupclasses'); 
        return $query;  
      }
      //Function To Add new subject
      public function AddNewCelender($data)  {  
        $this->db->insert('calender', $data);  
      }
       // get Acadmic Year 
      public function getWorkload($clsaa)  {

        $this->db->distinct();
        $this->db->select('e.id,e.name,tt.teacherid');
        $this->db->from('employees as e');
        $this->db->join('timetable as tt', 'tt.academicyear ='.$clsaa);
        $this->db->where('tt.teacherid= e.id');
        $query = $this->db->get()->result(); 
        //$query = $this->db->last_query($query);print_r($query);die;
        return $query;
         
      }

      //Function To Add class group
      public function Addaclassgroup($data)  { 
         $data= $this->db->insert('groupclasses', $data);
      }

      //Function To Add lecture
      public function AddLetureTime($data)  { 
         $data= $this->db->insert('lecture', $data);
      }

      //Function To Add new building
      public function AddaNewbuilding($data)  {  
         $this->db->insert('building', $data);  
      }
       //Function To Delet  buiding  Record
      public function deletbuiding($id,$data)  {  
         $this->db->where('id', $id);
         $this->db->update('building', $data);
      }
      //Function To Delet  buiding  Record
      public function getTeacherList()  {
        $this->db->where('emp_role', 1);  
        $this->db->order_by("name", "asc");  
        $query = $this->db->get('employees');
        //$query = $this->db->last_query($query);print_r($query);die;
         return $query;
      }


      //Function To Delet  buiding  Record
      public function getTimetable($class,$acadmic)  { 
      
         $this->db->select('e.name,sb.id,sb.suject,tt.teacherid,tt.day,tt.lectureid');
         $this->db->from('employees as e');
         $this->db->join('timetable as tt', 'tt.teacherid = e.id');
         $this->db->join('subjects as sb', 'sb.id = tt.subjectid');
         $this->db->where('tt.classid='.$class);
         $this->db->where('tt.academicyear='.$acadmic);
         $this->db->order_by("tt.id", "asc");
         $query = $this->db->get(); 
         return $query;
      }

      //Save time Table
      public function Savetimetable($data){ 
      
         $query=$this->db->insert('timetable',$data); 
         return $query;
      }
      // get get Subjectsdata data  
      public function getSubjectsdata($class,$acadmic)  { 
      
         $this->db->select('subject');
         $this->db->from('classes');
         $this->db->where('class_id='.$class);
         $query = $this->db->get();  
         return $query;
      
      }
      // get getLectures data  
      public function getLecturesdata()  {
        $this->db->order_by("from", "asc");  
        $query = $this->db->get('lecture');
         return $query;  
      }
      // Update Query For Update Building 
       public function updateBuilding($id,$data){
        $this->db->where('id', $id);
        $this->db->update('building', $data);
      }
       // get Acadmic Year 
      public function getAcadmicYear()  {  
        $query = $this->db->get('academicyear');
         return $query;  
      }  
   }  
?>  