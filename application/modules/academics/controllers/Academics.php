<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Academics extends MY_Controller {

	
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library("pagination");
		$this->load->library('session');
        $this->load->model('Academics_model');
        $this->load->helper(array('form', 'url'));
    }
	//Start to showing data of classes table//
	public function Classes()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$config = array();
		$num_rows=$this->db->count_all("classes");
	    $config["base_url"] = base_url() . "classes/";
	    $config["total_rows"] = $this->Academics_model->Countrows_classes();
	    $config["per_page"] = 5;
	    $ppage =$config["per_page"];
		$config['num_links'] = $num_rows;
		$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
	    $this->pagination->initialize($config);
	    $page = ($this->uri->segment(2)) ? $ppage*($this->uri->segment(2)-(1)) : 0;
	    $response["page"]=$page;
	    $response["links"] = $this->pagination->create_links();
	    $response['classesview']=$this->Academics_model->Limitrows_classes($config["per_page"],$page);
	    $response['teacher'] = $this->Academics_model->getTeacherList();
	    //print_r($response);die;
		$this->load->view('classes',$response);
		$this->load->view('../../inc/footer');
	}
	//to view the Add class form
	public function Add_Class()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['teacher']=$this->Academics_model->getTeacherList();
		$data['subject']=$this->Academics_model->getSubjectsdata();
		$this->load->view('add_class',$data);
		$this->load->view('../../inc/footer');
	}
	//to insert new class data 
	public function Insert_Class()
	{
		
		$this->form_validation->set_rules('class_title', 'class', 'required');
		$this->form_validation->set_rules('section', 'section', 'required');
		$this->form_validation->set_rules('teacher', 'Teacher', 'required');
		$this->form_validation->set_rules('notes', ' Remarks', 'required');

		if ($this->form_validation->run() == true){

		
			$class   = $this->input->post('class_title');
			$section = $this->input->post('section');
			$teacher = $this->input->post('teacher');
			$subject = $this->input->post('subject');
            $notes   = $this->input->post('notes');
            $capacity= $this->input->post('capacity');

     		$data=$this->input->post();
			$this->Academics_model->Insertclass($data);
			redirect('classes');
		}else{
			$this->load->view('../../inc/header');
			$this->load->view('../../inc/sidebar');
			$this->load->view('add_class');
			$this->load->view('../../inc/footer');
		}
	}
	//get class id for delete a row
	public function Class_delete($del_id)
	{
		$this->Academics_model->Classdelete($del_id);
		redirect('classes');
	}
	//get class id to edit a row
	public function Class_edit($edit_id)
	{
		$classdata=$this->Academics_model->Classedit($edit_id);
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$teacher=$this->Academics_model->getTeacherList();
		$subject=$this->Academics_model->getSubjectsdata();
		$this->load->view('edit_class',compact('classdata','teacher','subject'));
		$this->load->view('../../inc/footer');
	}
	//to update class data
	public function Class_update()
	{
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		$this->form_validation->set_rules('class_title', 'class', 'required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('section', 'section', 'required');
		$this->form_validation->set_rules('notes', ' Remarks', 'required');

		if ($this->form_validation->run() == true){
			$data=$this->input->post();
			$id=$data['id'];
	        unset($data['id']);
			$this->Academics_model->Classupdate($data,$id);
            redirect('classes');
		}else{
			$this->load->view('../../inc/header');
			$this->load->view('../../inc/sidebar');
			$data=$this->input->post();
			$id=$data['id'];
			$this->load->view('edit_class',compact('u_id'));
			$this->load->view('../../inc/footer');
		}
	}
	//to showing data of Rooms table
	public function Rooms()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$config = array();
		$num_rows=$this->db->count_all("rooms");
	    $config["base_url"] = base_url() . "rooms/";
	    $config["total_rows"] = $this->Academics_model->Countrows_rooms();
	    $config["per_page"] = 2;
	    $ppage =$config["per_page"];
		$config['num_links'] = $num_rows;
		$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
	    $this->pagination->initialize($config);
	    $page = ($this->uri->segment(2)) ? $ppage*($this->uri->segment(2)-(1)) : 0;
	    $response["page"]=$page;
	    $response["links"] = $this->pagination->create_links();
	    $response['roomsview']=$this->Academics_model->Limitrows_rooms($config["per_page"], $page);
		$this->load->view('rooms',$response);
		$this->load->view('../../inc/footer');
	}
	//to view the Add Rooms form
	public function Add_Rooms()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add_room');
		$this->load->view('../../inc/footer');
	}
	//to check validation and insert new data in Rooms table 
	public function Insert_Rooms()
	{
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('building', 'Building name', 'required');
		$this->form_validation->set_rules('capacity', 'Building Capacity', 'required');
		$this->form_validation->set_rules('type', 'Rooms type', 'required');
		$this->form_validation->set_rules('notes', ' Remarks', 'required');
		if ($this->form_validation->run() == true){
			$data=$this->input->post();
			$this->Academics_model->Insertrooms($data);
			redirect('rooms');
		}else{
			$this->load->view('../../inc/header');
			$this->load->view('../../inc/sidebar');
			$this->load->view('add_room');
			$this->load->view('../../inc/footer');
		}
	}
	//get Rooms id for delete a row
	public function Delete_Rooms($del_id)
	{
		$this->Academics_model->Deleterooms($del_id);
		redirect('rooms');
	}
	//get Rooms id to edit a row
	public function Edit_Rooms($edit_id)
	{
		$roomdata=$this->Academics_model->Eeditrooms($edit_id);
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('edit_room',compact('roomdata'));
		$this->load->view('../../inc/footer');
	}
	//to update Rooms data
	public function Update_Rooms()
	{
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('building', 'Building name', 'required');
		$this->form_validation->set_rules('capacity', 'Building Capacity', 'required');
		$this->form_validation->set_rules('type', 'Rooms type', 'required');
		$this->form_validation->set_rules('notes', ' Remarks', 'required');

		if ($this->form_validation->run() == true){
			$data=$this->input->post();
			$u_id=$data['update_id'];
	        unset($data['update_id']);
	        $data['modifieddate']=date('Y-m-d H:i:s');
			$this->Academics_model->Updaterooms($data,$u_id);
			redirect('rooms');
		}else{
			$this->load->view('../../inc/header');
			$this->load->view('../../inc/sidebar');
			$data=$this->input->post();
			$u_id=$data['update_id'];
			$this->load->view('edit_room',compact('u_id'));
			$this->load->view('../../inc/footer');
		}
	}
	//to showing data of Houses table
	public function Houses()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$config = array();
		$num_rows=$this->db->count_all("houses");
	    $config["base_url"] = base_url() . "houses/";
	    $config["total_rows"] = $this->Academics_model->Countrows_houses();
	    $config["per_page"] = 2;
	    $ppage =$config["per_page"];
		$config['num_links'] = $num_rows;
		$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
	    $this->pagination->initialize($config);
	    $page = ($this->uri->segment(2)) ? $ppage*($this->uri->segment(2)-(1)) : 0;
	    $response["page"]=$page;
	    $response["links"] = $this->pagination->create_links();
	    $response['housesview']=$this->Academics_model->Limitrows_houses($config["per_page"], $page);
		$this->load->view('houses',$response);
		$this->load->view('../../inc/footer');
	}
	//to view the Add Houses form
	public function Add_Houses()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add_house');
		$this->load->view('../../inc/footer');
	}
	//to check validation and insert new data in Houses table 
	public function Insert_Houses()
	{
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('code', 'code name', 'required');
		$this->form_validation->set_rules('academicyear', 'Academic year', 'required');
		$this->form_validation->set_rules('notes', ' Remarks', 'required');

		if ($this->form_validation->run() == true){
			$data=$this->input->post();
			$this->Academics_model->Inserthouses($data);
			redirect('houses');
		}else{
			$this->load->view('../../inc/header');
			$this->load->view('../../inc/sidebar');
			$this->load->view('add_house');
			$this->load->view('../../inc/footer');
		}
	}
	//get Houses id for delete a row
	public function Delete_Houses($del_id)
	{
		$this->Academics_model->Deletehouses($del_id);
		redirect('houses');
	}
	//get Houses id to edit a row
	public function Edit_Houses($edit_id)
	{
		$housedata=$this->Academics_model->Eedithouses($edit_id);
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('edit_house',compact('housedata'));
		$this->load->view('../../inc/footer');
	}
	//to update Houses data
	public function Update_Houses()
	{
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('code', 'code name', 'required');
		$this->form_validation->set_rules('academicyear', 'Academic year', 'required');
		$this->form_validation->set_rules('notes', ' Remarks', 'required');

		if ($this->form_validation->run() == true){
			$data=$this->input->post();
			$u_id=$data['update_id'];
	        unset($data['update_id']);
	        $data['modifieddate']=date('Y-m-d H:i:s');
			$this->Academics_model->Updatehouses($data,$u_id);
			redirect('houses');
		}else{
			$this->load->view('../../inc/header');
			$this->load->view('../../inc/sidebar');
			$data=$this->input->post();
			$u_id=$data['update_id'];
			$this->load->view('edit_house',compact('u_id'));
			$this->load->view('../../inc/footer');
		}
	}///end update houses///

    // Controller for Academics_year
	public function Academics_year(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$config = array();
		$num_rows=$this->db->count_all("academicyear");
	    $config["base_url"] = base_url() . "academics_year/";
	    $config["total_rows"] = $this->Academics_model->Countrows_academicyear();
	    $config["per_page"] = 2;
	    $ppage =$config["per_page"];
		$config['num_links'] = $num_rows;
		$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
	    $this->pagination->initialize($config);
	    $page = ($this->uri->segment(2)) ? $ppage*($this->uri->segment(2)-(1)) : 0;
	    $response["page"]=$page;
	    $response["links"] = $this->pagination->create_links();
	    $response['data']=$this->Academics_model->Limitrows_academicyear($config["per_page"],$page);
		$this->load->view('academics_year',$response);
		//$data['h']=$this->Academics_model->AcademicYear();
		//$this->load->view('academics_year',$data);
		$this->load->view('../../inc/footer');
	}
	  // Controller for Add_Acadmic_year
	public function show_Acadmic_year(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('Add_Acadmic');
		$this->load->view('../../inc/footer');
	} 
	 // Controller for show  Acadmic Year
	public function ShowAcadmicYear(){
		
        $data['AcadmicYear'] = $this->Academics_model->show_academicyear();
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('updateAcadmic',$data);
		$this->load->view('../../inc/footer');
	}
	//to change status in Acadmic year
	public function StatusChange($id)
	{
		$status= $this->Academics_model->Check_status($id);
		if ($status == 1) {
			$s['status']='0';
			$this->Academics_model->Change_status($id,$s);
			redirect('academics_year');
		}else{
			$s['status']='1';
			$this->Academics_model->Change_status($id,$s);
			redirect('academics_year');
		}
	}
	 // Controller for Edit  Subject Form
	public function EditSubjectForm($id){

		$data['AcadmicYear'] = $this->Academics_model->EditSubject($id);
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('subjectedit',$data);
		$this->load->view('../../inc/footer');
	}
	// Controller for show  viewRecord
	public function DeletRecord(){

		$id= $_POST ['employee_id'];
		$data = array(

			        'delete'         => 2,
			        'modifieddate'  =>date("Y-m-d")
                );
		$data = $this->Academics_model->deletacadmic($id,$data);
		
	}

    // Controller for show  Delet Subject
	public function DeletSubject(){

		$id= $_POST ['employee_id'];
		$data = array(
			      'delete'         => 2,
			      'modifieddate'  =>date("Y-m-d")
                );
		$data = $this->Academics_model->deletSubject($id,$data);
		
	}

	// Controller for Update Acadmic Year
	public function UpdateAcadmic(){
        $id = $this->input->post('Acadmic_id');
		$acadmic_year = $this->input->post('Acadmic_year');
		$acadmic_notes = $this->input->post('Acadmic_notes');
		$data = array(
			     'name'  => $acadmic_year,
		         'notes' => $acadmic_notes,
		         'modifieddate'  =>date("Y-m-d")
                );

		$this->form_validation->set_rules('Acadmic_year', 'Acadmic year', 'required');
        $this->form_validation->set_rules('Acadmic_notes', 'Acadmic notes', 'required');
        if( 
        	$this->form_validation->run() == FALSE){
            redirect(base_url('Edit/'));
        }
        else
        {
          $this->Academics_model->Acadmic_update1($id, $data);
          $data['AcadmicYear'] = $this->Academics_model->show_academicyear($id);
		 redirect(base_url('academics_year'));
        }
		
	}

    // Controller for Update Acadmic Year
	public function updateSubject(){
		//print_r($_POST);die;
        $id = $this->input->post('dataid');
		$Subject_code = $this->input->post('Subject_code');
		$Subject_Name = $this->input->post('Subject_Name');
		$Notes = $this->input->post('Notes');
		$data = array(
                    'code'  => $Subject_code,
			        'suject'=> $Subject_Name,
			        'notes' => $Notes,
			        'modifieddate'  =>date("Y-m-d")
                );

		$this->form_validation->set_rules('Subject_code', 'Subject code', 'required');
        $this->form_validation->set_rules('Subject_Name', 'Subject Name', 'required');
        $this->form_validation->set_rules('Notes', 'Notes', 'required');
        if( 
        	$this->form_validation->run() == FALSE){
   
        }
        else
        {
          $this->Academics_model->updateSubject($id, $data);
		  redirect(base_url('subjects'));
        }
		
	}

	// Controller for AddAcadmicyearr
	public function AddAcadmicyear(){

        $year = $this->input->post('year');
		$Notes = $this->input->post('Notes');
		$data = array(
                 'name'  => $year,
		         'notes' => $Notes,
		         'createdate'  =>date("Y-m-d")
                );
		$this->form_validation->set_rules('year', 'year', 'required');
        $this->form_validation->set_rules('Notes', 'notes', 'required');
        if( 
        	$this->form_validation->run() == FALSE){
   
        }
        else
        {
          $this->Academics_model->AddacadmicYear($data);
          $data['AcadmicYear'] = $this->Academics_model->show_academicyear($id);
		  redirect(base_url('academics_year'));
        }
	
	}

	// Controller for Subjects
	public function Subjects(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$config = array();
		$num_rows=$this->db->count_all("subjects");
	    $config["base_url"] = base_url() . "subjects/";
	    $config["total_rows"] = $this->Academics_model->Countrows_subjects();
	    $config["per_page"] = 10;
	    $ppage =$config["per_page"];
		$config['num_links'] = $num_rows;
		$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
	    $this->pagination->initialize($config);
	    $page = ($this->uri->segment(2)) ? $ppage*($this->uri->segment(2)-(1)) : 0;
	    $response["page"]=$page;
	    $response["links"] = $this->pagination->create_links();
	    $response['subject']=$this->Academics_model->Limitrows_subjects($config["per_page"],$page);
		$this->load->view('subject',$response);
		//$data['subject']=$this->Academics_model->getSubject();
		//$this->load->view('subject',$data);
		$this->load->view('../../inc/footer');
	}
	// Controller for  Add_Subject
	public function Add_Subject(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('Add_subject');
		$this->load->view('../../inc/footer');
	}

	// Controller for  Add_New_Subject
	public function AddnewsubjectData(){
		//print_r($_POST);die;
        $Subject_code = $this->input->post('Subject_code');
		$Subject_Name = $this->input->post('Subject_Name');
		$Notes = $this->input->post('Notes');
		$data = array(

			        'code'  => $Subject_code,
			        'name'  => $Subject_Name,
			        'notes' => $Notes,
			        'createdate'  =>date("Y-m-d")
                );//print_r($data);die;

		$this->form_validation->set_rules('Subject_code', 'Subject code', 'required');
        $this->form_validation->set_rules('Subject_Name', 'Subject Name', 'required');
        $this->form_validation->set_rules('Notes', 'notes', 'required');
        if( 
        	$this->form_validation->run() == FALSE){
            //redirect(base_url('Edit/'));
        }
        else
        {
          $this->Academics_model->AddaNewSubject($data);
		  redirect(base_url('subjects'));
        }
		
	}

	// Controller for Classes_Group
	public function Classes_Group(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['classgroup'] = $this->Academics_model->classname();
		$this->load->view('classesgroup',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for Edit Comment
	public function EditComment(){

		$id   = $this->input->post('comment_id');
		$comment        = $this->input->post('comment');

		$this->form_validation->set_rules('comment', 'comment', 'required');
	}

      // Controller for Add Classes_Group
	public function AddGroup(){
		$Group_Name   = $this->input->post('Group_Name_');
		$class        = $this->input->post('class');
		$class        =  implode(",",$class);
		$Notes        = $this->input->post('Notes');

		$this->form_validation->set_rules('Group_Name', 'Group Name', 'required');
		$this->form_validation->set_rules('class', 'class', 'required');
		$this->form_validation->set_rules('Notes', 'Notes', 'required');

		$data = array(
			'name'   => $Group_Name,
			'classid'=> $class,
			'notes'  => $Notes,
			'modifieddate'=>date("Y-m-d")
		);

		$this->Academics_model->Addaclassgroup($data);
		redirect(base_url('Classes_Group'));

	}

	   // Controller for Add lecturetime
	public function Addlecturetime(){
		
		$Name       = $this->input->post('Name');
		$From_time  = $this->input->post('from_time');
		$to_time    = $this->input->post('to_time');
		$Notes      = $this->input->post('Notes');
		$Code       = $this->input->post('Code');

		$this->form_validation->set_rules('Name', 'Name', 'required');
		$this->form_validation->set_rules('from_time', 'from time', 'required');
		$this->form_validation->set_rules('to_time', 'to time', 'required');
		$this->form_validation->set_rules('Notes', 'Notes', 'required');
		$this->form_validation->set_rules('Code', 'Code', 'required');

		$data = array(
			'name'      => $Name,
			'from'      => $From_time,
			'to'        => $to_time,
			'code'      => $Code,
			'notes'     => $Notes,
			'createdate'=>date("Y-m-d")
		);

		$this->Academics_model->AddLetureTime($data);
		redirect(base_url('Lectures'));

	}

	// Controller for  Add Class Group
	public function AddClassGroup(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['classgroup'] = $this->Academics_model->classgroup();
		$this->load->view('AddClassGroup',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for  Add Lecture
	public function AddLecture(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['classgroup'] = $this->Academics_model->classgroup();
		$this->load->view('AddLecture',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for  Add_building
	public function Add_building(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('Add_building');
		$this->load->view('../../inc/footer');
	}
	// Controller for  Add_building Data
	public function AddnewbuildingData(){
		//print_r($_POST);die;
		$Subject_code = $this->input->post('building_code');
		$Subject_Name = $this->input->post('building_Name');
		$Notes = $this->input->post('Notes');
		$data = array(

			'code'  => $Subject_code,
			'name'  => $Subject_Name,
			'notes' => $Notes,
			'createdate'  =>date("Y-m-d")
                );//print_r($data);die;

		$this->form_validation->set_rules('building_code', 'building code', 'required');
		$this->form_validation->set_rules('building_Name', 'building Name', 'required');
		$this->form_validation->set_rules('Notes', 'notes', 'required');
		if( 
			$this->form_validation->run() == FALSE){
            //redirect(base_url('Edit/'));
		}
		else
		{
			$this->Academics_model->AddaNewbuilding($data);
			redirect(base_url('Buildings'));
		}
		
	}

	// Controller for Lectures
	public function Lectures(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['Lectures'] = $this->Academics_model->getLectures();
		$this->load->view('Lectures',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for Timetable
	public function Timetable(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['classgroup'] = $this->Academics_model->classgroup();
		$data['Acadmic']    = $this->Academics_model->AcademicYear();
		$this->load->view('Timetable',$data);
		$this->load->view('../../inc/footer');
	}
	// Controller for GetTimetable for sarch 
	public function GetTimetable(){

		$class = $this->input->post('clsaa');
		$acadmic = $this->input->post('acadmic');
		$data['Timetable']   = $this->Academics_model->getTimetable($class,$acadmic);
		$data['teacher']   = $this->Academics_model->getTeacherList($class,$acadmic);
		$data['getLectures'] = $this->Academics_model->getLecturesdata($class,$acadmic);
		$data['getSubjects'] = $this->Academics_model->getSubjectsdata($class,$acadmic);
		$data['class']   =$class;
		$data['acadmic'] =$acadmic;
		$this->load->view('timetableview',$data);
		
	}
	public function Savetimetable(){

        $clsaa    = $this->input->post('clsaa');
        $acadmic  = $this->input->post('acadmic');
        $leacture = $this->input->post('leacture');
        $teacher  = $this->input->post('teacher');
        $subject  = $this->input->post('subject');
        $leacture = explode(',',$leacture[0]);
        $day      = $leacture[0];
        $leacture = $leacture[1];
        

        $data = array(

			'day'      => $day,
			'classid'  => $clsaa,
			'subjectid'=> $subject[0],
			'lectureid'=> $leacture,
			'teacherid'=> $teacher[0],
			'academicyear'=>$acadmic,
			'createtdate'=>date("Y-m-d H:i:s")
                );
        $data = $this->Academics_model->Savetimetable($data);       
		
	}
	// Controller for Buildings
	public function Buildings(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['building'] = $this->Academics_model->getBuilding();
		$this->load->view('building',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for Substitute
	public function Substitute(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}
    // For frontdask class_teacher
	public function class_teacher(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// For frontdask lesson_planing
	public function class_room(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// For frontdask Class_Subject
	public function Class_Subject(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// For frontdask Student_Subject
	public function Student_Subject(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// For frontdask lesson_planning
	public function lesson_planning(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// For frontdask homework
	public function homework(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// For acadmic calendar
	public function calendar(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['calendar'] = $this->Academics_model->getcalendar();
		$this->load->view('calendar',$data);
		$this->load->view('../../inc/footer');
	}

	// For Addcalendar
	public function Addcalendar(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['class'] = $this->Academics_model->classgroup();
		$this->load->view('addcalendar',$data);
		$this->load->view('../../inc/footer');
	}

	// For Addnewcelender
	public function Addnewcelender(){

		
		$date = $this->input->post('date');
		$event = $this->input->post('event');
		$Holiday = $this->input->post('holiday');
		$Notes = $this->input->post('notes');
		$class = $this->input->post('class');
		if(!empty($_POST['class']) && isset($_POST['class'])){
			$class =implode(',', $class);
		}
		
		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('event', 'Event', 'required');
		$this->form_validation->set_rules('holiday', 'Holiday', 'required');
       // $this->form_validation->set_rules('Notes', 'Notes', 'required');
		
		if( 
			$this->form_validation->run() == FALSE){

		}
		else
		{
			$data = array(
				'date'       => $date,
				'event'      => $event,
				'holiday'    => $Holiday,
				'notes'      => $Notes,
				'classlistid'=> $class,
				'createdate'=>date("Y-m-d")
			);
			$data = array_filter($data);
            //print_r($data);die;
			$data['routes'] = $this->Academics_model->AddNewCelender($data);
			redirect(base_url('calendar'));
		}
	}

	// For  workload
	public function workload(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['year'] = $this->Academics_model->getAcadmicYear();
		$this->load->view('workload',$data);
		$this->load->view('../../inc/footer');
	}

	// For  workload
	public function Getworkload(){
		$clsaa = $this->input->post('clsaa');
		$data['workload'] = $this->Academics_model->getWorkload($clsaa);
		$data['day']  = array('Mon','Tue','Wed','Thu','Fri','Sat');
		$this->load->view('workload_table',$data);

	}

	// For frontdask Parents_sms
	public function Parents_sms(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}
	
    // For frontdask circular
	public function circular(){
		
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}
	
}
