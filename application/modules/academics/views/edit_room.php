<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Add Room Form
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dashboard</a></li>
        <li class="active">addroom</li>
      </ol>
    </section>
    <!-- Add class content section -->
    <section class="content">
      <div class="row">
        <!-- SELECT2 EXAMPLE -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Add Room</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <div class="box-body">
              <div class="row">
                <div class="col-md-5">
                  <form method="post" action="<?php echo base_url('roomsupdate'); ?>" data-toggle="validator" role="form">
                    <div class="form-group">
                      <label>Name*</label>
                      <input type="hidden" name="update_id" value="<?php if(isset($roomdata)){ echo $roomdata->id;}else{ echo $u_id; } ?>">
                      <input type="text" name="name" class="form-control" value="<?php if(isset($roomdata)){ echo $roomdata->name; }else{echo set_value('name');} ?>" required><?php echo form_error('name'); ?>
                    </div>
                    <div class="form-group">
                      <label>Type*</label>
                      <input type="text" name="type" class="form-control" value="<?php if(isset($roomdata)){ echo $roomdata->type; }else{echo set_value('type');} ?>" required><?php echo form_error('type'); ?>
                    </div>
                    <div class="form-group">
                      <label>Building*</label>
                      <input type="text" name="building" class="form-control" value="<?php if(isset($roomdata)){ echo $roomdata->building; }else{echo set_value('building'); } ?>" required><?php echo form_error('building'); ?>
                    </div>
                    <div class="form-group">
                      <label>Capacity*</label>
                      <input type="text" name="capacity" class="form-control" value="<?php if(isset($roomdata)){ echo $roomdata->capacity; }else{ echo set_value('capacity');} ?>" required><?php echo form_error('capacity'); ?>
                    </div>
                    <div class="form-group">
                      <label>Counselor’s Remarks </label><?php echo form_error('notes'); ?>
                      <textarea class="form-control" name="notes" rows="3" placeholder="Counselor’s Remarks " required><?php if(isset($roomdata)){ echo $roomdata->notes; }else{echo set_value('notes'); } ?></textarea>
                    </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>