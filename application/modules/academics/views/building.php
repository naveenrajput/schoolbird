<style>



 </style>
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
  <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        building
     
      </h1>
    <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"> Acadmic</a></li>
        <li class="active">building</li>
      </ol>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;">
    
        <a  href="<?php echo base_url('Add_building')?>" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp; Add building</a>
     <a  href="#" class="btn  btn-primary sp-10"  data-toggle="modal" data-target="#modal-enquiry"> <i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp; Send Enquiry</a>
    </div>
    </section>

    <!-- Main content -->
     <section class="content">
      <div class="row">
        <div class="col-xs-12">
    

     <div class="box">
      <div class="box-header with-border mr-top-20 text-center">
      <div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
      <div class="col-md-2" >
        <button type="submit" id="getJsonSrc" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
      
      
      
        </div>
            <!-- /.box-header -->
      <!----<div class="box-body">
      <div class="dropdown pull-right">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> Default Reminder
  <span class="caret"></span></button>
  <ul class="dropdown-menu">
    <li><a href="#">HTML</a></li>
    <li><a href="#">CSS</a></li>
    <li><a href="#">JavaScript</a></li>
  </ul>
</div>




      <!----<div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example
    <span class="caret"></span></button>
    <ul class="dropdown-menu" style=" max-height: 150px; overflow-y: auto;">
      <input class="form-control" id="myInput" type="text" placeholder="Search..">
      <li><a href="#">1 Day</a></li>
      <li><a href="#">2 Days</a></li>
    <li><a href="#">3 Day</a></li>
      <li><a href="#">4 Days</a></li>
      <li><a href="#">5 Day</a></li>
      <li><a href="#">6 Days</a></li>
    <li><a href="#">7 Day</a></li>
      <li><a href="#">8 Days</a></li>
   
    </ul>
  </div>
      
      
      
      </div>----->
      
            <div class="box-body table-responsive">
      
             <!-- <table id="example" class="display nowrap" style="width:100%">--->
              <table id="example" class="table table-bordered " >
        <div class="txt-dis">Export in Below Format</div>
        <thead>
            <tr>
    
                <th><input type="checkbox" id="selectall"> All</input></th>
                <th>S.no</th>
                <th>name</th>
                <th>code</th>
                <th>NOTES</th>
                <th>date</th>
                <th>Action</th>
                
        
        
            </tr>
        </thead>
        <tbody>
    <?php  foreach ($building->result() as $row)  {  ?>
            <tr>
      <td>
    <input type="checkbox" class="selectedId" name="selectedId" />
      </td>
              
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->name;?></td>
                <td><?php echo $row->code;?></td>
                <td><?php echo $row->notes;?></td>
                <td><?php echo $row->createdate;?></td>
         
         
               
        <td>
              <ul class="table-icons">
      
          
      
    <li><a href=" <?php echo base_url('Edit_building/').$row->id;?>" class="table-icon" title="Edit"> 
    
    <span class="glyphicon glyphicon-edit display-icon"></span>
    </a></li>
          
     
    <li>
    <span class="glyphicon glyphicon-trash display-icon view_data" id="<?php echo $row->id;?>"></span>
    </li>
    </ul>
          </td>
              
        
            </tr>
           
    <?php }?>
        </tbody>
        <tfoot>
            <tr>
              <th> All</input></th>
                 <th>S.no</th>
                <th>SESSION</th>
                <th>STATUS</th>
                <th>NOTES</th>
                <th>Forward</th>
               

            </tr>
        </tfoot>
    </table>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

 
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        lengthChange: false,
    autoWidth : true,
        buttons: [   'csv', 'excel', 'pdf', 'print' ],
    
    
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
    
} );




</script>

<script>


$(document).ready(function () {
    $('#selectall').click(function () {
        $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function () {
        var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
        $('#selectall').prop("checked", check);
    });
});

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
  //Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});


//review


$(document).ready(function() {
 
  handleStatusChanged();
  
});

function handleStatusChanged() {
    $('#toggleElement').on('change', function () {
      toggleStatus();   
    });
}

function toggleStatus() {
    if ($('#toggleElement').is(':checked')) {
        $('#checktwo').attr('disabled', true);
          $('#name').show('#name');
    } else {
      $('#name').hide('#name');
        $('#checktwo').removeAttr('disabled');
    }   
}
function toggleStatusone() {
    if ($('#checktwo').is(':checked')) {
        $('#toggleElement').attr('disabled', true);
         $('#name1').show('#name1');
    } else {
       $('#name1').hide('#name1');
        $('#toggleElement').removeAttr('disabled');
    }   
}

</script>
<script>  
 $(document).ready(function(){ 
      $('.view_data').click(function(){  
           var employee_id = $(this).attr("id");
           //alert(employee_id);
           var x=confirm("Are you sure to delete record?")
          if (x) {
            $.ajax({  
                        url:"<?php echo base_url('Academics/Deletbuilding'); ?>",  
                        method:"post",  
                        data:{employee_id:employee_id},  
                        success:function(data){  
                               location.reload();
                        }  
                   }); 
          } else {
            return false;
          } 
              });  
 });  



 // date filter

 
 </script>