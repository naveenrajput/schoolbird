<style>



 </style>
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
  <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        calender
     
      </h1>
    <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Acadmic</a></li>
        <li><a href="#"> Acadmics</a></li>
        <li class="active">calender</li>
      </ol>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;">
    
        <a  href="<?php echo base_url('addcalender')?>" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp; Add calender</a>
     <a  href="#" class="btn  btn-primary sp-10"  data-toggle="modal" data-target="#modal-enquiry"> <i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp; Send Enquiry</a>
    </div>
    </section>

    <!-- Main content -->
     <section class="content">
      <div class="row">
        <div class="col-xs-12">
    

     <div class="box">
      <div class="box-header with-border mr-top-20 text-center">
      <div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
      <div class="col-md-2" >
        <button type="submit" id="getJsonSrc" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
      
      
      
        </div>
        
      
            <div class="box-body table-responsive">
      
             <!-- <table id="example" class="display nowrap" style="width:100%">--->
              <table id="example" class="table table-bordered " >
        <div class="txt-dis">Export in Below Format</div>
        <thead>
            <tr>
    
                <th><input type="checkbox" id="selectall"> All</input></th>
                <th>S.no</th>
                <th>DATE</th>
                <th>DAY</th>
                <th>EVENT</th>
                <th>HOLIDAY</th>
                <th>NOTES</th>
                
               
                
        
        
            </tr>
        </thead>
        <tbody>
    <?php  foreach ($calendar->result() as $row)  { 
      ?>
            <tr>
      <td>
    <input type="checkbox" class="selectedId" name="selectedId" />
      </td>
              
                <td><?php echo $row->id;?></td>
                <td><?php echo date("d-m-Y",strtotime($row->date));?></td>
                <td><?php echo  date("l",strtotime($row->date)); ?></td>
                 <td><?php echo $row->event;?></td>
                  <td><?php if($row->holiday=='0'){ echo "No";} else{echo "Yes";} ?></td>
                  <td><?php echo $row->notes;?></td>
                 
        <td>
              <ul class="table-icons">
      
    </ul>
          </td>
              
        
            </tr>
           
    <?php }?>
        </tbody>
        <tfoot>
            <tr>
              <th> All</input></th>
                </th>
                <th>S.no</th>
                <th>DATE</th>
                <th>DAY</th>
                <th>EVENT</th>
                <th>HOLIDAY</th>
                <th>NOTES</th>
               

            </tr>
        </tfoot>
    </table>


            </div>
            <!-- /.box-body -->
          </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<div class="modal fade" id="myModal" role="dialog"  data-keyboard="false">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="softview">

    <i class="fa fa-compass"></i>&nbsp;&nbsp;Comments
    </div>
        </div>
        <div class="modal-body">
          <form method="post" id="myform" action="<?php echo base_url('Academics/EditComment') ?>"  >
          <input type="hidden" id="bookId" value=""name="comment_id">
          <textarea  name="comment" class="form-control pull-right" style="height:100px;" placeholder="Add a comment.." id="commentBox" required=""></textarea><br><br><br>


          <button class="btn btn-sm btn-primary editcomment" >
           POST&nbsp;&nbsp;<i class="fa fa-send"></i>


</button></form>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
 
