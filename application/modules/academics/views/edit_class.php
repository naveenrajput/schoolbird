
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit Class Form
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dashboard</a></li>
        <li class="active">Editclass</li>
      </ol>
    </section>
    <!-- Add class content section -->
    <section class="content">
      <div class="row">
        <!-- SELECT2 EXAMPLE -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Class</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <div class="box-body">
                <div class="row">
                 <form method="post" action="<?php echo base_url('classupdate'); ?>" data-toggle="validator" role="form">
                      
             <div class="col-md-4">
              <div class="form-group">

                <input type="hidden" name="id" value="<?php if(isset($classdata)){ echo $classdata->id; }else{echo $id; } ?>" >
                      <label>Class Name*</label>
                      <input type="text" name="class_title" class="form-control" value="<?php if(isset($classdata)){ echo $classdata->class_title; }else{ echo set_value('class'); } ?>" required><?php echo form_error('class'); ?>
                    </div>
            </div>
             <div class="col-md-4">
              <div class="form-group">
                <label>Section*</label><?php echo form_error('section'); ?>
                  <input type="text" name="section" class="form-control" value="<?php if(isset($classdata)){ echo $classdata->section; }else{echo set_value('section'); } ?>" required><?php echo form_error('section'); ?>
               </div>
            </div>
            <div class="col-md-4">
               <div class="form-group">
                <label for="">Number of Student </label>
                <input type="Number"value="<?php if(isset($classdata->capacity)){ echo $classdata->capacity; }else{echo set_value('section'); } ?>" name="capacity" class="form-control" placeholder="Number of Student" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">

                <label>Class Teacher*</label><?php echo form_error('Teacher'); ?>
                  <select type="Number" name="teacher" class="form-control" placeholder="Number of Student" required>    <?php 
                        foreach ($teacher->result() as $key) { ?>
                         <option value="<?php echo $key->id;?>" <?php if( $key->id == $classdata->teacher ): ?> selected="selected"<?php endif; ?>><?php echo $key->name;?></option>
                      <?php  }
                      ?>
                     </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Class Subject*</label><?php echo form_error('Subject'); ?>

                  <select name="subject[]" class="form-control select2" multiple="multiple" data-placeholder="Select a State" required>  
                  <?php 
                        foreach ($subject->result() as $row) { ?>
                         <?php $data= json_decode($classdata->subject);
                         foreach ($data as $key ) {?>
                         <option  value='{"sub_id":<?php echo $row->id; ?>,"sub_title":"<?php echo $row->suject;?>","sub_ord":<?php echo $row->sub_ord;?>}'<?php if( $key->sub_id == $row->id ): ?> selected="selected"<?php endif;} ?> ><?php echo $row->suject;?></option>
                      <?php  }
                      ?> 
                  </select>  

                </div>
            </div>
            <div class="col-md-12">
             <div class="form-group">
                      <label>Counselor’s Remarks </label><?php echo form_error('notes'); ?>
                      <textarea class="form-control" name="notes" rows="3" placeholder="Counselor’s Remarks " required><?php  if(isset($classdata->notes)){ echo $classdata->notes; }else{echo set_value('notes'); } ?></textarea>
                    </div>

            </div>
            <div class="clearfix"></div>
            <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

          </form>
      </div>

                
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  