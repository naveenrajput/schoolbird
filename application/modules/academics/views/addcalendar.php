
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Calender
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Academics</a></li>
        <li><a href="#">Academics</a></li>
        <li class="active">Calender</li>
        <li class="active">Add Calender</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
          
            <form method="post" action="<?php echo base_url('Addnewcelender');?>" data-toggle="validator" role="form">

              <div class="box-body">
        
        
          <div class="row">
          <div class="col-md-12 "> <p class="lead">celender</p></div>
         
        <div class="col-md-6">
        <div class="form-group">
                  <label for="">date </label>
                  <input type="date" class="form-control"name="date" placeholder="date" required>
                </div>
        </div>
          <div class="col-md-6">
        <div class="form-group">
                  <label for="">Event</label>
                  <input type="text" name="event" class="form-control" placeholder="Event" required>
                </div>
        </div>
         <div class="col-md-12">
      
       <div class="form-group">
                 
                <select id="colorselector" name="holiday" class="form-control"  data-placeholder="Select a State"
                        style="width: 100%;">
                  <option>select option</option>
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
         
                </div>
                <div class="form-group has-error has-danger Notes" style="display:none">
                  <label>Notes</label>
                  <textarea class="form-control" name="notes" rows="3" placeholder="Notes"></textarea>
                </div>
                <div class="form-group has-error has-danger class"style="display:none" >
                  <select class="form-control select2" name="class[]" multiple="multiple" data-placeholder="Select a State"
                        style="width: 100%;">
                  <?php foreach ($class->result() as $row ) {print_r($row); ?>
                  <option value="<?php echo $row->id;?>"><?php echo $row->name.' '.$row->section;?></option>
                <?php }?>
                </select>
                </div>


      </div>
    </div>

        
          
       
         </div>
         
   
        </div>
  </div>
             </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
           
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
        


        </div>
      <!-- /.box -->

     </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

<script type="text/javascript">
  
$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    })
$(function() {
  $('#colorselector').change(function(){

  var selecter=  $(this).val();
 
  if (selecter == 1) {
     $('.class').show();
     $('.Notes').hide();
   
    
  }else{
    $('.Notes').show();
    $('.class').hide();
  }
  });
});
</script>