<style>



 </style>
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
  <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
       Timetable
     
      </h1>
   <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Academics</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Timetable</li>
       
      </ol>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;">
    
        <a  href="<?php echo base_url('Add_Lecture')?>" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp; Add Timetable</a>
     <a  href="#" class="btn  btn-primary sp-10"  data-toggle="modal" data-target="#modal-enquiry"> <i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp; Send Enquiry</a>
    </div>
    </section>

    <!-- Main content -->
     <section class="content">
      <div class="row">
        <div class="col-xs-12">
    

     <div class="box">
      
        
      
            <div class="box-body table-responsive">
      
              <div class="box-body">
        
        
          <div class="row">
         
         
        <div class="col-md-12">
       
        </div>

          <div class="col-md-5">    
        <div class="form-group">
                  <label for="">Class </label>
                  <select id="class" value=""class="form-control"name="class" placeholder="Subject Name " required>
                     <option>select</option>
                    <?php foreach ($classgroup->result() as $row) { 
                    ?>
                       <option value="<?php echo $row->class_id ?>"><?php echo 'for class'.' '. $row->class_title.$row->section ?></option>
                   <?php } ?>
                   
                  </select>
                </div>
        </div>
          <div class="col-md-5">    
        <div class="form-group">
                  <label for="">Acadmic Year </label>
                  <select id="acadmic" type="text" class="form-control"  name="Acadmicyear" required>
                   <?php foreach ($Acadmic->result() as $row) {?>
                       <option value="<?php echo $row->id ?>"><?php echo 'for '.' '. $row->name ?></option>
                   <?php } ?>
                  </select>
          
                </div>
        </div>
                <div class="col-md-2" style="padding-top:20px;">    
        
                       <button class="btn btn-sm btn-primary"  onclick="changeFunc();" id="submit">
      GO&nbsp;&nbsp;
      <i class="fa fa-arrow-right"></i>
    </button>
            </div>
            </div>
      
        </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive"> 
            <div id="Table_id"></div>
            
          </div>
          <!-- /.box-body --> 
        </div>
     

        </div>
        <!-- /.col -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  
</div>
<script type="text/javascript">
  function changeFunc() {
     var selectBox = document.getElementById("class");
    var selectedclass = selectBox.options[selectBox.selectedIndex].value; 
    var selectBox = document.getElementById("acadmic");
    var selecteAcadmic = selectBox.options[selectBox.selectedIndex].value;
   
   $.ajax({

        type:"post",//or POST
        url:'Academics/GetTimetable',
                          
        data:{clsaa:selectedclass,acadmic:selecteAcadmic},
      
       success:function(data){
              //console.log(data);
            $('#Table_id').html(data);  

              
            } 
     })
   }
 </script>