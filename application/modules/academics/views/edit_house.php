<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Add Room Form
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dashboard</a></li>
        <li class="active">addroom</li>
      </ol>
    </section>
    <!-- Add class content section -->
    <section class="content">
      <div class="row">
        <!-- SELECT2 EXAMPLE -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Add Room</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <div class="box-body">
              <div class="row">
                <div class="col-md-5">
                  <form method="post" action="<?php echo base_url('housesupdate'); ?>" data-toggle="validator" role="form">
                    <div class="form-group">
                      <input type="hidden" name="update_id" value="<?php if(isset($housedata)){ echo $housedata->id;}else{ echo $u_id; } ?>">
                      <label>Name*</label>
                      <input type="text" name="name" class="form-control" value="<?php if(isset($housedata)){ echo $housedata->name; }else{echo set_value('name');} ?>" required><?php echo form_error('name'); ?>
                    </div>
                    <div class="form-group">
                      <label>Code*</label>
                      <input type="text" name="code" class="form-control" value="<?php if(isset($housedata)){ echo $housedata->code; }else{echo set_value('code');} ?>" required><?php echo form_error('code'); ?>
                    </div>
                    <div class="form-group">
                      <label>Academicyear*</label>
                      <input type="text" name="academicyear" class="form-control" value="<?php if(isset($housedata)){ echo $housedata->academicyear; }else{echo set_value('academicyear'); } ?>" required><?php echo form_error('academicyear'); ?>
                    </div>
                    <div class="form-group">
                      <label>Counselor’s Remarks </label><?php echo form_error('notes'); ?>
                      <textarea class="form-control" name="notes" rows="3" placeholder="Counselor’s Remarks " required><?php if(isset($housedata)){ echo $housedata->notes; }else{echo set_value('notes'); } ?></textarea>
                    </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>