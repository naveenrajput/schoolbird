
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
  <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Add Lecture
     
      </h1>
   <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Academics</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Lecture</li>
        <li class="active"> Add Lecture</li>
      </ol>
    </div>
  
    </section>

    <!-- Main content -->
     <section class="content">
      <div class="row">
        <div class="col-xs-12">
    

     <div class="box">
      
        
      
            <div class="box-body table-responsive">
      
             <!-- <table id="example" class="display nowrap" style="width:100%">--->
             <form method="post" action="<?php echo base_url('addLecture');?>" data-toggle="validator" role="form">

              <div class="box-body">
        
        
          <div class="row">
         
         
        <div class="col-md-6">
        <div class="form-group">
                  <label for="">Name</label>
                  <input type="text" class="form-control"name="Name" placeholder="Subject Name " required>
                </div>
        </div>
    
        <div class="col-md-3">
        <div class="form-group">
                  <label for="">From Time </label>
                  <input type="time" class="form-control"name="from_time" placeholder="From Time" required>
                </div>
          </div>
          <div class="col-md-3">    
        <div class="form-group">
                  <label for="">To Time </label>
                  <input type="time" class="form-control"name="to_time" placeholder="To Time" required>
                </div>
        </div>
         
       
              
         <div class="col-md-6">
        <div class="form-group">
                  <label>Code </label>
                  <input type="text" class="form-control"name="Code" placeholder="Code" required>
                </div>
        </div>      
         <div class="col-md-6">
        <div class="form-group">
                  <label>Notes </label>
          <textarea  class="form-control"  placeholder="Notes" name="Notes" required></textarea>
                
                </div>
        </div>
         </div>
         
   
        </div>
  <div class="bootstrap-timepicker">
                <div class="form-group">
                  <label>Time picker:</label>

                  <div class="input-group">
          <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" class="form-control timepicker">

                    
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
</div>
<script type="text/javascript">
  
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

   
    

  })

</script>