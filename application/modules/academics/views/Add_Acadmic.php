
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Add Academic year
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Academic</a></li>
        <li class="active"> Add Academic year</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12">
          <!-- general form elements -->
      
       <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Add Academic year</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <form role="form" method='post' action="<?php echo base_url('Add_Acadmic_year');?>"data-toggle="validator" role="form" >
              <div class="box-body">
       
       <div class="row">
      
        
        <div class="col-md-4">
       <div class="form-group">
                <label>Session </label>
        
                <div class="input-group">
               <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                <input type="text" id="inputName"  name= "year"class="form-control" placeholder="2000-2001" required>
              </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
        </div>
          
         <div class="col-md-12">
        <div class="form-group">
                  <label>Notes</label>
                  <textarea class="form-control"name= "Notes" rows="3" placeholder="Notes" required></textarea>
                </div>
        </div>
         </div>
             </div>
             <div class="box-footer">
                   <button type="submit" class="btn btn-primary">Submit</button>
              </div>  
         
            </form>
                    </div>
      
      
      
         
      <!-- /.box -->

     </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

<!-- page script -->



