
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Add Class Form
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dashboard</a></li>
        <li class="active">addclass</li>
      </ol>
      <div class="clearfix"></div>
    </section>
    <!-- Add class content section -->
    <section class="content">
      <div class="row">
        <!-- SELECT2 EXAMPLE -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Add Class</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <div class="box-body">
                <div class="row">
                   <form method="post" action="<?php echo base_url('insertclass'); ?>" data-toggle="validator" role="form">
                      
             <div class="col-md-4">
              <div class="form-group">
                        <label>Class Name*</label><?php echo form_error('name');?>
                        <select name="class_title" class="form-control" required>
                            <option value="<?php echo set_value('class'); ?>" selected="selected">------Select Class------</option>
                            <?php foreach (range('1','12') as $char):?>
                            <option value="class<?php echo $char;?>">class<?php echo $char;?></option>
                            <?php endforeach;?>
                      </select>
                      <!-- <input type="text" name="name" class="form-control"  value="<?php //echo set_value('name'); ?>" required><?php //echo form_error('name'); ?> -->
                    </div>
            </div>
             <div class="col-md-4">
              <div class="form-group">
                      <label>Section*</label><?php echo form_error('section'); ?>
                      <select name="section" class="form-control" required>
                          <option value="<?php echo set_value('section'); ?>">-----Select Section-----</option>
                          <?php foreach (range('A','Z') as $char):?>
                          <option value="<?php echo $char;?>"><?php echo $char;?></option>
                          <?php endforeach;?>
                      </select>
                      <!-- <input type="text" name="section" class="form-control"  value="<?php //echo set_value('section'); ?>" required><?php //echo form_error('section'); ?> -->
                    </div>
            </div>
            <div class="col-md-4">
               <div class="form-group">
                <label for="">Number of Student </label>
                <input type="Number" name="capacity" class="form-control" placeholder="Number of Student" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Class Teacher*</label><?php echo form_error('Teacher'); ?>

                  <select class="form-control"   name="teacher">
                    <option value="<?php echo set_value('Teacher'); ?>">-----Select Teacher-----</option>
                    <?php foreach($teacher->result() as $row){?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->name;?></option>
                    <?php } ?>
                    </select>

                     
                    
                    </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                      <label>Class Subject*</label><?php echo form_error('Subject'); ?>
                      <select name="subject[]" class="form-control select2" multiple="multiple" data-placeholder="Select a State" required>
                    <option value="<?php echo set_value('section'); ?>">-----Select Section-----</option>
                          <?php foreach($subject->result() as $row){?>
                            <option value='{"sub_id":<?php echo $row->id; ?>,"sub_title":"<?php echo $row->suject;?>","sub_ord":<?php echo $row->sub_ord;?>}'><?php echo $row->suject;?></option>
                          <?php } ?>
                </select>


                     
                      <!-- <input type="text" name="section" class="form-control"  value="<?php //echo set_value('section'); ?>" required><?php //echo form_error('section'); ?> -->
                    </div>
            </div>
            <div class="col-md-12">
             <div class="form-group">
                      <label>Counselor’s Remarks </label><?php echo form_error('notes'); ?>
                      <textarea class="form-control" name="notes" rows="3" placeholder="Counselor’s Remarks " required><?php echo set_value('notes'); ?></textarea>
                    </div>

            </div>
            <div class="clearfix"></div>
            <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

          </form>
      </div>

                
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  