
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header clearfix">
      <h1>
       Add Class Group
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Academics</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Class group</li>
        <li class="active"> Add Class group</li>
      </ol>
    </section>

    
    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <!-- /.box-header -->
          
            <form method="post" action="<?php echo base_url('addnewgroup');?>" data-toggle="validator" role="form">

              <div class="box-body">
        
        
          <div class="row">
          <div class="col-md-12 "> <p class="lead">Class group Details  </p></div>
         
        <div class="col-md-6">
        <div class="form-group">
                  <label for="">Group Name  </label>
                  <input type="text" class="form-control"name="Group_Name " placeholder="Group Name " required>
                </div>
        </div>
          <div class="col-md-6">
        <div class="form-group">
                  <label for="">Select Classes </label>
                  <select name="class[]" class="form-control select2" multiple="multiple" data-placeholder="Select a State" name="Subject_Name" style="width: 100%;" >
                    <option> select</option>
                    <?php foreach ($classgroup->result() as $row ) {?>
                       <option value='<?php echo $row->id ?>'> <?php echo $row->class_title.$row->section ?></option>
                   <?php } ?>
                  </select>
                </div>
        </div>
       
              
         <div class="col-md-12">
        <div class="form-group">
                  <label>Notes</label>
                  <textarea class="form-control" name="Notes" rows="3" placeholder="Notes" required></textarea>
         </div>
        </div>
         
        
          
       
         </div>
         
   
        </div>
      
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
           
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
        


        </div>
      <!-- /.box -->

     </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript">
  
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
})

</script>
