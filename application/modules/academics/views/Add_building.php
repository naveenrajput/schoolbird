
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Add building
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Acadmic</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">building </li>
        <li class="active">Add building </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            
            <!-- /.box-header -->
          
            <form method="post" action="<?php echo base_url('Addnewbuilding');?>" data-toggle="validator" role="form">

              <div class="box-body">
        
        
          <div class="row">
          <div class="col-md-12 "> <p class="lead">building Details  </p></div>
         
        <div class="col-md-6">
        <div class="form-group">
                  <label for="">building code </label>
                  <input type="text" class="form-control"name="building_code" placeholder="building code" required>
                </div>
        </div>
          <div class="col-md-6">
        <div class="form-group">
                  <label for="">building Name  </label>
                  <input type="text" name="building_Name" class="form-control" placeholder="building Name" required>
                </div>
        </div>
         <div class="col-md-12">
        <div class="form-group">
                  <label>Notes</label>
                  <textarea class="form-control" name="Notes" rows="3" placeholder="Notes" required></textarea>
                </div>
        </div>
         </div>
        
          
       
         </div>
         
   
        </div>
  </div>
             </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
           
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
        


        </div>
      <!-- /.box -->

     </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

