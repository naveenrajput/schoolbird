  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
	<div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Houses
     
      </h1>
	  <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Academics</a></li>
        <li class="active">Houses</li>
      </ol>
	  </div>
	  <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;">
	  
        <a  href="<?php echo base_url('addhouse'); ?>" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp;ADD NEW</a>
		 <a  href="#" class="btn  btn-primary sp-10"  data-toggle="modal" data-target="#modal-enquiry"> <i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp; Send Enquiry</a>
	  </div>
    </section>
    <!-- Main content -->
     <section class="content">
      <div class="row">
        <div class="col-xs-12">
		

		 <div class="box">
		  <div class="box-header with-border mr-top-20 text-center">
        <!-- <form method="post" action="<?php //echo base_url() ?>"></form> -->
			<div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
			<div class="col-md-2" >
			  <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
			
			
			
			  </div>
            <!-- /.box-header -->
			<!----<div class="box-body">
			<div class="dropdown pull-right">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> Default Reminder
  <span class="caret"></span></button>
  <ul class="dropdown-menu">
    <li><a href="#">HTML</a></li>
    <li><a href="#">CSS</a></li>
    <li><a href="#">JavaScript</a></li>
  </ul>
</div>




			<!----<div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example
    <span class="caret"></span></button>
    <ul class="dropdown-menu" style=" max-height: 150px; overflow-y: auto;">
      <input class="form-control" id="myInput" type="text" placeholder="Search..">
      <li><a href="#">1 Day</a></li>
      <li><a href="#">2 Days</a></li>
	  <li><a href="#">3 Day</a></li>
      <li><a href="#">4 Days</a></li>
      <li><a href="#">5 Day</a></li>
      <li><a href="#">6 Days</a></li>
	  <li><a href="#">7 Day</a></li>
      <li><a href="#">8 Days</a></li>
   
    </ul>
  </div>
			
			
			
			</div>----->
			
            <div class="box-body table-responsive">
			
             <!-- <table id="example" class="display nowrap" style="width:100%">--->
              <table id="example" class="table table-bordered " >
			  <div class="txt-dis">Export in Below Format</div>
        <thead>
            <tr>
		
                <th><input type="checkbox" id="selectall"> All</th>
                <th>S.no</th>
                <th>Name</th>
                <th>Code</th>
                <th>Academicyear</th>
                <th>Action</th>
				    </tr>
        </thead>
        <tbody>
		<?php $i=($page+1); foreach ($housesview as $key => $housesview) { ?>
            <tr>
			<td>
		<input type="checkbox" class="selectedId" name="selectedId" />
			</td> 
                <td><?php echo $i; ?></td>
                <td><?php echo $housesview->name; ?></td>
                <td><?php echo $housesview->code; ?></td>
                <td><?php echo $housesview->academicyear; ?></td>
				<td>
			     <ul class="table-icons">
        <li><a href="<?php echo base_url('housesedit/').$housesview->id; ?>" class="table-icon" title="Edit"> 
		
		<span class="glyphicon glyphicon-edit display-icon"></span>
		</a></li>
          
		<!--  <li><a href="" class="table-icon" title="Set Reminder"  data-toggle="modal" data-target="#modal-reminder"> 
		
		<span class="glyphicon glyphicon-time display-icon"></span>
		</a></li>
		  <li><a href="" class="table-icon" title="Print"> 
		
		<span class="glyphicon glyphicon-print display-icon"></span>
		</a></li> -->
		<li>
		<a href="<?php echo base_url('housesdelete/').$housesview->id; ?>" class="table-icon" title="Delete" ><span class="glyphicon glyphicon-trash display-icon " ></span></a>
		</li>
    </ul>
				</td>
   </tr>
           
		<?php   $i++; }?>
        </tbody>
        <tfoot>
            <tr>
              <th><input type="checkbox" id="selectall"> All</th>
                <th>S.no</th>
                <th>Name</th>
                <th>Code</th>
                <th>Academicyear</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item"><?php echo $links ; ?></li>
      </ul>
    </nav>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

 <div class="modal fade" id="modal-reminder">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Reminder</h4>
              </div>
              <div class="modal-body">
              <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2019
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                      <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                      <div class="timeline-body">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                        quora plaxo ideeli hulu weebly balihoo...
                      </div>
                     
                    </div>
                  </li>
                  <!-- END timeline item -->
                 
                  <li class="time-label">
                        <span class="bg-green">
                          3 Mar. 2019
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-yellow"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                      <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                      <div class="timeline-body">
                        Take me to your leader!
                        Switzerland is small and neutral!
                        We are more like Germany, ambitious and misunderstood!
                      </div>
                      
                    </div>
                  </li>
                  <!-- END timeline item -->
                 
                </ul>
				
				<div class="row">
				
				<div class="col-md-6">
                  <div class="form-group">
                    <label> Date </label>
                    <div class="input-group date">
                      <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                      <input type="text" class="form-control pull-right" id="datepicker">
                    </div>
                    <!-- /.input group --> 
                  </div>
        </div>
				<div class="col-md-6">
                  <div class="form-group">
                    <label> Time </label>
                    <div class="input-group">
					<div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" class="form-control timepicker">

                    
                  </div>
                    <!-- /.input group --> 
                  </div>
        </div>
            <div class="col-md-12">
			<textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
				
			 </div> 
				</div>
				
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		<div class="modal fade" id="modal-review">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Review</h4>
              </div>
              <div class="modal-body">
			
             <div class="callout callout-info" >
        <h4><i class="fa fa-info"></i> Student Details:</h4>
       <a href="#" target="_blank" >https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js</a>
      </div>
	  
	  
	   <div class="row">
	   <form  class="">
   <div class="col-md-6" id="pageForm">
   
		 <label class="checkbox-inline">
         <input id="toggleElement" type="checkbox" name="toggle" onchange="toggleStatus()" /> &nbsp;Message
     
    </label>
	
	
   </div>
    <div class="col-md-6 " id="elementsToOperateOn>
     <label class="checkbox-inline">
     <input type="checkbox" name="participate" id="checktwo" onchange="toggleStatusone()" />&nbsp;Mail
		
      
    </label>
   </div>
   
   <div class="col-md-12 mr-top-20" id="name" style="display:none;">
   <div class="col-md-6 ">
   <div class="form-group">
                  <label for="">Contact Person   </label>
                 <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  	<option value="Name1">Name1 </option>
			<option value="Name2">Name2</option>
			<option value="Name3">Name3</option>
			<option value="Name4">Name1</option>
			<option value="Name5">Name5</option>
			<option value="Name6">Name6</option>
                
                </select>
                </div>
   </div>
     <div class="col-md-6 ">

			  <div class="form-group">
                  <label for="">Contact No.</label>
                <div class="input-group">
                <input type="text" class="form-control" placeholder="9087654321" disabled="">
                    <span class="input-group-btn">
                      <div type="button"  class="btn btn-primary">Change</div>
                    </span>
              </div>
                </div>
			
	 </div>
	 <div class="col-md-12">
	  <div class="form-group">
     <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..."  spellcheck="false"></textarea>
                </div>
	 
	 </div>
	
    
   
   </div>
   
   
   
    <div class="col-md-12 mr-top-20" id="name1" style="display:none;">
   <div class="col-md-6 ">
   <div class="form-group">
                  <label for="">Contact Person   </label>
                 <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  	<option value="Name1">Name1 </option>
			<option value="Name2">Name2</option>
			<option value="Name3">Name3</option>
			<option value="Name4">Name1</option>
			<option value="Name5">Name5</option>
			<option value="Name6">Name6</option>
                
                </select>
                </div>
   </div>
     <div class="col-md-6 ">

			  <div class="form-group">
                  <label for="">Email</label>
                <div class="input-group">
                <input type="text" class="form-control" placeholder="abc@gmail.com" disabled="">
                    <span class="input-group-btn">
                      <div type="button"  class="btn btn-primary">Change</div>
                    </span>
              </div>
                </div>
			
	 </div>
	 <div class="col-md-12">
	  <div class="form-group">
     <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..."  spellcheck="false"></textarea>
                </div>
	 
	 </div>
	
    
   
   </div>
     
</form>
	  
  </div>
          </div>
			  
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

		<div class="modal fade" id="modal-interaction">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Interaction</h4>
              </div>
              <div class="modal-body">
			  <div class="row">
			<div class="col-md-12">
             <div class="form-group">
                  <label for="">Contact Person   </label>
                 <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  	<option value="Name1">Name1 </option>
			<option value="Name2">Name2</option>
			<option value="Name3">Name3</option>
			<option value="Name4">Name1</option>
			<option value="Name5">Name5</option>
			<option value="Name6">Name6</option>
                
                </select>
                </div>
                </div>
				<div class="col-md-6">
                  <div class="form-group">
                    <label>Interaction Date </label>
                    <div class="input-group date">
                      <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                      <input type="text" class="form-control pull-right" id="datepicker">
                    </div>
                    <!-- /.input group --> 
                  </div>
                </div>
				<div class="col-md-6">
                  <div class="form-group">
                    <label>Interaction Time </label>
                    <div class="input-group">
					<div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" class="form-control timepicker">

                    
                  </div>
                    <!-- /.input group --> 
                  </div>
                </div>
            <div class="col-md-12">
			<textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
				
			 </div> 
			 </div>
             
              </div>
			  
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		
		<div class="modal fade" id="modal-enquiry">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Enquiry</h4>
              </div>
              <div class="modal-body">
			<div class="callout callout-info" >
        <h4><i class="fa fa-info"></i> Enquiry Form</h4>
       <a href="#" target="_blank" >https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js</a>
      </div>
	  
	  <div class="row">
	  <div class="form-group col-md-offset-1 col-md-10">
                  <label for="">Contact  </label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask="&quot;mask&quot;: &quot; 99999-99999&quot;" data-mask="">
                </div>
                </div>
				<p class="lead col-md-12 text-center">Or</p>
            
			  <div class="form-group col-md-offset-1 col-md-10">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="Email">
              </div>
                <!-- /.input group -->
              </div>
	  
	  </div>
				 
			 
			
			  </div>
			  
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        lengthChange: false,
    autoWidth : true,
        buttons: [   'csv', 'excel', 'pdf', 'print' ],
    
    
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
    
} );




</script>

<script>


$(document).ready(function () {
    $('#selectall').click(function () {
        $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function () {
        var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
        $('#selectall').prop("checked", check);
    });
});

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
	//Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});


//review


$(document).ready(function() {
 
  handleStatusChanged();
  
});

function handleStatusChanged() {
    $('#toggleElement').on('change', function () {
      toggleStatus();   
    });
}

function toggleStatus() {
    if ($('#toggleElement').is(':checked')) {
        $('#checktwo').attr('disabled', true);
          $('#name').show('#name');
    } else {
      $('#name').hide('#name');
        $('#checktwo').removeAttr('disabled');
    }   
}
function toggleStatusone() {
    if ($('#checktwo').is(':checked')) {
        $('#toggleElement').attr('disabled', true);
         $('#name1').show('#name1');
    } else {
       $('#name1').hide('#name1');
        $('#toggleElement').removeAttr('disabled');
    }   
}

</script>

