<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Finance extends MY_Controller {

	
	public function __construct(){
        parent::__construct();
        //$this->load->model('contact_model', 'contact');
        $this->load->helper('url');
      }

	// For frontdask fee_type
	public function fee_type()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // For frontdask fee_collection
	public function fee_collection()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // For frontdask canceled_fee_receipts
	public function canceled_fee_receipts()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // For frontdask fee_settings
	public function fee_settings()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // For frontdask fee_defaulters
	public function fee_defaulters()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // For frontdask fee_outstanding
	public function fee_outstanding()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

   // For frontdask fee_reports
	public function fee_reports()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// For frontdask transaction_category
	public function transaction_category()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// For frontdask transactions
	public function transactions()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}


}
