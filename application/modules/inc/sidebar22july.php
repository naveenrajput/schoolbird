 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin Name </p>
          <a href="#"><i class="fa fa-circle text-success"></i> Active</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
       <div class="input-group">
          <input type="text"  class="form-control" placeholder="Search..." id="myInput" >
          <span class="input-group-btn">
                <button type="submit" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
		
      </form>
	  
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree" id="myList">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active">
          <a href="<?php echo base_url();?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            
          </a></li>
		  <li class="treeview">
          <a href="#">
            <i class="fa fa-desktop"></i>
            <span>Front Desk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('docs')?>"><i class="fa fa-circle-o"></i> Front Desk Docs</a></li>
            <li><a href="<?php echo base_url('enquiry')?>"><i class="fa fa-circle-o"></i>  Enquiry</a></li>
             <li><a href="<?php echo base_url('Interaction')?>"><i class="fa fa-circle-o"></i> Interaction Reports</a></li>
            <li><a href="<?php echo base_url('admission')?>"><i class="fa fa-circle-o"></i> Student Admission</a></li>
            <li><a href="<?php echo base_url('Employee')?>"><i class="fa fa-circle-o"></i>Employee Application</a></li>
           <li><a href="<?php echo base_url('visitors')?>"><i class="fa fa-circle-o"></i> Visitors</a></li>
            <li><a href="<?php echo base_url('Gate_Pass')?>"><i class="fa fa-circle-o"></i> Gate Pass</a></li>
           
          </ul>
        </li>
		
		
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Student</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('Master_List')?>"><i class="fa fa-circle-o"></i> Master List</a></li>
            <li><a href="<?php echo base_url('Fees')?>"><i class="fa fa-circle-o"></i>  Fees</a></li>
             <li><a href="<?php echo base_url('Attendance')?>"><i class="fa fa-circle-o"></i> Attendance</a></li>
            <li><a href="<?php echo base_url('Remarks')?>"><i class="fa fa-circle-o"></i> Remarks</a></li>
            <li><a href="<?php echo base_url('Leave')?>"><i class="fa fa-circle-o"></i>Leave Requests</a></li>
           <li><a href="<?php echo base_url('Parent')?>"><i class="fa fa-circle-o"></i> Parent Requests</a></li>
            <li><a href="<?php echo base_url('Transfer')?>"><i class="fa fa-circle-o"></i> Session Transfer</a></li>  
			<li><a href="<?php echo base_url('Category')?>"><i class="fa fa-circle-o"></i>Category</a></li>
           <li><a href="<?php echo base_url('No_Dues')?>"><i class="fa fa-circle-o"></i> No Dues</a></li>
            <li><a href="<?php echo base_url('TC')?>"><i class="fa fa-circle-o"></i> TC</a></li>
           
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-list"></i> <span>Academics</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           
			 <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Setup
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url('academics_year')?>"><i class="fa fa-circle"></i> Academic Year</a></li>
                <li><a href="<?php echo base_url('classes')?>"><i class="fa fa-circle"></i> Classes</a></li>
                <li><a href="<?php echo base_url('Classes_Group')?>"><i class="fa fa-circle"></i> Classes Group</a></li>
                <li><a href="<?php echo base_url('subjects')?>"><i class="fa fa-circle"></i> Subjects</a></li>
                <li><a href="<?php echo base_url('Lectures')?>"><i class="fa fa-circle"></i> Lectures</a></li>
                <li><a href="<?php echo base_url('Timetable')?>"><i class="fa fa-circle"></i> Time Table</a></li>
                <li><a href="<?php echo base_url('Substitute')?>"><i class="fa fa-circle"></i> Substitute</a></li>
                <li><a href="<?php echo base_url('Buildings')?>"><i class="fa fa-circle"></i> Buildings</a></li>
                <li><a href="<?php echo base_url('rooms')?>"><i class="fa fa-circle"></i> Rooms</a></li>
                <li><a href="<?php echo base_url('houses')?>"><i class="fa fa-circle"></i> Houses</a></li>

               
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Academics
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
               
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle"></i> Mapping
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('class_teacher')?>"><i class="fa fa-circle"></i> Class-Teacher</a></li>
                    <li><a href="<?php echo base_url('class_room')?>"><i class="fa fa-circle"></i> Class-Room</a></li>
                    <li><a href="<?php echo base_url('Class_Subject')?>"><i class="fa fa-circle"></i> Class-Subject</a></li>
                    <li><a href="<?php echo base_url('Student_Subject')?>"><i class="fa fa-circle"></i> Student-Subject</a></li>
                  </ul>
                </li>
				  <li><a href="<?php echo base_url('lesson_planning')?>"><i class="fa fa-circle"></i> Lesson Planning</a></li>
				  <li><a href="<?php echo base_url('homework')?>"><i class="fa fa-circle"></i> Home Work</a></li>
				  <li><a href="<?php echo base_url('calendar')?>"><i class="fa fa-circle"></i> School Calendar</a></li>
				  <li><a href="<?php echo base_url('workload')?>"><i class="fa fa-circle"></i> Workload Management</a></li>
				  <li><a href="<?php echo base_url('Parents_sms')?>"><i class="fa fa-circle"></i> Messages To Parents</a></li>
				  <li><a href="<?php echo base_url('circular')?>"><i class="fa fa-circle"></i> Circular</a></li>
              </ul>
            </li>
          
          </ul>
        </li>
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Library</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           
			 <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Setup
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url('library')?>"><i class="fa fa-circle"></i> Library</a></li>
                <li><a href="<?php echo base_url('compartment')?>"><i class="fa fa-circle"></i> Compartment</a></li>
                <li><a href="<?php echo base_url('racks')?>"><i class="fa fa-circle"></i> Racks</a></li>
                <li><a href="<?php echo base_url('category')?>"><i class="fa fa-circle"></i> Category</a></li>
                <li><a href="<?php echo base_url('sub_category')?>"><i class="fa fa-circle"></i> Sub-category</a></li>
                <li><a href="<?php echo base_url('genre')?>"><i class="fa fa-circle"></i> Genre</a></li>
                <li><a href="<?php echo base_url('books')?>"><i class="fa fa-circle"></i> Books</a></li>


               
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Library
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
               
               
				  <li><a href="<?php echo base_url('issue_book')?>"><i class="fa fa-circle"></i> Issue Book</a></li>
				  <li><a href="<?php echo base_url('return_book')?>"><i class="fa fa-circle"></i> Return Book</a></li>
				  <li><a href="<?php echo base_url('book_movement')?>"><i class="fa fa-circle"></i> Book Movement Log</a></li>
				  <li><a href="<?php echo base_url('book_defaulters')?>"><i class="fa fa-circle"></i> Book Defaulters</a></li>
				  <li><a href="<?php echo base_url('daily_log')?>"><i class="fa fa-circle"></i> Daily Log</a></li>
              </ul>
            </li>
          
          </ul>
        </li>
		
			<li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Examination</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           
			 <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Setup
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url('exam_type')?>"><i class="fa fa-circle"></i> Exam Type</a></li>
                <li><a href="<?php echo base_url('exam')?>"><i class="fa fa-circle"></i> Exams</a></li>
                <li><a href="<?php echo base_url('grading_scale')?>"><i class="fa fa-circle"></i> Grading Scale</a></li>
                <li><a href="<?php echo base_url('exam_allotment')?>"><i class="fa fa-circle"></i> Exam Allotment</a></li>
                <li><a href="<?php echo base_url('time_table')?>"><i class="fa fa-circle"></i> Time Table</a></li>
                <li><a href="<?php echo base_url('update_marks')?>"><i class="fa fa-circle"></i> Update Marks</a></li>


               
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Examination
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
               
               
				  <li><a href="<?php echo base_url('exam_reports')?>"><i class="fa fa-circle"></i> Exam Reports</a></li>
				  <li><a href="<?php echo base_url('classwise_report')?>"><i class="fa fa-circle"></i> Classwise Report Card</a></li>
				  <li><a href="<?php echo base_url('export_report')?>"><i class="fa fa-circle"></i> Export Final Report</a></li>
				
              </ul>
            </li>
          
          </ul>
        </li>
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Hr-payroll</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('employees')?>"><i class="fa fa-circle-o"></i> Employees</a></li>
            <li><a href="<?php echo base_url('employee_category')?>"><i class="fa fa-circle-o"></i>  Employee Category</a></li>
             <li><a href="<?php echo base_url('designations')?>"><i class="fa fa-circle-o"></i> Designations</a></li>
            <li><a href="<?php echo base_url('departments')?>"><i class="fa fa-circle-o"></i> Departments</a></li>
            <li><a href="<?php echo base_url('employee_attendance')?>"><i class="fa fa-circle-o"></i>Employee Attendance</a></li>
           <li><a href="<?php echo base_url('leave_management')?>"><i class="fa fa-circle-o"></i> Leave Management</a></li>
            <li><a href="<?php echo base_url('leave_type')?>"><i class="fa fa-circle-o"></i> Leave Type</a></li>
            <li><a href="<?php echo base_url('payheads')?>"><i class="fa fa-circle-o"></i> Payheads</a></li>
            <li><a href="<?php echo base_url('payslips')?>"><i class="fa fa-circle-o"></i>Payslips</a></li>
           <li><a href="<?php echo base_url('Employee Access')?>"><i class="fa fa-circle-o"></i> Employee Access Control</a></li>
            <li><a href="<?php echo base_url('dashboard_access')?>"><i class="fa fa-circle-o"></i> Dashboard Access Control</a></li>
           
          </ul>
        </li>
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-bus"></i>
            <span>Transport</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('buses')?>"><i class="fa fa-circle-o"></i> Buses</a></li>
            <li><a href="<?php echo base_url('routes')?>"><i class="fa fa-circle-o"></i>  Routes</a></li>
             <li><a href="<?php echo base_url('stops')?>"><i class="fa fa-circle-o"></i> Stops</a></li>
            <li><a href="<?php echo base_url('reporting')?>"><i class="fa fa-circle-o"></i> Reporting </a></li>
            <li><a href="<?php echo base_url('misreports')?>"><i class="fa fa-circle-o"></i> MIS REPORTS </a></li>
            <li><a href="<?php echo base_url('live_tracking')?>"><i class="fa fa-circle-o"></i>Live Tracking</a></li>
           <li><a href="<?php echo base_url('replay_tracking')?>"><i class="fa fa-circle-o"></i> Replay Tracking</a></li>
            <li><a href="<?php echo base_url('speed_chart')?>"><i class="fa fa-circle-o"></i> Speed Chart</a></li>
            <li><a href="<?php echo base_url('halt_summary')?>"><i class="fa fa-circle-o"></i> Halt Summary </a></li>
            <li><a href="<?php echo base_url('activity_reports')?>"><i class="fa fa-circle-o"></i>Activity Reports</a></li>
           <li><a href="<?php echo base_url('Bus_Route')?>"><i class="fa fa-circle-o"></i> Bus Route Allotment</a></li>
            <li><a href="<?php echo base_url('in_process')?>"><i class="fa fa-circle-o"></i> In Process<br> Student Admission</a></li>
           
          </ul>
        </li>
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-folder-open-o"></i>
            <span>Finance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('fee_type')?>"><i class="fa fa-circle-o"></i>Fee Type</a></li>
            <li><a href="<?php echo base_url('fee_collection')?>"><i class="fa fa-circle-o"></i>Fee Collection</a></li>
             <li><a href="<?php echo base_url('canceled_fee_receipts')?>"><i class="fa fa-circle-o"></i>Canceled Fee Receipts</a></li>
            <li><a href="<?php echo base_url('fee_settings')?>"><i class="fa fa-circle-o"></i>Fee Settings</a></li>
            <li><a href="<?php echo base_url('fee_defaulters')?>"><i class="fa fa-circle-o"></i>Fee Defaulters</a></li>
           <li><a href="<?php echo base_url('fee_outstanding')?>"><i class="fa fa-circle-o"></i> Fee Outstanding</a></li>
            <li><a href="<?php echo base_url('fee_reports')?>"><i class="fa fa-circle-o"></i> Fee Reports</a></li>
           <li><a href="<?php echo base_url('transaction_category')?>"><i class="fa fa-circle-o"></i> Transaction Category</a></li>
            <li><a href="<?php echo base_url('transactions')?>"><i class="fa fa-circle-o"></i> Transactions</a></li>
           
          </ul>
        </li>
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Inventory</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('store')?>"><i class="fa fa-circle-o"></i> Store</a></li>
            <li><a href="<?php echo base_url('supplier_type')?>"><i class="fa fa-circle-o"></i>  Supplier Type</a></li>
             <li><a href="<?php echo base_url('supplier')?>"><i class="fa fa-circle-o"></i> Supplier</a></li>
            <li><a href="<?php echo base_url('item_category')?>"><i class="fa fa-circle-o"></i>Item Category</a></li>
            <li><a href="<?php echo base_url('items')?>"><i class="fa fa-circle-o"></i>Items</a></li>
           <li><a href="<?php echo base_url('issue_item')?>"><i class="fa fa-circle-o"></i> Issue Item</a></li>
            <li><a href="<?php echo base_url('new_stock_request')?>"><i class="fa fa-circle-o"></i> New Stock Request</a></li>
            <li><a href="<?php echo base_url('requests_list')?>"><i class="fa fa-circle-o"></i> Requests List</a></li>
           
          </ul>
        </li>
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-hospital-o"></i>
            <span>Hostel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('hostel_type')?>"><i class="fa fa-circle-o"></i> Hostel Type</a></li>
            <li><a href="<?php echo base_url('hostels')?>"><i class="fa fa-circle-o"></i>  Hostels</a></li>
             <li><a href="<?php echo base_url('room_type')?>"><i class="fa fa-circle-o"></i> Room Type</a></li>
            <li><a href="<?php echo base_url('assigning_rooms')?>"><i class="fa fa-circle-o"></i> Assigning Rooms</a></li>
            <li><a href="<?php echo base_url('facilities')?>"><i class="fa fa-circle-o"></i>Facilities</a></li>
           <li><a href="<?php echo base_url('warden_details')?>"><i class="fa fa-circle-o"></i> Warden Details</a></li>
            <li><a href="<?php echo base_url('Hostel_student_allotment')?>"><i class="fa fa-circle-o"></i> Hostel Student Allotment</a></li>
             <li><a href="<?php echo base_url('mess_type')?>"><i class="fa fa-circle-o"></i> Mess Type</a></li>
            <li><a href="<?php echo base_url('meal_type')?>"><i class="fa fa-circle-o"></i> Meal Type</a></li>
            <li><a href="<?php echo base_url('mess_details')?>"><i class="fa fa-circle-o"></i>Mess Details</a></li>
           <li><a href="<?php echo base_url('mess_student_allotment')?>"><i class="fa fa-circle-o"></i> Mess-student Allotment</a></li>
            <li><a href="<?php echo base_url('mess_attendance')?>"><i class="fa fa-circle-o"></i> Mess Attendance</a></li>
           
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-commenting"></i>
            <span>Communication</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Front Desk Docs</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>  Enquiry</a></li>
             <li><a href="#"><i class="fa fa-circle-o"></i> Interaction Reports</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Student Admission</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Employee Application</a></li>
           <li><a href="#"><i class="fa fa-circle-o"></i> Visitors</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Gate Pass</a></li>
           
          </ul>
        </li>
		 
		
        
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
 