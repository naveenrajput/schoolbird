  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
     <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Mess Details
      </h1>
      <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Hostel</a></li>
        <li class="active">Mess Details</li>
      </ol>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;">

      <a  href="<?php echo base_url ('add-messdetails'); ?>" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp;Mess</a>
    </div>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">


       <div class="box">
        <div class="box-header with-border mr-top-20 text-center">
         <div class="form-group col-md-5">

          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right datepicker" placeholder="Start Date">
          </div>
          <!-- /.input group -->
        </div>
        <!-- /.form group -->

        <!-- Date range -->
        <div class="form-group col-md-5">

          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right datepicker"  placeholder="End Date">
          </div>
          <!-- /.input group -->
        </div>
        <div class="col-md-2" >
         <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>

       </div>



     </div>


     <div class="box-body table-responsive">

       <!-- <table id="example" class="display nowrap" style="width:100%">--->
        <table id="example" class="table table-bordered " >

          <thead>
            <tr>
              <th><input type="checkbox" id="selectall"> All</th>
              <th>S.no</th>
              <th>Hostel</th>
              <th>Mess Type</th>
              <th>Mess Name</th>
              <th>Occupancy</th>
              <th>Capacity</th>
              <th>Notes</th>
              <th>Create Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              if (isset($messdetails)) {
                $i=1; foreach ($messdetails as $mess) { 
            ?>
              <tr>
                <td>
                  <input type="checkbox" class="selectedId" name="selectedId" />
                </td>    
                <td><?php echo $i; ?></td>
                <td><?php echo $mess->hostelname; ?></td>
                <td><?php echo $mess->messtype; ?></td>
                <td><?php echo $mess->messname; ?></td>
                <td>
                  <a href="" class="mess_occupancy" id="<?php echo $mess->id;?>">
                    <?php if($mess->occupancy=='AVAILABLE'){ ?>
                      AVAILABLE
                    <?php } elseif($mess->occupancy=='UNAVAILABLE'){ ?>  UNAVAILABLE
                    <?php } ?>
                  </a> 
                </td>
                <td><?php echo $mess->capacity; ?></td>
                <td><?php echo $mess->notes; ?></td>
                <td><?php echo $mess->createdate; ?></td>
                <td>
                  <ul class="table-icons">
                    <li><a href="<?php echo base_url('edit-messdetails/').$mess->id; ?>" class="table-icon" title="Edit"> 
                      <span class="glyphicon glyphicon-edit display-icon"></span>
                    </a>
                    </li>
                    <li>
                      <a href="" class="table-icon" title="Delete" ><span class="glyphicon glyphicon-trash display-icon close_details" id="<?php echo $mess->id; ?>"></span></a>
                    </li> 
                  </ul>
                </td>
              </tr>
              <?php  $i++; } } ?>
            </tbody>
            <tfoot>
              <tr>
                <th><input type="checkbox" id="selectall"> All</th>
                <th>S.no</th>
                <th>Hostel</th>
                <th>Mess Type</th>
                <th>Mess Name</th>
                <th>Occupancy</th>
                <th>Capacity</th>
                <th>Notes</th>
                <th>Create Date</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>

<script>
  $(document).ready(function() {
    var table = $('#example').DataTable( {
      lengthChange: true,
      autoWidth : true,
     });

    $('.close_details').click(function(){
         var messid= $(this).attr("id");
         var x=confirm("Are you sure to delete record?")
        if (x) {
          $.ajax({  
                    url:"<?php echo base_url('close-messdetails'); ?>",  
                    method:"post",  
                    data:{messid:messid},  
                 }); 
        }else {
          return false;
        } 
    });

    $('.mess_occupancy').click(function(){
      var Mid= $(this).attr("id");
      var x=confirm("Are you sure to change Status?")
        if (x) {
          $.ajax({  
                  url:"<?php echo base_url('mess-occupancy'); ?>",
                  method:"post",  
                  data:{Mid:Mid},
                  success:function(data){
                    alert(data);
                  }
              });
        }else {
          return false;
        }
    });
  });
</script>

