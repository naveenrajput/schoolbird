<?php $i=1; foreach ($data as $value) { ?>
	<tr>
		<td><?php echo $i; ?></td>
		<td><?php 
				foreach ($students as $row) {
				if ($row->id == $value->stdid) {
				echo $row->name; } } 
			?>
	    </td>
		<?php foreach ($messdetails as $row) {
				if ($row->id == $value->messid) { ?>
				<td><?php echo $row->messname; ?></td>
				<td><?php echo $row->capacity; ?></td>
		<?php } } ?>
		<td><?php echo $value->dt_allotment; ?></td>
		<td><button type="button" class="btn btn-block btn-info btn-sm release_mess" id="<?php echo $value->id; ?>" >Release</button></td>
	</tr>
<?php $i++; } ?>
<script>
	$(document).ready(function(){
		$('.release_mess').click(function(){
			var rel_id= $(this).attr("id");
			var x=confirm("Are you sure to Release student?")
			if (x) {
				$.ajax({  
					url:"<?php echo base_url('releasestatus'); ?>",  
					method:"post",  
					data:{rel_id:rel_id},  
					success:function(data){
					}  
				}); 
			}else {
				return false;
			} 
		});
	});
</script>