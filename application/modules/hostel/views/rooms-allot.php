<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class=" ">
    <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        ROOM Allot
      </h1>
      <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Hostel</a></li>
        <li class="active">Room Allot</li>
      </ol>
    </div>
  </section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
     <div class="box">
      <div class="box-header with-border mr-top-20 text-center">
       <div class="form-group col-md-5">
        <select name="hosteltype" id="htype_id" class="form-control" required>
          <option value="" selected="selected">Hostel Type</option>
          <option value="BOYS">BOYS</option>
          <option value="GIRLS">GIRLS</option>
        </select>
      </div>
   
      <div class="form-group col-md-5">

        <select name="hostelname" id="h_name" class="form-control" required>
          <option value="" selected="selected">Select Hostel</option>
          <?php foreach ($hostels as $hostels) { ?>
            <option value="<?php echo $hostels->name;?>"><?php echo $hostels->name;?></option>
          <?php } ?>
        </select>
      </div>
      <div class="col-md-2" >
        <button type="submit" id="hostel_info" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
      </div>
    </div>
    <div class="box-body">
      <div class="col-sm-offset-4 col-md-8 ">
        <div id="roomdetails">
        </div>
      </div>
    </div>   
    <!-- /.box-body -->
  </div>
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<div class="modal fade" id="Mymodal_view">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Room Allot</h4>
        </div>
        <div class="modal-body">

          <form method="post" data-toggle="validator" role="form">
            <!-- text input -->
            <div class="form-group" id="roomid"></div>
            <div class="form-group">
              <label>Student ID /Name</label>
              <select name="stdid" id="stid" class="form-control select2" style="width:100%" required>
                <option selected="selected" value="">Select</option>
                <?php foreach ($students as $stud) { ?>
                  <option value="<?php echo $stud->id; ?>"><?php echo $stud->name; ?></option>
                <?php } ?>
              </select>
            </div>
            </div>			  
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary allot_info">Allot</button>
            </div>
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <script>
    $(document).ready(function(){
      $('#htype_id').change(function(){
        var h_id=$('#htype_id').val();
        $.ajax({
          url:'<?php echo base_url('get-hname');?>',
          data:{h_id:h_id},
          type:'post',
          success:function(response){
            $('#h_name').html(response);
          }
        });
      });

      $('#hostel_info').click(function(){
        var type=$('#htype_id').val();
        var name=$('#h_name').val();
        $.ajax({
          url:'<?php echo base_url('get-rooms');?>',
          data:{type:type,name:name},
          type:'post',
          success:function(response){
            $('#roomdetails').html(response);
          }
        });
      });

      $('.allot_info').click(function(){
        var rmid=$('#rmid').val();
        var rmno=$('#rmno').val();
        var stid=$('#stid').val();
        $.ajax({
          url:'<?php echo base_url('newroom-allot');?>',
          data:{rmid:rmid,stid:stid,rmno:rmno},
          type:'post',
          success:function(response){
            $('#Mymodal_view').modal('toggle');
          }
        });
      });
    });  
  </script>
  <!-- <script>
    $("#smt").click(function(){
      var fm =$("#myForm").serialize();
        $.ajax({
          url:'<?php // base_url(''); ?>',
          data:fm,
          type:"POST",
          success: function(response){
            alert(response);
          }
        });
      });
  </script> -->
<script type="text/javascript">
  $('.txtChk').click(function(){ 
      var id = $(this).attr("id");
      $.ajax({  
          url:"<?php echo base_url('openallotrooms'); ?>",  
          method:"post",  
          data:{id:id},  
          success:function(data){
            $('#roomid').html(data);
            $('#Mymodal_view').modal({
               backdrop: 'static',
              keyboard: false 
            }); 
          }                             
      });
    }); 
</script>