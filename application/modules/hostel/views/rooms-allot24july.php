<style>
.box1 {
  display: flex;
  align-items: center;
  justify-content: center;
  background: #fff;
 
margin-top:10px;
  padding: 10px;
  width: 100%;
  min-height: 50px;
  border: 2px #ccc solid;
  color: #333;
}
.box-1disable {
     opacity: 0.3;
	  border: 2px #ccc solid;
}
</style> 


 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
     <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        ROOM Allot
      </h1>
      <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Hostel</a></li>
        <li class="active">Room Allot</li>
      </ol>
    </div>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">


       <div class="box">
        <div class="box-header with-border mr-top-20 text-center">
         <div class="form-group col-md-5">
            <label>Hostel Type</label>
            <select name="hosteltype" id="htype_id" class="form-control" required>
              <option value="" selected="selected">Select Type</option>
              <option value="BOYS">BOYS</option>
              <option value="GIRLS">GIRLS</option>
            </select>
          <!-- /.input group -->
        </div>
        <!-- /.form group -->

        <!-- Date range -->
        <div class="form-group col-md-5">
            <label>Hostel</label>
            <select name="hostelname" id="h_name" class="form-control" required>
              <option value="" selected="selected">Select Hostel</option>
              <?php foreach ($hostels as $hostels) { ?>
                <option value="<?php echo $hostels->name; ?>"><?php echo $hostels->name;?></option>
              <?php } ?>
            </select>
          <!-- /.input group -->
        </div>
        <div class="form-group col-md-2" >
         <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>

       </div>



     </div>

	<div >
	
	 <div class="box-body ">
	 
	 <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
      <a href="#"  data-toggle="modal" data-target="#modal-room">
	  <div class="box1">
         <p>Room-1</p>
	
      </div>
      
      </a>
    </div> <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
      <a href="">
      <div class="box1">
         <p>Room-2</p>
       
      </div>
      </a>
    </div> <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
      <a href="">
      <div class="box1">
         <p>Room-3</p>
       
      </div>
      </a>
    </div> <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
      <a href="">
      <div class="box1">
         <p>Room-4</p>
       
      </div>
      </a>
    </div> <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
      <a href="">
      <div class="box1 box-1disable">
         <p>Room-5</p>
		 
		<!-- <div >
		 <span>1</span>
		 <span>2</span>
		
        </div>-->
      </div>
      </a>
    </div>
  
    
    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
      <a href="">
      <div class="box1">
         <p>Room-6</p>
       
      </div>
      </a>
    </div>
    
	   
	
       <!-- <table id="example" class="display nowrap" style="width:100%">--->
       
		
		</div>
	
	</div>
    
        <!-- /.box-body -->
      </div>

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<div class="modal fade" id="modal-room">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Room Allot</h4>
              </div>
              <div class="modal-body">
              
			  <form role="form">
                <!-- text input -->
                <div class="form-group">
                  <label>Student ID/Name</label>
                  <input type="text" class="form-control" placeholder="Enter ...">
                </div>
                

              </form>
			  
			  
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Allot</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<script>
  $(document).ready(function(){
    $('#htype_id').change(function(){
      var h_id=$('#htype_id').val();
      $.ajax({
        url:'<?php echo base_url('get-hname');?>',
        data:{h_id:h_id},
        type:'post',
        success:function(response){
          $('#h_name').html(response);
        }
      });
    });

    $('.hostel_info').change(function(){
      var type=$('#htype_id').val();
      var name=$('#h_name').val();
        $.ajax({
          url:'<?php echo base_url('get-hostel');?>',
          data:{type:type,name:name},
          type:'post',
          success:function(response){
            console.log(response);
            $('#student_id').html(response);
          }
        });
      });
  });    
</script>