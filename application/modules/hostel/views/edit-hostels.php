<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Edit Hostel Form
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Edit Hostel</li>
  </ol>
</section>
<!-- Edit Hostels content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
       <div class="box-header with-border">
        <h3 class="box-title">Edit Hostel</h3>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form method="post" action="<?php echo base_url('update-hostel'); ?>" data-toggle="validator" role="form">
        <?php 
          if (isset($hostels)){
          foreach ($hostels as $hostels){ 
        ?>
        <div class="box-body">
          <div class="row">
          <input type="hidden" name="updateid" value="<?php echo $hostels->id;?>">
            <div class="col-md-6">
              <label>Hostel Type</label>
              <select name="hosteltypeid" class="form-control select2" required>
                <option value="<?php echo $hostels->hosteltypeid;?>">
                  <?php foreach($hostype as $row ){
                        if($row->id==$hostels->hosteltypeid){
                        echo $row->name;
                  } } ?>          
                </option>
                <?php foreach ($hostype as $hostype) { ?>
                  <option value="<?php echo $hostype->id; ?>"><?php echo $hostype->name;?></option>
                <?php } ?>
              </select>
            </div>
           <div class="col-md-6">
            <div class="form-group">
             <label>Hostel Name</label>
             <input type="text" name="name" id="h_name" class="form-control" value="<?php echo $hostels->name; ?>" required>
           </div>
         </div>
         <div class="col-md-6">
          <div class="form-group">
            <label>Address</label>
            <input type="text" name="address" class="form-control" value="<?php echo $hostels->address; ?>" required>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Contact</label>
            <div class="input-group">
              <div class="input-group-addon"> <i class="fa fa-phone"></i></div>
              <input type="text" name="phone" class="form-control" data-inputmask='"mask":"99999-99999"' value="<?php echo $hostels->phone;?>" data-mask required>
            </div>
          </div> 
        </div>
        <div class="col-md-6">
          <label>Warden</label>
          <select name="warden" class="form-control select2" required>
            <option value="<?php echo $hostels->warden; ?>" selected>
              <?php foreach($emp as $row ){
                    if($row->id==$hostels->warden){
                    echo $row->name;
              } } ?>
            </option>
            <?php foreach ($emp as $emp) { ?>
              <option value="<?php echo $emp->id; ?>"><?php echo $emp->name;?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col-md-12">
         <div class="form-group">
          <label>Notes</label>
          <textarea class="form-control" name="notes" rows="3" placeholder="Counselor’s Remarks " required><?php echo $hostels->notes; ?></textarea>
        </div>
      </div>

    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>	
  </div>
  <?php } } ?>
</form>
</div>

</div>
</div>
</section>

</div>