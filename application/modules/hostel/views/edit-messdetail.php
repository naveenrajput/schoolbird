<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Mess Detail
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Add Mess Detail</li>
  </ol>
</section>
<!-- Add Mess Detail content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
       <div class="box-header with-border">
        <h3 class="box-title">Add Mess Detail</h3>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form method="post" action="<?php echo base_url('update-messdetails'); ?>" data-toggle="validator" role="form">
        <?php if (isset($messdetail)) {
              foreach ($messdetail as $mess) {
        ?>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <input type="hidden" name="updateid" value="<?php echo $mess->id;?>">
              <div class="form-group">
                <label>Hostel Type</label>
                <select name="hosteltype" id="htype_id" class="form-control" required>
                  <option value="<?php echo $mess->hosteltype; ?>" selected="selected"><?php echo $mess->hosteltype;?></option>
                  <option value="BOYS">BOYS</option>
                  <option value="GIRLS">GIRLS</option>
                </select>
              </div>
            </div>  
            <div class="col-md-6">
              <div class="form-group">
                <label>Hostel</label>
                <select name="hostelname" id="h_name" class="form-control" required>
                  <option value="<?php echo $mess->hostelname;?>" selected="selected"><?php echo $mess->hostelname;?></option>
                  <?php foreach ($hostels as $hostels) { ?>
                    <option value="<?php echo $hostels->name; ?>"><?php echo $hostels->name;?></option>
                  <?php } ?>
                </select>
              </div>
            </div>  
          <div class="col-md-6">
            <div class="form-group">
              <label>Mess Type</label>
              <select name="messtype" class="form-control" required>
                <option value="<?php echo $mess->messtype;?>" selected="selected"><?php echo $mess->messtype;?></option>
                <option value="type1">Type1</option>
                <option value="type2">Type2</option>              
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Mess Name</label>
              <input type="text" name="messname" class="form-control" value="<?php echo $mess->messname;?>" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Occupancy</label>
              <select name="occupancy" class="form-control" required>
                <option selected="selected" value="<?php echo $mess->occupancy;?>"><?php echo $mess->occupancy;?></option>
                <option value="AVAILABLE">AVAILABLE</option>
                <option value="UNAVAILABLE">UNAVAILABLE</option>              
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Capacity</label>
              <input type="text" name="capacity" class="form-control" value="<?php echo $mess->capacity;?>" required>
            </div>
          </div>
          <div class="col-md-12">
           <div class="form-group">
            <label>Notes</label><?php echo form_error('notes'); ?>
            <textarea class="form-control" name="notes" rows="3" placeholder="Counselor’s Remarks " required><?php echo $mess->notes;?></textarea>
          </div>
        </div>
      </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>	
    </div>
    <?php } } ?>
  </form>
</div>

</div>
</div>
</section>
</div>
<script>
  $(document).ready(function(){
    $('#htype_id').change(function(){
      var h_id=$('#htype_id').val();
      $.ajax({
        url:'<?php echo base_url('get-hname');?>',
        data:{h_id:h_id},
        type:'post',
        success:function(response){
          $('#h_name').html(response);
        }
      });
    });
  });    
</script>