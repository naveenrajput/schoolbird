<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Edit Room Type Form
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Edit Room Type</li>
  </ol>
</section>
<!-- Edit Room Type content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
       <div class="box-header with-border">
        <h3 class="box-title">Edit Room Type</h3>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form method="post" action="<?php echo base_url('update-roomtype'); ?>" data-toggle="validator" role="form">
        <?php 
          if (isset($roomtype)){
          foreach ($roomtype as $roomtype){ 
        ?>
        <div class="box-body">
          <div class="row">
          <input type="hidden" name="updateid" value="<?php echo $roomtype->id;?>">
           <div class="col-md-6">
            <div class="form-group">
             <label>Name</label>
             <input type="text" name="name" class="form-control" value="<?php echo $roomtype->name; ?>" required>
           </div>
         </div>
         <div class="col-md-6">
          <div class="form-group">
            <label>Code</label>
            <input type="text" name="code" class="form-control" value="<?php echo $roomtype->code; ?>" required>
          </div>
        </div>
        <div class="col-md-12">
         <div class="form-group">
          <label>Notes</label>
          <textarea class="form-control" name="notes" rows="3" required><?php echo $roomtype->notes; ?></textarea>
        </div>
      </div>

    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>  
  </div>
  <?php } } ?>
</form>
</div>

</div>
</div>
</section>

</div>
