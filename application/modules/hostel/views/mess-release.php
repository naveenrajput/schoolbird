 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class=" ">
   <div class="col-md-6 col-xs-12 col-sm-8 content-header">
    <h1 class="">
      Mess Release
    </h1>
    <ol class="breadcrumb" style="background:none;">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li><a href="#">Hostel</a></li>
      <li class="active">Mess Release</li>
    </ol>
  </div>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">

     <div class="box">
      <div class="box-header with-border mr-top-20">
      <div class="hostel_info">
        <div class="form-group col-md-3">
          <select name="hosteltype" id="htype_id" class="form-control" required>
            <option value="" selected="selected">Hostel Type</option>
            <option value="BOYS">BOYS</option>
            <option value="GIRLS">GIRLS</option>
          </select>
        </div>
        <div class="form-group col-md-3">
          <select name="hostelname" id="h_name" class="form-control" required>
            <option value="" selected="selected">Select Hostel</option>
            <?php foreach ($hostels as $hostels) { ?>
              <option value="<?php echo $hostels->name;?>"><?php echo $hostels->name;?></option>
            <?php } ?>
          </select>
        </div>
      </div>
       <div class="form-group  col-md-4"> 
        <select id="mess_id" class="form-control select2"   style="width: 100%;" required>
          <option value="" selected="selected">Select Mess</option>
          <?php foreach ($mess as $messno) { ?>
            <option value="<?php echo $messno->id; ?>"><?php echo $messno->messname; ?></option>
          <?php } ?>
        </select>
      </div>
      <!-- /.form group -->
      <!-- Date range -->
      <div class="col-md-2" >
        <button type="submit" id="rmck_id" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
      </div>
      <div class="clearfix"></div>

    </div>
    <div class="box-body ">

       <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">#</th>
            <th>Name</th>
            <th>Mess id</th>
            <th>Capacity</th>
            <th>Allotment Date</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody id="mess_info">
        </tbody>
      </table>

    </div>
  <!-- /.box-body -->
</div>

</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->

</div>
<script>
  $(document).ready(function(){    
    $('#htype_id').change(function(){
        var h_id=$('#htype_id').val();
        $.ajax({
          url:'<?php echo base_url('get-hname');?>',
          data:{h_id:h_id},
          type:'post',
          success:function(response){
            $('#h_name').html(response);
          }
        });
    });

    $('.hostel_info').click(function(){
        var type=$('#htype_id').val();
        var name=$('#h_name').val();
        $.ajax({
          url:'<?php echo base_url('release-messno');?>',
          data:{type:type,name:name},
          type:'post',
          success:function(response){
            $('#mess_id').html(response);
          }
        });
    });

    $('#rmck_id').click(function(){
      var ms_id=$('#mess_id').val();
      $.ajax({
        url:'<?php echo base_url('mess-info');?>',
        data:{ms_id:ms_id},
        type:'post',
        success:function(response){
          $('#mess_info').html(response);
        }
      });
    });
  });
</script>