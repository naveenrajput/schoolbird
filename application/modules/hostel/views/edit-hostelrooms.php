<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Hostel Rooms Form
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Edit Hostel Rooms</li>
  </ol>
</section>
<!-- Edit Hostel Rooms content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
       <div class="box-header with-border">
        <h3 class="box-title">Edit Hostel Rooms</h3>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form method="post" action="<?php echo base_url('update-rooms'); ?>" data-toggle="validator" role="form">
        <?php 
          if (isset($h_rooms)){
            foreach ($h_rooms as $h_rooms){ 
              $facility= explode(",",$h_rooms->facility);
        ?>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <input type="hidden" name="">
              <label>Hostel</label>
              <select name="hostelid" class="form-control select2" required>
                <option selected="selected" value="<?php echo $h_rooms->hostelid;?>">
                  <?php foreach($hostels as $row ){
                    if($row->id==$h_rooms->hostelid){
                    echo $row->name;
                  } } ?>    
                </option>
                <?php foreach ($hostels as $hostels) { ?>
                  <option value="<?php echo $hostels->id; ?>"><?php echo $hostels->name;?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-6">
              <label>Room Type</label>
              <select name="roomtypeid" class="form-control select2" required>
                <option value="<?php echo $h_rooms->roomtypeid; ?>" selected="selected">
                  <?php foreach($roomtype as $row ){
                    if($row->id==$h_rooms->roomtypeid){
                    echo $row->name;
                  } } ?>
                </option>
                <?php foreach ($roomtype as $roomtype) { ?>
                  <option value="<?php echo $roomtype->id; ?>"><?php echo $roomtype->name;?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-6">
              <div class="form-group">
               <label>Room</label>
               <input type="text" name="name" class="form-control" value="<?php echo $h_rooms->name; ?>" required>
              </div>
            </div>
           <div class="col-md-6">
            <div class="form-group">
              <label>Code</label>
              <input type="text" name="code" class="form-control" value="<?php echo $h_rooms->code; ?>" required>
            </div>
          </div>



          <div class="col-md-12 "> <p class="lead">Room Details </p></div>

          <div class="col-md-12 table-responsive">
            <table class="table table-bordered" id="test-table"  style="margin-bottom:10px;">
              <tbody id="test-body">
                <tr  id="row0">
                  <th>FACILITY</th>
                  <th>QUANTITY</th>
                </tr>
                <?php 
                  if (isset($facility)){
                    
                    $quantity= explode(",",$h_rooms->quantity);
                ?>
                <tr>
                
                  <td class="col-md-6">
                    <?php foreach ($facility as $facility ){?>
                    <div class="form-group">
                      <input type="text" name="facility[]" class="form-control" value="<?php echo $facility; ?>" >
                    </div>
                    <?php } ?>
                  </td>
                  <td class="col-md-6">
                    <?php foreach ($quantity as $quant ){ ?>
                    <div class="form-group col-md-8">

                      <input type="text" name="quantity[]" class="form-control " value="<?php echo $quant; ?>" >
                    </div><div class="form-group col-md-4">
                      <button  class='delete-row btn btn-primary btn-sm' type='button' >Delete</button>

                    </div><?php }?>
                 
                  </td>
                 
                  
                  
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <div class="form-group">
              <button id='add-row-fc' class='btn btn-primary btn-sm' type='button' value='Add'> Add </button>             
            </div>
          </div>
          
          <div class="col-md-12">
           <div class="form-group">
            <label>Notes</label>
            <textarea class="form-control" name="notes" rows="3" placeholder="Counselor’s Remarks " required><?php echo $h_rooms->notes; ?></textarea>
          </div>
        </div>

      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>	
    </div>
<?php } } ?>
  </form>
</div>

</div>
</div>
</section>

</div>