<?php foreach ($data as $key => $vacantRm) {
  $id=$vacantRm->id;
  $no=$vacantRm->messname;
  $cap=$vacantRm->capacity;
?>
    <a href="#" class="txtChk" id="<?php echo $id?>">
      <div class="content-room bg-light-blue">
          <h4><?php echo $no; ?></h4>
          <small class="label">0/<?php echo $cap; ?></small>
      </div>
    </a>   
<?php } ?>
<?php foreach ($Messalloted as $allot){
  $id=$allot->msid;
  $Rno= $allot->messname;
  $c= $allot->count;
  $cap= $allot->capacity;
?>
  <!--<?php //if ($c !=0) { ?>-->
  <?php if ($cap > $c) { ?>
    <a href="#" class="txtChk" id="<?php echo $id?>">
      <div class="content-room bg-aqua">
          <h4><?php echo $Rno; ?></h4>
          <small class="label"><?php echo $c."/".$cap; ?></small>
      </div>
    </a>
  <?php } elseif($cap <= $c){ ?>
    <div class="content-room bg-red">
      <h4><?php echo $Rno; ?></h4>
      <small class="label"><?php echo $c; ?>/<?php echo $cap; ?></small>
    </div>
  <?php } ?>
<?php } ?>

<script type="text/javascript">
  $('.txtChk').click(function(){ 
      var id = $(this).attr("id");
      $.ajax({  
            url:"<?php echo base_url('mallot-popup'); ?>",
            method:"post",  
            data:{id:id},  
            success:function(data){
              $('#roomid').html(data);
              $('#Mymodal_view').modal({
                 backdrop: 'static',
                 keyboard: false 
              }); 
            }                             
      });
    }); 
</script>