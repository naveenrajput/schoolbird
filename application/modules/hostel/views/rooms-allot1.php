<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Hostel Rooms Form
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Add Hostel Rooms</li>
  </ol>
</section>
<!-- Add Hostel Rooms content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
       <div class="box-header with-border">
        <h3 class="box-title">Add Hostel Rooms</h3>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form method="post" action="<?php echo base_url('insert-rooms'); ?>" data-toggle="validator" role="form">
        <div class="box-body">


          <div class="row">
            <div class="hostel_info">
            <div class="col-md-4">
              <div class="form-group">
                <label>Hostel Type</label>
                <select name="hosteltype" id="htype_id" class="form-control" required>
                  <option value="" selected="selected">Select Type</option>
                  <option value="BOYS">BOYS</option>
                  <option value="GIRLS">GIRLS</option>
                </select>
              </div>
            </div>  
            <div class="col-md-4">
              <div class="form-group">
                <label>Hostel</label>
                <select name="hostelname" id="h_name" class="form-control" required>
                  <option value="" selected="selected">Select Hostel</option>
                  <?php foreach ($hostels as $hostels) { ?>
                    <option value="<?php echo $hostels->name; ?>"><?php echo $hostels->name;?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>	
    </div>

  </form>
</div>

</div>
</div>
</section>
</div>
<script>
  $(document).ready(function(){
    $('#htype_id').change(function(){
      var h_id=$('#htype_id').val();
      $.ajax({
        url:'<?php echo base_url('get-hname');?>',
        data:{h_id:h_id},
        type:'post',
        success:function(response){
          $('#h_name').html(response);
        }
      });
    });

    $('.hostel_info').change(function(){
      var type=$('#htype_id').val();
      var name=$('#h_name').val();

        alert(type);
        alert(name);die();
        $.ajax({
          url:'<?php echo base_url('get-hostel');?>',
          data:{type:type,name:name},
          type:'post',
          success:function(response){
            console.log(response);
            $('#student_id').html(response);
          }
        });
      });
  });    
</script>