<?php foreach ($data as $key => $vacantRm) {
  $id=$vacantRm->id;
  $no=$vacantRm->room_no;
  $cap=$vacantRm->capacity;
?>
    <a href="#" class="txtChk" id="<?php echo $id?>">
      <input type="hidden" class="Txt" id="<?php echo $no;?>" value="<?php echo $no; ?>">
      <div class="content-room bg-light-blue">
          <h4><?php echo $no; ?></h4>
          <small class="label">0/<?php echo $cap; ?></small>
      </div>
    </a>   
<?php } ?>
<?php foreach ($allotedRm as $allot) {
  $id=$allot->rmid;
  $Rno= $allot->room_no;
  $c= $allot->count;
  $cap= $allot->capacity;
?>
<?php if ($cap > $c) { ?>
    <a href="#" class="txtChk" id="<?php echo $id?>">
      <input type="hidden" class="Txt" id="<?php echo $Rno;?>" value="<?php echo $Rno; ?>">
      <div class="content-room bg-aqua">
          <h4><?php echo $Rno; ?></h4>
          <small class="label"><?php echo $c."/".$cap; ?></small>
      </div>
    </a>
<?php } elseif($cap <= $c){ ?>
  <div class="content-room bg-red">
    <h4><?php echo $Rno; ?></h4>
    <small class="label"><?php echo $c; ?>/<?php echo $cap; ?></small>
  </div>
<?php } ?>
<?php } ?>

<script type="text/javascript">
  $('.txtChk').click(function(){ 
      var id = $(this).attr("id");
      var no = $('.Txt').attr("id");
      var Rno = $('.Txt').val();
      $.ajax({  
            url:"<?php echo base_url('openallotrooms'); ?>",
            method:"post",
            data:{id:id,no:no,Rno:Rno},  
            success:function(data){
              $('#roomid').html(data);
              $('#Mymodal_view').modal({
                 backdrop: 'static',
                 keyboard: false 
              }); 
            }                             
      });
    }); 
</script>

<!-- 
    <a href="#"  data-toggle="modal" data-target="#modal-room">
        <div class="content-room bg-aqua">
          <h4>Room-1</h4>
      <small class="label">3/4</small>
         
        </div>
    </a>
        <div class="content-room bg-red">
          <h4>Room-1</h4>
          <small class="label">3/4</small>
        </div>
    <a href="#"  data-toggle="modal" data-target="#modal-room">
        <div class="content-room bg-light-blue">
          <h4>Room-1</h4>
      <small class="label">3/4</small>
         
        </div>
    </a>
 <a href="#"  data-toggle="modal" data-target="#modal-room">
        <div class="content-room bg-olive">
          <h4>Room-1</h4>
      <small class="label">3/4</small>
         
        </div>
    </a>-->