<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Add Hostel Form
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Add Hostel</li>
  </ol>
</section>
<!-- Add Hostels content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
       <div class="box-header with-border">
        <h3 class="box-title">Add Hostel</h3>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form method="post" action="<?php echo base_url('insert-hostel'); ?>" data-toggle="validator" role="form">
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <label>Hostel Type</label><?php echo form_error('hosteltypeid');?>
              <select name="hosteltypeid" class="form-control select2" id="htype_id" required>
                <option value="<?php echo set_value('hosteltypeid'); ?>" selected="selected">Select Type</option>
                <?php foreach ($hostype as $hostype) { ?>
                  <option value="<?php echo $hostype->id; ?>"><?php echo $hostype->name;?></option>
                <?php } ?>
              </select>
            </div>
           <div class="col-md-6">
            <div class="form-group">
             <label>Hostel Name</label><?php echo form_error('name');?>
             <input type="text" name="name" id="h_name" class="form-control" value="<?php echo set_value('name'); ?>" required>
           </div>
         </div>
        <div class="col-md-6">
          <label>Warden</label><?php echo form_error('warden');?>
          <select name="warden" class="form-control select2" required>
            <option value="<?php echo set_value('warden'); ?>" selected="selected">Select Employee</option>
            <?php foreach ($emp as $emp) { ?>
              <option value="<?php echo $emp->id; ?>"><?php echo $emp->name;?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Contact</label>
            <div class="input-group">
              <div class="input-group-addon"> <i class="fa fa-phone"></i></div>
              <input type="text" name="phone" class="form-control" data-inputmask='"mask":"99999-99999"' data-mask required>
            </div>
          </div> 
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Address</label><?php echo form_error('address'); ?>
            <textarea class="form-control" name="address" rows="3" placeholder="Address" required><?php echo set_value('address'); ?></textarea>
          </div>
        </div>
        <div class="col-md-6">
         <div class="form-group">
          <label>Notes</label><?php echo form_error('notes'); ?>
          <textarea class="form-control" name="notes" rows="3" placeholder="Counselor’s Remarks " required><?php echo set_value('notes'); ?></textarea>
        </div>
      </div>

    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>	
  </div>

</form>
</div>

</div>
</div>
</section>

</div>
<!-- <script>
  $(document).ready(function(){
    $('#htype_id').change(function(){
      var h_id=$('#htype_id').val();
      $.ajax({
        url:'<?php //echo base_url('get-hname');?>',
        data:{h_id:h_id},
        type:'post',
        success:function(response){
          alert(response);
          $('#h_name').html(response);
        }
      });
    });
  });
</script> -->