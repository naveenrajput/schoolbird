<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Hostel Facility Form
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Add Hostel Facility</li>
  </ol>
</section>
<!-- Add Hostel Facility content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
       <div class="box-header with-border">
        <h3 class="box-title">Add Hostel Facility</h3>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form method="post" action="<?php echo base_url('insert-facility'); ?>" data-toggle="validator" role="form">
        <div class="box-body">
          <div class="row">
           <div class="col-md-6">
            <div class="form-group">
             <label>Name</label><?php echo form_error('name');?>
             <input type="text" name="name" class="form-control" value="<?php echo set_value('name'); ?>" required>
           </div>
         </div>
         <div class="col-md-6">
          <div class="form-group">
            <label>Code</label><?php echo form_error('code'); ?>
            <input type="text" name="code" class="form-control" value="<?php echo set_value('code'); ?>" required>
          </div>
        </div>
        <div class="col-md-12">
         <div class="form-group">
          <label>Notes</label><?php echo form_error('notes'); ?>
          <textarea class="form-control" name="notes" rows="3" placeholder="Counselor’s Remarks " required><?php echo set_value('notes'); ?></textarea>
        </div>
      </div>

    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>	
  </div>

</form>
</div>

</div>
</div>
</section>

</div>
