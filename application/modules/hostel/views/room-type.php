  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
     <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        ROOM TYPE
      </h1>
      <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Hostel</a></li>
        <li class="active">Room Type</li>
      </ol>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;">

      <a  href="<?php echo base_url ('add-roomtype'); ?>" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp;Room Type</a>

    </div>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">


       <div class="box">
        <div class="box-header with-border mr-top-20 text-center">
         <div class="form-group col-md-5">

          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right datepicker" placeholder="Start Date">
          </div>
          <!-- /.input group -->
        </div>
        <!-- /.form group -->

        <!-- Date range -->
        <div class="form-group col-md-5">

          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right datepicker"  placeholder="End Date">
          </div>
          <!-- /.input group -->
        </div>
        <div class="col-md-2" >
         <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>

       </div>



     </div>


     <div class="box-body table-responsive">

       <!-- <table id="example" class="display nowrap" style="width:100%">--->
        <table id="example" class="table table-bordered " >

          <thead>
            <tr>
              <th><input type="checkbox" id="selectall"> All</th>
              <th>S.no</th>
              <th>Name</th>
              <th>Codes</th>
              <th>Notes</th>
              <th>Create Date</th>
              <th>Updated By</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              if (isset($roomtype)) {
                $i=1; foreach ($roomtype as $roomtype) { 
            ?>
              <tr>
                <td>
                  <input type="checkbox" class="selectedId" name="selectedId" />
                </td>     
                <td><?php echo $i; ?></td>
                <td><?php echo $roomtype->name; ?></td>
                <td><?php echo $roomtype->code; ?></td>
                <td><?php echo $roomtype->notes; ?></td>
                <td><?php echo $roomtype->createdate; ?></td>
                <td><?php echo $roomtype->updatedby; ?></td>
                <td>
                  <ul class="table-icons">
                    <li><a href="<?php echo base_url('edit-roomtype/').$roomtype->id; ?>" class="table-icon" title="Edit"> 

                      <span class="glyphicon glyphicon-edit display-icon"></span>
                    </a>
                    </li>
                    <li>
                      <a href="" class="table-icon" title="Delete" ><span class="glyphicon glyphicon-trash display-icon delete-rtype" id="<?php echo $roomtype->id;?>"></span></a>
                    </li>
                  </ul>
                </td>
              </tr>
              <?php  $i++; } } ?>
            </tbody>
            <tfoot>
              <tr>
                <th><input type="checkbox" id="selectall"> All</th>
                <th>S.no</th>
                <th>Name</th>
                <th>Codes</th>
                <th>Notes</th>
                <th>Create Date</th>
                <th>Updated By</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>

<script>
  $(document).ready(function() {
    var table = $('#example').DataTable( {
      lengthChange: true,
      autoWidth : true,
       // buttons: [   'csv', 'excel', 'pdf', 'print' ],
     } );

    /* table.buttons().container()
    .appendTo( '#example_wrapper .col-sm-6:eq(0)' ); */
    // alag hu mai
    $('#selectall').click(function () {
      $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function () {
      var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
      $('#selectall').prop("checked", check);
    });


    $('.delete-rtype').click(function(){
         var r_type= $(this).attr("id");
         var x=confirm("Are you sure to delete record?")
        if (x) {
          $.ajax({  
                    url:"<?php echo base_url('close-roomtype'); ?>",  
                    method:"post",  
                    data:{r_type:r_type},  
                    success:function(data){ 
                           location.reload();
                    }  
                 }); 
        }else {
          return false;
        } 
    });
  });
</script>