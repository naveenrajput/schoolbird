<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Edit Hostel Facility Form
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Edit Hostel Facility</li>
  </ol>
</section>
<!-- Edit Hostel Facility content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
       <div class="box-header with-border">
        <h3 class="box-title">Edit Hostel Facility</h3>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form method="post" action="<?php echo base_url('update-facility'); ?>" data-toggle="validator" role="form">
        <?php 
          if (isset($facility)){
          foreach ($facility as $facility){ 
        ?>
        <div class="box-body">
          <div class="row">
          <input type="hidden" name="updateid" value="<?php echo $facility->id;?>">
           <div class="col-md-6">
            <div class="form-group">
             <label>Name</label>
             <input type="text" name="name" class="form-control" value="<?php echo $facility->name; ?>" required>
           </div>
         </div>
         <div class="col-md-6">
          <div class="form-group">
            <label>Code</label>
            <input type="text" name="code" class="form-control" value="<?php echo $facility->code; ?>" required>
          </div>
        </div>
        <div class="col-md-12">
         <div class="form-group">
          <label>Notes</label>
          <textarea class="form-control" name="notes" rows="3" required><?php echo $facility->notes; ?></textarea>
        </div>
      </div>

    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>  
  </div>
  <?php } } ?>
</form>
</div>

</div>
</div>
</section>

</div>
