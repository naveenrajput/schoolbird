Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Hostel Rooms Form
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Add Hostel Rooms</li>
  </ol>
</section>
<!-- Add Hostel Rooms content section -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
       <div class="box-header with-border">
        <h3 class="box-title">Add Hostel Rooms</h3>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form method="post" action="<?php echo base_url('insert-rooms'); ?>" data-toggle="validator" role="form">
        <div class="box-body">


          <div class="row">
            <div class="col-md-6">
              <label>Hostel</label><?php echo form_error('hostelid');?>
              <select name="hostelid" class="form-control select2" required>
                <option value="<?php echo set_value('hostelid'); ?>" selected="selected">Select Hostel</option>
                <?php foreach ($hostels as $hostels) { ?>
                  <option value="<?php echo $hostels->id; ?>"><?php echo $hostels->name;?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-6">
              <label>Room Type</label><?php echo form_error('roomtypeid');?>
              <select name="roomtypeid" class="form-control select2" required>
                <option value="<?php echo set_value('roomtypeid'); ?>" selected="selected">Select Room Type</option>
                <?php foreach ($roomtype as $roomtype) { ?>
                  <option value="<?php echo $roomtype->id; ?>"><?php echo $roomtype->name;?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-6">
              <div class="form-group">
               <label>Room</label><?php echo form_error('name');?>
               <input type="text" name="name" class="form-control" value="<?php echo set_value('name'); ?>" required>
             </div>
           </div>
           <div class="col-md-6">
            <div class="form-group">
              <label>Code</label><?php echo form_error('code'); ?>
              <input type="text" name="code" class="form-control" value="<?php echo set_value('code'); ?>" required>
            </div>
          </div>

          <div class="col-md-12">
           <div class="form-group">
            <label>Notes</label><?php echo form_error('notes'); ?>
            <textarea class="form-control" name="notes" rows="3" placeholder="Counselor’s Remarks " required><?php echo set_value('notes'); ?></textarea>
          </div>
        </div>

<!--kavita  -->
    <div class="col-md-12">
       <div class="form-group">
       <ul class="list-inline">
        <li>
        <label for="chkPassport">
          <input type="checkbox" class="chkPassport" />
          Do you have Passport?
        </label>
        
        <div class="dvPassport" style="display: none">
            Passport Number:
            <input type="text" id="" />
        </div></li>
        
      </ul>
  <?php
    for($i=1; $i<=5;$i++)
  { ?>
          <ul class="list-inline">

            <li><input type="checkbox" name="facility[]" class="myRoom" value="<?php echo $i; ?>"> &nbsp; AC</li>
            <li>
              <input type="text" name="Qty[]" id="allot_<?php echo $i; ?>" style="display: none;" placeholder="quantity">
            </li>
  <?php } ?>
          </ul>
                   
       </div>
      <div class="clearfix"></div>
    </div>
<!-- kavita -->

      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>	
    </div>

  </form>
</div>

</div>
</div>
</section>

</div>
<script type="text/javascript">
  $(document).ready(function(){
        $('.myRoom').on('click',function(){
 
  var checkBox = document.getElementById("myRoom");
  var text = document.getElementById("allot");
  if (checkBox.checked == true){
    text.style.display = "block";
  } else {
     text.style.display = "none";
  }
});
      });

</script>
 <script type="text/javascript">
    $(function () {
        $(".chkPassport").click(function () {
            if ($(this).is(":checked")) {
                $(".dvPassport").show();
            } else {
                $(".dvPassport").hide();
            }
        });
    });
</script>
