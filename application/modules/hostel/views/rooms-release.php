 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class=" ">
   <div class="col-md-6 col-xs-12 col-sm-8 content-header">
    <h1 class="">
      Room Release
    </h1>
    <ol class="breadcrumb" style="background:none;">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li><a href="#">Hostel</a></li>
      <li class="active">Room Release</li>
    </ol>
  </div>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">

     <div class="box">
      <div class="box-header with-border mr-top-20">
      <div class="hostel_info">
        <div class="form-group col-md-3">
          <select name="hosteltype" id="htype_id" class="form-control" required>
            <option value="" selected="selected">Hostel Type</option>
            <option value="BOYS">BOYS</option>
            <option value="GIRLS">GIRLS</option>
          </select>
        </div>
        <div class="form-group col-md-3">

          <select name="hostelname" id="h_name" class="form-control" required>
            <option value="" selected="selected">Select Hostel</option>
            <?php foreach ($hostels as $hostels) { ?>
              <option value="<?php echo $hostels->name;?>"><?php echo $hostels->name;?></option>
            <?php } ?>
          </select>
        </div>
      </div>
       <div class="form-group  col-md-4"> 
        <select id="room_id" class="form-control select2"   style="width: 100%;" required>
          <option value="" selected="selected">Select Room No</option>
          <?php foreach ($allotedroom as $rooms) { ?>
            <option value="<?php echo $rooms->id; ?>"><?php echo $rooms->room_no; ?></option>
          <?php } ?>
        </select>
      </div>
      <!-- /.form group -->
      <!-- Date range -->
      <div class="col-md-2" >
        <button type="submit" id="rmck_id" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
      </div>
      <div class="clearfix"></div>

    </div>
    <div class="box-body ">

       <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">#</th>
            <th>Name</th>
            <th>Room No</th>
            <th>Capacity </th>
            <th>Allotment Date</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody id="room_info">
        </tbody>
      </table>

    </div>
  <!-- /.box-body -->
</div>

</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->

</div>
<script>
  $(document).ready(function(){    
    $('#htype_id').change(function(){
        var h_id=$('#htype_id').val();
        $.ajax({
          url:'<?php echo base_url('get-hname');?>',
          data:{h_id:h_id},
          type:'post',
          success:function(response){
            $('#h_name').html(response);
          }
        });
    });

    $('.hostel_info').click(function(){
        var type=$('#htype_id').val();
        var name=$('#h_name').val();
        $.ajax({
          url:'<?php echo base_url('release-roomno');?>',
          data:{type:type,name:name},
          type:'post',
          success:function(response){
            $('#room_id').html(response);
          }
        });
    });

    $('#rmck_id').click(function(){
      var r_id=$('#room_id').val();
      $.ajax({
        url:'<?php echo base_url('rooms-info');?>',
        data:{r_id:r_id},
        type:'post',
        success:function(response){
          $('#room_info').html(response);
        }
      });
    });
  });
</script>