<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hostel extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->model('Hostel_model');
        $this->load->helper('url');
    }
	//For Show hostel_type details
	public function hostel_type()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$hostype =$this->Hostel_model->hosteltype();
		$this->load->view('hosteltype',compact('hostype'));
		$this->load->view('../../inc/footer');
	}
	//For Add hostel_type
	public function Add_hosteltype()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('addhosteltype');
		$this->load->view('../../inc/footer');
	}
	//For Insert hostel_type
	public function Insert_hosteltype()
	{
		$data=$this->input->post();
		$this->Hostel_model->Inserthostype($data);
		redirect('hostel-type');
	}
	//For Edit hostel_type
	public function Edit_hosteltype($eid)
	{
		$hostype =$this->Hostel_model->edithosteltype($eid);
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('edit-hosteltype',compact('hostype'));
		$this->load->view('../../inc/footer');
	}
	//For Update hostel_type
	public function Update_hosteltype()
	{
		$data=$this->input->post();
		$data['modifieddate']=date("Y-m-d");
		unset($data['updateid']);
		$id=$this->input->post('updateid');
		$this->Hostel_model->updatehosteltype($id,$data);
		redirect('hostel-type');
	}
	//For close or delete hostel type detail
	public function Closehostype()
	{
		$id=$this->input->post('htypeid');
		$data = array(
			      'delete'         => 2,
                );
		$this->Hostel_model->close_htype($id,$data);
	}

    //For Show hostels details
	public function hostels()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$hostels=$this->Hostel_model->hosteldetails();
		$hostype =$this->Hostel_model->hosteltype();
		$emp =$this->Hostel_model->Get_empname();
		$this->load->view('hostels',compact('hostels','hostype','emp'));
		$this->load->view('../../inc/footer');
	}
	//For Add hostels
	public function Add_hostel()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$hostype =$this->Hostel_model->hosteltype();
		$emp =$this->Hostel_model->Get_empname();
		$this->load->view('add-hostels',compact('hostype','emp'));
		$this->load->view('../../inc/footer');
	}
	//For Edit hostel_type
	public function Edit_hostel($eid)
	{
		$hostels =$this->Hostel_model->edithostel($eid);
		$hostype =$this->Hostel_model->hosteltype();
		$emp =$this->Hostel_model->Get_empname();
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('edit-hostels',compact('hostype','emp','hostels'));
		$this->load->view('../../inc/footer');
	}
	//For Update hostel_type
	public function Update_hostel()
	{
		$data=$this->input->post();
		$data['modifieddate']=date("Y-m-d");
		unset($data['updateid']);
		$id=$this->input->post('updateid');
		$this->Hostel_model->updatehostel($id,$data);
		redirect('hostels');
	}
	//For close or delete hostels detail
	public function Close_hostels()
	{
		$id=$this->input->post('hosid');
		$data = array(
			      'delete'         => 2,
                );
		$this->Hostel_model->closehostels($id);
	}
	//For Insert hostels
	public function Insert_hostel()
	{
		$data=$this->input->post();
		$this->Hostel_model->Inserthostel($data);
		redirect('hostels');
	}
	//for select hostel name
	public function Get_hostelname()
	{
		$h_id=$this->input->post('h_id');
		$data=$this->Hostel_model->get_hname($h_id);
		echo $balnk='<option selected="selected" value="">Select Employee</option>';
		foreach ($data as $value) 
		{
		  echo $resp= '<option value="'.$value->id.'">'.$value->name.'</option>';
		}
	}

    //For showing details of room_type section
	public function room_type()
	{
		$roomtype =$this->Hostel_model->roomtype();
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('room-type',compact('roomtype'));
		$this->load->view('../../inc/footer');
	}
	//For Add hostel_type
	public function Add_roomtype()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add-roomtype');
		$this->load->view('../../inc/footer');
	}
	//For Insert hostel_type
	public function Insert_roomtype()
	{
		$data=$this->input->post();
		$this->Hostel_model->Insertroomtype($data);
		redirect('room_type');
	}
	//For Edit hostel_type
	public function Edit_roomtype($eid)
	{
		$roomtype =$this->Hostel_model->editroomtype($eid);
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('edit-roomtype',compact('roomtype'));
		$this->load->view('../../inc/footer');
	}
	//For Update hostel_type
	public function Update_roomtype()
	{
		$data=$this->input->post();
		$data['modifieddate']=date("Y-m-d");
		unset($data['updateid']);
		$id=$this->input->post('updateid');
		$this->Hostel_model->updateroomtype($id,$data);
		redirect('room_type');
	}
	//For close or delete hostel type detail
	public function Closeroomtype()
	{
		$id=$this->input->post('r_type');
		$data = array(
			      'delete'         => 2,
                );
		$this->Hostel_model->close_roomtype($id,$data);
	}
	

    // For Showing assigning_rooms/hostelrooms in Hotel module
	public function assigning_rooms()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$roomtype=$this->Hostel_model->roomtype();
		$hostels=$this->Hostel_model->hosteldetails();
		$h_rooms=$this->Hostel_model->hostel_rooms();
		$this->load->view('hostelrooms',compact('h_rooms','hostels','roomtype'));
		$this->load->view('../../inc/footer');
	}
	//To Add assigning_rooms/hostelrooms in Hotel module
	public function Addassigning_rooms()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$hostels=$this->Hostel_model->hosteldetails();
		$roomtype=$this->Hostel_model->roomtype();
		$this->load->view('add-hostelrooms',compact('hostels','roomtype'));
		$this->load->view('../../inc/footer');
	}
	public function Insert_rooms()
	{
		$data=$this->input->post();
		$facility=$this->input->post('facility');
		$Qty=$this->input->post('Qty');

		$c=array_combine($facility,$Qty);
		$fac= json_encode($c);
		/*$de=json_decode($fac);
		foreach ($de as $key => $value) {
			echo $key.",".$value.";";
		}
		print_r($de);die;*/
		
		$roomdetails = array('facility' =>implode(",",$data['facility']),
							 'quantity' =>implode(",",$data['quantity']),
							 'createdate' =>date("Y-m-d"),
							 'delete' =>1
							);
        unset($data['Qty']);
		$data['facility']=$fac;
 		$data['createdate']=date("Y-m-d");
 		$data['delete']=1;
 		$this->Hostel_model->insertrooms($data); 
 		redirect('assigning-rooms');       
	}
	public function Close_rooms()
	{
		$id=$this->input->post('roomid');
		$data['delete'] = 2;
		$this->Hostel_model->closerooms($id,$data);
	}
	public function Edit_rooms($eid)
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$h_rooms=$this->Hostel_model->Edithostel_rooms($eid);
		$hostels=$this->Hostel_model->hosteldetails();
		$roomtype=$this->Hostel_model->roomtype();
		$this->load->view('edit-hostelrooms',compact('hostels','roomtype','h_rooms'));
		$this->load->view('../../inc/footer');
	}
	public function Update_rooms()
	{
		$data=$this->input->post();
		$data['modifieddate']=date("Y-m-d");
		unset($data['updateid']);
		$id=$this->input->post('updateid');
		$this->Hostel_model->updaterooms($id,$data);
	}

    //For frontdask facilities
	public function facilities()
	{
		$hfacility=$this->Hostel_model->gethostelfacility();
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('hostelfacility',compact('hfacility'));
		$this->load->view('../../inc/footer');
	}
	//For Add hostel facility
	public function Add_facility()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add-hostelfacility');
		$this->load->view('../../inc/footer');
	}
	//For Insert hostel facility
	public function Insert_facility()
	{
		$data=$this->input->post();
		$this->Hostel_model->Insertfacility($data);
		redirect('facilities');
	}
	//For Edit hostel facility
	public function Edit_facility($eid)
	{
		$facility =$this->Hostel_model->editfacility($eid);
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('edit-hostelfacility',compact('facility'));
		$this->load->view('../../inc/footer');
	}
	//For Update hostel facility
	public function Update_facility()
	{
		$data=$this->input->post();
		$data['modifieddate']=date("Y-m-d");
		unset($data['updateid']);
		$id=$this->input->post('updateid');
		$this->Hostel_model->updatefacility($id,$data);
		redirect('facilities');
	}
	//For close or delete hostel facility detail
	public function Close_facility()
	{
		$id=$this->input->post('hfid');
		$data['delete']=2;
		$this->Hostel_model->close_facility($id,$data);
	}


    // For frontdask warden_details
	public function warden_details()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('');
		$this->load->view('../../inc/footer');
	}

   // For frontdask Hostel_student_allotment
	public function Hostel_student_allotment()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('');
		$this->load->view('../../inc/footer');
	}

	// For frontdask mess_type
	public function mess_type()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('');
		$this->load->view('../../inc/footer');
	}

   // For frontdask mess_details
	public function mess_details()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('');
		$this->load->view('../../inc/footer');
	}

   // For frontdask mess_student_allotment
	public function mess_student_allotment()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

   // For frontdask mess_attendance
	public function mess_attendance()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}
}
?>
