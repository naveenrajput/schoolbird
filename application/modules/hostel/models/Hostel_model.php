<?php  
class Hostel_model extends CI_Model  
{  
	function __construct()  
	{  
		parent::__construct();  
	}
	public function hosteltype()
	{
		$this->db->select('*');
		$this->db->from('hosteltype');
		$this->db->where('delete',1);
		return $query=$this->db->get()->result();
	    //print_r($this->db->last_query($query));die;
	}
	public function edithosteltype($eid)
	{
		$this->db->select('*');
		$this->db->from('hosteltype');
		$this->db->where('id',$eid);
		return $query=$this->db->get()->result();
	}
	public function updatehosteltype($id,$data)
	{
		$query=$this->db->where('id',$id)->update('hosteltype',$data);
	}
	public function close_htype($id,$data)
	{
		$query=$this->db->where('id',$id)->update('hosteltype',$data);
	}
	public function Inserthostype($data)
	{
		$data['createdate']=date("Y-m-d");
		$data['delete']=1;
		$query=$this->db->insert('hosteltype',$data);
	}
	public function hosteldetails()
	{
		$this->db->select('*');
		$this->db->from('hostels');
		$this->db->where('delete',1);
		return $query=$this->db->get()->result();
	}
	public function get_hname($h_id)
	{
		$this->db->select('id,name');
		$this->db->from('hosteltype');
		$this->db->where('id',$h_id);
		return $query=$this->db->get()->result();
	}
	public function Get_empname()
	{
		$this->db->select('id,name');
		$this->db->from('employees');
		return $query=$this->db->get()->result();
	}
	public function closehostels($id,$data)
	{
		$query=$this->db->where('id',$id)->update('hostels',$data);
	}
	public function Inserthostel($data)
	{
		$data['createdate']=date("Y-m-d");
		$data['delete']=1;
		$query=$this->db->insert('hostels',$data);
	}
	public function edithostel($eid)
	{
		$this->db->select('*');
		$this->db->from('hostels');
		$this->db->where('id',$eid);
		return $query=$this->db->get()->result();
	}
	public function updatehostel($id,$data)
	{
		$query=$this->db->where('id',$id)->update('hostels',$data);
	}
	
	//query to showing data of roomtype table
	public function roomtype()
	{
		$this->db->select('*');
		$this->db->from('roomtype');
		$this->db->where('delete',1);
		return $query=$this->db->get()->result();
	}
	//query to insert data in roomtype table
	public function Insertroomtype($data)
	{
		$data['createdate']=date("Y-m-d");
		$data['delete']=1;
		$query=$this->db->insert('roomtype',$data);
	}
	//query to Show data In edit form
	public function editroomtype($eid)
	{
		$this->db->select('*');
		$this->db->from('roomtype');
		$this->db->where('id',$eid);
		return $query=$this->db->get()->result();
	}
	//query to update data in roomtype table
	public function updateroomtype($id,$data)
	{
		$query=$this->db->where('id',$id)->update('roomtype',$data);
	}
	//query to close/delete data
	public function close_roomtype($id,$data)
	{
		$query=$this->db->where('id',$id)->update('roomtype',$data);
	}
	public function hostel_rooms()
	{
		$this->db->select('*');
		$this->db->from('hostelrooms');
		$this->db->where('delete',1);
		return $query=$this->db->get()->result();
	    //print_r($this->db->last_query($query));die;
	}
	public function insertrooms($data)
	{
		$query=$this->db->insert('hostelrooms',$data);
		$insid=$this->db->insert_id($query);
		$roomdetails['hosteltableid']=$insid;
		//$this->db->insert('roomdetails',$roomdetails);
	}
	public function Edithostel_rooms($eid)
	{
		$this->db->select('*');
		$this->db->from('hostelrooms');
		$this->db->join('roomdetails', 'roomdetails.hosteltableid = hostelrooms.id','left');
		$this->db->where('hostelrooms.delete',1);
		$this->db->where('hostelrooms.id',$eid);
		return $query=$this->db->get()->result();
	    //print_r($this->db->last_query($query));die;
	}
	public function updaterooms($id,$data)
	{
		$this->db->where('id',$id)->update('hostelrooms',$data);
	}
	public function closerooms($id,$data)
	{
		$query=$this->db->where('id',$id)->update('hostelrooms',$data);
	}
	public function gethostelfacility()
	{
		$this->db->select('*');
		$this->db->from('hostelfacility');
		$this->db->where('delete',1);
		return $query=$this->db->get()->result();
	}
	public function Insertfacility($data)
	{
		$data['createdate']=date("Y-m-d");
		$data['delete']=1;
		$query=$this->db->insert('hostelfacility',$data);
	}
	public function editfacility($eid)
	{
		$this->db->select('*');
		$this->db->from('hostelfacility');
		$this->db->where('id',$eid);
		return $query=$this->db->get()->result();
	}
	public function updatefacility($id,$data)
	{
		$query=$this->db->where('id',$id)->update('hostelfacility',$data);
	}
	public function close_facility($id,$data)
	{
		$query=$this->db->where('id',$id)->update('hostelfacility',$data);
	}
	
}  
?>  