<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    public function __construct(){
        parent::__construct();
        //$this->load->model('contact_model', 'contact');
        $this->load->helper('url');
    }

    //Controller for load login view
	public function index()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

   //Controller for profile view
	public function profile()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('profile');
		$this->load->view('../../inc/footer');
	}

	//Controller for Sign-out view
	public function sign_out()
	{
		$this->load->view('../../inc/header');
		$this->load->view('login');
		
	}
}
