<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_desk extends MY_Controller {

	
	public function __construct(){
        parent::__construct();
        //$this->load->model('contact_model', 'contact');
        $this->load->helper('url');
        $this->load->helper('MY_helper.php');
        $this->load->helper('directory');
      }

	// For frontdask Docs
	public function docs()
	{
	
	$this->load->view('../../inc/header');
	$this->load->view('../../inc/sidebar');
    $data['folders'] = directory_map('files');
    $data['contenido'] = "index";
    $this->load->view('front-desk-docs',$data);
    $this->load->view('../../inc/footer');
		
	}


	// For frontdask Enquiry
	public function Enquiry()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('enquiry');
		$this->load->view('../../inc/footer');
	}
	
	// For frontdask Add_Enquiry
	public function Add_Enquiry()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add-enquiry');
		$this->load->view('../../inc/footer');
	}

    // For frontdask Reports
	public function Reports()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('interaction');
		$this->load->view('../../inc/footer');
	}
// For frontdask Add_Interaction
	public function Add_Interaction()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add-interaction');
		$this->load->view('../../inc/footer');
	}
	
	// For frontdask Add_Interaction
	public function Send_Approval()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('send_approval');
		$this->load->view('../../inc/footer');
	}

	// For frontdask Add_Interaction
	public function moveDocs()
	{
		$baseUrl = base_url();
		
		$source_file = pathinfo($this->input->post('file'));
		$target_dir  = $this->input->post('folder');

		$Src_path = str_replace($baseUrl, '',$source_file['dirname']); 
		$src_abs_path = str_replace('/', '\\', $Src_path); 
		$srcFileName =  $src_abs_path.'\\'.$source_file['basename'];
		$targFileName = '.\files\\'.$target_dir.'\\'.$source_file['basename'];
		copy($srcFileName, $targFileName);
		unlink($srcFileName);
	   

		
	}
	
// For frontdask Add_Interaction
	public function Admit_Child ()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('admit_child');
		$this->load->view('../../inc/footer');
	}

    // For frontdask Admission
	public function Admission()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('admission');
		$this->load->view('../../inc/footer');
	}

    // For frontdask Employee
	public function Employee()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('employee-application');
		$this->load->view('../../inc/footer');
		
	}	
	// For frontdask Add_Employee
	public function Add_Employee()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add-employee-application');
		$this->load->view('../../inc/footer');
	}



    // For frontdask Visitors
	public function Visitors()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('visitors');
		$this->load->view('../../inc/footer');
	}
	
	 // For frontdask Add-Visitor
	public function Add_Visitor()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add-visitor');
		$this->load->view('../../inc/footer');
	}

   // For frontdask Gate_Pass
	public function Gate_Pass()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('gatepass');
		$this->load->view('../../inc/footer');
	}
	
	 // For frontdask Add-Gate_Pass
	public function Add_Gate_Pass()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add-gatepass');
		$this->load->view('../../inc/footer');
	}


}
