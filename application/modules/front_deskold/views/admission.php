<style>

</style>
  <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Admission
      
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Front Desk</a></li>
        <li class="active">Admission</li>
      </ol>
    </section>

    <!-- Main content -->
     <section class="content">
    <div class="row"> 
      
      <!-- /.col -->
      <div class="col-md-12">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#enq" data-toggle="tab">From Enquiry</a></li>
            <li><a href="#intrec" data-toggle="tab">From Interaction</a></li>
            <li><a href="#admission" data-toggle="tab">Admission</a></li>
            <li><a href="#newadmission" data-toggle="tab">New Admission</a></li>
          </ul>
          <div class="tab-content">
            <div class="active tab-pane" id="enq"> 
                <div class="box" style="border:none;">
		  <div class="box-header with-border mr-top-20 mr-bottom-20 text-center">
			<div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker2" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
			<div class="col-md-2" >
			  <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
			
			
			
			  </div>
			
            <!-- /.box-header -->
            <div class="box-body table-responsive">
             <!-- <table id="example" class="display nowrap" style="width:100%">--->
			
              <table id="example" class="table table-bordered " >
			   <div class="txt-dis">Export in Below Format</div>
        <thead>
            <tr>
		
                <th><input type="checkbox" id="selectall"> All</input></th>
                <th>S.no</th>
                <th>Child Name</th>
                <th>Father's Name</th>
				<th>Class</th>
				<th>Section</th>
				<th>Date</th>
				<th>Updated By</th>
				<th>Forward</th>
                <th>Action</th>
				
				
            </tr>
        </thead>
        <tbody>
		<?php for($i=0;$i<100;$i++){?>
            <tr>
			<td>
		<input type="checkbox" class="selectedId" name="selectedId" />
			</td>
              
                <td>1</td>
                <td>Shivam Yadav</td>
                <td>Ravi Yadav</td>
				 <td>9th</td>
				 <td>B</td>
				 <td>12/07/2018</td>
				 <td>Admin</td>
				 
               <td>
			     <ul class="table-icons">
        <li><a href="" class="table-icon" title="Review" data-toggle="modal" data-target="#modal-review"> 
		
		<span class="glyphicon glyphicon-eye-open display-icon"></span>
		</a></li>
          <li><a href="" class="table-icon " title="Interaction" data-toggle="modal" data-target="#modal-interaction"> 
		
		<span class="glyphicon glyphicon-open-file display-icon"></span>
		</a></li>
		 <li><a href="" class="table-icon" title="Archive"> 
		
		<span class="display-icon" > <img src="<?php echo base_url();?>assets/dist/img/icon/archive.png" style="vertical-align: baseline;" class=""> </span>
		</a></li>
    </ul>
				</td>
				<td>
			     <ul class="table-icons">
        <li><a href="" class="table-icon" title="Edit"> 
		
		<span class="glyphicon glyphicon-edit display-icon"></span>
		</a></li>
          <li><a href="" class="table-icon" title="Delete"> 
		
		<span class="glyphicon glyphicon-trash display-icon"></span>
		</a></li>
		 <li><a href="" class="table-icon" title="Reminder"  data-toggle="modal" data-target="#modal-reminder"> 
		
		<span class="glyphicon glyphicon-time display-icon"></span>
		</a></li>
		  <li><a href="" class="table-icon" title="Print"> 
		
		<span class="glyphicon glyphicon-print display-icon"></span>
		</a></li>
    </ul>
				</td>
              
				
            </tr>
           
		<?php }?>
        </tbody>
        <tfoot>
            <tr>
              <th><input type="checkbox" id="selectall"> All</input></th>
                <th>S.no</th>
                <th>Child Name</th>
                <th>Father's Name</th>
				<th>Class</th>
				<th>Section</th>
				<th>Date</th>
				<th>Updated By</th>
				<th>Forward</th>
                <th>Action</th>

            </tr>
        </tfoot>
    </table>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="intrec"> 
               <div class="box" style="border:none;">
		  <div class="box-header with-border mr-top-20 mr-bottom-20 text-center">
			<div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker4" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker3" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
			<div class="col-md-2" >
			  <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
			
			
			
			  </div>
			
            <!-- /.box-header -->
            <div class="box-body table-responsive">
             <!-- <table id="example" class="display nowrap" style="width:100%">--->
			
              <table id="example2" class="table table-bordered " >
			   <div class="txt-dis">Export in Below Format</div>
        <thead>
                <tr>
                  <th><input type="checkbox" id="selectall">
                    All
                    </input></th>
                  <th>S.no</th>
                  <th>Student Name</th>
                  <th>General Awareness</th>
                  <th>Mental Ability</th>
                  <th>Confidence</th>
                  <th>Communication Skills</th>
                  <th>Personality</th>
                  <th>Previous School</th>
                  <th>Forward</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<100;$i++){?>
                <tr>
                  <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
                  <td>1</td>
                  <td>Shivam Yadav</td>
                  <td>5%</td>
                  <td>20</td>
                  <td>80</td>
                  <td>40</td>
                  <td>5%</td>
                  <td>St.pauls</td>
                  <td><ul class="table-icons">
                      <li><a href="" class="table-icon" title="Review" data-toggle="modal" data-target="#modal-review"> <span class="glyphicon glyphicon-eye-open display-icon"></span> </a></li>
                      <li><a href="" class="table-icon" title="Archive"> 
		
		<span class="display-icon" > <img src="<?php echo base_url();?>assets/dist/img/icon/archive.png" style="vertical-align: baseline;" class=""> </span>
		</a></li>
                    </ul></td>
                  <td><ul class="table-icons">
                      <li><a href="" class="table-icon" title="Edit"> <span class="glyphicon glyphicon-edit display-icon"></span> </a></li>
                      <li><a href="" class="table-icon" title="Delete"> <span class="glyphicon glyphicon-trash display-icon"></span> </a></li>
                      <li><a href="" class="table-icon" title="Set Reminder"  data-toggle="modal" data-target="#modal-reminder"> <span class="glyphicon glyphicon-time display-icon"></span> </a></li>
                      <li><a href="" class="table-icon" title="Print"> <span class="glyphicon glyphicon-print display-icon"></span> </a></li>
                    </ul></td>
                </tr>
                <?php }?>
              </tbody>
              <tfoot>
                <tr>
                  <th><input type="checkbox" id="selectall">
                    All
                    </input></th>
                  <th>S.no</th>
                  <th>Student Name</th>
                  <th>General Awareness</th>
                  <th>Mental Ability</th>
                  <th>Confidence</th>
                  <th>Communication Skills</th>
                  <th>Personality</th>
                  <th>Previous School</th>
                  <th>Forward</th>
                  <th>Action</th>
                </tr>
              </tfoot>
    </table>
            </div>
            <!-- /.box-body -->
          </div>
             
            </div>
			<div class="tab-pane" id="admission"> 
               <div class="box" style="border:none;">
		  <div class="box-header with-border mr-top-20 mr-bottom-20 text-center">
			<div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker6" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker7" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
			<div class="col-md-2" >
			  <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
			
			
			
			  </div>
			
            <!-- /.box-header -->
            <div class="box-body table-responsive">
             <!-- <table id="example" class="display nowrap" style="width:100%">--->
			
              <table id="example1" class="table table-bordered ">
			   <div class="txt-dis">Export in Below Format</div>
        <thead>
                <tr>
                  <th><input type="checkbox" id="selectall">
                    All
                    </input></th>
                  <th>S.no</th>
                  <th>Student<br>Name</th>
                  <th>Father <br>Name</th>
                  <th>Contact<br> No</th>
                  <th>Applied <br>Class</th>
                  <th>Student <br>Interaction<br> Report </th>
                  <th>Parent <br>Interaction<br> Report </th>
                  <th>Interaction <br>Date</th>
                  <th>Interaction<br> Time</th>
                  <th>Remarks</th>
                  <th>Forward</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<100;$i++){?>
                <tr>
                  <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
                  <td>1</td>
                  <td>Shivam Yadav</td>
                  <td>Ravi Yadav</td>
                  <td>9087654321</td>
                  <td>4th</td>
                  <td><a href="#" data-toggle="modal" data-target="#modal-stu-report"><strong>Click Here</strong></a></td>
				     <td><a href="#" data-toggle="modal" data-target="#modal-parent-report"><strong>Click Here</strong></a></td>
                  <td>10/05/2019</td>
                  <td>2:00 PM</td>
                  <td>Text</td>
				  
                  <td><ul class="table-icons">
                      <li><a href="" class="table-icon" title="Review" data-toggle="modal" data-target="#modal-review"> <span class="glyphicon glyphicon-eye-open display-icon"></span> </a></li>
                      <li><a href="" class="table-icon" title="Archive"> 
		
		<span class="display-icon" > <img src="<?php echo base_url();?>assets/dist/img/icon/archive.png" style="vertical-align: baseline;" class=""> </span>
		</a></li>
                    </ul></td>
                  <td><ul class="table-icons">
                      <li><a href="" class="table-icon" title="Edit"> <span class="glyphicon glyphicon-edit display-icon"></span> </a></li>
                      <li><a href="" class="table-icon" title="Delete"> <span class="glyphicon glyphicon-trash display-icon"></span> </a></li>
                      <li><a href="" class="table-icon" title="Reminder"  data-toggle="modal" data-target="#modal-reminder"> <span class="glyphicon glyphicon-time display-icon"></span> </a></li>
                      <li><a href="" class="table-icon" title="Print"> <span class="glyphicon glyphicon-print display-icon"></span> </a></li>
                    </ul></td>
                </tr>
                <?php }?>
              </tbody>
              <tfoot>
                <tr>
                  <th><input type="checkbox" id="selectall">
                    All
                    </input></th>
                 <th>S.no</th>
                  <th>Student<br>Name</th>
                  <th>Father <br>Name</th>
                  <th>Contact<br> No</th>
                  <th>Applied <br>Class</th>
                  <th>Student <br>Interaction<br> Report </th>
                  <th>Parent <br>Interaction<br> Report </th>
                  <th>Interaction <br>Date</th>
                  <th>Interaction<br> Time</th>
                  <th>Remarks</th>
                  <th>Forward</th>
                  <th>Action</th>
                </tr>
              </tfoot>
    </table>
            </div>
            <!-- /.box-body -->
          </div>
             
            </div>
			<div class="tab-pane" id="newadmission"> 
                <form role="form">
              <div class="box-body">
			  <div class="row">
			   <div class="col-md-2 col-xs-12 imgUp pull-right">
						<div class="imagePreview"></div>
						<label class="btn btn-upload btn-primary">
							Student Pic<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
							</label>
						</div><!-- col-2 -->
			  <div class="col-md-10 remove-pd">
			  <div class="col-md-6  ">
			  
				<div class="form-group ">
                  <label for="">For Academic Year </label>
                 <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>2018-19</option>
                  <option>2019-20</option>

                </select>
                </div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
                <label>Date Of Admission </label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker5">
                </div>
				
				
                <!-- /.input group -->
              </div>
				</div>
				<div class="col-md-6">
			  <div class="form-group">
                  <label for="">Samagrah Id </label>
                  <input type="text" class="form-control"  placeholder="Samagrah Id  ">
                </div>
			  </div>
				<div class="col-md-6">
			  <div class="form-group">
                  <label for="">Aadhar Card</label>
                  <input type="text" class="form-control"  placeholder="Aadhar Card ">
                </div>
			  </div>
			
				
				<div class="col-md-6 ">
			  <div class="form-group">
                  <label for="">First Name </label>
                  <input type="text" class="form-control"  placeholder="First Name">
                </div>
			  </div>
			    <div class="col-md-6 ">
				<div class="form-group">
                  <label for="">Last Name </label>
                  <input type="text" class="form-control" placeholder="Last Name ">
                </div>
				</div>
				
				</div>
			 
						
						</div><!--row-->
						  <div class="row">
			  <div class="col-md-3">
				<div class="form-group">
                <label>Date of Birth</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
                <!-- /.input group -->
              </div>
				</div>
				
				
				  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Gender </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>Male</option>
                  <option>Female</option>

                </select>
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Student Category  </label>
                 <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>General</option>
                  <option>OBC</option>
                  <option>ST</option>
                  <option>SC</option>
                  <option>None</option>

                </select>
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Caste  </label>
                  <input type="text" class="form-control"  placeholder="Caste ">
                </div>
			  </div>
			  
			    
			    <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Religion </label>
                  <input type="text" class="form-control"  placeholder="Religion">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Mother Tongue </label>
                  <input type="text" class="form-control"  placeholder="Mother Tongue ">
                </div>
			  </div>
			    <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Nationality  </label>
                  <input type="text" class="form-control"  placeholder="Nationality ">
                </div>
			  </div>
			    <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Blood Group  </label>
                  <input type="text" class="form-control"  placeholder="Blood Group ">
                </div>
			  </div>
			
				 <div class="col-md-8">
				<div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" rows="2" placeholder="Address"></textarea>
                </div>
				</div>
				 
				<div class="col-md-4">
			  <div class="form-group">
                  <label for="">Municipal Corporation Ward No.  </label>
                  <input type="text" class="form-control"  placeholder="Ward No. ">
                </div>
			  </div>
			  </div>
			   <div class="row">
				<div class="col-md-12 "> <p class="lead">Special Ability</p></div>
					<div class="col-md-3">
			  <div class="form-group">
			
			  <input type="checkbox" class="sibling-hide" id="myCheck3" onclick="myFunction3()">
				   <label>&nbsp; Special Ability  </label>
				</div>
			  </div>
			  <div class="col-md-9" id="text3" style="display: none;">
			  
			   
			  <div class="form-group col-md-6">
			    <label>Select  </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>Type 1</option>
                  <option>Type 2</option>
                  <option>Type 3</option>
                  <option>Type 4</option>
                  <option>Other</option>

                </select>
                </div>
				
				<div class="form-group col-md-6">
			     <label>Certificate </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="Certificate">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
		<div class="form-group col-md-12">
    <label for="">Remarks </label>
             
  <textarea class="form-control" rows="3" placeholder="Type Here.." spellcheck="false"></textarea>
  </div>		 
			 </div>
				
				</div>
			  
			<div class="row">
			    <div class="col-md-12 "> <p class="lead">Upload Documents </p></div>
			   
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label> Date Of Birth Certificate	 </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="DOB Certificate">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			   <div class="col-md-4 ">
			   <div class="form-group">
			     <label>SSSMID </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			   <div class="col-md-4 ">
			   <div class="form-group">
			     <label>Aadhar Card </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="Aadhar Card">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label> Transfer Certificate (Original) </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="Transfer Certificate">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label>Caste Certificate </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="Caste Certificate">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label>Address Proof</label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="Address Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			 
						
						</div>
						
			  <div class="row">
			    <div class="col-md-12 "> <p class="lead">Father's Details  </p></div>
			   <div class="col-md-2 col-xs-12 imgUp pull-right">
						<div class="imagePreview"></div>
						<label class="btn btn-upload btn-primary">
							Father Pic<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
							</label>
						</div><!-- col-2 -->
			  <div class="col-md-10 remove-pd">
			  <div class="col-md-6  ">
			  <div class="form-group">
                  <label for="">Father's Name</label>
                  <input type="text" class="form-control"  placeholder="Father's Name ">
                </div>
				
				</div>
				<div class="col-md-6">
				 <div class="form-group">
                  <label for="">Father's Profession </label>
                  <input type="text" class="form-control"  placeholder="Father's Profession  ">
                </div>
				</div>
				<div class="col-md-4">
			  <div class="form-group">
                <label>Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask>
                </div>
                <!-- /.input group -->
              </div>
			  </div>
				<div class="col-md-4">
			  <div class="form-group">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="Email">
              </div>
                <!-- /.input group -->
              </div>
			  </div>
			
				
				<div class="col-md-4 ">
			   <div class="form-group">
			     <label>ID Proof </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			    
				
				</div>
			 
						
						</div>
						
						
						
						 <div class="row">
			    <div class="col-md-12 "> <p class="lead">Mother's Details  </p></div>
			   <div class="col-md-2 col-xs-12 imgUp pull-right">
						<div class="imagePreview"></div>
						<label class="btn btn-upload btn-primary">
							Mother Pic<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
							</label>
						</div><!-- col-2 -->
			  <div class="col-md-10 remove-pd">
			  <div class="col-md-6  ">
			  <div class="form-group">
                  <label for="">Mother's Name</label>
                  <input type="text" class="form-control"  placeholder="Father's Name ">
                </div>
				
				</div>
				<div class="col-md-6">
				 <div class="form-group">
                  <label for="">Mother's Profession </label>
                  <input type="text" class="form-control"  placeholder="Father's Profession  ">
                </div>
				</div>
				<div class="col-md-4">
			  <div class="form-group">
                <label>Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask>
                </div>
                <!-- /.input group -->
              </div>
			  </div>
				<div class="col-md-4">
			  <div class="form-group">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="Email">
              </div>
                <!-- /.input group -->
              </div>
			  </div>
			
				
				<div class="col-md-4 ">
			   <div class="form-group">
			     <label>ID Proof </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			    
				
				</div>
			 
						
						</div>
				<div class="row">
			    <div class="col-md-12 "> <p class="lead">Guardian Details  </p></div>
			   <div class="col-md-2 col-xs-12 imgUp pull-right">
						<div class="imagePreview"></div>
						<label class="btn btn-upload btn-primary">
							Guardian Pic<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
							</label>
						</div><!-- col-2 -->
			  <div class="col-md-10 remove-pd">
			  <div class="col-md-6  ">
			  <div class="form-group">
                  <label for="">Guardian's Name</label>
                  <input type="text" class="form-control"  placeholder="Father's Name ">
                </div>
				
				</div>
				<div class="col-md-6">
				 <div class="form-group">
                  <label for="">Guardian's Profession </label>
                  <input type="text" class="form-control"  placeholder="Father's Profession  ">
                </div>
				</div>
				<div class="col-md-4">
			  <div class="form-group">
                <label>Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask>
                </div>
                <!-- /.input group -->
              </div>
			  </div>
				<div class="col-md-4">
			  <div class="form-group">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="Email">
              </div>
                <!-- /.input group -->
              </div>
			  </div>
			
				
				<div class="col-md-4 ">
			   <div class="form-group">
			     <label>ID Proof </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			    
				<div class="col-md-6">
				 <div class="form-group">
                  <label for="">Relation With </label>
                  <input type="text" class="form-control"  placeholder="Relation With">
                </div>
				</div>
				
				<div class="col-md-6">
				 <div class="form-group">
                  <label for="">Address</label>
                <textarea class="form-control" rows="2" placeholder="Address" spellcheck="false"></textarea>
                </div>
				</div>
				</div>
			 
						
						</div>		
						
			  <div class="row">
			    <div class="col-md-12 "> <p class="lead">Sibling's Details  </p></div>
			   <div class="col-md-3">
			  <div class="form-group">
			 
			  <input type="checkbox" class="sibling-hide"   id="myCheck"  onclick="myFunction()">
				   <label>&nbsp; Is Sibling? </label>
				</div>
			  </div>
			  <div class="col-md-9" id="text1" style="display:none">
			  <div class="form-group">
			 
			  <div class="input-group">
                <input type="text" class="form-control"  placeholder="Enter Sibling Registration ID   ">
                    <span class="input-group-btn">
                      <div type="button"  id="sibling" class="btn btn-primary">Search</div>
                    </span>
              </div>
				</div>
			  </div>
			  <div class="col-md-12 table-responsive" id="sibling-details" style="display:none">
			  
				<table class="table fetchdata table-hover table-bordered">
							  <tbody><tr>
							  <th>Name</th>
								<th>Relation</th>
								<th>Age</th>
								<th>Qualification</th>
								<th>Occupation</th>

								<th>Mobile</th>

							   
							  </tr>
						</tbody>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							
						</tr>
						</table>
						  </div>
			  
			  <div class="col-md-12 "> <p class="lead">Bank Details  </p></div>
			<div class="col-md-6">
			
			  <div class="form-group">
                  <label for="">Annual Family Income  </label>
                  <input type="text" class="form-control"  placeholder="Income ">
                </div>
			  </div>
			  
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Name Of Account Holder  </label>
                  <input type="text" class="form-control"  placeholder="Account Holder  ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Bank Account Number  </label>
                  <input type="text" class="form-control"  placeholder="Account Number  ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Bank Name </label>
                  <input type="text" class="form-control"  placeholder="Bank Name ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">IFSC Code  </label>
                  <input type="text" class="form-control"  placeholder="IFSC Code  ">
                </div>
			  </div>
			   <div class="col-md-12"> <p class="lead">Previous Educational Information </p></div>
			
				<div class="col-md-6">
			  <div class="form-group">
                  <label for="">School Name  </label>
                  <input type="text" class="form-control"  placeholder="School Name ">
                </div>
			  </div>
			  <div class="col-md-6">
						
				 <div class="form-group">
                  <label for="">Previous Class  </label>
                  <input type="text" class="form-control"  placeholder="Previous Class ">
                </div>
				</div>
			<div class="col-md-3">
			  <div class="form-group">
                  <label for="">Board	 </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>MP</option>
                  <option>CBSE</option>
                  <option>ISCE</option>
                  <option>Other</option>
				    </select>
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Passed/Appeared	 </label>
                  <input type="text" class="form-control"  placeholder=" ">
                </div>
			  </div>
		<div class="col-md-3">
			  <div class="form-group">
                  <label for="">Year</label>
                  <input type="text" class="form-control"  placeholder="Year ">
                </div>
			  </div>			  
					<div class="col-md-3">
			  <div class="form-group">
                  <label for="">Percentage </label>
                  <input type="text" class="form-control"  placeholder="Percentage ">
                </div>
			  </div>
				<div class="col-md-12 "> <p class="lead">Suggest Class </p></div>
			   <div class="col-md-3">
			  <div class="form-group">
			
			  <input type="checkbox" class="sibling-hide"   id="myCheck1"  onclick="myFunction1()">
				   <label>&nbsp; Suggest Class </label>
				</div>
			  </div>
			  <div class="col-md-9" id="text2" style="display:none">
			  
			   
			  <div class="form-group col-md-4" >
			    <label>Class </label>
                  <div class="input-group">
                <input type="text" class="form-control" placeholder="6th" disabled>
                    <span class="input-group-btn">
                      <div type="button" id="sibling" class="btn btn-primary">Change</div>
                    </span>
              </div>
                </div>
				<div class="form-group col-md-4" >
				 <label>Section </label>
                  <div class="input-group">
                <input type="text" class="form-control" placeholder="B" disabled>
                    <span class="input-group-btn">
                      <div type="button" id="sibling" class="btn btn-primary">Change</div>
                    </span>
              </div>
                </div>
				<div class="form-group col-md-4" >
				 <label>House </label>
                  <div class="input-group">
                <input type="text" class="form-control" placeholder="Red" disabled> 
                    <span class="input-group-btn">
                      <div type="button" id="sibling" class="btn btn-primary">Change</div>
                    </span>
              </div>
                </div>
				 
			 </div>
			  <!---<div class="col-md-12 table-responsive" id="sibling-details" style="display:none">
			  
				<table class="table fetchdata table-hover table-bordered">
							  <tbody><tr>
							  <th>Name</th>
								<th>Relation</th>
								<th>Age</th>
								<th>Qualification</th>
								<th>Occupation</th>

								<th>Mobile</th>

							   
							  </tr>
						</tbody>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							
						</tr>
						</table>
						  </div>---->
				
				
				</div>
				
			
			  
			
							
					
					 </div>
						<div class="box-footer mb-btn">
						<div class="col-md-4 mr-top-10  ">
						<button type="submit" class="btn btn-primary">Submit</button>
						</div>
						<div class="col-md-4 mr-top-10  ">
						<button type="submit" class="btn btn-primary">Submit & Save to DB</button>
						</div>
						<div class="col-md-4 mr-top-10  print ">
						<button type="submit" class="btn btn-primary">Print</button>
						</div>
                

              </div>	
                    </div>
              <!-- /.box-body -->

              
            </form>
            </div>
          </div>
          <!-- /.tab-content --> 
        </div>
        <!-- /.nav-tabs-custom --> 
      </div>
      <!-- /.col --> 
    
    
  </section>
    <!-- /.content -->
  </div>

 <div class="modal fade" id="modal-reminder">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Reminder</h4>
              </div>
              <div class="modal-body">
              <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2019
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                      <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                      <div class="timeline-body">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                        quora plaxo ideeli hulu weebly balihoo...
                      </div>
                     
                    </div>
                  </li>
                  <!-- END timeline item -->
                 
                  <li class="time-label">
                        <span class="bg-green">
                          3 Mar. 2019
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-yellow"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                      <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                      <div class="timeline-body">
                        Take me to your leader!
                        Switzerland is small and neutral!
                        We are more like Germany, ambitious and misunderstood!
                      </div>
                      
                    </div>
                  </li>
                  <!-- END timeline item -->
                 
                </ul>
				<textarea class="form-control" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		<div class="modal fade" id="modal-review">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Review</h4>
              </div>
              <div class="modal-body">
			
             <div class="callout callout-info" >
        <h4><i class="fa fa-info"></i> Student Details:</h4>
       <a href="#" target="_blank" >https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js</a>
      </div>
            <div class="form-group">
                  <label for="">Contact Person   </label>
                 <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  	<option value="Name1">Name1 </option>
			<option value="Name2">Name2</option>
			<option value="Name3">Name3</option>
			<option value="Name4">Name1</option>
			<option value="Name5">Name5</option>
			<option value="Name6">Name6</option>
                
                </select>
                </div>
			  
             <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
				
              </div>
			  
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div class="modal fade" id="modal-interaction">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Interaction</h4>
              </div>
              <div class="modal-body">
			
             <div class="form-group">
                  <label for="">Contact Person   </label>
                 <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  	<option value="Name1">Name1 </option>
			<option value="Name2">Name2</option>
			<option value="Name3">Name3</option>
			<option value="Name4">Name1</option>
			<option value="Name5">Name5</option>
			<option value="Name6">Name6</option>
                
                </select>
                </div>
            
			  
             <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
				
              </div>
			  
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		
		
		<div class="modal fade" id="modal-stu-report">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center">Student Interaction Report</h4>
              </div>
              <div class="modal-body">
			<div class="box" style="border:none; box-shadow:none; margin-bottom:0;">
            <div class="box-body box-profile" style="padding:0 20px;">
              

              <ul class="list-group list-group-bordered">
                <li class="list-group-item">
                  <b>General Awareness</b> <a class="pull-right">65/100</a>
                </li>
                <li class="list-group-item">
                  <b>Mental Ability</b> <a class="pull-right">7/10</a>
                </li>
                <li class="list-group-item">
                  <b>Confidence</b> <a class="pull-right">9/10</a>
                </li>
                <li class="list-group-item">
                  <b>Communication Skills</b> <a class="pull-right">75%</a>
                </li>
                <li class="list-group-item">
                  <b>Personality</b> <a class="pull-right">85%</a>
                </li>
              </ul>

             
            </div>
            <!-- /.box-body -->
          </div>
         
              </div>
			  
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">OK</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		<div class="modal fade" id="modal-parent-report">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center">Parent Interaction Report</h4>
              </div>
              <div class="modal-body">
			<div class="box" style="border:none; box-shadow:none; margin-bottom:0;">
            <div class="box-body box-profile" style="padding:0 20px;">
              
  <h4 class="modal-title pd-bottom-10">Father Interaction Report</h4>
              <ul class="list-group list-group-bordered">
                <li class="list-group-item" style="padding-bottom:30px;">
                  <b>Language:</b>
				 
				   <div class="form-group text-center  ">
						    
							<div class=" ">
							<label class="col-md-3">English </label>
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked>
				  Read
                </label>
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked>
				  Write
                </label>
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked >
                 Speak
                </label>
              </div>	  
                            </div>
							<hr>
							<div class="form-group text-center ">
						    
							<div class="">
						<label class="col-md-3">Hindi </label>	
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked>
				  Read
                </label>
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked>
				  Write
                </label>
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked >
                 Speak
                </label>
              </div>	  
                            </div>
				  
				
                </li>
                <li class="list-group-item">
                  <b>Qualification:</b> BE
                </li>
                <li class="list-group-item">
                  <b>Overall Understanding:</b> 
				  <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </li><li class="list-group-item">
                  <b>Any Other Observation:</b> 
				  <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </li>
                
              </ul>
			  
			  <h4 class="modal-title pd-bottom-10">Mother Interaction Report</h4>
			  <ul class="list-group list-group-bordered">
                <li class="list-group-item" style="padding-bottom:30px;">
                  <b>Language:</b>
				 
				   <div class="form-group text-center  ">
						    
							<div class=" ">
							<label class="col-md-3">English </label>
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked>
				  Read
                </label>
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked>
				  Write
                </label>
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked >
                 Speak
                </label>
              </div>	  
                            </div>
							<hr>
							<div class="form-group text-center ">
						    
							<div class="">
						<label class="col-md-3">Hindi </label>	
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked>
				  Read
                </label>
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked>
				  Write
                </label>
                <label class="col-md-3">
                  <input type="checkbox" class="flat-red" checked >
                 Speak
                </label>
              </div>	  
                            </div>
				  
				
                </li>
                <li class="list-group-item">
                  <b>Qualification:</b> BE
                </li>
                <li class="list-group-item">
                  <b>Overall Understanding:</b> 
				  <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </li><li class="list-group-item">
                  <b>Any Other Observation:</b> 
				  <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </li>
                
              </ul>
              </ul>

             
            </div>
			
			
            <!-- /.box-body -->
          </div>
         
              </div>
			  
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">OK</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		
<script>

$(document).ready(function() {
    var table = $('#example').DataTable( {
       
		lengthChange: false,
		autoWidth : true,
        buttons: [   'csv', 'excel', 'pdf', 'print' ],
		
		
    } );

	 table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
	 
} );

$(document).ready(function() {
    var table2 = $('#example2').DataTable( {
       
		lengthChange: false,
		autoWidth : true,
        buttons: [   'csv', 'excel', 'pdf', 'print' ],
		
		
    } );

	 table2.buttons().container()
        .appendTo( '#example2_wrapper .col-sm-6:eq(0)' );
	 
} );
$(document).ready(function() {
    var table2 = $('#example1').DataTable( {
       
		lengthChange: false,
		autoWidth : true,
        buttons: [   'csv', 'excel', 'pdf', 'print' ],
		
		
    } );

	 table2.buttons().container()
        .appendTo( '#example1_wrapper .col-sm-6:eq(0)' );
	 
} );

$(document).ready(function () {
    $('#selectall').click(function () {
        $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function () {
        var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
        $('#selectall').prop("checked", check);
    });
});


</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
	//Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
	 $('#datepicker2').datepicker({
      autoclose: true
    })
	 $('#datepicker3').datepicker({
      autoclose: true
    }) 
	 $('#datepicker4').datepicker({
      autoclose: true
    }) 
	 $('#datepicker5').datepicker({
      autoclose: true
    }) 
	 $('#datepicker6').datepicker({
      autoclose: true
    }) 
	 $('#datepicker7').datepicker({
      autoclose: true
    }) 
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>


