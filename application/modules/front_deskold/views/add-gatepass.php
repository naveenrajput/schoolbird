
<!-- Content Wrapper. Contains page content -->
<style>

</style>

  <link href="<?php echo base_url();?>assets/dist/css/bootstrap-pincode-input.css" rel="stylesheet">
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Gate Pass </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Dashboard</a></li>
      <li class="active">Gate pass</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row"> 
      
      <!-- /.col -->
      <div class="col-md-12">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#stgatepass" data-toggle="tab">Student Gate Pass</a></li>
            <li><a href="#epgatepass" data-toggle="tab">Employee Gate Pass</a></li>
          </ul>
          <div class="tab-content">
            <div class="active tab-pane" id="stgatepass"> 
                <form role="form">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="">S No. </label>
                        <input type="text" class="form-control"  placeholder="Auto Generated" disabled>
                      </div>
                    </div>
					 <div class="col-md-3">
                      <div class="form-group">
                        <label>Date</label>
                        <div class="input-group date">
                          <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                          <input type="text" class="form-control pull-right" id="datepicker1">
                        </div>
                        <!-- /.input group --> 
                      </div>
                    </div>
					<div class="col-md-3">
                       <div class="bootstrap-timepicker">
                <div class="form-group">
                  <label>Time Out</label>
			
                  <div class="input-group">
				  <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" class="form-control timepicker">

                    
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
                    </div>
					<div class="col-md-3">
                      <div class="form-group">
                        <label for="">Class</label>
                        <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                          <option selected="selected">Select</option>
                          <option value="1">1 </option>
                          <option value="2">2 </option>
                          <option value="3">3 </option>
                          <option value="4">4 </option>
                          <option value="5">5 </option>
                          <option value="6">6 </option>
                          <option value="7">7 </option>
                          <option value="8">8 </option>
                          <option value="9">9 </option>
                          <option value="10">10 </option>
                          <option value="11">11 </option>
                          <option value="12">12 </option>
                          
                        </select>
                      </div>
                    </div>
					<div class="col-md-3">
                      <div class="form-group">
                        <label for="">Section</label>
                        <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                          <option selected="selected">Select</option>
                          <option value="A">A </option>
                          <option value="B">B </option>
                          <option value="C">C </option>
                          <option value="D">D </option>
                          <option value="E">E </option>
                         
                        </select>
                      </div>
                    </div>
					<div class="col-md-6">
                      <div class="form-group">
                        <label for="">Student</label>
                        <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                          <option selected="selected">Select</option>
                          <option value="A">Ab </option>
                          <option value="B">Bv </option>
                          <option value="C">Cf </option>
                          <option value="D">Dd </option>
                          <option value="E">Ew </option>  
						  <option value="D">Ff </option>
                          <option value="E">Egh </option>
                         
                        </select>
                      </div>
                    </div>
                   
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Guardian/ Pick Up Person</label>
                        <input type="text" class="form-control"  placeholder="Guardian Name">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" rows="3" placeholder="Address"></textarea>
                      </div>
                    </div>
					<div class="col-md-6">
                      <div class="form-group">
                        <label>Purpose of Going Out </label>
                        <textarea class="form-control" rows="3" placeholder="Type.."></textarea>
                      </div>
                    </div>
                    
                   
                    </div>
					<div class="row">
					<div class="col-md-6">
                      <div class="form-group">
                        <label>Contact </label>
                        <div class="input-group">
                          <div class="input-group-addon"> <i class="fa fa-phone"></i> </div>
                          <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask>
                        </div>
                        <!-- /.input group --> 
                      </div>
                      <!-- /.form group --> 
                    </div>

                  <div class="col-md-2 text-center">
					    <label>&nbsp; </label>
                      <div class="form-group">
                       <a  class="btn btn-info" data-toggle="modal" data-target="#modal-OTP">Generate OTP</a>
                      </div>
                    </div>
					
					
					</div>
					<div class="row">
					<!--<div class="col-md-4">
					    <label>Enter the OTP </label>
                   <table class=" table" >
				   <tr class="">
				   <td style="border-top:none;"> <input type="password" class="form-control"  placeholder=""></td>
				   <td style="border-top:none;"> <input type="password" class="form-control"  placeholder=""></td>
				   <td style="border-top:none;"> <input type="password" class="form-control"  placeholder=""></td>
				   <td style="border-top:none;"> <input type="password" class="form-control"  placeholder=""></td>
				   </tr>
				   </table>
                    </div>--->
                   <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Vehicle Type </label>
                        <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                          <option selected="selected">Select</option>
                          <option value="Two Wheeler">Two Wheeler </option>
                          <option value="Four Wheeler">Four Wheeler </option>
                          <option value="Other">Other </option>
                        
                         
                        </select>
                      </div>
                    </div>
					 <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Vehicle Number</label>
                        <input type="text" class="form-control"  placeholder="Vehicle Number">
                      </div>
                    </div>
					 <div class="col-md-4 ">
			   <div class="form-group">
			     <label> Document </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="Document">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
                  </div>
                </div>
                <!-- /.box-body -->
                
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="epgatepass"> 
               <form role="form">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="">S No. </label>
                        <input type="text" class="form-control"  placeholder="Auto Generated" disabled>
                      </div>
                    </div>
					 <div class="col-md-4">
                      <div class="form-group">
                        <label>Date</label>
                        <div class="input-group date">
                          <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                          <input type="text" class="form-control pull-right" id="datepicker">
                        </div>
                        <!-- /.input group --> 
                      </div>
                    </div>
					<div class="col-md-4">
                       <div class="bootstrap-timepicker">
                <div class="form-group">
                  <label>Time Out</label>
			
                  <div class="input-group">
				  <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" class="form-control timepicker">

                    
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
                    </div>
					
					<div class="col-md-4">
                      <div class="form-group">
                        <label for="">Department </label>
                        <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                          <option selected="selected">Select</option>
                          <option value="Administration">Administration </option>
                          <option value="School">School </option>
                          
                         
                        </select>
                      </div>
                    </div>
					<div class="col-md-6">
                      <div class="form-group">
                        <label for="">Employee</label>
                        <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                          <option selected="selected">Select</option>
                          <option value="Accounts  ">Accounts   </option>
                          <option value="Admin">Admin  </option>
                        
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Contact </label>
                        <div class="input-group">
                          <div class="input-group-addon"> <i class="fa fa-phone"></i> </div>
                          <input type="text" class="form-control" data-inputmask="&quot;mask&quot;: &quot; 99999-99999&quot;" data-mask="">
                        </div>
                        <!-- /.input group --> 
                      </div>
                      <!-- /.form group --> 
                    </div>
                   
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" rows="3" placeholder="Address"></textarea>
                      </div>
                    </div>
					<div class="col-md-6">
                      <div class="form-group">
                        <label>Purpose of Going Out </label>
                        <textarea class="form-control" rows="3" placeholder="Type.."></textarea>
                      </div>
                    </div>
                    
                   <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Vehicle Type </label>
                        <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                          <option selected="selected">Select</option>
                          <option value="Two Wheeler">Two Wheeler </option>
                          <option value="Four Wheeler">Four Wheeler </option>
                          <option value="Other">Other </option>
                        
                         
                        </select>
                      </div>
                    </div>
					 <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Vehicle Number</label>
                        <input type="text" class="form-control"  placeholder="Vehicle Number">
                      </div>
                    </div>
					
                    <div class="col-md-4 ">
			   <div class="form-group">
			     <label> Document </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="Document">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
                  </div>
                </div>
                <!-- /.box-body -->
                
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
             
            </div>
          </div>
          <!-- /.tab-content --> 
        </div>
        <!-- /.nav-tabs-custom --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
    
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->


<div class="modal fade" id="modal-OTP">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center">OTP</h4>
              </div>
              <div class="modal-body text-center">
        
					  <label for="">Enter the OTP</label>
					<div class="form-group otp-style">
                      
                         <input type="text" id="pincode-input1"  >
                      </div>
					 <a href="#"><small class="">Resend OTP</small></a>
				<div class="pd-top-20"> <button type="button" class="btn btn-primary">Submit</button>	</div>
              </div>
             
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
	//Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

  
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
