
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Visitor Form </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Dashboard</a></li>
      <li class="active">Visitor Form</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row"> 
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="box ">
          <div class="box-header with-border">
            <h3 class="box-title">Add Details</h3>
          </div>
          <!-- /.box-header --> 
          <!-- form start -->
          <form role="form">
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="">Name </label>
                    <input type="text" class="form-control"  placeholder="Name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label> Date </label>
                    <div class="input-group date">
                      <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                      <input type="text" class="form-control pull-right" id="datepicker">
                    </div>
                    <!-- /.input group --> 
                  </div>
                </div>
               <div class="col-md-4">
                       <div class="bootstrap-timepicker">
                <div class="form-group">
                  <label>Time In</label>
			
                  <div class="input-group">
				  <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" class="form-control timepicker">

                    
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
                    </div>
				<div class="col-md-4">
                  <div class="form-group">
                    <label for="">Contact </label>
                    <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask="&quot;mask&quot;: &quot; 99999-99999&quot;" data-mask="">
                </div>
                  </div>
                </div>
				<div class="col-md-4">
                  <div class="form-group">
                    <label for="">Contact Person </label>
                    <input type="text" class="form-control"  placeholder="Name">
                  </div>
                </div>
				<div class="col-md-8">
                  <div class="form-group">
                    <label for="">Purpose Of Meeting </label>
                   <textarea class="form-control" rows="2" placeholder="Type Here"></textarea>
                  </div>
                </div>
              <div class="col-md-4 ">
			   <div class="form-group">
			     <label> Date Of Birth Certificate	 </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
                
              </div>
              
            </div>
            <!-- /.box-body -->
            
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.box --> 
        
        <!-- Form Element sizes --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
    <!-- /.row --> 
    
  </section>
  <!-- /.content --> 
</div>

<!-- page script --> 

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
	//Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

  
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
