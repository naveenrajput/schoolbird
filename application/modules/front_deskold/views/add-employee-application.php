
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Employee Application
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dashboard</a></li>
        <li class="active"> Application</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		  <div class="row">
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Add Employee</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
			  <div class="row">
			   <div class="col-md-2 col-xs-12 imgUp pull-right">
						<div class="imagePreview"></div>
						<label class="btn btn-upload btn-primary">
							Upload<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
							</label>
						</div><!-- col-2 -->
			  <div class="col-md-10 remove-pd">
			  <div class="col-md-6  ">
			  
				<div class="form-group ">
                  <label for="">Application For The Post Of  </label>
                  <input type="text" class="form-control"  placeholder="Post ">
                </div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
                <label>Date Of Admission </label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker5">
                </div>
				
				
                <!-- /.input group -->
              </div>
				</div>
				
				
				
				<div class="col-md-6 ">
			  <div class="form-group">
                  <label for="">Full Name </label>
                  <input type="text" class="form-control"  placeholder="Full Name">
                </div>
			  </div>
			    <div class="col-md-6 ">
				<div class="form-group">
                  <label for="">Father's / Husband's Name </label>
                  <input type="text" class="form-control" placeholder="Father's / Husband's Name  ">
                </div>
				</div>
				
				</div>
			 
						
						</div><!--row-->
						  <div class="row">
			  <div class="col-md-4">
				<div class="form-group">
                <label>Date of Birth</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
                <!-- /.input group -->
              </div>
				</div>
				
				<div class="col-md-4">
			  <div class="form-group">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="Email">
              </div>
                <!-- /.input group -->
              </div>
			  </div>
			  <div class="col-md-4">
			  <div class="form-group">
                <label>Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask>
                </div>
                <!-- /.input group -->
              </div>
			  </div>
			  
			   <div class="col-md-6">
				<div class="form-group">
                  <label>Address For Communication </label>
                  <textarea class="form-control" rows="2" placeholder="Address"></textarea>
                </div>
				</div>
				 <div class="col-md-6">
				<div class="form-group">
                  <label>Permanent Address  </label>
                  <textarea class="form-control" rows="2" placeholder="Address"></textarea>
                </div>
				</div>
				
				<div class="col-md-3">
			  <div class="form-group">
                  <label for="">Marital Status  </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                 <option value="Married">Married</option>
      <option value="Single">Single</option>
      <option value="Divorced">Divorced</option>
      <option value="Widowed">Widowed</option>

                </select>
                </div>
			  </div>
			  
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Occupation Of Spouse  </label>
                  <input type="text" class="form-control"  placeholder="Occupation Of Spouse">
                </div>
			  </div>
				
			  </div>
			<div class="row">
			    <div class="col-md-12 "> <p class="lead">Upload Documents </p></div>
			   
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label> Date Of Birth Certificate	 </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			   <div class="col-md-4 ">
			   <div class="form-group">
			     <label>SSSMID </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			   <div class="col-md-4 ">
			   <div class="form-group">
			     <label>Aadhar Card </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label> Transfer Certificate (Original) </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label>Caste Certificate </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label>Address Proof</label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			 
						
						</div>
						
			  <div class="row">
			    <div class="col-md-12 "> <p class="lead">Children (If Any)  </p></div>
			   
			  <div class="col-md-12 table-responsive">
			  <table class="table table-bordered" id="test-table"  style="margin-bottom:10px;">
				  <tbody id="test-body">
				  <tr  id="row0">
				  <th>Name</th>
					<th>Age</th>
					<th colspan="2">class</th>
				   
				  </tr>
				 
					 <tr >
					<td>
					<div class="form-group">
                
                  <input type="text" class="form-control"  placeholder=" Name">
                </div>
					</td>
					<td>
					<div class="form-group">
              
                  <input type="text" class="form-control"  placeholder="Age">
                </div>
					</td>
					<td>
					<div class="form-group">
                
                  <input type="text" class="form-control"  placeholder="Class">
                </div>
					</td>
					<td>
					
 <button class='delete-row btn btn-primary btn-sm' type='button' >         Delete</button>      
					</td>
				   

				  </tr>
				 

				</tbody></table>
			    <div class="form-group">
          
             
			<button id='add-row' class='btn btn-primary btn-sm' type='button' value='Add' /> Add </button>             
			   </div>
				
				</div>
			 
						
						</div>
						
						
						
						 
				
			  <div class="row">
			    <div class="col-md-12 "> <p class="lead">Sibling's Details  </p></div>
			   <div class="col-md-3">
			  <div class="form-group">
			 
			  <input type="checkbox" class="sibling-hide"   id="myCheck"  onclick="myFunction()">
				   <label>&nbsp; Is Sibling? </label>
				</div>
			  </div>
			  <div class="col-md-9" id="text1" style="display:none">
			  <div class="form-group">
			 
			  <div class="input-group">
                <input type="text" class="form-control"  placeholder="Enter Sibling Registration ID   ">
                    <span class="input-group-btn">
                      <div type="button"  id="sibling" class="btn btn-primary">Search</div>
                    </span>
              </div>
				</div>
			  </div>
			  <div class="col-md-12 table-responsive" id="sibling-details" style="display:none">
			  
				<table class="table fetchdata table-hover table-bordered">
							  <tbody><tr>
							  <th>Name</th>
								<th>Relation</th>
								<th>Age</th>
								<th>Qualification</th>
								<th>Occupation</th>

								<th>Mobile</th>

							   
							  </tr>
						</tbody>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							
						</tr>
						</table>
						  </div>
			  
			  <div class="col-md-12 "> <p class="lead">Bank Details  </p></div>
			<div class="col-md-6">
			
			  <div class="form-group">
                  <label for="">Annual Family Income  </label>
                  <input type="text" class="form-control"  placeholder="Income ">
                </div>
			  </div>
			  
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Name Of Account Holder  </label>
                  <input type="text" class="form-control"  placeholder="Account Holder  ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Bank Account Number  </label>
                  <input type="text" class="form-control"  placeholder="Account Number  ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Bank Name </label>
                  <input type="text" class="form-control"  placeholder="Bank Name ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">IFSC Code  </label>
                  <input type="text" class="form-control"  placeholder="IFSC Code  ">
                </div>
			  </div>
			   <div class="col-md-12"> <p class="lead">Previous Educational Information </p></div>
			
				<div class="col-md-6">
			  <div class="form-group">
                  <label for="">School Name  </label>
                  <input type="text" class="form-control"  placeholder="School Name ">
                </div>
			  </div>
			  <div class="col-md-6">
						
				 <div class="form-group">
                  <label for="">Previous Class  </label>
                  <input type="text" class="form-control"  placeholder="Previous Class ">
                </div>
				</div>
			<div class="col-md-3">
			  <div class="form-group">
                  <label for="">Board	 </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>MP</option>
                  <option>CBSE</option>
                  <option>ISCE</option>
                  <option>Other</option>
				    </select>
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Passed/Appeared	 </label>
                  <input type="text" class="form-control"  placeholder=" ">
                </div>
			  </div>
		<div class="col-md-3">
			  <div class="form-group">
                  <label for="">Year</label>
                  <input type="text" class="form-control"  placeholder="Year ">
                </div>
			  </div>			  
					<div class="col-md-3">
			  <div class="form-group">
                  <label for="">Percentage </label>
                  <input type="text" class="form-control"  placeholder="Percentage ">
                </div>
			  </div>
				<div class="col-md-12 "> <p class="lead">Class Admission Sorted  </p></div>
			   <div class="col-md-3">
			  <div class="form-group">
			
			  <input type="checkbox" class="sibling-hide"   id="myCheck1"  onclick="myFunction1()">
				   <label>&nbsp; Admission Sorted  </label>
				</div>
			  </div>
			  <div class="col-md-9" id="text2" style="display:none">
			  
			   
			  <div class="form-group col-md-4" >
			    <label>Class </label>
                  <div class="input-group">
                <input type="text" class="form-control" placeholder="6th" disabled>
                    <span class="input-group-btn">
                      <div type="button" id="sibling" class="btn btn-primary">Change</div>
                    </span>
              </div>
                </div>
				<div class="form-group col-md-4" >
				 <label>Section </label>
                  <div class="input-group">
                <input type="text" class="form-control" placeholder="B" disabled>
                    <span class="input-group-btn">
                      <div type="button" id="sibling" class="btn btn-primary">Change</div>
                    </span>
              </div>
                </div>
				<div class="form-group col-md-4" >
				 <label>House </label>
                  <div class="input-group">
                <input type="text" class="form-control" placeholder="Red" disabled> 
                    <span class="input-group-btn">
                      <div type="button" id="sibling" class="btn btn-primary">Change</div>
                    </span>
              </div>
                </div>
				 
			 </div>
			  <div class="col-md-12 table-responsive" id="sibling-details" style="display:none">
			  
				<table class="table fetchdata table-hover table-bordered">
							  <tbody><tr>
							  <th>Name</th>
								<th>Relation</th>
								<th>Age</th>
								<th>Qualification</th>
								<th>Occupation</th>

								<th>Mobile</th>

							   
							  </tr>
						</tbody>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							
						</tr>
						</table>
						  </div>
				
				
				</div>
				
			
			  
			
							
					
					 </div>
						 <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>	
                    </div>
              <!-- /.box-body -->

             
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
        

     </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

<!-- page script -->



<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
	//Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
	 $('#datepicker2').datepicker({
      autoclose: true
    })
	 $('#datepicker3').datepicker({
      autoclose: true
    }) 
	 $('#datepicker4').datepicker({
      autoclose: true
    }) 
	 $('#datepicker5').datepicker({
      autoclose: true
    }) 
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
  
  
  // add row 
    // Add row
  var row=1;
  $(document).on("click", "#add-row", function () {
  var new_row = '<tr id="row' + row + '"><td><input name="from_value' + row + '" type="text" class="form-control" /><td><input name="from_value' + row + '" type="text" class="form-control" /></td><td><input name="to_value' + row + '" type="text" class="form-control" /></td><td><input class="delete-row btn btn-primary btn-sm" type="button" value="Delete" /></td></tr>';

  $('#test-body').append(new_row);
  row++;
  return false;
  });
  
  // Remove criterion
  $(document).on("click", ".delete-row", function () {
  //  alert("deleting row#"+row);
    if(row>1) {
      $(this).closest('tr').remove();
      row--;
    }
  return false;
  });

</script>

