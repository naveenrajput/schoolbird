
 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
 <section class=" ">
	<div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Gate Pass
     
      </h1>
	  <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Front Desk</a></li>
        <li class="active">gate pass</li>
      </ol>
	  </div>
	  <div class="col-md-6 col-xs-12 col-sm-4 content-header">
        <a  href="Front_desk/Add_Gate_Pass" class="btn pull-right btn-info"> <i class="fa fa-plus"></i> &nbsp;Generate</a>
	  </div>
    </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        
        <div class="box">
         <div class="box-header with-border mr-top-20 mr-bottom-20 text-center">
			<div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
			<div class="col-md-2" >
			  <button type="submit" class="btn btn-info"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
			  </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive"> 
            <!-- <table id="example" class="display nowrap" style="width:100%">--->
            <table id="example" class="table table-bordered " >
			  <div class="txt-dis">Export in Below Format</div>
              <thead>
                <tr>
                  <th><input type="checkbox" id="selectall">
                    All
                    </input></th>
                  <th>S.no</th>
                  <th>Name</th>
                  <th>Department</th>
				   <th>Class</th>
				    <th>Section</th>
                  <th>Time Out</th>
                  <th>Contact</th>
                  <th>Purpose Of Going Out</th>
                  <th>File</th>
                
                 
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<100;$i++){?>
                <tr>
                  <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
                  <td>1</td>
                  <td>Shikha Yadav</td>
                  <td>Accounts</td>
                  <td>-</td>  
				  <td>-</td>  
				  <td>1:00 PM</td>
                  <td>9087654321</td>
                  <td>Family Function</td>
                  <td>Doc</td>
                 
                  
                  <td><ul class="table-icons">
                   
                      <li><a href="" class="table-icon" title="Delete"> <span class="glyphicon glyphicon-trash display-icon"></span> </a></li>
                     
                      <li><a href="" class="table-icon" title="Print"> <span class="glyphicon glyphicon-print display-icon"></span> </a></li>
                    </ul></td>
                </tr>
                <?php }?>
              </tbody>
              <tfoot>
                <tr>
                  <th><input type="checkbox" id="selectall">
                    All
                    </input></th>
                  <th>S.no</th>
                  <th>Child Name</th>
                  <th>Father's Name</th>
                  <th>Class</th>
                  <th>Section</th>
                  <th>Date</th>
                  <th>Updated By</th>
                  <th>Forward</th>
                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body --> 
        </div>
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>

<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        lengthChange: false,
		autoWidth : true,
        buttons: [   'csv', 'excel', 'pdf', 'print' ],
		
		
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
		
} );


$(document).ready(function () {
    $('#selectall').click(function () {
        $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function () {
        var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
        $('#selectall').prop("checked", check);
    });
});


</script> 
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
//Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script> 
