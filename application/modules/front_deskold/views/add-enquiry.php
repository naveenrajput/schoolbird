
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Enquiry Form
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dashboard</a></li>
        <li class="active">Enquiry Form</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		  <div class="row">
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Add Enquiry</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
			  <div class="row">
			  <div class="col-md-5">
			  <div class="form-group">
                  <label for="">Child's Name </label>
                  <input type="text" class="form-control"  placeholder="Child's Name">
                </div>
			  </div>
			    <div class="col-md-5">
				<div class="form-group">
                <label>Date of Birth</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
                <!-- /.input group -->
              </div>
				</div>
				
			  <div class="col-md-2 col-xs-12 imgUp pull-right">
						<div class="imagePreview"></div>
						<label class="btn btn-upload btn-primary">
							Child's Photo<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
							</label>
						</div>
						<div class="col-md-4">
			  <div class="form-group">
                  <label for="">Gender </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>Male</option>
                  <option>Female</option>

                </select>
                </div>
			  </div>
						<div class="col-md-4">
				<div class="form-group">
                  <label for="">Category   </label>
                 <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
               
                  <option>General</option>
                  <option>OBC</option>
                  <option>ST</option>
                  <option>SC</option>
                  <option>None</option>
                
                </select>
                </div>
				</div>
				
				<div class="col-md-2">
				<div class="form-group">
                  <label for="">Want Admission to Class   </label>
                 <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
               
                  <option>KG-1</option>
                  <option>KG-2</option>
                   <option>1</option>
                   <option>2</option>
                   <option>3</option>
                   <option>4</option>
                   <option>5</option>
                   <option>6</option>
                   <option>7</option>
                   <option>8</option>
                   <option>9</option>
                   <option>10</option>
                   <option>11</option>
                   <option>12</option>
                 
                
                </select>
                </div>
				</div>
						
						
				</div><!---/row---->
				
				
					<div class="row">
					<div class="col-md-12 "> <p class="lead">Family Details  </p></div>
			  <div class="col-md-6">
			  <div class="form-group">
                  <label for="">Father's Name </label>
                  <input type="text" class="form-control"  placeholder="Father's Name">
                </div>
			  </div>
			    <div class="col-md-6">
				<div class="form-group">
                  <label for="">Father's Profession </label>
                  <input type="text" class="form-control" placeholder="Father's Profession ">
                </div>
				</div>
				
				<div class="col-md-6">
			  <div class="form-group">
                  <label for="">Mother's Name </label>
                  <input type="text" class="form-control"  placeholder="Mother's Name">
                </div>
			  </div>
			    <div class="col-md-6">
				<div class="form-group">
                  <label for="">Mother's Profession </label>
                  <input type="text" class="form-control" placeholder="Mother's Profession ">
                </div>
				</div>
				 
				
				<div class="col-md-4">
			 <div class="form-group">
                <label>Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
			  </div>
			    <div class="col-md-4">
				<div class="form-group">
                <label>Alternate Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask>
                </div>
                <!-- /.input group -->
              </div>
				</div>
				
				<div class="col-md-4">
			 <div class="form-group">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="Email">
              </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
			  </div>
			    
				 <div class="col-md-12">
				<div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" rows="3" placeholder="Address"></textarea>
                </div>
				</div>
				 </div>
				 
				  <div class="row">
				<div class="col-md-12 "> <p class="lead">Special Ability</p></div>
					<div class="col-md-3">
			  <div class="form-group">
			
			  <input type="checkbox" class="sibling-hide" id="myCheck3" onclick="myFunction3()">
				   <label>&nbsp; Special Ability  </label>
				</div>
			  </div>
			  <div class="col-md-9" id="text3" style="display: none;">
			  
			   
			  <div class="form-group col-md-6">
			    <label>Select  </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>Type 1</option>
                  <option>Type 2</option>
                  <option>Type 3</option>
                  <option>Type 4</option>
                  <option>Other</option>

                </select>
                </div>
				
				<div class="form-group col-md-6">
			     <label>Certificate </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="Certificate">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
  
  <div class="form-group col-md-12">
    <label for="">Remarks </label>
             
  <textarea class="form-control" rows="3" placeholder="Type Here.." spellcheck="false"></textarea>
  </div>
				 
			 </div>
				
				</div>
				 <div class="row">
				
				
				<div class="col-md-12"> <p class="lead">Previous School Information </p></div>
			  
				
				<div class="col-md-4">
			  <div class="form-group">
                  <label for="">Current School’s Name </label>
                  <input type="text" class="form-control"  placeholder="School’s Name">
                </div>
			  </div>
			  <div class="col-md-4">
				<div class="form-group">
                  <label for="">Studying in Class  </label>
                 <select class="form-control select" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
               
                  <option>KG-1</option>
                  <option>KG-2</option>
                   <option>1</option>
                   <option>2</option>
                   <option>3</option>
                   <option>4</option>
                   <option>5</option>
                   <option>6</option>
                   <option>7</option>
                   <option>8</option>
                   <option>9</option>
                   <option>10</option>
                   <option>11</option>
                   <option>12</option>
                 
                
                </select>
                </div>
				</div>
				<div class="col-md-4">
			  <div class="form-group">
                  <label for=""> Board 	 </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>MP</option>
                  <option>CBSE</option>
                  <option>ISCE</option>
                  <option>Other</option>
				    </select>
                </div>
			  </div>
			  	<div class="col-md-12"> <p class="lead"> <small class="s-name" style="font-size:14px;">School Name</small> Information </p></div>
			  
			  <div class="col-md-6">
				<div class="form-group">
                  <label for="">How did you come to know about us?    </label>
                 <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  	<option value="Paper Advertisement">Paper Advertisement </option>
			<option value="Hoarding">Hoarding</option>
			<option value="Existing parents">Existing parents</option>
			<option value="Friends">Friends</option>
			<option value="Internet Search">Internet Search</option>
			<option value="others">Others</option>
                
                </select>
                </div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
                  <label for="">Your Expectation from <small class="s-name">School Name</small></label>
                 <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option value="Good Infrastructure">Good Infrastructure </option>
			<option value="Extracurricular Activities">Extracurricular Activities</option>
			<option value="Sports">Sports</option>
			<option value="Well balanced curriculum">Well balanced curriculum</option>
			<option value="others">Others</option>
                
                </select>
                </div>
				</div>
				
			  
				<div class="col-md-12">
				<div class="form-group">
                  <label>Counselor’s Remarks </label>
                  <textarea class="form-control" rows="3" placeholder="Counselor’s Remarks "></textarea>
                </div>
				</div>
				
				
				
				
			  </div>
                
                
              
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
        


        </div>
      <!-- /.box -->

     </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

<!-- page script -->



<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
	//Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

  
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

