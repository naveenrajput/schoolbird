
<style>
 .box-section{
	 
	 display: inline-block;
    font-size: 16px;
    margin:10px 0;
    line-height: 1;
	font-weight: 700;
}
.border-dotted{border-bottom: 1px dotted #f4f4f4;padding:0 20px;
}


.imagePreview {
    width: 100%;
    height: 140px;
    background-position: center center;
  background:url(http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg);
  background-color:#fff;
    background-size: cover;
  background-repeat:no-repeat;
    display: inline-block;
  box-shadow:0px -2px 2px 2px rgba(0,0,0,0.2);
}
.btn-upload
{
  display:block;
  border-radius:0px;
  box-shadow:0px 2px 2px 1px rgba(0,0,0,0.2);
  margin-top:-5px;
}
.imgUp
{
  margin-bottom:15px;
}
.del
{
  position:absolute;
  top:0px;
  right:15px;
  width:30px;
  height:30px;
  text-align:center;
  line-height:30px;
  background-color:rgba(255,255,255,0.3);
  cursor:pointer;
}
.remove-pd{padding:0;}
</style>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Enquiry Form
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dashboard</a></li>
        <li class="active">Enquiry Form</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		  <div class="row">
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Add Enquiry</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
			  <div class="row">
			   <div class="col-md-2 col-xs-12 imgUp pull-right">
						<div class="imagePreview"></div>
						<label class="btn btn-upload btn-primary">
							Upload<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
							</label>
						</div><!-- col-2 -->
			  <div class="col-md-10 remove-pd">
			  <div class="col-md-12  ">
			  
				<div class="form-group ">
                  <label for="">For Academic Year </label>
                 <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>2018-19</option>
                  <option>2019-20</option>

                </select>
                </div>
				</div>
				<div class="col-md-6 ">
			  <div class="form-group">
                  <label for="">First Name </label>
                  <input type="text" class="form-control"  placeholder="First Name">
                </div>
			  </div>
			    <div class="col-md-6 ">
				<div class="form-group">
                  <label for="">Last Name </label>
                  <input type="text" class="form-control" placeholder="Last Name ">
                </div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
                <label>Date of Birth</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
                <!-- /.input group -->
              </div>
				</div>
				
				<div class="col-md-6">
				<div class="form-group">
                <label>Date Of Admission </label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1">
                </div>
                <!-- /.input group -->
              </div>
				</div>
				</div>
			 
						
						</div><!--row-->
						  <div class="row">
			  
				
				
			    
				  <div class="col-md-2">
			  <div class="form-group">
                  <label for="">Gender </label>
                  <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>Male</option>
                  <option>Female</option>

                </select>
                </div>
			  </div>
			    <div class="col-md-2">
			  <div class="form-group">
                  <label for="">Caste  </label>
                  <input type="text" class="form-control"  placeholder="Caste ">
                </div>
			  </div>
			    <div class="col-md-2">
			  <div class="form-group">
                  <label for="">Religion </label>
                  <input type="text" class="form-control"  placeholder="Religion">
                </div>
			  </div>
			    <div class="col-md-2">
			  <div class="form-group">
                  <label for="">Nationality  </label>
                  <input type="text" class="form-control"  placeholder="Nationality ">
                </div>
			  </div>
			    <div class="col-md-2">
			  <div class="form-group">
                  <label for="">Mother Tongue </label>
                  <input type="text" class="form-control"  placeholder="Mother Tongue ">
                </div>
			  </div>
			    <div class="col-md-2">
			  <div class="form-group">
                  <label for="">Student Category  </label>
                 <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>General</option>
                  <option>OBC</option>
                  <option>ST</option>
                  <option>SC</option>
                  <option>None</option>

                </select>
                </div>
			  </div>
				<div class="col-md-4">
			 <div class="form-group">
                <label>Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
			  </div>
			    <div class="col-md-4">
				<div class="form-group">
                <label>Alternate Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask>
                </div>
                <!-- /.input group -->
              </div>
				</div>
				
				<div class="col-md-4">
			 <div class="form-group">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="Email">
              </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
			  </div>
			    
				 <div class="col-md-12">
				<div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" rows="3" placeholder="Address"></textarea>
                </div>
				</div>
				 
				<div class="col-md-6">
			  <div class="form-group">
                  <label for="">Municipal Corporation Ward No.  </label>
                  <input type="text" class="form-control"  placeholder="Ward No. ">
                </div>
			  </div>
			<div class="col-md-6">
			  <div class="form-group">
                  <label for="">Annual Family Income  </label>
                  <input type="text" class="form-control"  placeholder="Income ">
                </div>
			  </div>
			  
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Name Of Account Holder  </label>
                  <input type="text" class="form-control"  placeholder="Account Holder  ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Bank Account Number  </label>
                  <input type="text" class="form-control"  placeholder="Account Number  ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Bank Name </label>
                  <input type="text" class="form-control"  placeholder="Bank Name ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">IFSC Code  </label>
                  <input type="text" class="form-control"  placeholder="IFSC Code  ">
                </div>
			  </div>
			  <div class="col-md-6">
			  <div class="form-group">
                  <label for="">Aadhar Card</label>
                  <input type="text" class="form-control"  placeholder="Aadhar Card ">
                </div>
			  </div>
			<div class="col-md-6">
			  <div class="form-group">
                  <label for="">Samagrah Id </label>
                  <input type="text" class="form-control"  placeholder="Samagrah Id  ">
                </div>
			  </div>
			<div class="col-md-12"> <p class="lead">Family Details  </p></div>
			<div class="col-md-6">
			  <div class="form-group">
                  <label for="">Father's Name</label>
                  <input type="text" class="form-control"  placeholder="Father's Name ">
                </div>
			  </div>
			  <div class="col-md-6">
			  <div class="form-group">
                  <label for="">Father's Profession </label>
                  <input type="text" class="form-control"  placeholder="Father's Profession  ">
                </div>
			  </div>
			  <div class="col-md-6">
			  <div class="form-group">
                  <label for="">Mother's Name </label>
                  <input type="text" class="form-control"  placeholder="Mother's Name  ">
                </div>
			  </div>
			  
			  <div class="col-md-6">
			  <div class="form-group">
                  <label for="">Mother's Profession </label>
                  <input type="text" class="form-control"  placeholder="Mother's Profession ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
			 
			  <input type="checkbox" class="sibling-hide"   id="myCheck"  onclick="myFunction()">
				   <label>&nbsp; Is Sibling? </label>
				</div>
			  </div>
			  <div class="col-md-9" id="text1" style="display:none">
			  <div class="form-group">
			 
			  <div class="input-group">
                <input type="text" class="form-control"  placeholder="Enter Sibling Registration ID   ">
                    <span class="input-group-btn">
                      <div type="button"  id="sibling" class="btn btn-primary">Search</div>
                    </span>
              </div>
				</div>
			  </div>
			  <div class="col-md-12 table-responsive" id="sibling-details" style="display:none">
			  
				<table class="table fetchdata table-hover table-bordered">
							  <tbody><tr>
							  <th>Name</th>
								<th>Relation</th>
								<th>Age</th>
								<th>Qualification</th>
								<th>Occupation</th>

								<th>Mobile</th>

							   
							  </tr>
						</tbody>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							
						</tr>
						</table>
						  </div>
							
			<div class="col-md-12"> <p class="lead">Previous Educational Information </p></div>
			
				<div class="col-md-6">
			  <div class="form-group">
                  <label for="">School Name  </label>
                  <input type="text" class="form-control"  placeholder="School Name ">
                </div>
			  </div>
			  <div class="col-md-6">
						
				 <div class="form-group">
                  <label for="">Previous Class  </label>
                  <input type="text" class="form-control"  placeholder="Previous Class ">
                </div>
				</div>
			<div class="col-md-3">
			  <div class="form-group">
                  <label for="">Board	 </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  <option>MP</option>
                  <option>CBSE</option>
                  <option>ISCE</option>
                  <option>Other</option>
				    </select>
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Passed/Appeared	 </label>
                  <input type="text" class="form-control"  placeholder=" ">
                </div>
			  </div>
		<div class="col-md-3">
			  <div class="form-group">
                  <label for="">Year</label>
                  <input type="text" class="form-control"  placeholder="Year ">
                </div>
			  </div>			  
					<div class="col-md-3">
			  <div class="form-group">
                  <label for="">Percentage </label>
                  <input type="text" class="form-control"  placeholder="Percentage ">
                </div>
			  </div>		
					 </div>
							
                    </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
        
        </div>
      <!-- /.box -->

     </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

<!-- page script -->



<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
	//Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

  
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
  
  
</script>
<script>
function myFunction() {
  var checkBox = document.getElementById("myCheck");
  var text = document.getElementById("text1");
  if (checkBox.checked == true){
    text1.style.display = "block";
  } else {
     text1.style.display = "none";
  }
}

</script>
<script>
$(document).ready(function(){
 
  $("#sibling").click(function(){
    $("#sibling-details").show();
  });
  $(".sibling-hide").click(function(){
    $("#sibling-details").hide();
  });
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>

$(document).on("click", "i.del" , function() {
	$(this).parent().remove();
});
$(function() {
    $(document).on("change",".uploadFile", function()
    {
    		var uploadFile = $(this);
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url("+this.result+")");
            }
        }
      
    });
});
</script>