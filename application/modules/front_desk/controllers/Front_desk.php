<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_desk extends MY_Controller {

	
	public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->helper('email');
        $this->load->model('Frontdesk_model');
        $this->load->helper('url');
        $this->load->helper('MY_helper.php');
        $this->load->helper('directory');
        $this->load->library('email');
    }
	//Controller for view all type of status data on Enquiry Section
	public function Enquiry()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$stud_enq_data=$this->Frontdesk_model->enquiry_view();
		$close_data=$this->Frontdesk_model->close_view();
		$hold_data=$this->Frontdesk_model->hold_view();
		$follow_data=$this->Frontdesk_model->followup_view();
		//print_r($follow_data);die;
		$this->load->view('enquiry',compact('stud_enq_data','close_data','hold_data','follow_data'));
		$this->load->view('../../inc/footer');
	}
	/*public function Search_Enquiry()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$start=$this->input->post('start_date');
		$end=$this->input->post('end_date');
		$stud_enq_data=$this->Frontdesk_model->Search_enquiry($start,$end);
		$hold_data=$this->Frontdesk_model->Search_holded($start,$end);
		$close_data=$this->Frontdesk_model->Search_closed($start,$end);
		//print_r($stud_enq_data);die;
		$this->load->view('enquiry',compact('stud_enq_data','close_data','hold_data'));
		$this->load->view('../../inc/footer');
	}*/
	//controller for Show the Hold section
	public function Hold_Enquiry()
	{
		$id= $_POST ['enquiry_id'];
		$this->load->view('holdstatus',compact('id'));
	}
	//controller for Show the Unhold section
	public function Unhold_Enquiry()
	{
		$id= $_POST ['enquiry_id'];
		$this->load->view('unholdstatus',compact('id'));
	}
	//controller for Show the close section
	public function Close_Enquiry()
	{
		$id= $_POST ['enquiry_id'];
		$this->load->view('closestatus',compact('id'));
	}
	//controller for Show the Interaction section
	public function View_interaction()
	{
		$id= $_POST ['enquiry_id'];
		$emp_name=$this->Frontdesk_model->show_empname();
		$this->load->view('view-interaction',compact('id','emp_name'));
	}
	//controller for change status and insert interaction data
	public function Send_Interaction()
	{
		$data=$this->input->post();
		//print_r($data);die;
		$id=$data['enquiry_id'];
		$status = array('status' => $this->input->post('status'),
						'contactperson_id' => $this->input->post('name'),
						'is_delete' =>'2',
					);
		$followup= array(
						'enquiry_id' =>$id,
						'interact_date' => $this->input->post('date'),
						'time' => $this->input->post('time'),
						'comments' => $this->input->post('comments'),
						'createdate' =>date('Y-m-d'),
					);
		$int_data= array(
						'enquiry_id' =>$id,
						//'interact_date' => $this->input->post('date'),
					);
		$this->Frontdesk_model->Interaction_send($status,$id,$int_data,$followup);
		redirect('enquiry');
	}
	//controller for Show the Review section
	public function Review()
	{
		$id= $_POST ['enquiry_id'];
		$emp_name=$this->Frontdesk_model->show_empname();
		$this->load->view('add-review',compact('id','emp_name'));
	}
	//controller for change status and insert Review data
	public function Add_Review()
	{
		$id=$this->input->post('contactperson_id');
		$contact=$this->input->post('person_id');
		$mail=trim($this->input->post('contperson_id'));

		if (!empty($mail) && isset($mail)) {
			//set config for sending email 
	        $email = $this->input->post('email');
	        $message=$this->input->post('mailcomments');
	        $this->email->from('badalmiroliya19@gmail.com', 'Badal');
	        $this->email->to($email);
	        $this->email->subject('Send Email');
	        $this->email->message($message);
	        $this->email->send();

			$id=$this->input->post('enquiry_id');

			$status = array('status' => $this->input->post('status'),
							'contactperson_id' => $this->input->post('contperson_id'),
							'contact_email' =>$email,
							'is_delete' =>'1',
						);
			$review_data= array('enquiry_id' => $this->input->post('enquiry_id'),
							'review_comments' => $this->input->post('mailcomments'),
						);

			$this->Frontdesk_model->Addreviewcomments($id,$status,$review_data);
	        //print_r($em);die;
			redirect('enquiry');

		}elseif(!empty($contact) && isset($contact)){
			
			$data=$this->input->post();
			$id=$data['enquiry_id'];
			$con=trim($this->input->post('contact'));
			$usermessage=trim($this->input->post('mescomments'));
			print_r($con);
			print_r($usermessage);die;
			//Code For sending sms 
			//$sending = http_post("your_domain", 80, "/sendsms", array("Username" => $uname, "PIN" => $password, "SendTo" => $con, "Message" => $usermessage));

			$status = array('status' => $this->input->post('status'),
							'contactperson_id' => $this->input->post('person_id'),
							'contact_number' =>$con,
							'is_delete' =>'1',
						);
			$review_data= array('enquiry_id' => $this->input->post('enquiry_id'),
							'review_comments' => $usermessage,
						);
			$this->Frontdesk_model->Addreviewcomments($id,$status,$review_data);
			redirect('enquiry');
		}
		elseif(!empty($id) && isset($id)){
			$data=$this->input->post();
			$id=$data['enquiry_id'];
			$status = array('status' => $this->input->post('status'),
							'contactperson_id' => $this->input->post('contactperson_id'),
							'is_delete' =>'1',
						);
			$review_data= array('enquiry_id' => $this->input->post('enquiry_id'),
							'review_comments' => $this->input->post('review_comments'),
						);
			$this->Frontdesk_model->Addreviewcomments($id,$status,$review_data);
			redirect('enquiry');
		}else{
			redirect('enquiry');
		}		
	}
	//controller for detact number by employee name
	public function Detactnumber()
	{
		$id= $_POST ['c_id'];
		$number=$this->Frontdesk_model->Select_number($id);
		echo trim($number);
		//print_r($number);die;
		//$this->load->view('Detact-number',compact('number'));    
	}
	//controller for detact email by employee name
	public function Detactemail()
	{
		$id= $_POST ['c_id'];
		$email=$this->Frontdesk_model->Select_email($id);
		echo $email;
		//$this->load->view('Detact-number',compact('number'));
	}
	//controller for change status
	public function Status_change()
	{
		$data=$this->input->post();
		$id=$data['enquiry_id'];
		unset($data['comments']);
		$followup=array(
			'enquiry_id'=>$this->input->post('enquiry_id'),
			'comments'=>$this->input->post('comments'),
		);
		//print_r($data);die;
		$result=$this->Frontdesk_model->Change_status($data,$id,$followup);
		redirect('enquiry');
	}
	// For frontdask Add_Enquiry
	public function Add_Enquiry()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add-enquiry');
		$this->load->view('../../inc/footer');
	}
	//to check validation and insert new data in admissionenquiry table 
	public function Insert_Enquiry()
	{
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		$this->form_validation->set_rules('childname', 'childname', 'required');
		$this->form_validation->set_rules('fathername', 'fathername', 'required');
		$this->form_validation->set_rules('fatherprofession', 'fatherprofession', 'required');
		$this->form_validation->set_rules('mothername', 'mothername', 'required');
		$this->form_validation->set_rules('motherprofession', 'motherprofession', 'required');
		$this->form_validation->set_rules('dob', 'dob', 'required');
		$this->form_validation->set_rules('gender', 'gender', 'required');
		$this->form_validation->set_rules('category', 'category', 'required');
		$this->form_validation->set_rules('class', 'class', 'required');
		$this->form_validation->set_rules('contact', 'contact', 'required');
		$this->form_validation->set_rules('alternatecontact', 'alternatecontact', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('address', 'address', 'required');
		$this->form_validation->set_rules('know_aboutus', 'know_aboutus', 'required');
		$this->form_validation->set_rules('expectation', 'expectation', 'required');
		$this->form_validation->set_rules('studying_class', 'studying_class', 'required');
		$this->form_validation->set_rules('current_school', 'current_school', 'required');
		$this->form_validation->set_rules('board', 'board', 'required');
		$this->form_validation->set_rules('comments', 'comments', 'required');
	
		if ( $this->form_validation->run() == true ){
			$query=$this->db->query("SELECT enquiry_id FROM academicyear WHERE id=1")->row();
			$abc=($query->enquiry_id)+1;
			$LastReceiptID=sprintf("%04s",$abc);
			$query=$this->db->query("UPDATE academicyear SET enquiry_id = '$LastReceiptID' WHERE id=1 ");

			$config['upload_path']          = './uploads';
			$config['allowed_types']        = 'gif|jpg|png';

			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('childphoto') )
            {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('../../inc/header');
				$this->load->view('../../inc/sidebar');
				$this->load->view('add-enquiry', $error);
				$this->load->view('../../inc/footer');                    
            }
            else
            {
				 $data = array('upload_data' => $this->upload->data());
				 $u_data =$this->upload->data();
		         $path=$u_data['file_name'];
		         $config['upload_path']          = './upload';
				 $config['allowed_types']        = 'gif|jpg|png';
				 $this->load->library('upload', $config);
		         $this->upload->do_upload('certificate');
				 $certificate_data=$this->upload->data();
		         $certificate=$certificate_data['file_name'];
		        
				$acad_yr=$this->input->post('academicyear');

				$followup=array(
					'enquiry_id'=>$acad_yr.$LastReceiptID,
					'comments'=>$this->input->post('comments')
				);
				$u_data =$this->upload->data();
				$data=$this->input->post();
				$data['enquiry_id']=$acad_yr.$LastReceiptID;
				//$followup['enquiry_id']=$acad_yr.$LastReceiptID;
				$data['childphoto']=$path;
				$data['certificate']=$certificate;
				unset($data['comments']);
				$this->Frontdesk_model->Insertenquiry($data,$followup);
				redirect('enquiry');
			}
		}else{
			$this->load->view('../../inc/header');
			$this->load->view('../../inc/sidebar');
			$this->load->view('add-enquiry');
			$this->load->view('../../inc/footer');
		}
	}
	//set remainder for enquiry section
	public function Reminder(){
		$id= $_POST ['enquiry_id'];
		//print_r($id);die;
		$result= $this->Frontdesk_model->Comments($id);
		$this->load->view('add-reminder',compact('result'));
	}
	//set remainder for Interaction report section
	public function Int_reminder(){
		$id= $_POST ['enquiry_id'];
		$result= $this->Frontdesk_model->Int_comments($id);
		$this->load->view('add-reminder',compact('result'));
	}
	//Add Reminder for Enquiry purpose
	public function Add_Reminder()
	{
		/*$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		$this->form_validation->set_rules('nextfollowup_date', 'Date', 'required');*/
		$check=$this->input->post('status');
		//print_r($status);die;
		if ($check =='INTERACTION'){
			$data = $this->input->post();
			//print_r($data);die;
			$id=$data['enquiry_id'];
			$data['interact_date']=$data['nextfollowup_date'];
			unset($data['status']);
			$status = array('status' =>$this->input->post('status'),
						);
			$int_date = array('interact_date' =>$this->input->post('nextfollowup_date'),
						);
			//print_r($data);die;
			$this->Frontdesk_model->Add_comments($status,$id,$data,$int_date);
			redirect('Interaction');
		}else{
			$data = $this->input->post();
			$id=$data['enquiry_id'];
			unset($data['status']);
			$status = array('status' =>'FOLLOWUP',
						);
			$this->Frontdesk_model->Add_comments($status,$id,$data);
			redirect('enquiry');
		}
	}
	//get edit_id id to edit a row in admissionenquiry table
	public function Edit_Enquiry($edit_id)
	{
		$edit_id=base64_decode($edit_id);
		$enquirydata=$this->Frontdesk_model->Eeditenquiry($edit_id);
		//print_r($enquirydata);die;
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('edit-enquiry',compact('enquirydata'));
		$this->load->view('../../inc/footer');
	}
	//to update Enquiry data
	public function Update_Enquiry()
	{
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		$this->form_validation->set_rules('childname', 'childname', 'required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('fathername', 'fathername', 'required');
		$this->form_validation->set_rules('fatherprofession', 'fatherprofession', 'required');
		$this->form_validation->set_rules('mothername', 'mothername', 'required');
		$this->form_validation->set_rules('motherprofession', 'motherprofession', 'required');
		$this->form_validation->set_rules('dob', 'dob', 'required');
		$this->form_validation->set_rules('gender', 'gender', 'required');
		$this->form_validation->set_rules('category', 'category', 'required');
		$this->form_validation->set_rules('class', 'class', 'required');
		$this->form_validation->set_rules('contact', 'contact', 'required');
		$this->form_validation->set_rules('alternatecontact', 'alternatecontact', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('address', 'address', 'required');
		$this->form_validation->set_rules('know_aboutus', 'know_aboutus', 'required');
		$this->form_validation->set_rules('expectation', 'expectation', 'required');
		$this->form_validation->set_rules('studying_class', 'studying_class', 'required');
		$this->form_validation->set_rules('current_school', 'current_school', 'required');
		$this->form_validation->set_rules('board', 'board', 'required');
		$data=$this->input->post();
		
		if ($this->form_validation->run() == true){
				$config['upload_path']          = './uploads';
				$config['allowed_types']        = 'gif|jpg|png';

				$this->load->library('upload', $config);
				$this->upload->do_upload('childphoto');
				 $data = array('upload_data' => $this->upload->data());
				 $u_data =$this->upload->data();
		         $path=$u_data['file_name'];
		         $config['upload_path']          = './upload';
				 $config['allowed_types']        = 'gif|jpg|png';
				 $this->load->library('upload', $config);
		         $this->upload->do_upload('certificate');
				 $certificate_data=$this->upload->data();
		         $certificate=$certificate_data['file_name'];
		         
			$data=$this->input->post();
			unset($data['comments']);
			$u_id=$data['update_id'];
	        unset($data['update_id']);
	        $data['childphoto']=$path;
			$data['certificate']=$certificate;
			$this->Frontdesk_model->Updateenquiry($data,$u_id,$followup);
			redirect('enquiry');
		}else{
			$this->load->view('../../inc/header');
			$this->load->view('../../inc/sidebar');
			if (!empty($data)) {
				$data=$this->input->post();
				$u_id=$data['update_id'];
				$this->load->view('edit-enquiry',compact('u_id'));
				$this->load->view('../../inc/footer');
			}else{
				redirect('enquiry');
			}		
		}
	}
	//For frontdask Docs
	public function docs(){
	$this->load->view('../../inc/header');
	$this->load->view('../../inc/sidebar');
    $data['folders'] = directory_map('files');
    $data['contenido'] = "index";
    $this->load->view('front-desk-docs',$data);
    $this->load->view('../../inc/footer');
		
	}
	//to move directry file
	public function moveDocs()
	{
		$baseUrl = base_url();
		
		$source_file = pathinfo($this->input->post('file'));
		$target_dir  = $this->input->post('folder');

		$Src_path = str_replace($baseUrl, '',$source_file['dirname']); 
		$src_abs_path = str_replace('/', '\\', $Src_path); 
		$srcFileName =  $src_abs_path.'\\'.$source_file['basename'];
		$targFileName = '.\files\\'.$target_dir.'\\'.$source_file['basename'];
		copy($srcFileName, $targFileName);
		unlink($srcFileName);
	}
    //To show frontdask Interaction Reports
	public function Reports()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$interact_data=$this->Frontdesk_model->Todays_Interaction();
		$Pending_int_data=$this->Frontdesk_model->Pending_Interaction();
		$this->load->view('interaction',compact('interact_data','Pending_int_data'));
		$this->load->view('../../inc/footer');
	}
	//Open Interaction form
	public function Add_Interaction($int_id)
	{
		$int_id=base64_decode($int_id);
		$int_data=$this->Frontdesk_model->AddIntraction($int_id);
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('open-interaction',compact('int_data'));
		$this->load->view('../../inc/footer');
	}
	//Add or update interaction form
	public function Insert_Interaction()
	{
		$approval=$this->input->post('approval');
		
		if (isset($approval) && !empty($approval)){
			//print_r($approval);die;
			$data=$this->input->post();
			$status=array(
				'status'=>'APPROVAL',
			);
			$id=$data['enquiry_id'];
			unset($data['enquiry_id']);
			//print_r($data);die;
			$this->Frontdesk_model->InsertIntraction($data,$id,$status);
			redirect('Interaction');
		}else{
			$data=$this->input->post();
			$status=array(
				'status'=>'ADMISSION',
			);
			$id=$data['enquiry_id'];
			unset($data['enquiry_id']);
			unset($data['approval']);
			$this->Frontdesk_model->InsertIntraction($data,$id,$status);
			redirect('Interaction');
		}
	}
	// For frontdask Send_Approval
	public function Send_Approval()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('send_approval');
		$this->load->view('../../inc/footer');
	}
	
	// For frontdask Admit_Child
	public function Admit_Child ()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('admit_child');
		$this->load->view('../../inc/footer');
	}
    // For frontdask Admission
	public function Admission()
	{	
		$fromenquiry=$this->Frontdesk_model->fromenquiry();
		$frominteraction=$this->Frontdesk_model->frominteraction();
		$forwardadmission=$this->Frontdesk_model->forwardadmission();
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('admission',compact('fromenquiry','frominteraction','forwardadmission'));
		$this->load->view('../../inc/footer');
	}
	//To insert new student admission data
	public function Stud_Admission()
	{
		$config['upload_path']          = './students';
		$config['allowed_types']        = 'gif|jpg|png';
		$this->load->library('upload', $config);

		$this->upload->do_upload('stud_pic');
		$stud_pic=$this->upload->data('file_name');
		
		$this->upload->do_upload('certificate');
		$certificate=$this->upload->data('file_name');

		$this->upload->do_upload('birthcerti');
		$birthcerti=$this->upload->data('file_name');

		$this->upload->do_upload('SSSMID');
		$SSSMID=$this->upload->data('file_name');

		$this->upload->do_upload('aadharcard');
		$aadharcard=$this->upload->data('file_name');

		$this->upload->do_upload('TC');
		$TC=$this->upload->data('file_name');

		$this->upload->do_upload('castecerti');
		$castecerti=$this->upload->data('file_name');		

		$this->upload->do_upload('addressproof');
		$addressproof=$this->upload->data('file_name');
		
		$this->upload->do_upload('f_pic');
		$f_pic=$this->upload->data('file_name');

		$this->upload->do_upload('f_idproof');
		$f_idproof=$this->upload->data('file_name');
		
		$this->upload->do_upload('m_pic');
		$m_pic=$this->upload->data('file_name');
		
		$this->upload->do_upload('m_idproof');
		$m_idproof=$this->upload->data('file_name');		

		$this->upload->do_upload('g_pic');
		$g_pic=$this->upload->data('file_name');
		
		$this->upload->do_upload('g_idproof');
		$g_idproof=$this->upload->data('file_name');

		$acad_yr=$this->input->post('academicyear');
		$query=$this->db->query("SELECT enquiry_id FROM academicyear WHERE id=1")->row();
		$abc=($query->enquiry_id)+1;
		$LastReceiptID=sprintf("%04s",$abc);
		//print_r($LastReceiptID);die;
		$query=$this->db->query("UPDATE academicyear SET enquiry_id = '$LastReceiptID' WHERE id=1 ");

		$interaction=array(
					'enquiry_id'=>$acad_yr.$LastReceiptID,
					'name'=>$this->input->post('name'),
					'previousschool'=>$this->input->post('prev_schname'),
					'generalawareness'=>$this->input->post('generalawareness'),
					'confidence'=>$this->input->post('confidence'),
					'personality'=>$this->input->post('personality'),
					'mentalability'=>$this->input->post('mentalability'),
					'commskills'=>$this->input->post('commskills'),
					'stud_remark'=>$this->input->post('stud_remark'),
					'F_name'=>$this->input->post('f_name'),
					'F_English'=>$this->input->post('F_English'),
					'F_Hindi'=>$this->input->post('F_Hindi'),
					'F_qualification'=>$this->input->post('F_qualification'),
					'F_overall'=>$this->input->post('F_overall'),
					'F_observation'=>$this->input->post('F_observation'),
					'F_remark'=>$this->input->post('F_remark'),
					'M_name'=>$this->input->post('m_name'),
					'M_English'=>$this->input->post('M_English'),
					'M_Hindi'=>$this->input->post('M_Hindi'),
					'M_qualification'=>$this->input->post('M_qualification'),
					'M_overall'=>$this->input->post('M_overall'),
					'M_observation'=>$this->input->post('M_observation'),
					'M_remark'=>$this->input->post('M_remark'),
				);
		$enquiry=array(
					'childphoto'=>$stud_pic,
					'certificate'=>$certificate,
					'enquiry_id'=>$acad_yr.$LastReceiptID,
					'academicyear'=>$this->input->post('academicyear'),
					'certificate'=>$this->input->post('admissiondate'),
					'childname'=>$this->input->post('name'),
					'dob'=>$this->input->post('dob'),
					'gender'=>$this->input->post('gender'),
					'category'=>$this->input->post('category'),
					'address'=>$this->input->post('address'),
					'certificatename'=>$this->input->post('certificatename'),
					'remarks'=>$this->input->post('certiremarks'),
					'fathername'=>$this->input->post('f_name'),
					'fatherprofession'=>$this->input->post('f_profession'),
					'contact'=>$this->input->post('f_contact'),
					'email'=>$this->input->post('f_email'),
					'mothername'=>$this->input->post('m_name'),
					'motherprofession'=>$this->input->post('m_profession'),
					'alternatecontact'=>$this->input->post('m_contact'),
					'current_school'=>$this->input->post('prev_schname'),
					'studying_class'=>$this->input->post('prev_class'),
					'board'=>$this->input->post('prev_board'),
				);
		$student=array(
					'stud_pic'=>$stud_pic,
					'certificate'=>$certificate,
					'TC'=>$TC,
					'birthcerti'=>$birthcerti,
					'SSSMID'=>$SSSMID,
					'aadharcard'=>$aadharcard,
					'castecerti'=>$castecerti,
					'addressproof'=>$addressproof,
					'f_pic'=>$f_pic,
					'f_idproof'=>$f_idproof,
					'm_pic'=>$m_pic,
					'm_idproof'=>$m_idproof,
					'g_pic'=>$g_pic,
					'g_idproof'=>$g_idproof,
					'enquiry_id'=>$acad_yr.$LastReceiptID,
					'academicyear'=>$this->input->post('academicyear'),
					'admissiondate'=>$this->input->post('admissiondate'),
					'joindate'=>$this->input->post('joindate'),
					'samagraid'=>$this->input->post('samagraid'),
					'aadhar'=>$this->input->post('aadhar'),
					'name'=>$this->input->post('name'),
					'dob'=>$this->input->post('dob'),
					'gender'=>$this->input->post('gender'),
					'category'=>$this->input->post('category'),
					'caste'=>$this->input->post('caste'),
					'religion'=>$this->input->post('religion'),
					'mothertongue'=>$this->input->post('mothertongue'),
					'nationality'=>$this->input->post('nationality'),
					'bloodgroup'=>$this->input->post('bloodgroup'),
					'address'=>$this->input->post('address'),
					'wardno'=>$this->input->post('wardno'),
					'certificatename'=>$this->input->post('certificatename'),
					'certiremarks'=>$this->input->post('certiremarks'),
					'f_name'=>$this->input->post('f_name'),
					'f_profession'=>$this->input->post('f_profession'),
					'f_contact'=>$this->input->post('f_contact'),
					'f_email'=>$this->input->post('f_email'),
					'm_name'=>$this->input->post('m_name'),
					'm_profession'=>$this->input->post('m_profession'),
					'm_contact'=>$this->input->post('m_contact'),
					'm_email'=>$this->input->post('m_email'),
					'g_name'=>$this->input->post('g_name'),
					'g_profession'=>$this->input->post('g_profession'),
					'g_contact'=>$this->input->post('g_contact'),
					'g_email'=>$this->input->post('g_email'),
					'g_relation'=>$this->input->post('g_relation'),
					'g_address'=>$this->input->post('g_address'),
					'sibling'=>$this->input->post('sibling'),
					'annualfamilyincome'=>$this->input->post('annualfamilyincome'),
					'accountholdername'=>$this->input->post('accountholdername'),
					'bankaccountnumber'=>$this->input->post('bankaccountnumber'),
					'bankname'=>$this->input->post('bankname'),
					'ifsccode'=>$this->input->post('ifsccode'),
					'prev_schname'=>$this->input->post('prev_schname'),
					'prev_class'=>$this->input->post('prev_class'),
					'prev_board'=>$this->input->post('prev_board'),
					'result'=>$this->input->post('result'),
					'passingyear'=>$this->input->post('passingyear'),
					'percentage'=>$this->input->post('percentage'),
				);
		$this->Frontdesk_model->StudAdmission($interaction,$student,$enquiry);
		redirect('admission');
	}
	//To show perticular student interaction reports
	public function Stud_report()
	{
		$id= $_POST ['enquiry_id'];
		$studreport= $this->Frontdesk_model->Studreport($id);
		$this->load->view('stud-report',compact('studreport'));
	}
	//To show perticular student's parent interaction reports
	public function Parent_report()
	{
		$id= $_POST ['enquiry_id'];
		$parentreport= $this->Frontdesk_model->Parentreport($id);
		$this->load->view('parent-report',compact('parentreport'));
	}
    //For frontdask Employee
	public function Employee()
	{
		$emp_data=$this->Frontdesk_model->ShowEmployee();
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('employee-application',compact('emp_data'));
		$this->load->view('../../inc/footer');
		
	}	
	// For frontdask Add_Employee
	public function Add_Employee()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add-employee-application');
		$this->load->view('../../inc/footer');
	}
	//To insertion of employees
	public function Insert_employee()
	{		
		$config['upload_path']          = './employeeinfo';
		$config['allowed_types']        = 'gif|jpg|png';

		$this->load->library('upload', $config);
		$this->upload->do_upload('emp_pic');
		$u_data1 =$this->upload->data();
		$path1=$u_data1['file_name'];

		$this->upload->do_upload('A_card');
		$u_data2 =$this->upload->data();
		$path2=$u_data2['file_name'];

		$this->upload->do_upload('Exp_letter');
		$u_data3 =$this->upload->data();
		$path3=$u_data3['file_name'];

		$this->upload->do_upload('Add_proof');
		$u_data4 =$this->upload->data();
		$path4=$u_data4['file_name'];

		$data=$this->input->post();
		$data['emp_pic']=$path1;
		$data['A_card']=$path2;
		$data['Exp_letter']=$path3;
		$data['Add_proof']=$path4;

		$this->Frontdesk_model->Insertemployee($data);
		redirect('employee');
	}
    // For frontdask Visitors
	public function Visitors()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$visitor=$this->Frontdesk_model->ShowVisitors();
		$this->load->view('visitors',compact('visitor'));
		$this->load->view('../../inc/footer');
	}
	// For frontdask Add-Visitor
	public function Add_Visitor()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add-visitor');
		$this->load->view('../../inc/footer');
	}
	//insertion of visiters form
	public function Insert_Visitor()
	{
		$config['upload_path']          = './upload';
		$config['allowed_types']        = 'gif|jpg|png';
		$this->load->library('upload', $config);
		$this->upload->do_upload('dobcertificate');
		$u_data=$this->upload->data();
		$dobcertificate=$u_data['file_name'];
		$data=$this->input->post();
		$data['dobcertificate']=$dobcertificate;
		$this->Frontdesk_model->Insertvisitor($data);
		redirect('visitors');
	}
   //For frontdask Gate_Pass
	public function Gate_Pass()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$gatepass=$this->Frontdesk_model->Studgatepassinfo();
		$empgatepass=$this->Frontdesk_model->Empgatepassinfo();
		$students=$this->Frontdesk_model->Students();
		$employ=$this->Frontdesk_model->Emplyoee();
		$dept=$this->Frontdesk_model->Dept_data();
		$this->load->view('gatepass',compact('gatepass','empgatepass','students','employ','dept'));
		$this->load->view('../../inc/footer');
	}
	//for geting departmentid to show employees
	public function Get_departmentid()
	{
	    $dep_id=$this->input->post('dep_id');
		$data=$this->Frontdesk_model->empfrom_depid($dep_id);
		echo $balnk='<option selected="selected" value="">Select</option>';
		foreach ($data as $value) 
		{
		 echo $resp= '<option value="'.$value->id.'">'.$value->name.'</option>';
		}
	}
	//for geting class and section to show students
	public function Get_section()
	{
		$class=$this->input->post('classid');
		$section=$this->input->post('sectionid');
		$data=$this->Frontdesk_model->studfrom_class($class,$section);
		echo $balnk='<option selected="selected" value="">Select</option>';
		foreach ($data as $value) 
		{
		 echo $resp= '<option value="'.$value->id.'">'.$value->name.'</option>';
		}
	}
	// For frontdask Add-Gate_Pass
	public function Add_Gate_Pass()
	{
		$dept=$this->Frontdesk_model->Dept_data();
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('add-gatepass',compact('dept'));
		$this->load->view('../../inc/footer');
	}
	//insert student gatepass
	public function Stud_GatePass()
	{
		$config['upload_path']          = './upload';
		$config['allowed_types']        = 'gif|jpg|png';
		$this->load->library('upload', $config);
		$this->upload->do_upload('document');
		$u_data=$this->upload->data();
		$document=$u_data['file_name'];
		$data=$this->input->post();
		$data['document']=$document;
		$data['delete']=1;
		$this->Frontdesk_model->StudGatePass($data);
		redirect('gate-pass');
	}
	//insert employee gatepass
	public function Emp_GatePass()
	{
		$config['upload_path']          = './upload';
		$config['allowed_types']        = 'gif|jpg|png';
		$this->load->library('upload', $config);
		$this->upload->do_upload('document1');
		$u_data=$this->upload->data();
		$document=$u_data['file_name'];
		$data=$this->input->post();
		$data['document']=$document;
		$data['delete']=1;
		$this->Frontdesk_model->EmpGatePass($data);
		redirect('gate-pass');
	}
	//To close Gatepass
	public function Closegatepass()
	{
		$stgp_id= $_POST['stgp'];
		$empgp_id= $_POST['empgp'];
		$data = array(
			      'delete'         => 2,
			      'timeout'  => date("h:i:s"),
			      'modifieddate'  =>date("Y-m-d")
                );
		$query=$this->db->where('id',$stgp_id)->update('studentgatepass',$data);
		$query=$this->db->where('id',$empgp_id)->update('employeegatepass',$data);	
	}
}
