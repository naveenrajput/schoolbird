<?php  
   class Frontdesk_model extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }  
      //Query to View all type of status data on Enquiry Section
      public function enquiry_view(){          
         $this->db->select('*');
         $this->db->from('admissionenquiry');
         $this->db->where('is_delete',1);
         return $query=$this->db->get()->result();
         //print_r($this->db->last_query($query));die;
      } 
      //Query to View close status data on Enquiry Section
      public function close_view(){
         $this->db->select('*');
         $this->db->from('admissionenquiry');
         $this->db->where('is_delete',1);
         $this->db->where('status','CLOSE');
         return $query=$this->db->get()->result();
      } 
      //Query to View close status data on Enquiry Section
      public function hold_view(){     
         $this->db->select('*');
         $this->db->from('admissionenquiry');
         $this->db->where('is_delete',1);
         $this->db->where('status','HOLD');
         return $query=$this->db->get()->result();
      } 
      //Query to View data in Follow up Section
      public function followup_view(){ 
        $this->db->select('*,admissionenquiry.createdate as date');
        $this->db->from('admissionenquiry');
        $this->db->join('enquiryfollowup', 'enquiryfollowup.enquiry_id = admissionenquiry.enquiry_id','left');
        $this->db->where('status','FOLLOWUP');
        $this->db->where('nextfollowup_date',date('Y-m-d'));
        return $query=$this->db->get()->result();
      } 
      /*public function Search_enquiry($start,$end)
      {
        $this->db->select('*');
        $this->db->from('admissionenquiry');
        $this->db->where('createdate >= ',$start);
        $this->db->where('createdate <= ',$end);
        return $q = $this->db->get()->result();
      }
      public function Search_holded($start,$end)
      {
        $this->db->select('*');
        $this->db->from('admissionenquiry');
        $this->db->where('status','Hold');
        $this->db->where('createdate >= ',$start);
        $this->db->where('createdate <= ',$end);
        return $q = $this->db->get()->result();
      }
      public function Search_closed($start,$end)
      {
        $this->db->select('*');
        $this->db->from('admissionenquiry');
        $this->db->where('status','Close');
        $this->db->where('createdate >= ',$start);
        $this->db->where('createdate <= ',$end);
        return $q = $this->db->get()->result();
      }*/
       //to show Reminder section
       public function Comments($id)
       {
         $this->db->select('status,fathername,enquiryfollowup.enquiry_id as fw_id,comments,followup_date,nextfollowup_date');
         $this->db->from('admissionenquiry');
         $this->db->join('enquiryfollowup', 'enquiryfollowup.enquiry_id = admissionenquiry.enquiry_id','left');
         $this->db->where('admissionenquiry.enquiry_id',$id);
         $this->db->order_by('id','DESC');
         return $query=$this->db->get()->result();
         //print_r($this->db->last_query($query));die;
       }
       public function Int_comments($id)
       {
         $this->db->select('status,fathername,enquiryfollowup.enquiry_id as fw_id,comments,enquiryfollowup.createdate as followup_date,nextfollowup_date,interact_date,review_comments');
         $this->db->from('admissionenquiry');
         $this->db->join('enquiryfollowup', 'enquiryfollowup.enquiry_id = admissionenquiry.enquiry_id','left');
         $this->db->where('admissionenquiry.enquiry_id',$id);
         $this->db->where('interact_date !=',0000-00-00);
         $this->db->order_by('id','DESC');
         return $query=$this->db->get()->result();
         print_r($this->db->last_query($query));die;
       }
       //to set Remainder and change status to folllowup
       public function Add_comments($status,$id,$data)
       {
          $query= $this->db->where('enquiry_id',$id)->update('admissionenquiry',$status);
          $query=$this->db->where('enquiry_id',$id)->update('interactionreport',$int_data); 
          $data['followup_date']=date('Y-m-d');
          $data['createdate']=date('Y-m-d');
          $query=$this->db->insert('enquiryfollowup',$data);          
       }
       //for showing employee name on Intraction and Review section
       public function show_empname()
       {
          $this->db->select('name,id,mobile,email');
          $this->db->from('employees');
          return $query=$this->db->get()->result();
       }
       //for Insert data and update status from Intraction section
       public function Interaction_send($status,$id,$int_data,$followup)
       {
          $query= $this->db->where('enquiry_id',$id)->update('admissionenquiry',$status);
          $query=$this->db->insert('enquiryfollowup',$followup); 
          $query=$this->db->insert('interactionreport',$int_data);   

       }
       //to Insert data and update status from Review section
       public function Addreviewcomments($id,$status,$review_data)
       {
          $review_data['createdate']=date('Y-m-d');
          $query= $this->db->where('enquiry_id',$id)->update('admissionenquiry',$status);
          $query=$this->db->insert('enquiryfollowup',$review_data);        
       }
       //Query to detact number of employees
       public function Select_number($id)
       {
          $this->db->select('mobile');
          $this->db->from('employees');
          $this->db->where('id',$id);
          $query=$this->db->get()->row();
          return $query->mobile;
       }
       //Query to detact email of employees
       public function Select_email($id)
       {
          $this->db->select('email');
          $this->db->from('employees');
          $this->db->where('id',$id);
          $query=$this->db->get()->row();
          return $query->email;
       }
       // query to colse status in admissionenquiry and enquiryfollowup table
       public function Change_status($data,$id,$followup)
       {
          $query= $this->db->where('enquiry_id',$id)->update('admissionenquiry',$data);
          $followup['createdate']=date('Y-m-d');
          $query=$this->db->where('enquiry_id',$id)->insert('enquiryfollowup',$followup);    
       }
      // query to insert data in admissionenquiry table
      public function Insertenquiry($data,$followup)
      {
        $data['updatedby']='0';
        $data['status']='ACTIVE';
        $data['is_delete']='1';
        $data['createdate']=date('Y-m-d');
        $query=$this->db->insert('admissionenquiry',$data);
        $followup['updatedby']='0';
        $followup['is_delete']='1';
        $followup['createdate']=date('Y-m-d');
        $query=$this->db->insert('enquiryfollowup',$followup);
      }
      // query to select admissionenquiry table data
      public function Eeditenquiry($edit_id)
      {
        $this->db->select('*');
        $this->db->from('admissionenquiry');
        $this->db->where('enquiry_id',$edit_id);
        return $query=$this->db->get()->row();
      }
      //query to update data of admissionenquiry table
      public function Updateenquiry($data,$u_id,$followup)
      {
        $data['modifieddate']=date('Y-m-d');
        $this->db->where('enquiry_id',$u_id)->update('admissionenquiry',$data);
        //$query=$this->db->insert('enquiryfollowup',$followup);
      }
      //
      public function Todays_Interaction()
      {
        $this->db->select('interactionreport.id as id,interactionreport.enquiry_id as enquiry_id,childname,current_school,generalawareness, `mentalability`, `confidence`, `commskills`, `personality`, `previousschool`');
        $this->db->from('interactionreport');
        $this->db->join('admissionenquiry','admissionenquiry.enquiry_id=interactionreport.enquiry_id','left');
        $this->db->join('enquiryfollowup','interactionreport.enquiry_id=enquiryfollowup.enquiry_id','left');
        $this->db->where('admissionenquiry.status','INTERACTION');
        $this->db->where('enquiryfollowup.interact_date',date('Y-m-d'));
        return $query=$this->db->get()->result();
        //print_r($this->db->last_query($query));die;
      }
      //
      public function Pending_Interaction()
      {
        $this->db->select('interactionreport.id as id,interactionreport.enquiry_id as enquiry_id,childname,current_school,generalawareness, `mentalability`, `confidence`, `commskills`, `personality`, `previousschool`');
        $this->db->from('interactionreport');
        $this->db->join('admissionenquiry','admissionenquiry.enquiry_id=interactionreport.enquiry_id','left');
        $this->db->join('enquiryfollowup','interactionreport.enquiry_id=enquiryfollowup.enquiry_id','left');
        $this->db->where('admissionenquiry.status','INTERACTION');
        $this->db->where('enquiryfollowup.interact_date !=',date('Y-m-d'));
        $this->db->where('enquiryfollowup.interact_date !=',0000-00-00);

        return $query=$this->db->get()->result();
        print_r($this->db->last_query($query));die;
      }
      public function AddIntraction($int_id)
      {
        $this->db->select('*,admissionenquiry.class as class`,admissionenquiry.status as status,admissionenquiry.enquiry_id as enquiry_id,enquiryfollowup.interact_date as int_date');
        $this->db->from('admissionenquiry');
        $this->db->where('admissionenquiry.enquiry_id',$int_id);
        $this->db->where('admissionenquiry.status','INTERACTION');
        $this->db->order_by('enquiryfollowup.id','DESC');
        $this->db->limit(1);
        $this->db->join('interactionreport','admissionenquiry.enquiry_id=interactionreport.enquiry_id','left');
        $this->db->join('enquiryfollowup','admissionenquiry.enquiry_id=enquiryfollowup.enquiry_id','left');
        //$this->db->where('interact_date',date('Y-m-d'));
        return $query=$this->db->get()->result();
        //print_r($this->db->last_query($query));die;
      }
      public function InsertIntraction($data,$id,$status)
      {
        $FE=implode(",",$data['F_English']);
        $FH=implode(",",$data['F_Hindi']);
        $ME=implode(",",$data['M_English']);
        $MH=implode(",",$data['M_Hindi']);
        $data['F_English']=$FE;
        $data['F_Hindi']=$FH;
        $data['M_English']=$ME;
        $data['M_Hindi']=$MH;
        $query=$this->db->where('enquiry_id',$id)->update('interactionreport',$data);
        $query=$this->db->where('enquiry_id',$id)->update('admissionenquiry',$status);
        //$query=$this->db->insert('studentadmissiondetails',$enq_id);

        //print_r($this->db->last_query($query));die;
      }
      public function fromenquiry()
      {
        $this->db->select('*');
        $this->db->from('admissionenquiry');
        return $query=$this->db->get()->result();
        //print_r($this->db->last_query($query));die;
      }
      public function frominteraction()
      {
        /*$this->db->select('interactionreport.id as id,interactionreport.enquiry_id as enquiry_id,childname,current_school,generalawareness, `mentalability`, `confidence`, `commskills`, `personality`, `previousschool`');
        $this->db->from('interactionreport');
        $this->db->join('admissionenquiry','admissionenquiry.enquiry_id=interactionreport.enquiry_id','left');
        $this->db->join('enquiryfollowup','interactionreport.enquiry_id=enquiryfollowup.enquiry_id','left');
        $this->db->where('admissionenquiry.status','APPROVAL');
        $this->db->where('enquiryfollowup.interact_date !=',0000-00-00);
        return $query=$this->db->get()->result();*/
        //print_r($this->db->last_query($query));die;
        $this->db->select('*,interactionreport.id as id,interactionreport.enquiry_id as enquiry_id');
        $this->db->from('interactionreport');
        $this->db->join('admissionenquiry','admissionenquiry.enquiry_id=interactionreport.enquiry_id','left');
        $this->db->join('enquiryfollowup','interactionreport.enquiry_id=enquiryfollowup.enquiry_id','left');
        $this->db->where('admissionenquiry.status','ADMISSION');
        $this->db->where('enquiryfollowup.interact_date !=',0000-00-00);
        return $query=$this->db->get()->result();
        //print_r($this->db->last_query($query));die;
      }
      public function forwardadmission()
      {
        $this->db->select('*,interactionreport.id as id,interactionreport.enquiry_id as enquiry_id');
        $this->db->from('interactionreport');
        $this->db->join('admissionenquiry','admissionenquiry.enquiry_id=interactionreport.enquiry_id','left');
        $this->db->join('enquiryfollowup','interactionreport.enquiry_id=enquiryfollowup.enquiry_id','left');
        $this->db->where('admissionenquiry.status','ADMISSION');
        $this->db->where('enquiryfollowup.interact_date !=',0000-00-00);
        return $query=$this->db->get()->result();
        print_r($this->db->last_query($query));die;
      }
      public function Studreport($id)
      {
        $this->db->select('*');
        $this->db->from('interactionreport');
        $this->db->where('enquiry_id',$id);
        return $query=$this->db->get()->result();
        //print_r($this->db->last_query($query));die;
      }
      public function Parentreport($id)
      {
        $this->db->select('*');
        $this->db->from('interactionreport');
        $this->db->where('enquiry_id',$id);
        return $query=$this->db->get()->result();
        //print_r($this->db->last_query($query));die; 
      }
      public function StudAdmission($interaction,$student,$enquiry)
      {
        $FE=implode(",",$interaction['F_English']);
        $FH=implode(",",$interaction['F_Hindi']);
        $ME=implode(",",$interaction['M_English']);
        $MH=implode(",",$interaction['M_Hindi']);
        $interaction['F_English']=$FE;
        $interaction['F_Hindi']=$FH;
        $interaction['M_English']=$ME;
        $interaction['M_Hindi']=$MH;
        $query=$this->db->insert('interactionreport',$interaction);
        $query=$this->db->insert('students',$student);
        $query=$this->db->insert('admissionenquiry',$enquiry);
      }
      public function ShowEmployee()
      {
        $this->db->select('*');
        $this->db->from('employees');
        return $query=$this->db->get()->result();
      }
      public function Insertemployee($data)
      {
        $Child_name=implode(",",$data['Child_name']);
        $Child_age=implode(",",$data['Child_age']);
        $Child_class=implode(",",$data['Child_class']);
        $qualification=implode(",",$data['qualification']);
        $passingyear=implode(",",$data['passingyear']);
        $schoolname=implode(",",$data['schoolname']);
        $boardname=implode(",",$data['boardname']);
        $subject=implode(",",$data['subject']);
        $percentage=implode(",",$data['percentage']);
        $organization=implode(",",$data['organization']);
        $datefrom=implode(",",$data['datefrom']);
        $todate=implode(",",$data['todate']);
        $designation=implode(",",$data['designation']);
        $salary=implode(",",$data['salary']);

        $data['Child_name']=$Child_name;
        $data['Child_age']=$Child_age;
        $data['Child_class']=$Child_class;
        $data['qualification']=$qualification;
        $data['passingyear']=$passingyear;
        $data['schoolname']=$schoolname;
        $data['boardname']=$boardname;
        $data['subject']=$subject;
        $data['percentage']=$percentage;
        $data['organization']=$organization;
        $data['datefrom']=$datefrom;
        $data['todate']=$todate;
        $data['designation']=$designation;
        $data['salary']=$salary;

        $query=$this->db->insert('employees',$data);
      }
      public function ShowVisitors()
      {
        $this->db->select('*');
        $this->db->from('visitors');
        return $query=$this->db->get()->result();
      }
      public function Insertvisitor($data)
      {
        $query=$this->db->insert('visitors',$data);
      }
      public function Dept_data()
      {
        $this->db->select('id,name');
        $this->db->from('department');
        return $query=$this->db->get()->result();
      }
      public function Studgatepassinfo()
      {
        $this->db->select('*');
        $this->db->from('studentgatepass');
        $this->db->where('delete',1);
        return $query=$this->db->get()->result();
      }
      public function Empgatepassinfo()
      {
        $this->db->select('*');
        $this->db->from('employeegatepass');
        $this->db->where('delete',1);
        return $query=$this->db->get()->result();
      }
      public function Students()
      {
        $this->db->select('id,name');
        $this->db->from('students');
        return $query=$this->db->get()->result();
      }
      public function Emplyoee()
      {
        $this->db->select('id,name');
        $this->db->from('employees');
        return $query=$this->db->get()->result();
      }
      public function empfrom_depid($dep_id)
      {
        $this->db->select('id,name');
        $this->db->from('employees');
        $this->db->where('dep_id',$dep_id);
        return $query=$this->db->get()->result();
      }
      public function studfrom_class($class,$section)
      {
        $this->db->select('id,name');
        $this->db->from('students');
        $this->db->where('class',$class);
        $this->db->where('section',$section);
        return $query=$this->db->get()->result();
        //print_r($this->db->last_query($query));die;
      }
      public function StudGatePass($data)
      {
        $query=$this->db->insert('studentgatepass',$data);
      }
      public function EmpGatePass($data)
      {
        $query=$this->db->insert('employeegatepass',$data);
      }
     /* //query to Edit interactionreport table data
      public function Eeditinteraction($int_id)
      {
        $this->db->select('*');
        $this->db->from('interactionreport');
        $this->db->where('enquiry_id',$int_id);
        return $query=$this->db->get()->result();
        //print_r($this->db->last_query($query));die;
      }
      //query to Update interactionreport table data
      public function Updateinteraction($data,$id)
      {
        $FE=implode(",",$data['F_English']);
        $FH=implode(",",$data['F_Hindi']);
        $ME=implode(",",$data['M_English']);
        $MH=implode(",",$data['M_Hindi']);
        $data['F_English']=$FE;
        $data['F_Hindi']=$FH;
        $data['M_English']=$ME;
        $data['M_Hindi']=$MH;
        $query=$this->db->where('enquiry_id',$id)->update('interactionreport',$data); 
        //print_r($this->db->last_query($query));die;
      }*/
   }  
?>  