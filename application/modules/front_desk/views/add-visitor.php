
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Visitor Form </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Dashboard</a></li>
      <li class="active">Visitor Form</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row"> 
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="box ">
          <div class="box-header with-border">
            <h3 class="box-title">Add Details</h3>
          </div>
          <!-- /.box-header --> 
          <!-- form start -->
          <form method="post" action="<?php echo base_url('insert-visitor');?>" data-toggle="validator" role="form" enctype="multipart/form-data">
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="">Name </label>
                    <input type="text" name="name" class="form-control"  placeholder="Name" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label> Date </label>
                    <div class="input-group date">
                      <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                      <input type="text" name="date"  class="form-control pull-right" id="datepicker" required>
                    </div>
                    <!-- /.input group --> 
                  </div>
                </div>
               <div class="col-md-4">
                       <div class="bootstrap-timepicker">
                <div class="form-group">
                  <label>Time In</label>
			
                  <div class="input-group">
				  <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" name="timein"  class="form-control timepicker" required>
                
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
                    </div>
				<div class="col-md-4">
                  <div class="form-group">
                    <label for="">Contact </label>
                    <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" name="contact" class="form-control" data-inputmask="&quot;mask&quot;:&quot; 99999-99999&quot;" data-mask="" required>
                </div>
                  </div>
                </div>
				<div class="col-md-4">
                  <div class="form-group">
                    <label for="">Contact Person </label>
                    <input type="text" name="contactperson"  class="form-control"  placeholder="Name" required>
                  </div>
                </div>
				<div class="col-md-8">
                  <div class="form-group">
                    <label for="">Purpose Of Meeting </label>
                   <textarea class="form-control" name="purpose"  rows="2" placeholder="Type Here" required></textarea>
                  </div>
                </div>
              <div class="col-md-4 ">
			   <div class="form-group">
			     <label> Date Of Birth Certificate	 </label>
    <input type="file" name="dobcertificate" class="file1" required>
    <div class="input-group ">
    
      <input type="text" class="form-control" disabled="" placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
                
              </div>
              
            </div>
            <!-- /.box-body -->
            
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.box --> 
        
        <!-- Form Element sizes --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
    <!-- /.row --> 
    
  </section>
  <!-- /.content --> 
</div>

<!-- page script --> 


