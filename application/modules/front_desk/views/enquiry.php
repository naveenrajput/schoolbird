<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
<!-- Content Header (Page header) -->
<section class=" ">
  <div class="col-md-6 col-xs-12 col-sm-8 content-header">
    <h1 class=""> Enquiry </h1>
    <ol class="breadcrumb" style="background:none;">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li><a href="#">Front Desk</a></li>
      <li class="active">Enquiry</li>
    </ol>
  </div>
  <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;"> <a  href="add-enquiry" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp;Enquiry</a> <a  href="#" class="btn  btn-primary sp-10"  data-toggle="modal" data-target="#modal-enquiry"> <i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp; Send Enquiry</a> </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row"> 
  
  <!-- /.col -->
  <div class="col-md-12">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#enq1" data-toggle="tab">Enquiry</a></li>
        <li><a href="#follow" data-toggle="tab">Follow Up</a></li>
        <li><a href="#hold" data-toggle="tab">Hold</a></li>
        <li><a href="#closed" data-toggle="tab">Closed</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="enq1">
          <div class="box" style="border:none;">
            <div class="box-header with-border mr-top-10 mr-bottom-10 text-center">

              <form method="post" action="<?php //echo base_url('search-enquiry');?>" data-toggle="validator" role="form">
              <div class="form-group col-md-4">
                <div class="input-group date">
                  <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                  <input type="text" name="start_date" class="form-control pull-right search_data datepicker" placeholder="Start Date">
                </div>
                <!-- /.input group --> 
              </div>
              <!-- /.form group --> 
              
              <!-- Date range -->
              <div class="form-group col-md-4">
                <div class="input-group date">
                  <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                  <input type="text" name="end_date" class="form-control pull-right search_data datepicker" placeholder="End Date">
                </div>
                <!-- /.input group --> 
              </div>
              <div class="form-group col-md-3 ">
                <select class="form-control select1 search_data" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected" value="">Status</option>
                  <option>ALL</option>
                  <option>ACTIVE</option>
                  <option>HOLD</option>
                  <option>CLOSE</option>
                </select>
              </div>
              <div class="col-md-1">
                <button type="button" class="btn btn-primary" id="search_id"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              </div>
         </form>
            </div>
            <!-- /.box-header -->
            <div class="col-md-12 col-xs-12 col-sm-12 margin ">
              <div class="btn-group pull-right">
              <button class="btn btn-primary" type="button">
              Print
              </div>
              <div class="btn-group pull-right mr-right-10">
              <a class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-export">Export</a> </div>
            <div class="btn-group pull-right mr-right-10"> <a href="#"  data-toggle="modal" data-target="#modal-import" class="btn btn-primary" type="button">Import</a> </div>
          </div>
          <div class="box-body table-responsive ">            
<!-- <table id="example" class="display nowrap" style="width:100%">--->
<table id="example" class="table table-bordered ">
<!-- <div class="txt-dis">Export in Below Format</div> --->
  <thead>
    <tr>
      <th><input type="checkbox" id="selectall">All</th>
      <th>Enquiry<br>
        No</th>
      <th>Date</th>
      <th>Student<br>
        Name</th>
      <th>Father<br>
        Name</th>
      <th>Applied<br>
        Class</th>
      <th>Contact</th>
      <th>Updated By</th>
      <th>Status</th>
      <th>Forward</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php 
      if (isset($stud_enq_data)) {  
      foreach ($stud_enq_data as $key => $stud_enq_data) {
  ?>
    <tr>
      <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
      <td><?php echo $stud_enq_data->enquiry_id; ?></td>
      <td><?php echo $stud_enq_data->createdate; ?></td>
      <td><?php echo $stud_enq_data->childname; ?></td>
      <td><?php echo $stud_enq_data->fathername; ?></td>
      <td><?php echo $stud_enq_data->class; ?></td>
      <td><?php echo $stud_enq_data->contact; ?></td>
      <td><?php echo $stud_enq_data->updatedby; ?></td>
      <td class="active-text"><?php echo $stud_enq_data->status; ?></td>
      <td>
        <ul class="table-icons">
          <li>
            <a href="" class="table-icon table-review-icon" title="Review" data-toggle="modal" data-target="#Mymodal">
             <span class="glyphicon glyphicon-eye-open display-icon review_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon " title="Interaction" data-toggle="modal" data-target="#Mymodal"> 
              <span class="glyphicon glyphicon-open-file display-icon interaction_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Hold" data-toggle="modal" data-target="#Mymodal"> 
              <span class="display-icon hold_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"> <img src="<?php echo base_url();?>assets/dist/img/icon/hold.png" style="vertical-align: baseline;" class=""> </span> 
            </a>
          </li>
        </ul>
      </td>
      <td>
        <ul class="table-icons">
          <li>
            <a href="<?php echo base_url('edit-enquiry/').base64_encode($stud_enq_data->enquiry_id); ?>" class="table-icon" title="Edit"> 
              <span class="glyphicon glyphicon-edit display-icon"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Set Reminder"  data-toggle="modal" data-target="#Mymodal">
              <span class="glyphicon glyphicon-time display-icon remind_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span>
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Print"> 
              <span class="glyphicon glyphicon-print display-icon"></span>
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Close" data-toggle="modal" data-target="#Mymodal"> 
              <span class="glyphicon glyphicon-remove display-icon close_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span> 
            </a>
          </li>
        </ul>
      </td>
    </tr>
    <?php } }?>
  </tbody>
  <tfoot>
    <tr>
      <th><input type="checkbox" id="selectall">All</th>
      <th>Enquiry<br>
        No</th>
      <th>Date</th>
      <th>Student<br>
        Name</th>
      <th>Father<br>
        Name</th>
      <th>Applied<br>
        Class</th>
      <th>Contact</th>
      <th>Updated By</th>
      <th>Status</th>
      <th>Forward</th>
      <th>Action</th>
    </tr>
  </tfoot>
</table>
          </div>
          <!-- /.box-body --> 
        </div>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="follow">
        <div class="box" style="border:none;">
          <div class="box-header with-border mr-top-10 mr-bottom-10 text-center">
            <div class="form-group col-md-5">
              <div class="input-group date">
                <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                <input type="text" class="form-control pull-right datepicker" placeholder="Start Date">
              </div>
              <!-- /.input group --> 
            </div>
            <!-- /.form group --> 
            
            <!-- Date range -->
            <div class="form-group col-md-5">
              <div class="input-group date">
                <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                <input type="text" class="form-control pull-right datepicker" placeholder="End Date">
              </div>
              <!-- /.input group --> 
            </div>
            <div class="col-md-1">
              <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="col-md-12 col-xs-12 col-sm-12 margin ">
            <div class="btn-group pull-right">
            <button class="btn btn-primary" type="button">
            Print
            </div>
            <div class="btn-group pull-right mr-right-10">
            <a class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-export">Export</a> </div>
          <div class="btn-group pull-right mr-right-10"> <a href="#"  data-toggle="modal" data-target="#modal-import" class="btn btn-primary" type="button">Import</a> </div>
        </div>
        <div class="box-body table-responsive "> 
          
          <!-- <table id="example" class="display nowrap" style="width:100%">--->
          <table id="example1" class="table table-bordered " >
            <!-- <div class="txt-dis">Export in Below Format</div> --->
            <thead>
              <tr>
                <th><input type="checkbox" id="selectall">All</th>
                <th>Enquiry<br>
                  No</th>
                <th>Date</th>
                <th>Student<br>
                  Name</th>
                <th>Father<br>
                  Name</th>
                <th>Applied<br>
                  Class</th>
                <th>Contact</th>
                <th>Updated By</th>
                <th>Status</th>
                <th>Forward</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                  if (isset($follow_data)) {  
                  foreach ($follow_data as $key => $stud_enq_data) {
              ?>
              <tr>
                <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
                <td><?php echo $stud_enq_data->enquiry_id; ?></td>
                  <td><?php echo $stud_enq_data->date; ?></td>
                  <td><?php echo $stud_enq_data->childname; ?></td>
                  <td><?php echo $stud_enq_data->fathername; ?></td>
                  <td><?php echo $stud_enq_data->class; ?></td>
                  <td><?php echo $stud_enq_data->contact; ?></td>
                  <td><?php echo $stud_enq_data->updatedby; ?></td>
                  <td class="active-text"><?php echo $stud_enq_data->status; ?></td>
      <td>
        <ul class="table-icons">
          <li>
            <a href="" class="table-icon table-review-icon" title="Review" data-toggle="modal" data-target="#Mymodal">
             <span class="glyphicon glyphicon-eye-open display-icon review_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon " title="Interaction" data-toggle="modal" data-target="#Mymodal"> 
              <span class="glyphicon glyphicon-open-file display-icon interaction_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Hold" data-toggle="modal" data-target="#Mymodal"> 
              <span class="display-icon hold_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"> <img src="<?php echo base_url();?>assets/dist/img/icon/hold.png" style="vertical-align: baseline;" class=""> </span> 
            </a>
          </li>
        </ul>
      </td>
      <td>
        <ul class="table-icons">
          <li>
            <a href="<?php echo base_url('edit-enquiry/').base64_encode($stud_enq_data->enquiry_id); ?>" class="table-icon" title="Edit"> 
              <span class="glyphicon glyphicon-edit display-icon"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Set Reminder"  data-toggle="modal" data-target="#Mymodal">
              <span class="glyphicon glyphicon-time display-icon remind_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span>
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Print"> 
              <span class="glyphicon glyphicon-print display-icon"></span>
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Close" data-toggle="modal" data-target="#Mymodal"> 
              <span class="glyphicon glyphicon-remove display-icon close_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span> 
            </a>
          </li>
        </ul>
      </td>
              </tr>
              <?php } } ?>
            </tbody>
            <tfoot>
              <tr>
                <th><input type="checkbox" id="selectall">All</th>
                <th>Enquiry<br>
                  No</th>
                <th>Date</th>
                <th>Student<br>
                  Name</th>
                <th>Father<br>
                  Name</th>
                <th>Applied<br>
                  Class</th>
                <th>Contact</th>
                <th>Updated By</th>
                <th>Status</th>
                <th>Forward</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body --> 
      </div>
    </div>
    <div class="tab-pane" id="hold">
      <div class="box" style="border:none;">
        <div class="box-header with-border mr-top-10 mr-bottom-10 text-center">
          <div class="form-group col-md-5">
            <div class="input-group date">
              <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
              <input type="text" class="form-control pull-right datepicker" placeholder="Start Date">
            </div>
            <!-- /.input group --> 
          </div>
          <!-- /.form group --> 
          
          <!-- Date range -->
          <div class="form-group col-md-5">
            <div class="input-group date">
              <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
              <input type="text" class="form-control pull-right datepicker" placeholder="End Date">
            </div>
            <!-- /.input group --> 
          </div>
          <div class="col-md-1">
            <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="col-md-12 col-xs-12 col-sm-12 margin ">
          <div class="btn-group pull-right">
          <button class="btn btn-primary" type="button">
          Print
          </div>
          <div class="btn-group pull-right mr-right-10">
          <a class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-export">Export</a> </div>
        <div class="btn-group pull-right mr-right-10"> <a href="#"  data-toggle="modal" data-target="#modal-import" class="btn btn-primary" type="button">Import</a> </div>
      </div>
      <div class="box-body table-responsive "> 
        
        <!-- <table id="example" class="display nowrap" style="width:100%">--->
        <table id="example2" class="table table-bordered " >
          <!-- <div class="txt-dis">Export in Below Format</div> --->
          <thead>
            <tr>
              <th><input type="checkbox" id="selectall">All</th>
              <th>Enquiry<br>
                No</th>
              <th>Date</th>
              <th>Student<br>
                Name</th>
              <th>Father<br>
                Name</th>
              <th>Applied<br>
                Class</th>
              <th>Contact</th>
              <th>Updated By</th>
              <th>Reason</th>
              <th>Status</th>
              <th>Forward</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
                if (isset($hold_data)) {  
                foreach ($hold_data as $key => $stud_enq_data){
            ?>
            <tr>
              <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
              <td><?php echo $stud_enq_data->enquiry_id; ?></td>
              <td><?php echo $stud_enq_data->createdate; ?></td>
              <td><?php echo $stud_enq_data->childname; ?></td>
              <td><?php echo $stud_enq_data->fathername; ?></td>
              <td><?php echo $stud_enq_data->class; ?></td>
              <td><?php echo $stud_enq_data->contact; ?></td>
              <td><?php echo $stud_enq_data->updatedby; ?></td>
              <td>Lorem Ipsum is simply</td>
              <td class="hold-text"><?php echo $stud_enq_data->status; ?></td>
      <td>
        <ul class="table-icons">
          <li>
            <a href="" class="table-icon table-review-icon" title="Review" data-toggle="modal" data-target="#Mymodal">
             <span class="glyphicon glyphicon-eye-open display-icon review_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon " title="Interaction" data-toggle="modal" data-target="#Mymodal"> 
              <span class="glyphicon glyphicon-open-file display-icon interaction_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Unhold" data-toggle="modal" data-target="#Mymodal">   <span class="display-icon Unhold_data" id="<?php echo $stud_enq_data->enquiry_id; ?>">  <img src="<?php echo base_url();?>assets/dist/img/icon/hold.png" style="vertical-align: baseline; opacity:0.5;" class=""> </span> 
            </a>
          </li>
        </ul>
      </td>
      <td>
        <ul class="table-icons">
          <li>
            <a href="<?php echo base_url('edit-enquiry/').base64_encode($stud_enq_data->enquiry_id); ?>" class="table-icon" title="Edit"> 
              <span class="glyphicon glyphicon-edit display-icon"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Set Reminder"  data-toggle="modal" data-target="#Mymodal">
              <span class="glyphicon glyphicon-time display-icon remind_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span>
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Print"> 
              <span class="glyphicon glyphicon-print display-icon"></span>
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Close" data-toggle="modal" data-target="#Mymodal"> 
              <span class="glyphicon glyphicon-remove display-icon close_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span> 
            </a>
          </li>
        </ul>
      </td>
            </tr>
            <?php } } ?>
          </tbody>
          <tfoot>
            <tr>
              <th><input type="checkbox" id="selectall">All</th>
              <th>Enquiry<br>
                No</th>
              <th>Date</th>
              <th>Student<br>
                Name</th>
              <th>Father<br>
                Name</th>
              <th>Applied<br>
                Class</th>
              <th>Contact</th>
              <th>Updated By</th>
              <th>Reason</th>
              <th>Status</th>
              <th>Forward</th>
              <th>Action</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body --> 
    </div>
  </div>
  <div class="tab-pane" id="closed">
    <div class="box" style="border:none;">
      <div class="box-header with-border mr-top-10 mr-bottom-10 text-center">
        <div class="form-group col-md-5">
          <div class="input-group date">
            <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
            <input type="text" class="form-control pull-right datepicker" placeholder="Start Date">
          </div>
          <!-- /.input group --> 
        </div>
        <!-- /.form group --> 
        
        <!-- Date range -->
        <div class="form-group col-md-5">
          <div class="input-group date">
            <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
            <input type="text" class="form-control pull-right datepicker" placeholder="End Date">
          </div>
          <!-- /.input group --> 
        </div>
        <div class="col-md-1">
          <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="col-md-12 col-xs-12 col-sm-12 margin ">
        <div class="btn-group pull-right">
        <button class="btn btn-primary" type="button">
        Print
        </div>
        <div class="btn-group pull-right mr-right-10">
        <a class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-export">Export</a> </div>
      <div class="btn-group pull-right mr-right-10"> <a href="#"  data-toggle="modal" data-target="#modal-import" class="btn btn-primary" type="button">Import</a> </div>
    </div>
    <div class="box-body table-responsive "> 
      
      <!-- <table id="example" class="display nowrap" style="width:100%">--->
      <table id="example3" class="table table-bordered " >
        <!-- <div class="txt-dis">Export in Below Format</div> --->
        <thead>
          <tr>
            <th><input type="checkbox" id="selectall">All</th>
            <th>Enquiry<br>
              No</th>
            <th>Date</th>
            <th>Student<br>
              Name</th>
            <th>Father<br>
              Name</th>
            <th>Applied<br>
              Class</th>
            <th>Contact</th>
            <th>Updated By</th>
            <th>Reason</th>
            <th>Status</th>
            <th>Forward</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php 
              if (isset($close_data)) {  
              foreach ($close_data as $key => $stud_enq_data) {
          ?>
          <tr>
            <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
            <td><?php echo $stud_enq_data->enquiry_id; ?></td>
            <td><?php echo $stud_enq_data->createdate; ?></td>
            <td><?php echo $stud_enq_data->childname; ?></td>
            <td><?php echo $stud_enq_data->fathername; ?></td>
            <td><?php echo $stud_enq_data->class; ?></td>
            <td><?php echo $stud_enq_data->contact; ?></td>
            <td><?php echo $stud_enq_data->updatedby; ?></td>
            <td></td>
            <td class="close-text"><?php echo $stud_enq_data->status; ?></td>
                  <td>
        <ul class="table-icons">
          <li>
            <a href="" class="table-icon table-review-icon" title="Review" data-toggle="modal" data-target="#Mymodal">
             <span class="glyphicon glyphicon-eye-open display-icon review_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon " title="Interaction" data-toggle="modal" data-target="#Mymodal"> 
              <span class="glyphicon glyphicon-open-file display-icon interaction_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Hold" data-toggle="modal" data-target="#Mymodal"> 
              <span class="display-icon hold_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"> <img src="<?php echo base_url();?>assets/dist/img/icon/hold.png" style="vertical-align: baseline;" class=""> </span> 
            </a>
          </li>
        </ul>
      </td>
      <td>
        <ul class="table-icons">
          <li>
            <a href="<?php echo base_url('edit-enquiry/').base64_encode($stud_enq_data->enquiry_id); ?>" class="table-icon" title="Edit"> 
              <span class="glyphicon glyphicon-edit display-icon"></span> 
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Set Reminder"  data-toggle="modal" data-target="#Mymodal">
              <span class="glyphicon glyphicon-time display-icon remind_data" id="<?php echo $stud_enq_data->enquiry_id; ?>"></span>
            </a>
          </li>
          <li>
            <a href="" class="table-icon" title="Print"> 
              <span class="glyphicon glyphicon-print display-icon"></span>
            </a>
          </li>
        </ul>
      </td>
          </tr>
          <?php } }?>
        </tbody>
        <tfoot>
          <tr>
            <th><input type="checkbox" id="selectall">All</th>
            <th>Enquiry<br>
              No</th>
            <th>Date</th>
            <th>Student<br>
              Name</th>
            <th>Father<br>
              Name</th>
            <th>Applied<br>
              Class</th>
            <th>Contact</th>
            <th>Updated By</th>
            <th>Reason</th>
            <th>Status</th>
            <th>Forward</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body --> 
  </div>
</div>
</div>
<!-- /.tab-content -->
</div>
<!-- /.nav-tabs-custom -->
</div>
<!-- /.col -->

</section>

<!-- Main content --> 

<!-- /.content -->
</div>
<div class="modal fade" id="modal-export">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Export</h4>
    </div>
    <div class="modal-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Start Date </label>
            <div class="input-group date">
              <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
              <input type="text" class="form-control pull-right datepicker">
            </div>
            <!-- /.input group --> 
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>End Date </label>
            <div class="input-group date">
              <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
              <input type="text" class="form-control pull-right datepicker">
            </div>
            <!-- /.input group --> 
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Status </label>
            <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
              <option selected="selected">Status</option>
              <option>All</option>
              <option>Active</option>
              <option>Hold</option>
              <option>Close</option>
            </select>
            <!-- /.input group --> 
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Type </label>
            <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
              <option selected="selected">Select</option>
              <option>Excel</option>
              <option>CSV</option>
              <option>PDF</option>
            </select>
            <!-- /.input group --> 
          </div>
        </div>
        <div class="col-md-12">&nbsp;</div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary">Export</button>
    </div>
  </div>
  <!-- /.modal-content --> 
</div>
<!-- /.modal-dialog --> 
</div>
<div class="modal fade" id="modal-import">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title text-center ">Welcome To Class Attendance Import Wizard </h4>
    </div>
    <div class="modal-body">
      <h3 class=" ">Please follow below steps to complete the import. </h3>
      <ul>
        <li><a href="<?php echo base_url();?>files/sample-file/classattendance.csv">Click here</a> to download a sample file that will help you with this import.</li>
        <li>Once you have downloaded the file enter the data in the exact same columns as mentioned in the file and save the file in the exatc same format (.csv) as it is downloaded in.</li>
        <li>After that, <a href="#" id="file-upload">click here</a> to see an option to upload your file.</li>
      </ul>
      <div class="form-group " id="upload-str" style="display:none;">
        <input type="file" name="img[]" class="file1">
        <div class="input-group ">
          <input type="text" class="form-control " disabled="" placeholder="Upload here">
          <span class="input-group-btn">
          <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i>Upload</button>
          </span> </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary">Submit</button>
    </div>
  </div>
  <!-- /.modal-content --> 
</div>
<!-- /.modal-dialog --> 
</div>
<!-- ALL MODALS VIEW -->
<div class="modal fade" id="Mymodal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content" id="Mymodal_view">
      
    </div>
    <!-- /.modal-content --> 
  </div>
<!-- /.modal-dialog --> 
</div>

<div class="modal fade" id="modal-enquiry">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Send Enquiry</h4>
    </div>
    <div class="modal-body">
      <div class="callout callout-info" >
        <h4><i class="fa fa-info"></i> Enquiry Form</h4>
        <a href="#" target="_blank" >https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js</a> </div>
      <div class="row">
        <div class="form-group col-md-offset-1 col-md-10">
          <label for="">Contact </label>
          <div class="input-group">
            <div class="input-group-addon"> <i class="fa fa-phone"></i> </div>
            <input type="text" class="form-control" data-inputmask="&quot;mask&quot;: &quot; 99999-99999&quot;" data-mask="">
          </div>
        </div>
        <p class="lead col-md-12 text-center">Or</p>
        <div class="form-group col-md-offset-1 col-md-10">
          <label>Email </label>
          <div class="input-group"> <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input type="email" class="form-control" placeholder="Email">
          </div>
          <!-- /.input group --> 
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary">Send</button>
    </div>
  </div>
  <!-- /.modal-content --> 
</div>
<!-- /.modal-dialog --> 
</div>
<script>
$(document).ready(function(){
    var table = $('#example').DataTable( {
        lengthChange: true,
    autoWidth : true,
       /*  buttons: [   'csv', 'excel', 'pdf', 'print' ], */
    
    
    } );
 
  /*   table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' ); */
    
} );

$(document).ready(function() {
    var table = $('#example1').DataTable( {
        lengthChange: true,
    autoWidth : true,
       /*  buttons: [   'csv', 'excel', 'pdf', 'print' ], */
    
    
    } );

    
} );

$(document).ready(function() {
    var table = $('#example2').DataTable( {
        lengthChange: true,
    autoWidth : true,
       /*  buttons: [   'csv', 'excel', 'pdf', 'print' ], */
    
    
    } );
 
  
} );
$(document).ready(function() {
    var table = $('#example3').DataTable( {
        lengthChange: true,
    autoWidth : true,
       /*  buttons: [   'csv', 'excel', 'pdf', 'print' ], */
    
    
    } );
 
  
} );
</script> 
<script>


$(document).ready(function() {
    $('#selectall').click(function () {
        $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function () {
        var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
        $('#selectall').prop("checked", check);
    });
});

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
   //Datemask3 mm/dd/yyyy
    
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('.datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })

$(document).ready(function(){
    $('.remind_data').click(function(){  
      var enquiry_id = $(this).attr("id");
      $.ajax({  
              url:"<?php echo base_url('Front_desk/Reminder'); ?>",  
              method:"post",  
              data:{enquiry_id:enquiry_id},  
              success:function(data){
                 console.log(data);
                $('#Mymodal_view').html(data);
              }                             
         }); 
    });  
/*});  

$(document).ready(function(){*/
    $('.close_data').click(function(){  
         var enquiry_id = $(this).attr("id");
          $.ajax({  
                  url:"<?php echo base_url('Front_desk/Close_Enquiry'); ?>",  
                  method:"post",  
                  data:{enquiry_id:enquiry_id},  
                  success:function(data){
                     console.log(data);
                    $('#Mymodal_view').html(data);
                  }                             
             }); 
        });  
/*});  

$(document).ready(function(){*/
    $('.hold_data').click(function(){  
         var enquiry_id = $(this).attr("id");
          $.ajax({  
                  url:"<?php echo base_url('hold-enquiry'); ?>",  
                  method:"post",  
                  data:{enquiry_id:enquiry_id},  
                  success:function(data){
                     console.log(data);
                    $('#Mymodal_view').html(data);
                  }                             
             }); 
        });  
/*});  

$(document).ready(function(){*/
  $('.Unhold_data').click(function(){  
   var enquiry_id = $(this).attr("id");
    $.ajax({  
          url:"<?php echo base_url('unhold-enquiry'); ?>",  
          method:"post",  
          data:{enquiry_id:enquiry_id},  
          success:function(data){
             console.log(data);
            $('#Mymodal_view').html(data);
          }                             
     }); 
  });  
/*});  

$(document).ready(function(){*/
  $('.interaction_data').click(function(){  
   var enquiry_id = $(this).attr("id");
    $.ajax({  
          url:"<?php echo base_url('interact'); ?>",  
          method:"post",  
          data:{enquiry_id:enquiry_id},  
          success:function(data){
             console.log(data);
            $('#Mymodal_view').html(data);
          }                             
     }); 
  });  
/*});  

$(document).ready(function(){*/
  $('.review_data').click(function(){  
   var enquiry_id = $(this).attr("id");
    $.ajax({  
          url:"<?php echo base_url('review'); ?>",  
          method:"post",  
          data:{enquiry_id:enquiry_id},  
          success:function(data){
             console.log(data);
            $('#Mymodal_view').html(data);
          }                             
     }); 
  });  
});  
</script>
<!-- <script>
  $(document).ready(function(){
    $('#search_id').click(function(){
      var start=$('#datepicker1').val();
      var end=$('#datepicker').val();

      console.log(start);
      alert(start);
      alert(end);

            
      $.ajax({
        url:'<?php //echo base_url('search-enquiry');?>',
        data:{start:start,end:end},
        type:'post',
        success:function(response){
          console.log(response);
          $('#').html(response);
        }
      });
    });
  });
</script> -->