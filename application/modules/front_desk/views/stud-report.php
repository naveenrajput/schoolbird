
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title text-center">Student Interaction Report</h4>
    </div>
    <div class="modal-body">
     <div class="box" style="border:none; box-shadow:none; margin-bottom:0;">
      <div class="box-body box-profile" style="padding:0 20px;">
        

        <ul class="list-group list-group-bordered">
          <?php 
          if (isset($studreport)) {  
            foreach ($studreport as $key => $addmi_data){
              ?>
              <li class="list-group-item">
                <b>General Awareness</b> <a class="pull-right"><?php echo $addmi_data->generalawareness;?>/100</a>
              </li>
              <li class="list-group-item">
                <b>Mental Ability</b> <a class="pull-right"><?php echo $addmi_data->mentalability;?>/10</a>
              </li>
              <li class="list-group-item">
                <b>Confidence</b> <a class="pull-right"><?php echo $addmi_data->confidence;?>/10</a>
              </li>
              <li class="list-group-item">
                <b>Communication Skills</b> <a class="pull-right"><?php echo $addmi_data->commskills;?>%</a>
              </li>
              <li class="list-group-item">
                <b>Personality</b> <a class="pull-right"><?php echo $addmi_data->personality;?>%</a>
              </li>
            <?php } } ?>
          </ul>

          
        </div>
        <!-- /.box-body -->
      </div>
      
    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
    </div>
  </div>
  <!-- /.modal-content -->
</div>