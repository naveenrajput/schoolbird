<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class=" ">
	<div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Interaction
     
      </h1>
	  <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Front Desk</a></li>
        <li class="active">Interaction</li>
      </ol>
	  </div>
	  <div class="col-md-6 col-xs-12 col-sm-4 content-header">
        <a  href="" class="btn pull-right btn-primary"> <i class="fa fa-plus"></i> &nbsp;Interaction</a>
	  </div>
    </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
       
        <div class="box">
         <div class="box-header with-border mr-top-20 mr-bottom-20 text-center">
			<div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
			<div class="col-md-2" >
			  <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
			  </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive"> 
            <!-- <table id="example" class="display nowrap" style="width:100%">--->
            <table id="example" class="table table-bordered " >
			  <div class="txt-dis">Export in Below Format</div>
              <thead>
                <tr>
                  <th><input type="checkbox" id="selectall">
                    All
                    </input></th>
                  <th>S.no</th>
                  <th>Student <br>Name</th>
                  <th>General <br>Awareness</th>
                  <th>Mental <br>Ability</th>
                  <th>Confidence</th>
                  <th>Communication <br>Skills</th>
                  <th>Personality</th>
                  <th>Previous<br> School</th>
                  <th>Forward</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                    if (isset($interact_data)) {  
                    foreach ($interact_data as $key => $interact_data) {
                ?>
                <tr>
                  <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
                  <td>1</td>
                  <td><?php echo $interact_data->childname;?></td>
                  <td><?php echo $interact_data->generalawareness;?></td>
                  <td><?php echo $interact_data->mentalability;?></td>
                  <td><?php echo $interact_data->confidence;?></td>
                  <td><?php echo $interact_data->commskills;?>%</td>
                  <td><?php echo $interact_data->personality;?>%</td>
                  <td><?php echo $interact_data->previousschool; ?></td>
                  <td><ul class="table-icons">
                  <li>
                    <a href="<?php echo base_url('add-interaction/').$interact_data->enquiry_id; ?>" class="table-icon" title="Add"><i class="fa fa-plus"></i>
                    </a>
                  </li>
                  <li>
                    <a href="" class="table-icon table-review-icon" title="Review" data-toggle="modal" data-target="#Mymodal">
                     <span class="glyphicon glyphicon-eye-open display-icon review_data" id="<?php echo $interact_data->enquiry_id;; ?>"></span> 
                    </a>
                  </li>
                      <li><a href="" class="table-icon" title="Archive"> 
		
                		<span class="display-icon" > <img src="<?php echo base_url();?>assets/dist/img/icon/archive.png" style="vertical-align: baseline;" class=""> </span>
                		</a></li>
                    </ul></td>
                  <td><ul class="table-icons">
                      <li>
                        <a href="<?php echo base_url('edit-interaction/').$interact_data->enquiry_id; ?>" class="table-icon" title="Edit"> 
                          <span class="glyphicon glyphicon-edit display-icon"></span> 
                        </a>
                      </li>
                      
                      <li>
                        <a href="" class="table-icon" title="Set Reminder"  data-toggle="modal" data-target="#Mymodal">
                          <span class="glyphicon glyphicon-time display-icon remind_data" id="<?php echo $interact_data->enquiry_id; ?>"></span>
                        </a>
                      </li>
                      <li><a href="" class="table-icon" title="Print"> <span class="glyphicon glyphicon-print display-icon"></span> </a></li>
					  <li><a href="" class="table-icon" title="Delete"> <span class="glyphicon glyphicon-trash display-icon"></span> </a></li>
                    </ul></td>
                </tr>
                <?php } }?>
              </tbody>
              <tfoot>
                <tr>
                  <th><input type="checkbox" id="selectall">
                    All
                    </input></th>
                  <th>S.no</th>
                   <th>Student <br>Name</th>
                  <th>General <br>Awareness</th>
                  <th>Mental <br>Ability</th>
                  <th>Confidence</th>
                  <th>Communication <br>Skills</th>
                  <th>Personality</th>
                  <th>Previous<br> School</th>
                  <th>Forward</th>
                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body --> 
        </div>
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
<!-- ALL MODALS VIEW -->
<div class="modal fade" id="Mymodal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content" id="Mymodal_view">
      
    </div>
    <!-- /.modal-content --> 
  </div>
<!-- /.modal-dialog --> 
</div>

<div class="modal fade" id="modal-reminder">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Reminder</h4>
      </div>
      <div class="modal-body">
        <ul class="timeline timeline-inverse">
          <!-- timeline time label -->
          <li class="time-label"> <span class="bg-red"> 10 Feb. 2019 </span> </li>
          <!-- /.timeline-label --> 
          <!-- timeline item -->
          <li> <i class="fa fa-comments bg-blue"></i>
            <div class="timeline-item"> <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
              <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
              <div class="timeline-body"> Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                weebly ning heekya handango imeem plugg dopplr jibjab, movity
                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                quora plaxo ideeli hulu weebly balihoo... </div>
            </div>
          </li>
          <!-- END timeline item -->
          
          <li class="time-label"> <span class="bg-green"> 3 Mar. 2019 </span> </li>
          <!-- /.timeline-label --> 
          <!-- timeline item -->
          <li> <i class="fa fa-comments bg-yellow"></i>
            <div class="timeline-item"> <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>
              <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
              <div class="timeline-body"> Take me to your leader!
                Switzerland is small and neutral!
                We are more like Germany, ambitious and misunderstood! </div>
            </div>
          </li>
          <!-- END timeline item -->
          
        </ul>
        <textarea class="form-control" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<div class="modal fade" id="modal-review">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Review</h4>
      </div>
      <div class="modal-body">
        <div class="callout callout-info" >
          <h4><i class="fa fa-info"></i> Student Details:</h4>
          <a href="#" target="_blank" >https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js</a> </div>
		  <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  	<option value="Name1">Name1 </option>
			<option value="Name2">Name2</option>
			<option value="Name3">Name3</option>
			<option value="Name4">Name1</option>
			<option value="Name5">Name5</option>
			<option value="Name6">Name6</option>
                
                </select>
        <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        lengthChange: false,
		autoWidth : true,
        buttons: [   'csv', 'excel', 'pdf', 'print' ],
		
		
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
		
} );


$(document).ready(function () {
    $('#selectall').click(function () {
        $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function () {
        var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
        $('#selectall').prop("checked", check);
    });
});


</script> 
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
    })
//Date picker
    $('#datepicker1').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })

  $(document).ready(function(){
      $('.review_data').click(function(){  
       var enquiry_id = $(this).attr("id");
        $.ajax({  
              url:"<?php echo base_url('review'); ?>",  
              method:"post",  
              data:{enquiry_id:enquiry_id},  
              success:function(data){
                 console.log(data);
                $('#Mymodal_view').html(data);
              }                             
         }); 
      });

      $('.remind_data').click(function(){  
        var enquiry_id = $(this).attr("id");
        $.ajax({  
              url:"<?php echo base_url('Front_desk/Int_reminder'); ?>",  
              method:"post",  
              data:{enquiry_id:enquiry_id},  
              success:function(data){
                 console.log(data);
                $('#Mymodal_view').html(data);
              }                             
         }); 
      });   
  });
</script> 

