
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class=" ">
	<div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Admit Child
     
      </h1>
	  <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Front Desk</a></li>
        <li class="active">  Admit Child </li>
      </ol>
	  </div>
	  
    </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row"> 
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <!-- enquiry form start -->
		
		
		
		
        <div class="box ">
          <div class="box-header with-border">
            <h3 class="box-title">Add Details</h3>
          </div>
          <!-- /.box-header --> 
          <!-- enquiry form start -->
		      <input type="checkbox" class="read-more-state" id="post-2" />
			  <div class="read-more-wrap">
		   <form role="form ">
		<div class="box-body  ">
			  <div class="row">
			  <div class="col-md-5">
			  <div class="form-group">
                  <label for="">Child's Name </label>
                  <input type="text" class="form-control"  placeholder="Shivam Yadav" disabled>
                </div>
			  </div>
			    <div class="col-md-5">
				<div class="form-group">
                <label>Date of Birth</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="05/29/2001" disabled>
                </div>
                <!-- /.input group -->
              </div>
				</div>
				
			  <div class="col-md-2 col-xs-12 imgUp pull-right">
						<div class="imagePreview"></div>
						<label class="btn btn-upload btn-primary">
							Child's Photo<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
							</label>
						</div>
						<div class="col-md-4">
			  <div class="form-group">
                  <label for="">Gender </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                  <option >Select</option>
                  <option selected="selected">Male</option>
                  <option>Female</option>

                </select>
                </div>
			  </div>
						<div class="col-md-4">
				<div class="form-group">
                  <label for="">Category   </label>
                 <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                  <option >Select</option>
               
                  <option >General</option>
                  <option selected="selected">OBC</option>
                  <option>ST</option>
                  <option>SC</option>
                  <option>None</option>
                
                </select>
                </div>
				</div>
				
				<div class="col-md-2">
				<div class="form-group">
                  <label for="">Want Admission to Class   </label>
                 <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                  <option >Select</option>
               
                  <option>KG-1</option>
                  <option>KG-2</option>
                   <option selected="selected">1</option>
                   <option>2</option>
                   <option>3</option>
                   <option>4</option>
                   <option>5</option>
                   <option>6</option>
                   <option>7</option>
                   <option>8</option>
                   <option>9</option>
                   <option>10</option>
                   <option>11</option>
                   <option>12</option>
                 
                
                </select>
                </div>
				</div>
						
						
				</div><!---/row---->
				<div class="read-more-target">
				<div class="row " >
					<div class="col-md-12 "> <p class="lead">Family Details  </p></div>
			  <div class="col-md-6">
			  <div class="form-group">
                  <label for="">Father's Name </label>
                  <input type="text" class="form-control"  placeholder="Ravi Yadav" disabled>
                </div>
			  </div>
			    <div class="col-md-6">
				<div class="form-group">
                  <label for="">Father's Profession </label>
                  <input type="text" class="form-control" placeholder="Doctor " disabled>
                </div>
				</div>
				
				<div class="col-md-6">
			  <div class="form-group">
                  <label for="">Mother's Name </label>
                  <input type="text" class="form-control"  placeholder="Deepa Yadav" disabled>
                </div>
			  </div>
			    <div class="col-md-6">
				<div class="form-group">
                  <label for="">Mother's Profession </label>
                  <input type="text" class="form-control" placeholder="House Wife" disabled>
                </div>
				</div>
				 
				
				<div class="col-md-4">
			 <div class="form-group">
                <label>Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask placeholder="0987654321" disabled>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
			  </div>
			    <div class="col-md-4">
				<div class="form-group">
                <label>Alternate Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask placeholder="0987654321" disabled>
                </div>
                <!-- /.input group -->
              </div>
				</div>
				
				<div class="col-md-4">
			 <div class="form-group">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="a@gmail.com" disabled>
              </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
			  </div>
			    
				 <div class="col-md-12">
				<div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" rows="3" placeholder="vandana nagar indore mp" disabled></textarea>
                </div>
				</div>
				 </div>
				 
				  <div class="row">
				<div class="col-md-12 "> <p class="lead">Special Ability</p></div>
					<div class="col-md-3">
			  <div class="form-group">
			
			  <input type="checkbox" class="sibling-hide" id="myCheck3" onclick="myFunction3()">
				   <label>&nbsp; Special Ability  </label>
				</div>
			  </div>
			  <div class="col-md-9" id="text3" style="display: none;">
			  
			   
			  <div class="form-group col-md-6">
			    <label>Select  </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                  <option selected="selected">Select</option>
                  <option>Type 1</option>
                  <option>Type 2</option>
                  <option>Type 3</option>
                  <option>Type 4</option>
                  <option>Other</option>

                </select>
                </div>
				
				<div class="form-group col-md-6">
			     <label>Certificate </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="Certificate" disabled>
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
				 
			 </div>
				
				</div>
				 <div class="row">
				
				
				<div class="col-md-12"> <p class="lead">Previous School Information </p></div>
			  
				
				<div class="col-md-4">
			  <div class="form-group">
                  <label for="">Current School’s Name </label>
                  <input type="text" class="form-control"  placeholder="St.Pauls Indore" disabled>
                </div>
			  </div>
			  <div class="col-md-4">
				<div class="form-group">
                  <label for="">Studying in Class  </label>
                 <select class="form-control select" style="width: 100%;" data-placeholder="Select" disabled>
                  <option>Select</option>
               
                  <option>KG-1</option>
                  <option  selected="selected">KG-2</option>
                   <option>1</option>
                   <option>2</option>
                   <option>3</option>
                   <option>4</option>
                   <option>5</option>
                   <option>6</option>
                   <option>7</option>
                   <option>8</option>
                   <option>9</option>
                   <option>10</option>
                   <option>11</option>
                   <option>12</option>
                 
                
                </select>
                </div>
				</div>
				<div class="col-md-4">
			  <div class="form-group">
                  <label for=""> Board 	 </label>
                  <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                  <option>Select</option>
                  <option  selected="selected">MP</option>
                  <option>CBSE</option>
                  <option>ISCE</option>
                  <option>Other</option>
				    </select>
                </div>
			  </div>
			  	<div class="col-md-12"> <p class="lead"> <small class="s-name" style="font-size:14px;">School Name</small> Information </p></div>
			  
			  <div class="col-md-6">
				<div class="form-group">
                  <label for="">How did you come to know about us?    </label>
                 <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                  <option >Select</option>
                  	<option value="Paper Advertisement">Paper Advertisement </option>
			<option value="Hoarding">Hoarding</option>
			<option value="Existing parents">Existing parents</option>
			<option value="Friends" selected="selected">Friends</option>
			<option value="Internet Search">Internet Search</option>
			<option value="others">Others</option>
                
                </select>
                </div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
                  <label for="">Your Expectation from <small class="s-name">School Name</small></label>
                 <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                  <option >Select</option>
                  <option value="Good Infrastructure" selected="selected">Good Infrastructure </option>
			<option value="Extracurricular Activities">Extracurricular Activities</option>
			<option value="Sports">Sports</option>
			<option value="Well balanced curriculum">Well balanced curriculum</option>
			<option value="others">Others</option>
                
                </select>
                </div>
				</div>
				
			  
				<div class="col-md-12">
				<div class="form-group">
                  <label>Counselor’s Remarks </label>
                  <textarea class="form-control" rows="3" placeholder="Counselor’s Remarks " disabled></textarea>
                </div>
				</div>
				
				
				
				
			  </div>
				</div>
				  
                
              </div>
				   </form>
				</div>
				 <label for="post-2" class="read-more-trigger btn btn-primary"></label>
			 <label  class="btn btn-primary">Edit</label>

          <!-- enquiry form start -->
          <!-- form start -->
		  <form role="form">
            <div class="box-body">
		    <input type="checkbox" class="read-more-state1" id="post-3" />
			  <div class="read-more-wrap1">
			  
			   
              <div class="row">
               
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Interaction Date </label>
                    <div class="input-group date">
                      <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                      <input type="text" class="form-control pull-right" id="datepicker" placeholder="12/05/2019" disabled>
                    </div>
                    <!-- /.input group --> 
                  </div>
                </div>
               </div>
			   <div class="row">
			   <div class="col-md-12 "> <p class="lead">Interaction With Students</p></div>
                <div class="col-md-6">
               
                  <div class="form-group">
                    <table class="table  table-hover table-bordered">
                      <tbody>
                        <tr>
                          <th class="col-md-4" >General Awareness</th>
                          <td  class="col-md-8" ><div class="form-group">
                              <div class="input-group">
                                <select class="form-control select2" style="width: 100%;" data-placeholder="Select" disabled>
                                  <option >Select</option>
                                  <option value="1">1</option>
                                
                                  <option value="2">2</option>
                                  <option value="13">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                  <option value="6">6</option>
                                  <option value="7" selected="selected">70</option>
                                  <option value="8">8</option>
                                  <option value="9">9</option>
								    <option value="10">10</option>
                                  <option value="11">11</option>
                                  <option value="12">12</option>
                                </select>
                                <span class="input-group-addon"><strong>/ 100</strong></span> </div>
                              <!-- /.input group --> 
                            </div></td>
							
							
							
                        </tr>
                        
                        <tr>
                          <th>Confidence</th>
                          <td  class="col-md-8" ><div class="form-group">
                              <div class="input-group">
                                <select class="form-control select2" style="width: 100%;" data-placeholder="Select" disabled>
                                  <option >Select</option>
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                  <option value="6" selected="selected" disabled>6</option>
                                  <option value="7">7</option>
                                  <option value="8">8</option>
                                  <option value="9">9</option>
                                  <option value="10">10</option>
                                </select>
                                <span class="input-group-addon"><strong>/ 10</strong></span> </div>
                              <!-- /.input group --> 
                            </div></td>
                        </tr>
                       
                        <tr>
                          <th>Personality</th>
                          <td  class="col-md-8" ><div class="form-group">
                              <div class="input-group">
                                <select class="form-control select2" style="width: 100%;" data-placeholder="Select" disabled>
                                  <option >Select</option>
                                  <option value="10">10</option>
                                  <option value="20">20</option>
                                  <option value="30">30</option>
                                  <option value="40">40</option>
                                  <option value="50" selected="selected">50</option>
                                  <option value="60">60</option>
                                  <option value="70">70</option>
                                  <option value="80">80</option>
                                  <option value="90">90</option>
                                  <option value="100">100</option>
                                </select>
                                <span class="input-group-addon"><strong>%</strong></span> </div>
                              <!-- /.input group --> 
                            </div></td>
                        </tr>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
				<div class="col-md-6">
               
                  <div class="form-group">
                    <table class="table  table-hover table-bordered">
                      <tbody>
                       
                        <tr>
                          <th>Mental Ability</th>
                          <td  class="col-md-8" ><div class="form-group">
                              <div class="input-group">
                                <select class="form-control select2" style="width: 100%;" data-placeholder="Select" disabled>
                                  <option >Select</option>
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3" >3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                  <option value="6" selected="selected">6</option>
                                  <option value="7">7</option>
                                  <option value="8">8</option>
                                  <option value="9">9</option>
                                  <option value="10">10</option>
                                </select>
                                <span class="input-group-addon"><strong>/ 10</strong></span> </div>
                              <!-- /.input group --> 
                            </div></td>
                        </tr>
                        <tr>
                         
                        <tr>
                          <th>Communication Skills</th>
                          <td  class="col-md-8" ><div class="form-group">
                              <div class="input-group">
                                <select class="form-control select2" style="width: 100%;" data-placeholder="Select" disabled>
                                  <option selected="selected">Select</option>
                               
                                  <option value="10">10</option>
                                  <option value="20">20</option>
                                  <option value="30">30</option>
                                  <option value="40">40</option>
                                  <option value="50">50</option>
                                  <option value="60">60</option>
                                  <option value="70" selected="selected">70</option>
                                  <option value="80">80</option>
                                  <option value="90">90</option>
                                  <option value="100">100</option>
                                </select>
                                <span class="input-group-addon"><strong> %</strong></span> </div>
                              <!-- /.input group --> 
                            </div></td>
                        </tr>
                        
                        <tr>
                          <th>Previous School</th>
                          <td  class="col-md-8" ><div class="form-group">
                                <input type="text" class="form-control"  placeholder="Previous School" disabled>
                              <!-- /.input group --> 
                            </div></td>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
				<div class="col-md-12">
			  <div class="form-group">
                  <label for="">Remarks for Student </label>
                <textarea class="form-control" rows="3" placeholder="Remarks for Student " spellcheck="false" disabled></textarea>
                </div>
			  </div>
				
				</div>
					<div class="read-more-target1">
					<div class="row">
				 <div class="col-md-12 "> <p class="lead">Interaction With Parents</p></div>
                <div class="col-md-6">
                  <label for="">Interaction With Father </label>
                  <div class="form-group">
                    <table class="table  table-hover table-bordered">
                      <tbody>
                        <tr>
                          <th class="col-md-4" >Father's Name</th>
                          <td  class="col-md-8" ><div class="form-group">
                              <input type="text" class="form-control"  placeholder="Vijay Sharma" disabled>
                            </div></td>
                        </tr>
						 <tr>
                          <th class="col-md-4" >Language</th>
                          <td  class="col-md-8" >
						  <div class="form-group">
						    <label>English </label>
							<div class=" ">
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red"  checked>
				  Read
                </label>
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red" checked>
				  Write
                </label>
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red" disabled>
                 Speak
                </label>
              </div>	  
                            </div>
							
							<div class="form-group">
						    <label>Hindi </label>
							<div class=" ">
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red" checked>
				  Read
                </label>
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red"checked>
				  Write
                </label>
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red" checked >
                 Speak
                </label>
              </div>	  	  
                            </div>
							
							
							</td>
                        </tr>
						
                        <tr>
                          <th>Qualification </th>
                        <td  class="col-md-8" ><div class="form-group">
						
						
					
						
						 <select class="form-control select2" style="width: 100%;" data-placeholder="Select" disabled>
                       <option >Select</option>
                                  <option value="Till School">Till School</option>
                                  <option value="BA">BA</option>
                                  <option value="BSC" selected="selected">BSC</option>
                                  <option value="BCOM">BCOM</option>
                                  <option value="BE">BE</option>
                                  <option value="50">B. Tech</option>
                                  <option value="60">MA</option>
                                  <option value="60">MSC</option>
                                  <option value="70">MCOM</option>
                                  <option value="80">M.Tech</option>
                                  <option value="90">PGDCA</option>
                                  <option value="100">100</option>
                    </select>
						 
                            </div></td>
                        </tr>
                        <tr>
                          <th>Overall Understanding</th>
                          <td  class="col-md-8" ><div class="form-group">
                              <input type="text" class="form-control"  placeholder="Overall Understanding" disabled>
                            </div></td>
                        </tr>
                      
                        <tr>
                          <th>Any Other Observation</th>
                          <td  class="col-md-8" ><div class="form-group">
                             <textarea class="form-control" rows="3" placeholder="Any Other Observation" spellcheck="false" disabled></textarea>
                            </div></td>
                        </tr>
						<tr>
                          <th> Remarks for Father</th>
                          <td  class="col-md-8" ><div class="form-group">
                             <textarea class="form-control" rows="3" placeholder="Remarks" spellcheck="false" disabled></textarea>
                            </div></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
				
				<div class="col-md-6">
                  <label for="">Interaction With Mother </label>
                  <div class="form-group">
                    <table class="table  table-hover table-bordered">
                      <tbody>
                        <tr>
                          <th class="col-md-4" >Mother's Name</th>
                          <td  class="col-md-8" ><div class="form-group">
                              <input type="text" class="form-control"  placeholder="Vinita Sharma" disabled>
                            </div></td>
                        </tr>
						 <tr>
                          <th class="col-md-4" >Language</th>
                          <td  class="col-md-8" >
						  <div class="form-group">
						    <label>English </label>
							<div class=" ">
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red" checked>
				  Read
                </label>
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red" checked>
				  Write
                </label>
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red" disabled>
                 Speak
                </label>
              </div>	  
                            </div>
							
							<div class="form-group">
						    <label>Hindi </label>
							<div class=" ">
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red" checked >
				  Read
                </label>
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red" checked>
				  Write
                </label>
                <label class="col-md-4">
                  <input type="checkbox" class="flat-red" checked >
                 Speak
                </label>
              </div>	  	  
                            </div>
							
							
							</td>
                        </tr>
						
                        <tr>
                          <th>Qualification </th>
                        <td  class="col-md-8" ><div class="form-group">
						
						
					
						
						 <select class="form-control select2" style="width: 100%;" data-placeholder="Select" disabled>
                       <option >Select</option>
                                  <option value="Till School">Till School</option>
                                  <option value="BA" selected="selected">BA</option>
                                  <option value="BSC">BSC</option>
                                  <option value="BCOM">BCOM</option>
                                  <option value="BE">BE</option>
                                  <option value="50">B. Tech</option>
                                  <option value="60">MA</option>
                                  <option value="60">MSC</option>
                                  <option value="70">MCOM</option>
                                  <option value="80">M.Tech</option>
                                  <option value="90">PGDCA</option>
                                  <option value="100">100</option>
                    </select>
						 
                            </div></td>
                        </tr>
                        <tr>
                          <th>Overall Understanding</th>
                          <td  class="col-md-8" ><div class="form-group">
                              <input type="text" class="form-control"  placeholder="Overall Understanding" disabled>
                            </div></td>
                        </tr>
                      
                        <tr>
                          <th>Any Other Observation</th>
                          <td  class="col-md-8" ><div class="form-group">
                             <textarea class="form-control" rows="3" placeholder="Any Other Observation" spellcheck="false" disabled></textarea>
                            </div></td>
                        </tr>
						<tr>
                          <th> Remarks for Mother</th>
                          <td  class="col-md-8" ><div class="form-group">
                             <textarea class="form-control" rows="3" placeholder="Remarks" spellcheck="false" disabled></textarea>
                            </div></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
				
              </div>
					
					</div>
				
            
            </div>
            <!-- /.box-body -->
              
		   <label for="post-3" class="read-more-trigger1 btn btn-primary"></label>
			 <label  class="btn btn-primary">Edit</label>
         
          
          </form>
			 
		 <div class="row">
		 		  <div class="col-md-12 "> <p class="lead">Remaining Field</p></div>
			
			  
				<div class="col-md-4">
			  <div class="form-group">
                  <label for="">Samagrah Id </label>
                  <input type="text" class="form-control" placeholder="Samagrah Id  ">
                </div>
			  </div>
			  <div class="col-md-4">
			  <div class="form-group">
                  <label for="" class="">Aadhar Card</label>
                  <input type="text" class="form-control" placeholder="Aadhar Card ">
                </div>
			  </div>
				
				 <div class="col-md-4">
			  <div class="form-group">
                  <label for="" class="">Municipal Corporation Ward No.  </label>
                  <input type="text" class="form-control" placeholder="Ward No. ">
                </div>
			  </div>
			  
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Caste  </label>
                  <input type="text" class="form-control" placeholder="Caste ">
                </div>
			  </div>
			  
			    
			    <div class="col-md-3">
			  <div class="form-group">
                  <label for="" class="">Religion </label>
                  <input type="text" class="form-control" placeholder="Religion">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="" class="">Mother Tongue </label>
                  <input type="text" class="form-control" placeholder="Mother Tongue ">
                </div>
			  </div>
			    <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Nationality  </label>
                  <input type="text" class="form-control" placeholder="Nationality ">
                </div>
			  </div>
			    <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Blood Group  </label>
                  <input type="text" class="form-control" placeholder="Blood Group ">
                </div>
			  </div>
			
				 
				 
				
			
		 
		 
		 </div>
		 <div class="row">
			    <div class="col-md-12 "> <p class="lead">Upload Documents </p></div>
			   
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label> Date Of Birth Certificate	 </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			   <div class="col-md-4 ">
			   <div class="form-group">
			     <label>SSSMID </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			   <div class="col-md-4 ">
			   <div class="form-group">
			     <label>Aadhar Card </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="Aadhar Card">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label> Transfer Certificate (Original) </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="Transfer Certificate">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label>Caste Certificate </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="Caste Certificate">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label>Address Proof</label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="Address Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			 
						
						</div>
		 
		  <div class="row">
			    <div class="col-md-12 "> <p class="lead">Father's Details  </p></div>
			   <div class="col-md-2 col-xs-12 imgUp pull-right">
						<div class="imagePreview"></div>
						<label class="btn btn-upload btn-primary">
							Father Pic<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
							</label>
						</div><!-- col-2 -->
			  <div class="col-md-10 remove-pd">
			  
				
				<div class="col-md-4">
			  <div class="form-group">
                <label>Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask="&quot;mask&quot;: &quot; 99999-99999&quot;" data-mask="">
                </div>
                <!-- /.input group -->
              </div>
			  </div>
				<div class="col-md-4">
			  <div class="form-group">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="Email">
              </div>
                <!-- /.input group -->
              </div>
			  </div>
			
				
				<div class="col-md-4 ">
			   <div class="form-group">
			     <label>ID Proof </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			    
				
				</div>
			 
						
						</div>
						<div class="row">
			    <div class="col-md-12 "> <p class="lead">Mother's Details  </p></div>
			   <div class="col-md-2 col-xs-12 imgUp pull-right">
						<div class="imagePreview"></div>
						<label class="btn btn-upload btn-primary">
							Mother Pic<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
							</label>
						</div><!-- col-2 -->
			  <div class="col-md-10 remove-pd">
			  
				
				<div class="col-md-4">
			  <div class="form-group">
                <label>Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask="&quot;mask&quot;: &quot; 99999-99999&quot;" data-mask="">
                </div>
                <!-- /.input group -->
              </div>
			  </div>
				<div class="col-md-4">
			  <div class="form-group">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="Email">
              </div>
                <!-- /.input group -->
              </div>
			  </div>
			
				
				<div class="col-md-4 ">
			   <div class="form-group">
			     <label>ID Proof </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			    
				
				</div>
			 
						
						</div>
						<div class="row">
			    <div class="col-md-12 "> <p class="lead">Guardian Details  </p></div>
			   <div class="col-md-2 col-xs-12 imgUp pull-right">
						<div class="imagePreview"></div>
						<label class="btn btn-upload btn-primary">
							Guardian Pic<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
							</label>
						</div><!-- col-2 -->
			  <div class="col-md-10 remove-pd">
			  <div class="col-md-6  ">
			  <div class="form-group">
                  <label for="">Guardian's Name</label>
                  <input type="text" class="form-control" placeholder="Father's Name ">
                </div>
				
				</div>
				<div class="col-md-6">
				 <div class="form-group">
                  <label for="">Guardian's Profession </label>
                  <input type="text" class="form-control" placeholder="Father's Profession  ">
                </div>
				</div>
				<div class="col-md-4">
			  <div class="form-group">
                <label>Contact </label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask="&quot;mask&quot;: &quot; 99999-99999&quot;" data-mask="">
                </div>
                <!-- /.input group -->
              </div>
			  </div>
				<div class="col-md-4">
			  <div class="form-group">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="Email">
              </div>
                <!-- /.input group -->
              </div>
			  </div>
			
				
				<div class="col-md-4 ">
			   <div class="form-group">
			     <label>ID Proof </label>
    <input type="file" name="img[]" class="file1">
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled="" placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			    
				<div class="col-md-6">
				 <div class="form-group">
                  <label for="">Relation With </label>
                  <input type="text" class="form-control" placeholder="Relation With">
                </div>
				</div>
				
				<div class="col-md-6">
				 <div class="form-group">
                  <label for="">Address</label>
                <textarea class="form-control" rows="2" placeholder="Address" spellcheck="false"></textarea>
                </div>
				</div>
				</div>
			 
						
						</div>
						
						<div class="row">
			    <div class="col-md-12 "> <p class="lead">Sibling's Details  </p></div>
			   <div class="col-md-3">
			  <div class="form-group">
			 
			  <input type="checkbox" class="sibling-hide" id="myCheck" onclick="myFunction()">
				   <label>&nbsp; Is Sibling? </label>
				</div>
			  </div>
			  <div class="col-md-9" id="text1" style="display: none;">
			  <div class="form-group">
			 
			  <div class="input-group">
                <input type="text" class="form-control" placeholder="Enter Sibling Registration ID   ">
                    <span class="input-group-btn">
                      <div type="button" id="sibling" class="btn btn-primary">Search</div>
                    </span>
              </div>
				</div>
			  </div>
			  <div class="col-md-12 table-responsive" id="sibling-details" style="display:none">
			  
				<table class="table fetchdata table-hover table-bordered">
							  <tbody><tr>
							  <th>Name</th>
								<th>Relation</th>
								<th>Age</th>
								<th>Qualification</th>
								<th>Occupation</th>

								<th>Mobile</th>

							   
							  </tr>
						</tbody>
						<tbody><tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							
						</tr>
						</tbody></table>
						  </div>
			  
			  <div class="col-md-12 "> <p class="lead">Bank Details  </p></div>
			<div class="col-md-6">
			
			  <div class="form-group">
                  <label for="" class="">Annual Family Income  </label>
                  <input type="text" class="form-control" placeholder="Income ">
                </div>
			  </div>
			  
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Name Of Account Holder  </label>
                  <input type="text" class="form-control" placeholder="Account Holder  ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Bank Account Number  </label>
                  <input type="text" class="form-control" placeholder="Account Number  ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Bank Name </label>
                  <input type="text" class="form-control" placeholder="Bank Name ">
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="" class="">IFSC Code  </label>
                  <input type="text" class="form-control" placeholder="IFSC Code  ">
                </div>
			  </div>
			 
			
				<div class="col-md-12 "> <p class="lead">Suggest Class</p></div>
			   <div class="col-md-3">
			  <div class="form-group">
			
			  <input type="checkbox" class="sibling-hide" id="myCheck1" onclick="myFunction1()">
				   <label>&nbsp; Suggest Class</label>
				</div>
			  </div>
			  <div class="col-md-9" id="text2" style="display:none">
			  
			   
			  <div class="form-group col-md-4">
			    <label>Class </label>
                  <div class="input-group">
                <input type="text" class="form-control" placeholder="6th" disabled="">
                    <span class="input-group-btn">
                      <div type="button" id="sibling" class="btn btn-primary">Change</div>
                    </span>
              </div>
                </div>
				<div class="form-group col-md-4">
				 <label>Section </label>
                  <div class="input-group">
                <input type="text" class="form-control" placeholder="B" disabled="">
                    <span class="input-group-btn">
                      <div type="button" id="sibling" class="btn btn-primary">Change</div>
                    </span>
              </div>
                </div>
				<div class="form-group col-md-4">
				 <label>House </label>
                  <div class="input-group">
                <input type="text" class="form-control" placeholder="Red" disabled=""> 
                    <span class="input-group-btn">
                      <div type="button" id="sibling" class="btn btn-primary">Change</div>
                    </span>
              </div>
                </div>
				 
			 </div>
			
				
				</div>
						
		  </div>
		
		
		 
		 <div class="box-footer">
					
					<button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#modal-submit">Submit</button>		
              </div>
		 
        </div>
		
		
        <!-- /.box --> 
        
        <!-- Form Element sizes --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
    <!-- /.row --> 
    
  </section>
  <!-- /.content --> 
</div>
<div class="modal fade" id="modal-submit">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation Message</h4>
              </div>
              <div class="modal-body text-center">
			                <h4 class="modal-title "><b>Reference Number</b></h4>
							
							<p class="lead">1234V4566</p>
							
						
			  </div>
			  
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">OK</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<!-- page script --> 

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
	//Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
	//Date picker
    $('#datepicker2').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

  
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
  
  
 // more hide


</script>
