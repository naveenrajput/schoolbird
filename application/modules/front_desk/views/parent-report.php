
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title text-center">Parent Interaction Report</h4>
    </div>
    <div class="modal-body">
      <div class="box" style="border:none; box-shadow:none; margin-bottom:0;">
        <div class="box-body box-profile" style="padding:0 20px;">
          <?php 
          if (isset($parentreport)) {  
            foreach ($parentreport as $key => $addmi_data){
              $F_eng= explode(",",$addmi_data->F_English);
              $F_hin= explode(",",$addmi_data->F_Hindi);
              $M_eng= explode(",",$addmi_data->M_English);
              $M_hin= explode(",",$addmi_data->M_Hindi);
              ?>    

              <div class="col-md-6">
                 <h4 class="modal-title pd-bottom-10 text-center">Father Interaction Report</h4>
              <ul class="list-group list-group-bordered">
                <li class="list-group-item" style="padding-bottom:30px;">
                  <b>Language:</b>

                  <div class="form-group text-center  ">
                   
                    <div class=" ">
                      <label class="col-md-3">English </label>
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Read',$F_eng)) { echo "checked";} ?> > Read
                      </label>
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Write',$F_eng)) { echo "checked";} ?> > Write
                      </label>
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Speak',$F_eng)) { echo "checked";} ?> > Speak
                      </label>
                    </div>    
                  </div>
                  <hr>
                  <div class="form-group text-center ">
                   
                    <div class="">
                      <label class="col-md-3">Hindi </label>  
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Read',$F_hin)) { echo "checked";} ?> > Read
                      </label>
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Write',$F_hin)) { echo "checked";} ?> > Write
                      </label>
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Speak',$F_hin)) { echo "checked";} ?> > Speak
                      </label>
                    </div>    
                  </div>


                </li>
                <li class="list-group-item">
                  <b>Qualification:</b><?php echo $addmi_data->F_qualification;?>
                </li>
                <li class="list-group-item">
                  <b>Overall Understanding:</b> 
                  <p class=""><?php echo $addmi_data->F_overall;?></p>
                </li><li class="list-group-item">
                  <b>Any Other Observation:</b> 
                  <p class=""><?php echo $addmi_data->F_observation;?></p>
                </li>
                
              </ul>

              </div>
                <div class="col-md-6">
                   <h4 class="modal-title pd-bottom-10 text-center">Mother Interaction Report</h4>
              <ul class="list-group list-group-bordered">
                <li class="list-group-item" style="padding-bottom:30px;">
                  <b>Language:</b>

                  <div class="form-group text-center  ">
                   
                    <div class=" ">
                      <label class="col-md-3">English </label>
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Read',$M_eng)) { echo "checked";} ?> > Read
                      </label>
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Write',$M_eng)) { echo "checked"; }?> > Write
                      </label>
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Speak',$M_eng)) { echo "checked"; }?> > Speak
                      </label>
                    </div>    
                  </div>
                  <hr>
                  <div class="form-group text-center ">
                   
                    <div class="">
                      <label class="col-md-3">Hindi </label>  
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Read',$M_hin)) { echo "checked";} ?> > Read
                      </label>
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Write',$M_hin)) { echo "checked"; }?> > Write
                      </label>
                      <label class="col-md-3">
                        <input type="checkbox" class="flat-red" <?php if (in_array('Speak',$M_hin)) { echo "checked"; }?> > Speak
                      </label>
                    </div>    
                  </div>


                </li>
                <li class="list-group-item">
                  <b>Qualification:</b><?php echo $addmi_data->M_qualification;?>
                </li>
                <li class="list-group-item">
                  <b>Overall Understanding:</b> 
                  <p class=""><?php echo $addmi_data->M_overall;?></p>
                </li><li class="list-group-item">
                  <b>Any Other Observation:</b> 
                  <p class=""><?php echo $addmi_data->M_observation;?></p>
                </li>
                
              </ul>

                </div>
             

             
      
            
          </div>
        <?php } } ?>


        <!-- /.box-body -->
      </div>

    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary">OK</button>
    </div>
  </div>