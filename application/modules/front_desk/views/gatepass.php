
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class=" ">
   <div class="col-md-6 col-xs-12 col-sm-8 content-header">
    <h1 class="">
      Gate Pass

    </h1>
    <ol class="breadcrumb" style="background:none;">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Front Desk</a></li>
      <li class="active">gate pass</li>
    </ol>
  </div>
  <div class="col-md-6 col-xs-12 col-sm-4 content-header">
    <a  href="<?php echo base_url('add-gatepass');?>" class="btn pull-right btn-primary"> <i class="fa fa-plus"></i> &nbsp;Generate</a>
  </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row"> 
  
  <!-- /.col -->
  <div class="col-md-12">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#stud" data-toggle="tab">Student Gatepass</a></li>
        <li><a href="#emp" data-toggle="tab">Employee Gatepass</a></li>
       
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="stud">
          <div class="box" style="border:none;">
            <div class="box-header with-border mr-top-10 mr-bottom-10 text-center">

              <form method="post" action="<?php //echo base_url('search-enquiry');?>" data-toggle="validator" role="form">
              <div class="form-group col-md-5">
                <div class="input-group date">
                  <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                  <input type="text" name="start_date" class="form-control pull-right search_data datepicker" placeholder="Start Date">
                </div>
                <!-- /.input group --> 
              </div>
              <!-- /.form group --> 
              
              <!-- Date range -->
              <div class="form-group col-md-5">
                <div class="input-group date">
                  <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                  <input type="text" name="end_date" class="form-control pull-right search_data datepicker" placeholder="End Date">
                </div>
                <!-- /.input group --> 
              </div>
             
              <div class="col-md-2">
                <button type="button" class="btn btn-primary" id="search_id"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              </div>
         </form>
            </div>
            <!-- /.box-header -->
            <div class="col-md-12 col-xs-12 col-sm-12 margin ">
              <div class="btn-group pull-right">
              <button class="btn btn-primary" type="button">
              Print
              </div>
              <div class="btn-group pull-right mr-right-10">
              <a class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-export">Export</a> </div>
            <div class="btn-group pull-right mr-right-10"> <a href="#"  data-toggle="modal" data-target="#modal-import" class="btn btn-primary" type="button">Import</a> </div>
          </div>
          <div class="box-body table-responsive ">            
				<table id="example" class="table table-bordered " >
      
         <thead>
          <tr>
            <th><input type="checkbox" id="selectall">
              All
            </input></th>
            <th>S.no</th>
            <th>Name</th>
            <th>Class</th>
            <th>Section</th>
            <th>Time In</th>
            <th>Contact</th>
            <th>Purpose Of Going Out</th>
            <th>File</th>


            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            if (isset($gatepass)) {  
            $i=1; foreach ($gatepass as $gatepass){
          ?>
            <tr>
              <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
              <td><?php echo $i; ?></td>
              <td><?php foreach($students as $raw ){
                          if($raw->id==$gatepass->studentid){
                           echo $raw->name; }
                  } ?>
              </td>
              <td><?php echo $gatepass->class;?></td>
              <td><?php echo $gatepass->section;?></td>
              <td><?php echo $gatepass->timein;?></td>
              <td><?php echo $gatepass->contact;?></td>
              <td><?php echo $gatepass->purpose;?></td>
              <td><?php echo $gatepass->document;?></td>

              <td><ul class="table-icons">

                <li><a href="" class="table-icon" title="Delete"><span class="glyphicon glyphicon-trash display-icon close-stgp" id="<?php echo $gatepass->id;?>"></span></a></li>

                <li><a href="" class="table-icon" title="Print"> <span class="glyphicon glyphicon-print display-icon"></span> </a></li>
              </ul></td>
            </tr>
          <?php $i++; } } ?>
        </tbody>
        <tfoot>
          <tr>
            <th><input type="checkbox" id="selectall">
              All
            </input></th>
            <th>S.no</th>
            <th>Name</th>
            <th>Class</th>
            <th>Section</th>
            <th>Time In</th>
            <th>Contact</th>
            <th>Purpose Of Going Out</th>
            <th>File</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
          </div>
          <!-- /.box-body --> 
        </div>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="emp">
        <div class="box" style="border:none;">
          <div class="box-header with-border mr-top-10 mr-bottom-10 text-center">
            <div class="form-group col-md-5">
              <div class="input-group date">
                <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                <input type="text" class="form-control pull-right datepicker" placeholder="Start Date">
              </div>
              <!-- /.input group --> 
            </div>
            <!-- /.form group --> 
            
            <!-- Date range -->
            <div class="form-group col-md-5">
              <div class="input-group date">
                <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                <input type="text" class="form-control pull-right datepicker" placeholder="End Date">
              </div>
              <!-- /.input group --> 
            </div>
            <div class="col-md-2">
              <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="col-md-12 col-xs-12 col-sm-12 margin ">
            <div class="btn-group pull-right">
            <button class="btn btn-primary" type="button">
            Print
            </div>
            <div class="btn-group pull-right mr-right-10">
            <a class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-export">Export</a> </div>
          <div class="btn-group pull-right mr-right-10"> <a href="#"  data-toggle="modal" data-target="#modal-import" class="btn btn-primary" type="button">Import</a> </div>
        </div>
        <div class="box-body table-responsive "> 
          
         <table id="example1" class="table table-bordered " >
       
         <thead>
          <tr>
            <th><input type="checkbox" id="selectall">
              All
            </input></th>
            <th>S.no</th>
            <th>Name</th>
            <th>Department</th>
            <th>Time In</th>
            <th>Contact</th>
            <th>Purpose Of Going Out</th>
            <th>File</th>


            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            if (isset($empgatepass)) {  
            $i=1; foreach ($empgatepass as $gatepass){
          ?>
            <tr>
              <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
              <td><?php echo $i; ?></td>
              <td><?php foreach($employ as $raw ){
                          if($raw->id==$gatepass->employeeid ){
                           echo $raw->name; }
                  } ?>
              </td>
              <td><?php foreach($dept as $raw ){
                          if($raw->id==$gatepass->department ){
                           echo $raw->name; }
                  } ?>
              </td>
              <td><?php echo $gatepass->timein;?></td>
              <td><?php echo $gatepass->contact;?></td>
              <td><?php echo $gatepass->purpose;?></td>
              <td><?php echo $gatepass->document;?></td>

              <td><ul class="table-icons">

                <li><a href="" class="table-icon" title="Delete"><span class="glyphicon glyphicon-trash display-icon close-empgp" id="<?php echo $gatepass->id;?>"></span></a></li>

                <li><a href="" class="table-icon" title="Print"> <span class="glyphicon glyphicon-print display-icon"></span> </a></li>
              </ul></td>
            </tr>
          <?php $i++; } } ?>
        </tbody>
        <tfoot>
          <tr>
           <th><input type="checkbox" id="selectall">
              All
            </input></th>
            <th>S.no</th>
            <th>Name</th>
            <th>Department</th>
            <th>Time In</th>
            <th>Contact</th>
            <th>Purpose Of Going Out</th>
            <th>File</th>


            <th>Action</th>
          </tr>
        </tfoot>
      </table>
        </div>
        <!-- /.box-body --> 
      </div>
    </div>
   
  
</div>
<!-- /.tab-content -->
</div>
<!-- /.nav-tabs-custom -->
</div>
<!-- /.col -->
</div>
</section>
<!-- /.content --> 
</div>

<script>
$(document).ready(function(){
    var table = $('#example').DataTable( {
        lengthChange: true,
    autoWidth : true,
       /*  buttons: [   'csv', 'excel', 'pdf', 'print' ], */
    
    
    } );
 
  /*   table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' ); */
    

    var table = $('#example1').DataTable( {
        lengthChange: true,
    autoWidth : true,
       /*  buttons: [   'csv', 'excel', 'pdf', 'print' ], */
    
    
    } );

    $('.close-stgp').click(function(){  
         var stgp= $(this).attr("id");
         var x=confirm("Are you sure to delete record?")
        if (x) {
          $.ajax({  
                    url:"<?php echo base_url('Front_desk/Closegatepass'); ?>",  
                    method:"post",  
                    data:{stgp:stgp},  
                    success:function(data){
                           location.reload();
                    }  
                 }); 
        }else {
          return false;
        } 
   }); 

  $('.close-empgp').click(function(){  
         var empgp= $(this).attr("id");
         var x=confirm("Are you sure to delete record?")
        if (x) {
          $.ajax({  
                    url:"<?php echo base_url('Front_desk/Closegatepass'); ?>",  
                    method:"post",  
                    data:{empgp:empgp},  
                    success:function(data){ 
                           location.reload();
                    }  
                 }); 
        }else {
          return false;
        } 
  }); 
});  
</script> 
