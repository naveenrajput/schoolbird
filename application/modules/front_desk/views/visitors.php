
 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class=" ">
	<div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Visitors
     
      </h1>
	  <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Front Desk</a></li>
        <li class="active">Visitors</li>
      </ol>
	  </div>
	  <div class="col-md-6 col-xs-12 col-sm-4 content-header">
        <a  href="add-visitor" class="btn pull-right btn-info"> <i class="fa fa-plus"></i> &nbsp;Visitor</a>
	  </div>
    </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        
        <div class="box">
          <div class="box-header with-border mr-top-20 mr-bottom-20 text-center">
			<div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right datepicker" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right datepicker" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
			<div class="col-md-2" >
			  <button type="submit" class="btn btn-info"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
			  </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive"> 
            <!-- <table id="example" class="display nowrap" style="width:100%">--->
            <table id="example" class="table table-bordered " >
			  <div class="txt-dis">Export in Below Format</div>
              <thead>
                <tr>
                  <th><input type="checkbox" id="selectall">
                    All
                    </input></th>
                  <th>S.no</th>
                  <th>Name</th>
                  <th>Time In</th>
				   <th>Time Out</th>
				    <th>Contact	</th>
                  <th>Purpose Of Meeting	</th>
					<th>File</th>
                 <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                    if (isset($visitor)) {
                    $i=(1); foreach ($visitor as $key => $visitor){
                ?>
                <tr>
                  <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $visitor->name;?></td>
                  <td><?php echo $visitor->timein;?></td>
                  <td><?php echo $visitor->timeout;?></td>
                  <td><?php echo $visitor->contact;?></td>
                  <td><?php echo $visitor->purpose;?></td>
                  <td>Doc</td>                 
                  <td><ul class="table-icons">
                   
                      <li><a href="" class="table-icon" title="Delete"> <span class="glyphicon glyphicon-trash display-icon"></span> </a></li>
                     
                      <li><a href="" class="table-icon" title="Print"> <span class="glyphicon glyphicon-print display-icon"></span> </a></li>
                    </ul></td>
                </tr>
                <?php $i++; } } ?>
              </tbody>
              <tfoot>
                <tr>
                  <th><input type="checkbox" id="selectall">
                    All
                    </input></th>
                  <th>S.no</th>
                  <th>Name</th>
                  <th>Time In</th>
				   <th>Time Out</th>
				    <th>Contact	</th>
                  <th>Purpose Of Meeting	</th>
					<th>File</th>
                 <th>Action</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body --> 
        </div>
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        lengthChange: false,
		autoWidth : true,
        buttons: [   'csv', 'excel', 'pdf', 'print' ],
		
		
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
		
} );


$(document).ready(function () {
    $('#selectall').click(function () {
        $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function () {
        var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
        $('#selectall').prop("checked", check);
    });
});

</script> 