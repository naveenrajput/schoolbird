<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Review</h4>
</div>
<div class="modal-body">
  <div class="callout callout-info" >
    <h4><i class="fa fa-info"></i> Student Details:</h4>
    <a href="#" target="_blank" >https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js</a> </div>
    <div class="row">
      <div class="col-md-4" id="internal-msg">
        <label class="checkbox-inline">
          <input id="checkthree" type="checkbox" name="toggle" onChange="toggleStatusmsg()" />
        &nbsp; Internal Message </label>
      </div>
      <div class="col-md-4" id="pageForm">
        <label class="checkbox-inline">
          <input id="toggleElement" type="checkbox" name="toggle" onChange="toggleStatus()" />
        &nbsp;Message </label>
      </div>
      <div class="col-md-4 " id="elementsToOperateOn">
        <label class="checkbox-inline">
          <input type="checkbox" name="participate" id="checktwo" onChange="toggleStatusone()" />
        &nbsp;Mail </label>
      </div>
    </div>

    <form method="post" id="second_form" data-toggle="validator" role="form">
      <input type="hidden" name="enquiry_id" value="<?php echo $id; ?>">
      <input type="hidden" name="status" value="ONREVIEW">
      <div class="col-md-12 mr-top-20" id="name2" style="display:none;">
        <div class="col-md-12 ">
          <div class="form-group">
            <label for="">Contact Person </label>
            <select class="form-control select2" name="contactperson_id" style="width: 100%;" data-placeholder="Select">
              <option selected="selected" value="">Select</option>
              <?php foreach ($emp_name as $key => $value) { ?>  
               <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
             <?php } ?>
           </select>
         </div>
       </div>
       <div class="col-md-12">
        <div class="form-group">
          <textarea class="form-control mr-top-10" name="review_comments" rows="3" placeholder="Type a comment  ..."  spellcheck="false"></textarea>
        </div>
      </div>
    </div>
    <div class="col-md-12 mr-top-20" id="name" style="display:none;">
      <div class="col-md-6 ">
        <div class="form-group">
          <label for="">Contact Person </label>
          <select class="form-control select2" name="person_id" id="contact_id" style="width: 100%;" data-placeholder="Select">
            <option selected="selected" value="">Select</option>
            <?php foreach ($emp_name as $key => $value) { ?>  
             <option value="<?php echo $value->id ; ?>"><?php echo $value->name ; ?></option>
           <?php } ?>
         </select>
       </div>
     </div>
     <div class="col-md-6 ">
      <div class="form-group">
        <label for="">Contact No.</label>
        <div class="input-group" >
          <input type="text" class="form-control disabledCheckboxes" placeholder="9087654321" name="contact" id="contactnumber">
          <span class="input-group-btn">
            <div type="button"  class="btn btn-primary change">Change</div>
          </span> 
        </div>
      </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <textarea class="form-control mr-top-10" name="mescomments" rows="3" placeholder="Type a comment  ..."  spellcheck="false"></textarea>
        </div>
      </div>
    </div>
    <div class="col-md-12 mr-top-20" id="name1" style="display:none;">
      <div class="col-md-6 ">
        <div class="form-group">
          <label for="">Contact Person </label>
          <select class="form-control select2" name="contperson_id" id="contperson_id" style="width: 100%;" data-placeholder="Select">
            <option selected="selected" value="">Select</option>
            <?php foreach ($emp_name as $key => $value) { ?>  
             <option value="<?php echo $value->id ; ?>"><?php echo $value->name ; ?></option>
           <?php } ?>
         </select>
       </div>
     </div>
     <div class="col-md-6 ">
      <div class="form-group">
        <label for="">Email</label>
        <div class="input-group" >
          <input type="text" class="form-control disabledCheckboxestwo" name="email" placeholder="abc@gmail.com" id="email_id" value="">
          <span class="input-group-btn">
            <div type="button"  class="btn btn-primary changetwo">Change</div>
          </span> </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <textarea class="form-control mr-top-10" name="mailcomments" rows="3" placeholder="Type a comment  ..."  spellcheck="false"></textarea>
        </div>
      </div>
    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing">Submit</button><div id="success" style="color:red;"></div>
    </div>
  </form>
</div>
<script>
  $(document).ready(function(){
    $('#contact_id').on('change', function(){
      var c_id = $('#contact_id').val();
      var url1 ="<?php echo base_url('Front_desk/Detactnumber'); ?>";
        $.ajax({
          url:url1,
          method:"post",
          data:{c_id:c_id},
          success:function(response){
          $('#contactnumber').val(response);

          }
        });
    });

    $('#contperson_id').on('change', function(){
    var c_id = $('#contperson_id').val();
      $.ajax({
        url:'<?php echo base_url('Front_desk/Detactemail'); ?>',
        method:"post",
        data:{c_id:c_id},
        success:function(response){
          $('#email_id').val(response);
        }
      });
    });

    $('.btn').on('click', function(){
      var $this = $(this);
      $this.button('loading');
        setTimeout(function() {
           $this.button('reset');
       }, 8000);
    });

    $("#second_form").submit(function() {
    $.ajax({
        type : 'POST',
        url : '<?php echo base_url('add-review');?>',
        data : $('#second_form').serialize(),
            success: function(data) {
              location.reload();
              alert(data);
            }
      });
    });
  });
</script>