<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class=" ">
    <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class=""> Interaction Form </h1>
      <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Front Desk</a></li>
        <li class="active">Interaction</li>
      </ol>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;" > <a  href="#" class="btn btn-primary"  data-toggle="modal" data-target="#modal-reschedule"> <i class="glyphicon glyphicon-time"></i> &nbsp; Reschedule</a> </div>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row"> 
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12"> 
        <!-- general form elements --> 
        <!-- enquiry form start -->
        <div class="box ">
          <div class="box-header with-border">
            <h3 class="box-title">Add Details</h3>
          </div>
          <!-- /.box-header --> 
          <!-- enquiry form start -->
          <input type="checkbox" class="read-more-state" id="post-2" />
          <div class="read-more-wrap">
            <form role="form ">
              <?php 
            if (isset($int_data)) {  
              foreach ($int_data as $key => $int_data){
                $F_eng= explode(",",$int_data->F_English);
                $F_hin= explode(",",$int_data->F_Hindi);
                $M_eng= explode(",",$int_data->M_English);
                $M_hin= explode(",",$int_data->M_Hindi);
                ?>
              <div class="box-body  ">
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group">
                      <label for="">Student's Name </label>
                      <input type="text" class="form-control" value="<?php echo $int_data->childname; ?>" disabled>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group">
                      <label>Date of Birth</label>
                      <div class="input-group date">
                        <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                        <input type="text" class="form-control pull-right datepicker" value="<?php echo $int_data->dob; ?>" disabled>
                      </div>
                      <!-- /.input group --> 
                    </div>
                  </div>
                  <div class="col-md-2 col-xs-12 imgUp ">
                    <div class="imagePreview"><img src="../uploads/<?php echo $int_data->childphoto; ?>" width="100%" height="100%"></div>
                    <label class="btn btn-upload btn-primary">Student's Photo</label>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Gender </label>
                      <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                        <option selected="selected"><?php echo $int_data->gender; ?></option>
                        <option >Male</option>
                        <option>Female</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Category </label>
                      <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                        <option >Select</option>
                        <option >General</option>
                        <option selected="selected"><?php echo $int_data->category; ?></option>
                        <option>ST</option>
                        <option>SC</option>
                        <option>None</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label for="">Want Admission to Class </label>
                      <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                        <option selected="selected"><?php echo $int_data->class; ?></option>
                        <option>KG-1</option>
                        <option>KG-2</option>
                        <option >1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                        <option>11</option>
                        <option>12</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!---/row---->
                
                <div class="read-more-target">
                  <div class="row " >
                    <div class="col-md-12 ">
                      <p class="lead">Family Details </p>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Father's Name</label>
                        <input type="text" class="form-control" value="<?php echo $int_data->fathername; ?>" disabled>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Father's Profession </label>
                        <input type="text" class="form-control" value="<?php echo $int_data->fatherprofession;?>" disabled>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Mother's Name </label>
                        <input type="text" class="form-control"  value="<?php echo $int_data->mothername;?>" disabled>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Mother's Profession </label>
                        <input type="text" class="form-control" value="<?php echo $int_data->motherprofession;?>" disabled>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Contact </label>
                        <div class="input-group">
                          <div class="input-group-addon"> <i class="fa fa-phone"></i> </div>
                          <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask value="<?php echo $int_data->contact;?>" disabled>
                        </div>
                        <!-- /.input group --> 
                      </div>
                      <!-- /.form group --> 
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Alternate Contact </label>
                        <div class="input-group">
                          <div class="input-group-addon"> <i class="fa fa-phone"></i> </div>
                          <input type="text" class="form-control" data-inputmask='"mask": " 99999-99999"' data-mask value="<?php echo $int_data->alternatecontact;?>" disabled>
                        </div>
                        <!-- /.input group --> 
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Email </label>
                        <div class="input-group"> <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                          <input type="email" class="form-control" value="<?php echo $int_data->email;?>" disabled>
                        </div>
                        <!-- /.input group --> 
                      </div>
                      <!-- /.form group --> 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" rows="3" disabled><?php echo $int_data->address;?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 ">
                      <p class="lead">Special Ability</p>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <input type="checkbox" class="sibling-hide" id="myCheck3" onClick="myFunction3()">
                        <label>&nbsp; Special Ability </label>
                      </div>
                    </div>
                    <div class="col-md-9" id="text3" style="display: none;">
                      <div class="form-group col-md-6">
                        <label>Select</label>
                        <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                          <option selected="selected"><?php echo $int_data->certificatename;?></option>
                          <option>Type 1</option>
                          <option>Type 2</option>
                          <option>Type 3</option>
                          <option>Type 4</option>
                          <option>Other</option>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label>Certificate </label>
                        <input type="file" name="img[]" class="file1">
                        <div class="input-group ">
                          <input type="text" class="form-control " disabled value="<?php echo $int_data->certificate;?>" disabled>
                          <span class="input-group-btn">
                          <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
                          </span> </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <p class="lead">Previous School Information </p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Current School’s Name </label>
                        <input type="text" class="form-control"  value="<?php echo $int_data->current_school;?>" disabled>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Studying in Class </label>
                        <select class="form-control select" style="width: 100%;" data-placeholder="Select" disabled>
                          <option selected="selected"><?php echo $int_data->studying_class;?></option>
                          <option>KG-1</option>
                          <option>KG-2</option>
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                          <option>6</option>
                          <option>7</option>
                          <option>8</option>
                          <option>9</option>
                          <option>10</option>
                          <option>11</option>
                          <option>12</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for=""> Board </label>
                        <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                          <option selected="selected"><?php echo $int_data->board;?></option>
                          <option>MP</option>
                          <option>CBSE</option>
                          <option>ISCE</option>
                          <option>Other</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <p class="lead"> <small class="s-name" style="font-size:14px;">School Name</small> Information </p>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">How did you come to know about us? </label>
                        <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                          <option selected="selected"><?php echo $int_data->know_aboutus;?></option>
                          <option value="Paper Advertisement">Paper Advertisement </option>
                          <option value="Hoarding">Hoarding</option>
                          <option value="Existing parents">Existing parents</option>
                          <option value="Friends">Friends</option>
                          <option value="Internet Search">Internet Search</option>
                          <option value="others">Others</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Your Expectation from <small class="s-name">School Name</small></label>
                        <select class="form-control select1" style="width: 100%;" data-placeholder="Select" disabled>
                          <option selected="selected"><?php echo $int_data->expectation;?></option>
                          <option value="Good Infrastructure">Good Infrastructure </option>
                          <option value="Extracurricular Activities">Extracurricular Activities</option>
                          <option value="Sports">Sports</option>
                          <option value="Well balanced curriculum">Well balanced curriculum</option>
                          <option value="others">Others</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Counselor’s Remarks </label>
                        <textarea class="form-control" rows="3" placeholder="Counselor’s Remarks " disabled><?php echo $int_data->remarks;?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <label for="post-2" class="read-more-trigger btn btn-primary"></label>
          <a href="<?php echo base_url('edit-enquiry/').base64_encode($int_data->enquiry_id); ?>">
          <label  class="btn btn-primary">Edit</label>
          </a> 
          
          <!-- enquiry form start --> 
          <!-- form start -->
          <form method="post" action="<?php echo base_url('insert-interaction');?>" data-toggle="validator" role="form">
            <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Interaction Date</label>
                  <div class="input-group date">
                    <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                    <input type="text" class="form-control pull-right datepicker"  value="<?php echo $int_data->int_date;?>">
                  </div>
                  <!-- /.input group --> 
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 ">
                <p class="lead">Interaction With Students</p>
              </div>
              <div class="col-md-6">
                <input type="hidden" name="name" class="form-control" value="<?php echo $int_data->childname;?>">
                <input type="hidden" name="F_name" class="form-control" value="<?php echo $int_data->fathername;?>">
                <input type="hidden" name="M_name" class="form-control" value="<?php echo $int_data->mothername;?>">
                <input type="hidden" name="enquiry_id" class="form-control" value="<?php echo $int_data->enquiry_id;?>">
                <div class="form-group">
                  <table class="table  table-hover table-bordered">
                    <tbody>
                      <!-- <tr>
                <th class="col-md-4" >Student's Name</th>
                <td  class="col-md-8" ><div class="form-group">
                  <input type="text" name="name" class="form-control" value="<?php //echo $int_data->name;?>" disabled>
                </div></td>
              </tr> -->
                      <tr>
                        <th class="col-md-4" >General Awareness</th>
                        <td  class="col-md-8" ><div class="form-group">
                            <div class="input-group">
                              <select class="form-control select2" name="generalawareness" style="width: 100%;" data-placeholder="Select">
                                <option value="<?php echo $int_data->generalawareness;?>" selected="selected"><?php echo $int_data->generalawareness;?></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                              </select>
                              <span class="input-group-addon"><strong>/ 10</strong></span> </div>
                            <!-- /.input group --> 
                          </div></td>
                      </tr>
                      <tr>
                        <th>Confidence</th>
                        <td  class="col-md-8" ><div class="form-group">
                            <div class="input-group">
                              <select class="form-control select2" name="confidence" style="width: 100%;" data-placeholder="Select">
                                <option selected="selected"><?php echo $int_data->confidence;?></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                              </select>
                              <span class="input-group-addon"><strong>/ 10</strong></span> </div>
                            <!-- /.input group --> 
                          </div></td>
                      </tr>
                      <tr>
                        <th>Personality</th>
                        <td  class="col-md-8" ><div class="form-group">
                            <div class="input-group">
                              <select class="form-control select2" name="personality" style="width: 100%;" data-placeholder="Select">
                                <option selected="selected"><?php echo $int_data->personality;?></option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                                <option value="50">50</option>
                                <option value="60">60</option>
                                <option value="70">70</option>
                                <option value="80">80</option>
                                <option value="90">90</option>
                                <option value="100">100</option>
                              </select>
                              <span class="input-group-addon"><strong>%</strong></span> </div>
                            <!-- /.input group --> 
                          </div></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <table class="table  table-hover table-bordered">
                    <tbody>
                      <tr>
                        <th>Mental Ability</th>
                        <td  class="col-md-8" ><div class="form-group">
                            <div class="input-group">
                              <select class="form-control select2" name="mentalability" style="width: 100%;" data-placeholder="Select">
                                <option selected="selected"><?php echo $int_data->mentalability;?></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                              </select>
                              <span class="input-group-addon"><strong>/ 10</strong></span> </div>
                            <!-- /.input group --> 
                          </div></td>
                      </tr>
                      <tr>
                      <tr>
                        <th>Communication Skills</th>
                        <td  class="col-md-8" ><div class="form-group">
                            <div class="input-group">
                              <select class="form-control select2" name="commskills" style="width: 100%;" data-placeholder="Select">
                                <option selected="selected"><?php echo $int_data->commskills;?></option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                                <option value="50">50</option>
                                <option value="60">60</option>
                                <option value="70">70</option>
                                <option value="80">80</option>
                                <option value="90">90</option>
                                <option value="100">100</option>
                              </select>
                              <span class="input-group-addon"><strong> %</strong></span> </div>
                            <!-- /.input group --> 
                          </div></td>
                      </tr>
                      <tr>
                        <th>Previous School</th>
                        <td  class="col-md-8" ><div class="form-group">
                            <input type="text" name="previousschool" class="form-control"  value="<?php echo $int_data->current_school ?>">
                            <!-- /.input group --> 
                          </div></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="">Remarks for Student </label>
                  <textarea class="form-control" name="stud_remark" rows="3" value="<?php echo $int_data->stud_remark;?>" spellcheck="false"><?php echo $int_data->stud_remark;?></textarea>
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-md-12 ">
              <p class="lead">Interaction With Parents</p>
            </div>
            <div class="col-md-6">
              <label for="">Interaction With Father </label>
              <div class="form-group">
                <table class="table  table-hover table-bordered">
                  <tbody>
                    <tr>
                      <th class="col-md-4" >Father's Name</th>
                      <td  class="col-md-8" ><div class="form-group">
                          <input type="text" class="form-control" value="<?php echo $int_data->fathername; ?>" disabled>
                        </div></td>
                    </tr>
                    <tr>
                      <th class="col-md-4" >Language</th>
                      <td  class="col-md-8" ><div class="form-group">
                          <label>English</label>
                          <div class=" ">
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="F_English[]" value="Read" <?php if (in_array('Read',$F_eng)) { echo "checked";} ?> >
                              Read </label>
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="F_English[]" value="Write" <?php if (in_array('Write',$F_eng)) { echo "checked";} ?> >
                              Write </label>
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="F_English[]" value="Speak" <?php if (in_array('Speak',$F_eng)) { echo "checked";} ?> >
                              Speak </label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Hindi </label>
                          <div class=" ">
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="F_Hindi[]" value="Read" <?php if (in_array('Read',$F_hin)) { echo "checked";} ?> >
                              Read </label>
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="F_Hindi[]" value="Write" <?php if (in_array('Write',$F_hin)) { echo "checked";} ?> >
                              Write </label>
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="F_Hindi[]" value="Speak" <?php if (in_array('Speak',$F_hin)) { echo "checked";} ?> >
                              Speak </label>
                          </div>
                        </div></td>
                    </tr>
                    <tr>
                      <th>Qualification </th>
                      <td  class="col-md-8" ><div class="form-group">
                          <select class="form-control select2" name="F_qualification" style="width: 100%;" data-placeholder="Select">
                            <option selected="selected"><?php echo $int_data->F_qualification;?></option>
                            <option value="Till School">Till School</option>
                            <option value="BA">BA</option>
                            <option value="BSC">BSC</option>
                            <option value="BCOM">BCOM</option>
                            <option value="BE">BE</option>
                            <option value="50">B. Tech</option>
                            <option value="60">MA</option>
                            <option value="60">MSC</option>
                            <option value="70">MCOM</option>
                            <option value="80">M.Tech</option>
                            <option value="90">PGDCA</option>
                          </select>
                        </div></td>
                    </tr>
                    <tr>
                      <th>Overall Understanding</th>
                      <td  class="col-md-8" ><div class="form-group">
                          <input type="text" name="F_overall" class="form-control" value="<?php echo $int_data->F_overall;?>" >
                        </div></td>
                    </tr>
                    <tr>
                      <th>Any Other Observation</th>
                      <td  class="col-md-8" ><div class="form-group">
                          <textarea class="form-control" name="F_observation" rows="3" value="<?php echo $int_data->F_observation;?>" spellcheck="false"><?php echo $int_data->F_observation;?></textarea>
                        </div></td>
                    </tr>
                    <tr>
                      <th> Remarks for Father</th>
                      <td  class="col-md-8" ><div class="form-group">
                          <textarea class="form-control" name="F_remark" rows="3" value="<?php echo $int_data->F_remark;?>" spellcheck="false"><?php echo $int_data->F_remark;?></textarea>
                        </div></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-md-6">
              <label for="">Interaction With Mother </label>
              <div class="form-group">
                <table class="table  table-hover table-bordered">
                  <tbody>
                    <tr>
                      <th class="col-md-4" >Mother's Name</th>
                      <td  class="col-md-8" ><div class="form-group">
                          <input type="text" class="form-control" value="<?php echo $int_data->mothername;?>" disabled>
                        </div></td>
                    </tr>
                    <tr>
                      <th class="col-md-4" >Language</th>
                      <td  class="col-md-8" ><div class="form-group">
                          <label>English </label>
                          <div class=" ">
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="M_English[]" value="Read" <?php if (in_array('Read',$M_eng)) { echo "checked";} ?> >
                              Read </label>
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="M_English[]" value="Write" <?php if (in_array('Write',$M_eng)) { echo "checked"; }?> >
                              Write </label>
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="M_English[]" value="Speak" <?php if (in_array('Speak',$M_eng)) { echo "checked"; }?> >
                              Speak </label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Hindi </label>
                          <div class=" ">
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="M_Hindi[]" value="Read" <?php if (in_array('Read',$M_hin)) { echo "checked";} ?> >
                              Read </label>
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="M_Hindi[]" value="Write" <?php if (in_array('Write',$M_hin)) { echo "checked"; }?> >
                              Write </label>
                            <label class="col-md-4">
                              <input type="checkbox" class="flat-red" name="M_Hindi[]" value="Speak" <?php if (in_array('Speak',$M_hin)) { echo "checked"; }?> >
                              Speak </label>
                          </div>
                        </div></td>
                    </tr>
                    <tr>
                      <th>Qualification </th>
                      <td  class="col-md-8" ><div class="form-group">
                          <select class="form-control select2" name="M_qualification" style="width: 100%;" data-placeholder="Select">
                            <option selected="selected"><?php echo $int_data->M_qualification;?></option>
                            <option value="Till School">Till School</option>
                            <option value="BA">BA</option>
                            <option value="BSC">BSC</option>
                            <option value="BCOM">BCOM</option>
                            <option value="BE">BE</option>
                            <option value="50">B. Tech</option>
                            <option value="60">MA</option>
                            <option value="60">MSC</option>
                            <option value="70">MCOM</option>
                            <option value="80">M.Tech</option>
                            <option value="90">PGDCA</option>
                          </select>
                        </div></td>
                    </tr>
                    <tr>
                      <th>Overall Understanding</th>
                      <td  class="col-md-8" ><div class="form-group">
                          <input type="text" name="M_overall" class="form-control" value="<?php echo $int_data->M_overall;?>">
                        </div></td>
                    </tr>
                    <tr>
                      <th>Any Other Observation</th>
                      <td  class="col-md-8" ><div class="form-group">
                          <textarea class="form-control" name="M_observation" rows="3" value="<?php echo $int_data->M_observation;?>" spellcheck="false"><?php echo $int_data->M_observation;?></textarea>
                        </div></td>
                    </tr>
                    <tr>
                      <th> Remarks for Mother</th>
                      <td  class="col-md-8" ><div class="form-group">
                          <textarea class="form-control" name="M_remark" rows="3" value="<?php echo $int_data->M_remark;?>" spellcheck="false"><?php echo $int_data->M_remark;?></textarea>
                        </div></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <input type="checkbox" class="" id="myCheck4" onClick="myFunction4()">
                <label>Send for Approval </label>
              </div>
            </div>
            <div class="col-md-4" id="text4" style="display: none;">
              <div class="form-group">
             
                <select class="form-control select1" name="approval" data-placeholder="Select">
                  <option selected="selected" value="">select</option>
                  <option>Name 1</option>
                  <option>Name 2</option>
                  <option>Name 3</option>
                  <option>Name 4</option>
                  <option>Name 5</option>
                </select>
              </div>
            </div>
            <div class="clearfix"></div>
            
            <!-- /.box-body -->
            
           
        </div>
        <!-- /.box --> 
         <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <?php } } ?>
          </form>
          
        <!-- Form Element sizes --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
    <!-- /.row --> 
    
  </section>
  <!-- /.content --> 
</div>
<div class="modal fade" id="modal-reschedule">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Reschedule</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Date</label>
              <div class="input-group date">
                <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                <input type="text" class="form-control pull-right datepicker" >
              </div>
              <!-- /.input group --> 
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Time</label>
              <div class="input-group">
                <div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
                <input type="text" class="form-control timepicker">
              </div>
              <!-- /.input group --> 
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Comment</label>
              <textarea class="form-control" rows="3" placeholder="Type a comment  ... "></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- page script --> 

