
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Enquiry Form

   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Dashboard</a></li>
    <li class="active">Enquiry Form</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box ">
        <div class="box-header with-border">
          <h3 class="box-title">Add Enquiry</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form method="post" enctype="multipart/form-data" action="<?php echo base_url('insert-enquiry'); ?>" data-toggle="validator" role="form">
          <div class="box-body">
           <div class="row">
                <div class="col-md-5">
                  <div class="form-group ">
                    <label for="">For Academic Year</label>
                    <select class="form-control select1" name="academicyear" style="width: 100%;" data-placeholder="Select" required>
                      <option selected="selected" value="">Select</option>
                      <option value="18">2018-19</option>
                      <option value="19">2019-20</option>
                      <option value="20">2020-21</option>
                      <option value="21">2021-22</option>
                      <option value="23">2022-23</option>
                    </select>
                  </div>
                </div>
             <div class="col-md-5">
               <div class="form-group">
                <label for="">Child's Name </label><?php echo form_error('childname');?>
                <input type="text" name="childname" class="form-control"  placeholder="Child's Name" value="<?php echo set_value('childname'); ?>">
              </div>
            </div>
           

            <div class="col-md-2 col-xs-12 imgUp pull-right">
              <div class="imagePreview"></div>

              <label class="btn btn-upload btn-primary">
               Child's Photo<input type="file" class="uploadFile img" name="childphoto" style="width: 0px;height: 0px;overflow: hidden;">
             </label><span style="color: red;"><?php echo @$error;?></span>
           </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>Date of Birth</label><?php echo form_error('dob');?>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="dob" class="form-control pull-right" id="datepicker" value="<?php echo set_value('dob'); ?>" >
                </div>
                <!-- /.input group -->
              </div>
            </div>
           <div class="col-md-5">
             <div class="form-group">
              <label for="">Gender </label><?php echo form_error('gender');?>
              <select class="form-control select1" name="gender" style="width: 100%;" data-placeholder="Select">
                <option selected="selected" value="<?php echo set_value('gender'); ?>"><?php if (set_value('gender')){ echo set_value('gender');}else{ echo "select" ;}  ?></option>
                <option>Male</option>
                <option>Female</option>

              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="">Category </label><?php echo form_error('category');?>
              <select class="form-control select1" name="category"  style="width: 100%;" data-placeholder="Select">
                <option selected="selected" value="<?php echo set_value('category'); ?>"><?php if (set_value('category')){ echo set_value('category');}else{ echo "select" ;}  ?></option>
                <option>General</option>
                <option>OBC</option>
                <option>ST</option>
                <option>SC</option>
                <option>None</option>
                
              </select>
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label for="">Want Admission to Class </label><?php echo form_error('class');?>
              <select class="form-control select1" name="class" style="width: 100%;" data-placeholder="Select" value="<?php echo set_value('class'); ?>">
               <option selected="selected" value="<?php echo set_value('class'); ?>"><?php if (set_value('class')){ echo set_value('class');}else{ echo "select" ;}  ?></option>
                <option>KG-1</option>
                <option>KG-2</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
              </select>
            </div>
          </div>

        </div><!---/row---->


        <div class="row">
         <div class="col-md-12 "> <p class="lead">Family Details  </p></div>
         <div class="col-md-6">
           <div class="form-group">
            <label for="">Father's Name </label><?php echo form_error('fathername');?>
            <input type="text" class="form-control" name="fathername" placeholder="Father's Name" value="<?php echo set_value('fathername'); ?>">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Father's Profession </label><?php echo form_error('fatherprofession');?>
            <input type="text" class="form-control" name="fatherprofession" placeholder="Father's Profession" value="<?php echo set_value('fatherprofession'); ?>">
          </div>
        </div>

        <div class="col-md-6">
         <div class="form-group">
          <label for="">Mother's Name </label><?php echo form_error('mothername');?>
          <input type="text" class="form-control" name="mothername" placeholder="Mother's Name" value="<?php echo set_value('mothername'); ?>">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="">Mother's Profession </label><?php echo form_error('motherprofession');?>
          <input type="text" class="form-control" name="motherprofession" placeholder="Mother's Profession" value="<?php echo set_value('motherprofession'); ?>">
        </div>
      </div>


      <div class="col-md-4">
        <div class="form-group">
          <label>Contact </label><?php echo form_error('contact');?>

          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-phone"></i>
            </div>
            <input type="text" class="form-control" name="contact" data-inputmask='"mask": " 99999-99999"' value="<?php echo set_value('contact'); ?>" data-mask>
          </div>
          <!-- /.input group -->
        </div>
        <!-- /.form group -->
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Alternate Contact </label><?php echo form_error('alternatecontact');?>

          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-phone"></i>
            </div>
            <input type="text" class="form-control" name="alternatecontact" data-inputmask='"mask": " 99999-99999"' value="<?php echo set_value('alternatecontact'); ?>" data-mask>
          </div>
          <!-- /.input group -->
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label>Email </label><?php echo form_error('email');?>

          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo set_value('email'); ?>" >
          </div>
          <!-- /.input group -->
        </div>
        <!-- /.form group -->
      </div>

      <div class="col-md-12">
        <div class="form-group">
          <label>Address</label><?php echo form_error('address');?>
          <textarea class="form-control" name="address" rows="3" placeholder="Address"><?php echo set_value('address'); ?></textarea>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 "> <p class="lead">Special Ability</p></div>
      <div class="col-md-3">
       <div class="form-group">

         <input type="checkbox" class="sibling-hide" id="myCheck3" onclick="myFunction3()">
         <label>&nbsp; Special Ability  </label>
       </div>
     </div>
     <div class="col-md-9" id="text3" style="display: none;">


       <div class="form-group col-md-6">
         <label>Select</label>
         <select class="form-control select1" name="Certificatename" style="width: 100%;" data-placeholder="Select">
          <option selected="selected" value="">select</option>
          <option>Type 1</option>
          <option>Type 2</option>
          <option>Type 3</option>
          <option>Type 4</option>
          <option>Other</option>

        </select>
      </div>

      <div class="form-group col-md-6">
        <label>Certificate </label>
        <input type="file" name="certificate" class="file1">
        <div class="input-group ">

          <input type="text" class="form-control "  disabled placeholder="Certificate">
          <span class="input-group-btn">
            <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
          </span>
        </div>
      </div>

      <div class="form-group col-md-12">
        <label for="">Remarks </label>

        <textarea class="form-control" name="remarks" rows="3" placeholder="Type Here.." spellcheck="false"></textarea>
      </div>

    </div>

  </div>
  <div class="row">


    <div class="col-md-12"> <p class="lead">Previous School Information </p></div>


    <div class="col-md-4">
     <div class="form-group">
      <label for="">Current School’s Name </label><?php echo form_error('current_school');?>
      <input type="text" class="form-control" name="current_school" placeholder="School’s Name" value="<?php echo set_value('current_school'); ?>">
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label for="">Studying in Class  </label><?php echo form_error('studying_class');?>
      <select class="form-control select" name="studying_class" style="width: 100%;" data-placeholder="Select">
        <option selected="selected" value="<?php echo set_value('studying_class'); ?>"><?php if (set_value('studying_class')){ echo set_value('studying_class');}else{ echo "select" ;}  ?></option>

        <option>KG-1</option>
        <option>KG-2</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
        <option>6</option>
        <option>7</option>
        <option>8</option>
        <option>9</option>
        <option>10</option>
        <option>11</option>
        <option>12</option>


      </select>
    </div>
  </div>
  <div class="col-md-4">
   <div class="form-group">
    <label for=""> Board 	 </label><?php echo form_error('board');?>
    <select class="form-control select1" name="board" style="width: 100%;" data-placeholder="Select">
      <option selected="selected" value="<?php echo set_value('board'); ?>"><?php if (set_value('board')){ echo set_value('board');}else{ echo "select" ;}  ?></option>
      <option>MP</option>
      <option>CBSE</option>
      <option>ISCE</option>
      <option>Other</option>
    </select>
  </div>
</div>
<div class="col-md-12"> <p class="lead"> <small class="s-name" style="font-size:14px;">School Name</small> Information </p></div>

<div class="col-md-6">
  <div class="form-group">
    <label for="">How did you come to know about us?</label><?php echo form_error('know_aboutus');?>
    <select class="form-control select1" name="know_aboutus" style="width: 100%;" data-placeholder="Select">
      <option selected="selected" value="<?php echo set_value('know_aboutus'); ?>"><?php if (set_value('know_aboutus')){ echo set_value('know_aboutus');}else{ echo "select" ;}  ?></option>
      <option value="Paper Advertisement">Paper Advertisement </option>
      <option value="Hoarding">Hoarding</option>
      <option value="Existing parents">Existing parents</option>
      <option value="Friends">Friends</option>
      <option value="Internet Search">Internet Search</option>
      <option value="others">Others</option>

    </select>
  </div>
</div>
<div class="col-md-6">
  <div class="form-group">
    <label for="">Your Expectation from <small class="s-name">School Name</small></label><?php echo form_error('expectation');?>
    <select class="form-control select1" name="expectation" style="width: 100%;" data-placeholder="Select">
      <option selected="selected" value="<?php echo set_value('expectation'); ?>"><?php if (set_value('expectation')){ echo set_value('expectation');}else{ echo "select" ;}  ?></option>
      <option value="Good Infrastructure">Good Infrastructure </option>
      <option value="Extracurricular Activities">Extracurricular Activities</option>
      <option value="Sports">Sports</option>
      <option value="Well balanced curriculum">Well balanced curriculum</option>
      <option value="others">Others</option>

    </select>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label>Counselor’s Remarks </label><?php echo form_error('comments');?>
    <textarea class="form-control" name="comments" rows="3" placeholder="Counselor’s Remarks"><?php echo set_value('comments'); ?></textarea>
  </div>
</div>
</div>
</div>
<!-- /.box-body -->

<div class="box-footer">
  <button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>
</div>
<!-- /.box -->

<!-- Form Element sizes -->
</div>
<!-- /.box -->

</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>

<!-- page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )
    //Date picker
    $('#datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
    })
	//Date picker
  $('#datepicker1').datepicker({
    format: 'yyyy-mm-dd',
     autoclose: true,
  })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

