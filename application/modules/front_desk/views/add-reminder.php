<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title">Reminder</h4>
</div>
<div class="modal-body" id="modalid">
  
  <ul class="timeline timeline-inverse" style=" overflow-y: scroll;  max-height: 350px;">
    <?php
    foreach ($result as $key => $value) {
      ?>
      <li class="time-label">
        <span class="bg-green">
          <?php echo $value->followup_date; ?>
        </span>
      </li>
      <!-- /.timeline-label -->
      <!-- timeline item -->
      <li>
        <i class="fa fa-comments bg-yellow"></i>

        <div class="timeline-item">
          <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

          <h3 class="timeline-header"><a href="#"><?php echo $value->fathername; ?></a> commented on your post</h3>

          <div class="timeline-body">
            <?php echo $value->comments; ?>
              <!-- Take me to your leader!
              Switzerland is small and neutral!
              We are more like Germany, ambitious and misunderstood! -->
            </div>

          </div>
        </li>
        <!-- END timeline item -->
      <?php } ?>
    </ul>


    <form method="post" action="<?php echo base_url('add-reminder');?>" data-toggle="validator" role="form">
      <div class="row">
        <input type="hidden" name="enquiry_id" value="<?php echo $value->fw_id; ?>">
        <input type="hidden" name="status" value="<?php echo $value->status; ?>">
        <div class="col-md-6">
          <div class="form-group">
            <label> Date </label>
            <div class="input-group date">
              <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
              <input type="text" class="form-control pull-right" id="datepicker2" name="nextfollowup_date" required>
            </div>
            <!-- /.input group --> 
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label> Time </label>
            <div class="input-group">
             <div class="input-group-addon">
              <i class="fa fa-clock-o"></i>
            </div>
            <input type="text" class="form-control timepicker" name="time" required>


          </div>
          <!-- /.input group --> 
        </div>
      </div>
      <div class="col-md-12">
        <textarea class="form-control mr-top-10" name="comments" rows="3" placeholder="Type a comment  ..." spellcheck="false" required></textarea>

      </div> 
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>

<script>
  //Date picker
  $('#datepicker2').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  })
  //Timepicker
  $('.timepicker').timepicker({
    showInputs: false
  })
</script>