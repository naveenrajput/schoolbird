<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">Hold</h4>
</div>
<div class="modal-body">
	<form method="post" action="<?php echo base_url('status-change');?>" data-toggle="validator" role="form">

		<div class="callout callout-danger text-center" >
			<h4>Are you sure you want to move this student in hold section</h4>
		</div>
		<h3 class="lead text-center"> </h3>
		<input type="hidden" name="enquiry_id" value="<?php echo $id; ?>">
		<input type="hidden" name="status" value="HOLD">
		<textarea class="form-control mr-top-10" name="comments" rows="3" placeholder="Type a comment  ..." spellcheck="false" ></textarea>
		<div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Ok</button>
		</div>
	</form>

</div>