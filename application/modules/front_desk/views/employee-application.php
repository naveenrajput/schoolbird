
  <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
	<div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Employee Application
     
      </h1>
	  <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Front Desk</a></li>
        <li class="active">Employee Application</li>
      </ol>
	  </div>
	  <div class="col-md-6 col-xs-12 col-sm-4 content-header">
        <a  href="<?php echo base_url('add-employee'); ?>" class="btn pull-right btn-info"> <i class="fa fa-plus"></i> &nbsp;Application</a>
	  </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
		

		 <div class="box">
		  <div class="box-header with-border mr-top-20 mr-bottom-20 text-center">
			<div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
			<div class="col-md-2" >
			  <button type="submit" class="btn btn-info"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
			
			
			
			  </div>
			
            <!-- /.box-header -->
            <div class="box-body table-responsive">
             <!-- <table id="example" class="display nowrap" style="width:100%">--->
			
              <table id="example" class="table table-bordered " >
			   
        <thead>
            <tr>
                <th><input type="checkbox" id="selectall">All</th>
                <th>S.no</th>
                <th>Employee Name</th>
                <th>Post</th>
        				<th>Contact	</th>
        				<th>Email</th>
                <th>Joining Date</th>
        				<th>Forward</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
		      <?php 
              if (isset($emp_data)) {  
              foreach ($emp_data as $key => $emp_data) {
          ?>
            <tr>
			<td>
		<input type="checkbox" class="selectedId" name="selectedId" />
			</td>
          <td><?php echo $emp_data->id; ?></td>   
          <td><?php echo $emp_data->name; ?></td>
          <td><?php echo $emp_data->post; ?></td>
          <td><?php echo $emp_data->mobile; ?></td>
          <td><?php echo $emp_data->email; ?></td>
          <td><?php echo $emp_data->joiningdate; ?></td>
				 
               <td>
			     <ul class="table-icons">
        <li><a href="" class="table-icon" title="Review" data-toggle="modal" data-target="#modal-review"> 
		
		<span class="glyphicon glyphicon-eye-open display-icon"></span>
		</a></li>
          <li><a href="" class="table-icon " title="Interaction" data-toggle="modal" data-target="#modal-interaction"> 
		
		<span class="glyphicon glyphicon-open-file display-icon"></span>
		</a></li>
		  <li><a href="" class="table-icon" title="Archive"> 
		
		<span class="display-icon" > <img src="<?php echo base_url();?>assets/dist/img/icon/archive.png" style="vertical-align: baseline;" class=""> </span>
		</a></li>
    </ul>
				</td>
				<td>
			     <ul class="table-icons">
        <li><a href="" class="table-icon" title="Edit"> 
		
		<span class="glyphicon glyphicon-edit display-icon"></span>
		</a></li>
          <li><a href="" class="table-icon" title="Delete"> 
		
		<span class="glyphicon glyphicon-trash display-icon"></span>
		</a></li>
		 <li><a href="" class="table-icon" title="Reminder"  data-toggle="modal" data-target="#modal-reminder"> 
		
		<span class="glyphicon glyphicon-time display-icon"></span>
		</a></li>
		  <li><a href="" class="table-icon" title="Print"> 
		
		<span class="glyphicon glyphicon-print display-icon"></span>
		</a></li>
    </ul>
				</td>
              
				
            </tr>
           
		<?php } } ?>
        </tbody>
        <tfoot>
            <tr>
              <th><input type="checkbox" id="selectall">All</th>
                <th>S.no</th>
                <th>Employee Name</th>
                <th>Designation	</th>
				<th>Contact	</th>
				<th>Enquiry Date</th>
				<th>File</th>
				<th>Forward</th>
                <th>Action</th>

            </tr>
        </tfoot>
    </table>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

 <div class="modal fade" id="modal-reminder">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Reminder</h4>
              </div>
              <div class="modal-body">
              <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2019
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                      <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                      <div class="timeline-body">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                        quora plaxo ideeli hulu weebly balihoo...
                      </div>
                     
                    </div>
                  </li>
                  <!-- END timeline item -->
                 
                  <li class="time-label">
                        <span class="bg-green">
                          3 Mar. 2019
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-yellow"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                      <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                      <div class="timeline-body">
                        Take me to your leader!
                        Switzerland is small and neutral!
                        We are more like Germany, ambitious and misunderstood!
                      </div>
                      
                    </div>
                  </li>
                  <!-- END timeline item -->
                 
                </ul>
				<textarea class="form-control" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		<div class="modal fade" id="modal-review">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Review</h4>
              </div>
              <div class="modal-body">
			
             <div class="callout callout-info" >
        <h4><i class="fa fa-info"></i> Employee Details:</h4>
       <a href="#" target="_blank" >https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js</a>
      </div>
            <div class="form-group">
                  <label for="">Contact Person   </label>
                 <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  	<option value="Name1">Name1 </option>
			<option value="Name2">Name2</option>
			<option value="Name3">Name3</option>
			<option value="Name4">Name1</option>
			<option value="Name5">Name5</option>
			<option value="Name6">Name6</option>
                
                </select>
                </div>
			  
             <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
				
              </div>
			  
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		<div class="modal fade" id="modal-interaction">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Interaction</h4>
              </div>
              <div class="modal-body">
			
             <div class="form-group">
                  <label for="">Contact Person   </label>
                 <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                  <option selected="selected">Select</option>
                  	<option value="Name1">Name1 </option>
			<option value="Name2">Name2</option>
			<option value="Name3">Name3</option>
			<option value="Name4">Name1</option>
			<option value="Name5">Name5</option>
			<option value="Name6">Name6</option>
                
                </select>
                </div>
            
			  
             <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
				
              </div>
			  
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        lengthChange: true,
		autoWidth : true,
		  
      /*   buttons: [
            {
                text: 'Export',
                action: function ( e, dt, node, config ) {
                    alert( 'Button activated' );
                }
            }
        ] */
		
     // buttons: [ 'csv', 'excel', 'pdf', 'print'],  
	  /*  buttons: [
        { extend: 'copy', text: 'Copy to clipboard' }
    ], */
	/* "language": {
    "search": "Filter records:"
  }
   */
  
    } );
 
  /*  table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' );*/
		
} );


$(document).ready(function () {
    $('#selectall').click(function () {
        $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function () {
        var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
        $('#selectall').prop("checked", check);
    });
});




</script>



