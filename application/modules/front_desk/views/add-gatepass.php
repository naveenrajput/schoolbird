
<!-- Content Wrapper. Contains page content -->
<style>

</style>

<link href="<?php echo base_url();?>assets/dist/css/bootstrap-pincode-input.css" rel="stylesheet">
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Gate Pass </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Dashboard</a></li>
      <li class="active">Gate pass</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row"> 

      <!-- /.col -->
      <div class="col-md-12">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#stgatepass" data-toggle="tab">Student Gate Pass</a></li>
            <li><a href="#epgatepass" data-toggle="tab">Employee Gate Pass</a></li>
          </ul>
          <div class="tab-content">
            <div class="active tab-pane" id="stgatepass"> 
              <form method="post" enctype="multipart/form-data" action="<?php echo base_url('stud-gatepass');?>" data-toggle="validator" role="form">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="">S No. </label>
                        <input type="text" class="form-control"  placeholder="Auto Generated" disabled>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Date</label>
                        <div class="input-group date">
                          <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                          <input type="text" name="date" class="form-control pull-right datepicker" required>
                        </div>
                        <!-- /.input group --> 
                      </div>
                    </div>
                    <div class="col-md-3">
                     <div class="bootstrap-timepicker">
                      <div class="form-group">
                        <label>Time In</label>

                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" name="timein" class="form-control timepicker" required>


                        </div>
                        <!-- /.input group -->
                      </div>
                      <!-- /.form group -->
                    </div>
                  </div>
                  <div class="class_info">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="">Class</label>
                        <select class="form-control" name="class" id="classid" style="width: 100%;" required>
                          <option selected="selected" value="">Select</option>
                          <option value="1">1 </option>
                          <option value="2">2 </option>
                          <option value="3">3 </option>
                          <option value="4">4 </option>
                          <option value="5">5 </option>
                          <option value="6">6 </option>
                          <option value="7">7 </option>
                          <option value="8">8 </option>
                          <option value="9">9 </option>
                          <option value="10">10 </option>
                          <option value="11">11 </option>
                          <option value="12">12 </option>                     
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3" >
                      <div class="form-group">
                        <label for="">Section</label>
                        <select class="form-control" name="section" id="sectionid" style="width: 100%;" required>
                          <option selected="selected" value="">Select</option>
                          <option value="A">A </option>
                          <option value="B">B </option>
                          <option value="C">C </option>
                          <option value="D">D </option>
                          <option value="E">E </option>

                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Student</label>
                      <select class="form-control select2" name="studentid" id="student_id" style="width: 100%;" required>
                        <option selected="selected" value="">Select</option>
                        <option value="A">Ab </option>
                        <option value="B">Bv </option>
                        <option value="C">Cf </option>
                        <option value="D">Dd </option>
                        <option value="E">Ew </option>  
                        <option value="D">Ff </option>
                        <option value="E">Egh </option>

                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Guardian/ Pick Up Person</label>
                      <input type="text" name="guardians" class="form-control"  placeholder="Guardian Name" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Address</label>
                      <textarea class="form-control" name="address" rows="3" placeholder="Address" required></textarea>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Purpose of Going Out</label>
                      <textarea class="form-control" name="purpose" rows="3" placeholder="Type.." required></textarea>
                    </div>
                  </div>


                </div>
                <div class="row">
                 <div class="col-md-6">
                  <div class="form-group">
                    <label>Contact </label>
                    <div class="input-group">
                      <div class="input-group-addon"> <i class="fa fa-phone"></i> </div>
                      <input type="text" name="contact" class="form-control" data-inputmask='"mask":"99999-99999"' data-mask required>
                    </div>
                    <!-- /.input group --> 
                  </div>
                  <!-- /.form group --> 
                </div>

                <div class="col-md-2 text-center">
                  <label>&nbsp; </label>
                  <div class="form-group">
                   <a  class="btn btn-info" name="" data-toggle="modal" data-target="#modal-OTP">Generate OTP</a>
                 </div>
               </div>


             </div>
             <div class="row">
               <div class="col-md-4">
                <div class="form-group">
                  <label for="">Vehicle Type </label>
                  <select class="form-control" name="vehicletype" style="width: 100%;" data-placeholder="Select" required>
                    <option selected="selected" value="">Select</option>
                    <option value="Two Wheeler">Two Wheeler </option>
                    <option value="Four Wheeler">Four Wheeler </option>
                    <option value="Other">Other </option>


                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="">Vehicle Number</label>
                  <input type="text" name="vehicleno" class="form-control"  placeholder="Vehicle Number" required>
                </div>
              </div>
              <div class="col-md-4 ">
                <div class="form-group">
                  <label> Document </label>
                  <input type="file" name="document" class="file1" required>
                  <div class="input-group ">

                    <input type="text" class="form-control" disabled="" placeholder="Document" required>
                    <span class="input-group-btn">
                      <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="epgatepass"> 
       <form method="post" enctype="multipart/form-data" action="<?php echo base_url('emp-gatepass');?>" data-toggle="validator" role="form" >
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="">S No. </label>
                <input type="text" class="form-control"  placeholder="Auto Generated" disabled>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Date</label>
                <div class="input-group date">
                  <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                  <input type="text" name="date" class="form-control pull-right datepicker" required>
                </div>
                <!-- /.input group --> 
              </div>
            </div>
            <div class="col-md-4">
             <div class="bootstrap-timepicker">
              <div class="form-group">
                <label>Time In</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                  <input type="text" name="timein" class="form-control timepicker" required>

                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label for="">Department</label>
              <select class="form-control" name="department" id="dep_id" style="width: 100%;">
                <option selected="selected" value="">Select</option>
                <?php foreach ($dept as $dep) { ?>

                  <option value="<?php echo $dep->id; ?>"><?php echo $dep->name; ?></option>
                <?php } ?> 
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group"  >
              <label for="">Employee</label>
              <select class="form-control select2" name="employeeid" id="employee_id" style="width: 100%;" required>
                <option selected="selected" value="">Select</option>
                <option value="A">Ab </option>
                <option value="B">Bv </option>
                <option value="C">Cf </option>
                <option value="D">Dd </option>
                <option value="E">Ew </option>  
                <option value="D">Ff </option>
                <option value="E">Egh </option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Contact </label>
              <div class="input-group">
                <div class="input-group-addon"> <i class="fa fa-phone"></i> </div>
                <input type="text" name="contact" class="form-control" data-inputmask="&quot;mask&quot;:&quot;99999-99999&quot;" data-mask=""  required>
              </div>
              <!-- /.input group --> 
            </div>
            <!-- /.form group --> 
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Address</label>
              <textarea class="form-control" name="address" rows="3" placeholder="Address" required></textarea>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Purpose of Going Out </label>
              <textarea class="form-control" name="purpose" rows="3" placeholder="Type.." required></textarea>
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label for="">Vehicle Type</label>
              <select class="form-control" name="vehicletype" style="width: 100%;" data-placeholder="Select" required>
                <option selected="selected" value="">Select</option>
                <option value="Two Wheeler">Two Wheeler </option>
                <option value="Four Wheeler">Four Wheeler </option>
                <option value="Other">Other </option>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="">Vehicle Number</label>
              <input type="text" name="vehicleno" class="form-control"  placeholder="Vehicle Number" required>
            </div>
          </div>

          <div class="col-md-4 ">
            <div class="form-group">
              <label> Document </label>
              <input type="file" name="document1" class="file1" required>
              <div class="input-group ">

                <input type="text" class="form-control " disabled="" placeholder="Document" required>
                <span class="input-group-btn">
                  <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>

  </div>
</div>
<!-- /.tab-content --> 
</div>
<!-- /.nav-tabs-custom --> 
</div>
<!-- /.col --> 
</div>
<!-- /.row --> 

</section>
<!-- /.content --> 
</div>
<!-- /.content-wrapper -->


<div class="modal fade" id="modal-OTP">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-center">OTP</h4>
        </div>
        <div class="modal-body text-center">

         <label for="">Enter the OTP</label>
         <div class="form-group otp-style">

           <input type="text" id="pincode-input1"  >
         </div>
         <a href="#"><small class="">Resend OTP</small></a>
         <div class="pd-top-20"> <button type="button" class="btn btn-primary">Submit</button>	</div>
       </div>

     </div>
     <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
 </div>
 <script>
  $(document).ready(function(){
    $('#dep_id').change(function(){
      var dep_id=$('#dep_id').val();
      //console.log(dep_id);

      $.ajax({
        url:'<?php echo base_url('get-dept');?>',
        data:{dep_id:dep_id},
        type:'post',
        success:function(response){
          console.log(response);
          $('#employee_id').html(response);
        }
      });
    });

    $('.class_info').change(function(){
      var classid=$('#classid').val();
      var sectionid=$('#sectionid').val();

        //alert(classid);alert(sectionid);
        $.ajax({
          url:'<?php echo base_url('get-section');?>',
          data:{classid:classid,sectionid:sectionid},
          type:'post',
          success:function(response){
            console.log(response);
            $('#student_id').html(response);
          }
        });
      });
  });
</script>