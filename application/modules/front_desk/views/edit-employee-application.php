
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Edit Employee Application
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Dashboard</a></li>
        <li class="active">Application</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		  <div class="row">
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Employee</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" action="<?php echo base_url('update-emp');?>" data-toggle="validator" role="form" enctype="multipart/form-data">
              <div class="box-body">
			  <div class="row">
			   <div class="col-md-2 col-xs-12 imgUp pull-right">
					<div class="imagePreview"></div>
					<label class="btn btn-upload btn-primary">
						Upload<input type="file" name="emp_pic" class="uploadFile img" style="width: 0px;height: 0px;overflow: hidden;" required>
					</label>
				</div><!-- col-2 -->
			  <div class="col-md-10 remove-pd">
			  <div class="col-md-6  ">
			  
				<div class="form-group ">
                  <label for="">Application For The Post Of </label>
                  <input type="text" name="post" class="form-control"  placeholder="Post" required>
                </div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
                <label>Date Of Joining </label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="joiningdate" class="form-control pull-right datepicker" required>
                </div>
				
				
                <!-- /.input group -->
              </div>
				</div>
				
 
				<div class="col-md-6 ">
			  <div class="form-group">
                  <label for="">Full Name </label>
                  <input type="text" name="name" class="form-control"  placeholder="Full Name" required>
                </div>
			  </div>
			    <div class="col-md-6 ">
				<div class="form-group">
                  <label for="">Father's / Husband's Name </label>
                  <input type="text" name="f/h_name" class="form-control" placeholder="Father's / Husband's Name" required>
                </div>
				</div>
				
				</div>
			 
						
						</div><!--row-->
						  <div class="row">
			  <div class="col-md-4">
				<div class="form-group">
                <label>Date of Birth</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="dob" class="form-control pull-right datepicker" required>
                </div>
                <!-- /.input group -->
              </div>
				</div>
				
				<div class="col-md-4">
			  <div class="form-group">
                <label>Email </label>

                <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" name="email" class="form-control" placeholder="Email" required>
              </div>
                <!-- /.input group -->
              </div>
			  </div>
			  <div class="col-md-4">
			  <div class="form-group">
                <label>Contact </label>
	
				
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" name="mobile" class="form-control" data-inputmask='"mask":"99999-99999"' data-mask required>
                </div>
                <!-- /.input group -->
              </div>
			  </div>
			  
			   <div class="col-md-6">
				<div class="form-group">
                  <label>Address For Communication </label>
                  <textarea name="presentaddress" class="form-control" rows="2" placeholder="Address" required></textarea>
                </div>
				</div>
				 <div class="col-md-6">
				<div class="form-group">
                  <label>Permanent Address  </label>
                  <textarea name="permanentaddress" class="form-control" rows="2" placeholder="Address" required></textarea>
                </div>
				</div>
				 <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Gender </label>
                  <select class="form-control select1" name="gender" style="width: 100%;" data-placeholder="Select" required>
                  <option selected="selected">Select</option>
                  <option>Male</option>
                  <option>Female</option>

                </select>
                </div>
			  </div>
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for=""> Category  </label>
                 <select class="form-control select1" name="category" style="width: 100%;" data-placeholder="Select" required>
                  <option selected="selected" value="">Select</option>
                  <option value="General">General</option>
                  <option value="OBC">OBC</option>
                  <option value="ST">ST</option>
                  <option value="SC">SC</option>
                  <option value="None">None</option>

                </select>
                </div>
			  </div>
				<div class="col-md-3">
			  <div class="form-group">
                  <label for="">Marital Status  </label>
                  <select name="marstatus" class="form-control select1" style="width: 100%;" data-placeholder="Select" required>
                  <option selected="selected">Select</option>
                 <option value="Married">Married</option>
			      <option value="Single">Single</option>
			      <option value="Divorced">Divorced</option>
			      <option value="Widowed">Widowed</option>

                </select>
                </div>
			  </div>					  
			  <div class="col-md-3">
			  <div class="form-group">
                  <label for="">Occupation Of Father/Spouse  </label>
                  <input type="text" name="occupation" class="form-control"  placeholder="Occupation Of Father/Spouse" required>
                </div>
			  </div>
				
			  </div>
			<div class="row">
			    <div class="col-md-12 "> <p class="lead">Upload Documents </p></div>
			   
			  
			   
			   <div class="col-md-4 ">
			   <div class="form-group">
			     <label>Aadhar Card </label>
    <input type="file" name="A_card" class="file1" required>
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof" >
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label> Experience Letter </label>
    <input type="file" name="Exp_letter" class="file1" required>
    <div class="input-group ">
    
      <input type="text" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			 
			  
			  <div class="col-md-4 ">
			   <div class="form-group">
			     <label>Address Proof</label>
    <input type="file" name="Add_proof" class="file1" required>
    <div class="input-group ">
    
      <input type="text" name="" class="form-control " disabled placeholder="ID Proof">
      <span class="input-group-btn">
        <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
      </span>
    </div>
  </div>
			  </div>
			 
						
						</div>
						
			  <div class="row">
			    <div class="col-md-12 "> <p class="lead">Children (If Any)  </p></div>
			   
			  <div class="col-md-12 table-responsive">
			  <table class="table table-bordered" id="test-table"  style="margin-bottom:10px;">
				  <tbody id="test-body">
				  <tr  id="row0">
				  <th>Name</th>
					<th>Age</th>
					<th colspan="2">class</th>
				   
				  </tr>
				 
					 <tr >
					<td>
					<div class="form-group">
                
                  <input type="text" name="Child_name[]" class="form-control" required>
                </div>
					</td>
					<td>
					<div class="form-group">
              
                  <input type="text" name="Child_age[]" class="form-control" required>
                </div>
					</td>
					<td>
					<div class="form-group">
                
                  <input type="text" name="Child_class[]" class="form-control" required>
                </div>
					</td>
					<td>
					
 				<button class='delete-row btn btn-primary btn-sm' type='button' >Delete</button>      
					</td>
				   

				  </tr>
				 

				</tbody></table>
			    <div class="form-group">
          
             
			<button id='add-row' class='btn btn-primary btn-sm' type='button' value='Add' /> Add </button>             
			   </div>
				
				</div>
			 
						
						</div>
					 
						  <div class="row">
			    <div class="col-md-12 "> <p class="lead">Educational Qualification </p></div>
			   
			  <div class="col-md-12 table-responsive">
			  <table class="table table-bordered" id="test-table-ed"  style="margin-bottom:10px;">
				  <tbody id="test-body-ed">
				  <tr  id="row0">
				  	<th>Qualification </th>
					<th>Year of Passing</th>
					<th >Name of School / College</th>
					<th>Name of Board/ University</th>
					<th >Subject</th>
					<th colspan="2">Percentage</th>
				   
				  </tr>
				 
					 <tr >
					 
					 <td>
					<div class="form-group">
                
                  <input type="text" name="qualification[]" class="form-control" required>
                </div>
					</td>
					<td>
					<div class="form-group">
                
                  <input type="text" name="passingyear[]" class="form-control" required>
                </div>
					</td>
					
					<td>
					<div class="form-group">
                
                  <input type="text" name="schoolname[]" class="form-control" required>
                </div>
					</td>
					<td>
					<div class="form-group">
                
                  <input type="text" name="boardname[]" class="form-control" required>
                </div>
					</td>
					<td>
					<div class="form-group">
              
                  <input type="text" name="subject[]" class="form-control" required>
                </div>
					</td>
					<td>
					<div class="form-group">
                
                  <input type="text" name="percentage[]" class="form-control" required>
                </div>
					</td>
				
				   <td>
					
 				<button class='delete-row btn btn-primary btn-sm' type='button' > Delete</button>      
					</td>
				   

				  </tr>
				 

				</tbody></table>
			    <div class="form-group">
          
             
			<button id='add-row-ed' class='btn btn-primary btn-sm' type='button' value='Add' />Add</button>             
			   </div>
				
				</div>
			 
						
						</div>
						
<!-- 					 <div class="row">
			    <div class="col-md-12 "> <p class="lead">Professional Qualification:   </p></div>
			   
			  <div class="col-md-12 table-responsive">
			  <table class="table table-bordered" id="test-table-pq"  style="margin-bottom:10px;">
				  <tbody id="test-body-pq">
				  <tr  id="row0">
				  <th>Qualification </th>
					<th>Year of Passing</th>
					<th >Name of School / College</th>
					<th>Name of Board/ University</th>
					<th >Subject</th>
					<th colspan="2">Percentage</th>
				   
				  </tr>
				 
					 <tr >
					 
					 <td>
					<div class="form-group">
                
                  <input type="text" name="" class="form-control" >
                </div>
					</td>
					<td>
					<div class="form-group">
                
                  <input type="text" name="" class="form-control" >
                </div>
					</td>
					
					<td>
					<div class="form-group">
                
                  <input type="text" name="" class="form-control" >
                </div>
					</td>
					<td>
					<div class="form-group">
                
                  <input type="text" name="" class="form-control"  >
                </div>
					</td>
					<td>
					<div class="form-group">
              
                  <input type="text" name="" class="form-control"  >
                </div>
					</td>
					<td>
					<div class="form-group">
                
                  <input type="text" name="" class="form-control" >
                </div>
					</td>
				
				   <td>
					
 <button class='delete-row btn btn-primary btn-sm' type='button' >         Delete</button>      
					</td>
				   

				  </tr>
				 

				</tbody></table>
			    <div class="form-group">
          
             
			<button id='add-row-pq' class='btn btn-primary btn-sm' type='button' value='Add' /> Add </button>             
			   </div>
				
				</div>
			 
						
						</div> -->	 
				
				 <div class="row">
			    <div class="col-md-12 "> <p class="lead">Details Of Work Experience   </p></div>
			   
			  <div class="col-md-12 table-responsive">
			  <table class="table table-bordered" id="test-table-wrk"  style="margin-bottom:10px;">
				  <tbody id="test-body-wrk">
				  <tr  id="row0">
				  <th>Name Of Institute/Organization  </th>
					<th>From</th>
					<th >To</th>
					<th>Designation</th>
					<th colspan="2">Details Of Work	Salary Drawn</th>
				   
				  </tr>
				 
					 <tr >
					 
					 <td>
					<div class="form-group">
                
                  <input type="text" name="organization[]" class="form-control" required>
                </div>
					</td>
					
					
					<td>
					<div class="form-group">
                
                  <input type="text" name="datefrom[]" class="form-control" required>
                </div>
					</td>
					<td>
					<div class="form-group">
                
                  <input type="text" name="todate[]" class="form-control" required>
                </div>
					</td>
					<td>
					<div class="form-group">
              
                  <input type="text" name="designation[]" class="form-control" required>
                </div>
					</td>
					<td>
					<div class="form-group">
                
                  <input type="text" name="salary[]" class="form-control" required>
                </div>
					</td>
				
				   <td>
					
 <button class='delete-row btn btn-primary btn-sm' type='button'>Delete</button>      
					</td>
				   

				  </tr>
				 

				</tbody></table>
			    <div class="form-group">
          
             
			<button id='add-row-wrk' class='btn btn-primary btn-sm' type='button' value='Add' /> Add </button>             
			   </div>
				
				</div>
			 
						
						</div>	
				
				
			  
				
					 </div>
						 <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>	
                    </div>
              <!-- /.box-body -->

             
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
        

     </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

<!-- page script -->



