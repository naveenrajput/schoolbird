<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title">Interaction</h4>
</div>
<div class="modal-body" id="interaction_id">
  
  <form method="post" action="<?php echo base_url('send-interaction');?>" data-toggle="validator" role="form">
    <div class="row">
      <div class="col-md-12">
        <input type="hidden" name="enquiry_id" value="<?php echo $id; ?>">
        <input type="hidden" name="status" value="INTERACTION">

        <div class="form-group">
          <label for="">Contact Person </label>
          <select class="form-control select2" name="name" style="width: 100%;" data-placeholder="Select">
            <option selected="selected" value="">Select</option>
            <?php foreach ($emp_name as $key => $value) { ?>  
             <option value="<?php echo $value->id ; ?>"><?php echo $value->name ; ?></option>
           <?php } ?>
         </select>
       </div>
     </div>
     <div class="col-md-6">
      <div class="form-group">
        <label>Interaction Date </label>
        <div class="input-group date">
          <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
          <input type="text" name="date" class="form-control pull-right" id="datepicker5">
        </div>
        <!-- /.input group --> 
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Interaction Time </label>
        <div class="input-group">
          <div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
          <input type="text" name="time" class="form-control timepicker">
        </div>
        <!-- /.input group --> 
      </div>
    </div>
    <div class="col-md-12">
      <textarea class="form-control mr-top-10" name="comments" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>
</div>
<script>
    //Date picker
    $('#datepicker5').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
    })
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  </script>