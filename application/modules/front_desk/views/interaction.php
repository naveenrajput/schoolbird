<style>

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class=" ">
    <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class=""> Interaction </h1>
      <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Front Desk</a></li>
        <li class="active">Interaction</li>
      </ol>
    </div>
  <!--   <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;"> 
	<a  href="" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp;Interaction</a> 
	 </div> -->
  </section>
  
  <!-- Main content -->
  <section class="content">
  <div class="row"> 
    
    <!-- /.col -->
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#interaction" data-toggle="tab">Interaction</a></li>
        
          <li><a href="#pending" data-toggle="tab">Pending</a></li>
         
        </ul>
        <div class="tab-content">
          <div class="active tab-pane" id="interaction">
            <div class="box" style="border:none;">
              <div class="box-header with-border mr-top-10 mr-bottom-10 text-center">
                <div class="form-group col-md-5">
                  <div class="input-group date">
                    <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                    <input type="text" class="form-control pull-right datepicker" placeholder="Start Date">
                  </div>
                  <!-- /.input group --> 
                </div>
                <!-- /.form group --> 
                
                <!-- Date range -->
                <div class="form-group col-md-5">
                  <div class="input-group date">
                    <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                    <input type="text" class="form-control pull-right datepicker" placeholder="End Date">
                  </div>
                  <!-- /.input group --> 
                </div>
              
                <div class="col-md-2">
                  <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="col-md-12 col-xs-12 col-sm-12 margin ">
                <div class="btn-group pull-right">
                <button class="btn btn-primary" type="button">
                Print
                </div>
                <div class="btn-group pull-right mr-right-10">
                <a class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-export">Export</a> </div>
              <div class="btn-group pull-right mr-right-10"> <a href="#"  data-toggle="modal" data-target="#modal-import" class="btn btn-primary" type="button">Import</a> </div>
            </div>
            <div class="box-body table-responsive "> 
              
              <!-- <table id="example" class="display nowrap" style="width:100%">--->
              <table id="example" class="table table-bordered " >
                <!-- <div class="txt-dis">Export in Below Format</div> --->
                <thead>
                  <tr>
                  <th><input type="checkbox" id="selectall">All</input></th>
                  <th>id</th>
                  <th>Enquiry<br>No</th>
                  <th>Student <br>Name</th>
                  <th>General <br>Awareness</th>
                  <th>Mental <br>Ability</th>
                  <th>Confidence</th>
                  <th>Communication <br>Skills</th>
                  <th>Personality</th>
                  <th>Previous<br> School</th>
                  <th>Forward</th>
                  <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                      if (isset($interact_data)) {  
                      foreach ($interact_data as $key => $interact_data) {
                  ?>
                  <tr>
                    <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
                    <td><?php echo $interact_data->id;?></td>
                    <td><?php echo $interact_data->enquiry_id;?></td>
                    <td><?php echo $interact_data->childname;?></td>
                    <td><?php echo $interact_data->generalawareness;?></td>
                    <td><?php echo $interact_data->mentalability;?></td>
                    <td><?php echo $interact_data->confidence;?></td>
                    <td><?php echo $interact_data->commskills;?>%</td>
                    <td><?php echo $interact_data->personality;?>%</td>
                    <td><?php echo $interact_data->previousschool; ?></td>
					                  
                    <td>
                      <ul class="table-icons">
                        <!-- <li>
                          <a href="<?php //echo base_url('add-interaction/').base64_encode($interact_data->enquiry_id); ?>" class="table-icon " title="Add" data-toggle="modal" data-target=""> <span class="glyphicon glyphicon-plus display-icon"></span> </a>
                        </li> -->
                        <li>
                          <a href="" class="table-icon table-review-icon" title="Review" data-toggle="modal" data-target="#Mymodal">
                           <span class="glyphicon glyphicon-eye-open display-icon review_data" id="<?php echo $interact_data->enquiry_id; ?>"></span> 
                          </a>
                        </li>  
                      </ul>
                    </td>
                    <td>
                      <ul class="table-icons">
                        <li>
                          <a href="<?php echo base_url('add-interaction/').base64_encode($interact_data->enquiry_id); ?>" class="table-icon" title="open"> 
                            <span class="glyphicon glyphicon-edit display-icon"></span> 
                          </a>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <?php } } ?>
                </tbody>
                <tfoot>
                 <tr>
                  <th><input type="checkbox" id="selectall">All</input></th>
                  <th>id</th>
                  <th>Enquiry<br>No</th>
                  <th>Student <br>Name</th>
                  <th>General <br>Awareness</th>
                  <th>Mental <br>Ability</th>
                  <th>Confidence</th>
                  <th>Communication <br>Skills</th>
                  <th>Personality</th>
                  <th>Previous<br> School</th>
                  <th>Forward</th>
                  <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
        </div>
        <!-- /.tab-pane -->
        
      <div class="tab-pane" id="pending">
        <div class="box" style="border:none;">
          <div class="box-header with-border mr-top-10 mr-bottom-10 text-center">
            <div class="form-group col-md-5">
              <div class="input-group date">
                <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                <input type="text" class="form-control pull-right datepicker" placeholder="Start Date">
              </div>
              <!-- /.input group --> 
            </div>
            <!-- /.form group --> 
            
            <!-- Date range -->
            <div class="form-group col-md-5">
              <div class="input-group date">
                <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                <input type="text" class="form-control pull-right datepicker" placeholder="End Date">
              </div>
              <!-- /.input group --> 
            </div>
            <div class="col-md-1">
              <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="col-md-12 col-xs-12 col-sm-12 margin ">
            <div class="btn-group pull-right">
            <button class="btn btn-primary" type="button">
            Print
            </div>
            <div class="btn-group pull-right mr-right-10">
            <a class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-export">Export</a> </div>
          <div class="btn-group pull-right mr-right-10"> <a href="#"  data-toggle="modal" data-target="#modal-import" class="btn btn-primary" type="button">Import</a> </div>
        </div>
        <div class="box-body table-responsive "> 
              
              <!-- <table id="example" class="display nowrap" style="width:100%">--->
              <table id="example1" class="table table-bordered " >
                <!-- <div class="txt-dis">Export in Below Format</div> --->
                <thead>
                  <tr>
                    <th><input type="checkbox" id="selectall">
                      All
                      </input></th>
                      <th>id</th>
                    <th>Enquiry<br>
                      No</th>
                 
                  <th>Student <br>Name</th>
                  <th>General <br>Awareness</th>
                  <th>Mental <br>Ability</th>
                  <th>Confidence</th>
                  <th>Communication <br>Skills</th>
                  <th>Personality</th>
                  <th>Previous<br> School</th>
                  <th>Forward</th>
                  <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                      if (isset($Pending_int_data)) {  
                      foreach ($Pending_int_data as $key => $interact_data) {
                  ?>
                  <tr>
                    <td><input type="checkbox" class="selectedId" name="selectedId" /></td>
                    <td><?php echo $interact_data->id;?></td>
                    <td><?php echo $interact_data->enquiry_id;?></td>
                    <td><?php echo $interact_data->childname;?></td>
                    <td><?php echo $interact_data->generalawareness;?></td>
                    <td><?php echo $interact_data->mentalability;?></td>
                    <td><?php echo $interact_data->confidence;?></td>
                    <td><?php echo $interact_data->commskills;?>%</td>
                    <td><?php echo $interact_data->personality;?>%</td>
                    <td><?php echo $interact_data->previousschool; ?></td>
                    <td>
                      <ul class="table-icons">
                        <li>
                          <a href="" class="table-icon table-review-icon" title="Review" data-toggle="modal" data-target="#Mymodal">
                           <span class="glyphicon glyphicon-eye-open display-icon review_data" id="<?php echo $interact_data->enquiry_id; ?>"></span> 
                          </a>
                        </li>  
                      </ul>
                    </td>
                    <td>
                      <ul class="table-icons">
                        <li>
                          <a href="<?php echo base_url('add-interaction/').base64_encode($interact_data->enquiry_id); ?>" class="table-icon" title="open"> 
                            <span class="glyphicon glyphicon-edit display-icon"></span> 
                          </a>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <?php } } ?>
                </tbody>
                <tfoot>
                 <tr>
                    <th><input type="checkbox" id="selectall">
                      All
                      </input></th>
                      <th>id</th>
                    <th>Enquiry<br>
                      No</th>
                 
                  <th>Student <br>Name</th>
                  <th>General <br>Awareness</th>
                  <th>Mental <br>Ability</th>
                  <th>Confidence</th>
                  <th>Communication <br>Skills</th>
                  <th>Personality</th>
                  <th>Previous<br> School</th>
                  <th>Forward</th>
                  <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div>
        <!-- /.box-body --> 
      </div>
    </div>
    
</div>
<!-- /.tab-content -->
</div>
<!-- /.nav-tabs-custom -->
</div>
<!-- /.col -->

</section>

<!-- Main content --> 

<!-- /.content -->
</div>
<!-- ALL MODALS VIEW -->
<div class="modal fade" id="Mymodal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content" id="Mymodal_view">
      
    </div>
    <!-- /.modal-content --> 
  </div>
<!-- /.modal-dialog --> 
</div>


<div class="modal fade" id="modal-export">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Export</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Start Date </label>
              <div class="input-group date">
                <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                <input type="text" class="form-control pull-right datepicker">
              </div>
              <!-- /.input group --> 
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>End Date </label>
              <div class="input-group date">
                <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                <input type="text" class="form-control pull-right datepicker">
              </div>
              <!-- /.input group --> 
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Status </label>
              <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                <option selected="selected">Status</option>
                <option>All</option>
                <option>Active</option>
                <option>Hold</option>
                <option>Close</option>
              </select>
              <!-- /.input group --> 
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Type </label>
              <select class="form-control select1" style="width: 100%;" data-placeholder="Select">
                <option selected="selected">Select</option>
                <option>Excel</option>
                <option>CSV</option>
                <option>PDF</option>
              </select>
              <!-- /.input group --> 
            </div>
          </div>
          <div class="col-md-12">&nbsp;</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Export</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<div class="modal fade" id="modal-import">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center ">Welcome To Class Attendance Import Wizard </h4>
      </div>
      <div class="modal-body">
        <h3 class=" ">Please follow below steps to complete the import. </h3>
        <ul>
          <li><a href="<?php echo base_url();?>files/sample-file/classattendance.csv">Click here</a> to download a sample file that will help you with this import.</li>
          <li>Once you have downloaded the file enter the data in the exact same columns as mentioned in the file and save the file in the exatc same format (.csv) as it is downloaded in.</li>
          <li>After that, <a href="#" id="file-upload">click here</a> to see an option to upload your file.</li>
        </ul>
        <div class="form-group " id="upload-str" style="display:none;">
          <input type="file" name="img[]" class="file1">
          <div class="input-group ">
            <input type="text" class="form-control " disabled="" placeholder="Upload here">
            <span class="input-group-btn">
            <button class="browse btn btn-primary " type="button"><i class="glyphicon glyphicon-search"></i> Upload</button>
            </span> </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<div class="modal fade" id="modal-close">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Close</h4>
      </div>
      <div class="modal-body">
        <div class="callout callout-danger text-center" >
          <h4>Are you sure you want to close</h4>
        </div>
        <h3 class="lead text-center"> </h3>
        <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Ok</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>


<div class="modal fade" id="modal-reminder">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Reminder</h4>
      </div>
      <div class="modal-body">
        <ul class="timeline timeline-inverse" style=" overflow-y: scroll;  height: 400px;">
          <!-- timeline time label -->
          <li class="time-label"> <span class="bg-red"> 10 Feb. 2019 </span> </li>
          <!-- /.timeline-label --> 
          <!-- timeline item -->
          <li> <i class="fa fa-comments bg-blue"></i>
            <div class="timeline-item"> <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
              <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
              <div class="timeline-body"> Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                weebly ning heekya handango imeem plugg dopplr jibjab, movity
                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                quora plaxo ideeli hulu weebly balihoo... </div>
            </div>
          </li>
          <!-- END timeline item -->
          
          <li class="time-label"> <span class="bg-green"> 3 Mar. 2019 </span> </li>
          <!-- /.timeline-label --> 
          <!-- timeline item -->
          <li> <i class="fa fa-comments bg-yellow"></i>
            <div class="timeline-item"> <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>
              <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
              <div class="timeline-body"> Take me to your leader!
                Switzerland is small and neutral!
                We are more like Germany, ambitious and misunderstood! </div>
            </div>
          </li>
          <!-- END timeline item -->
          <li class="time-label"> <span class="bg-red"> 10 Feb. 2019 </span> </li>
          <!-- /.timeline-label --> 
          <!-- timeline item -->
          <li> <i class="fa fa-comments bg-blue"></i>
            <div class="timeline-item"> <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
              <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
              <div class="timeline-body"> Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                weebly ning heekya handango imeem plugg dopplr jibjab, movity
                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                quora plaxo ideeli hulu weebly balihoo... </div>
            </div>
          </li>
          <!-- END timeline item -->
          
          <li class="time-label"> <span class="bg-green"> 3 Mar. 2019 </span> </li>
          <!-- /.timeline-label --> 
          <!-- timeline item -->
          <li> <i class="fa fa-comments bg-yellow"></i>
            <div class="timeline-item"> <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>
              <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
              <div class="timeline-body"> Take me to your leader!
                Switzerland is small and neutral!
                We are more like Germany, ambitious and misunderstood! </div>
            </div>
          </li>
          <!-- END timeline item -->
          
        </ul>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label> Date </label>
              <div class="input-group date">
                <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                <input type="text" class="form-control pull-right datepicker">
              </div>
              <!-- /.input group --> 
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label> Time </label>
              <div class="input-group">
                <div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
                <input type="text" class="form-control timepicker">
              </div>
              <!-- /.input group --> 
            </div>
          </div>
          <div class="col-md-12">
            <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<div class="modal fade" id="modal-review">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Review</h4>
      </div>
      <div class="modal-body">
        <div class="callout callout-info" >
          <h4><i class="fa fa-info"></i> Student Details:</h4>
          <a href="#" target="_blank" >https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js</a> </div>
        <div class="row">
          <form  class="">
            <div class="col-md-4" id="internal-msg">
              <label class="checkbox-inline">
                <input id="checkthree" type="checkbox" name="toggle" onChange="toggleStatusmsg()" />
                &nbsp; Internal Message </label>
            </div>
            <div class="col-md-4" id="pageForm">
              <label class="checkbox-inline">
                <input id="toggleElement" type="checkbox" name="toggle" onChange="toggleStatus()" />
                &nbsp;Message </label>
            </div>
            <div class="col-md-4 " id="elementsToOperateOn">
              <label class="checkbox-inline">
                <input type="checkbox" name="participate" id="checktwo" onChange="toggleStatusone()" />
                &nbsp;Mail </label>
            </div>
            <div class="col-md-12 mr-top-20" id="name2" style="display:none;">
              <div class="col-md-12 ">
                <div class="form-group">
                  <label for="">Contact Person </label>
                  <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                    <option selected="selected">Select</option>
                    <option value="Name1">Name1 </option>
                    <option value="Name2">Name2</option>
                    <option value="Name3">Name3</option>
                    <option value="Name4">Name1</option>
                    <option value="Name5">Name5</option>
                    <option value="Name6">Name6</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..."  spellcheck="false"></textarea>
                </div>
              </div>
            </div>
            <div class="col-md-12 mr-top-20" id="name" style="display:none;">
              <div class="col-md-6 ">
                <div class="form-group">
                  <label for="">Contact Person </label>
                  <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                    <option selected="selected">Select</option>
                    <option value="Name1">Name1 </option>
                    <option value="Name2">Name2</option>
                    <option value="Name3">Name3</option>
                    <option value="Name4">Name1</option>
                    <option value="Name5">Name5</option>
                    <option value="Name6">Name6</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <label for="">Contact No.</label>
                  <div class="input-group">
                    <input type="text" class="form-control disabledCheckboxes" placeholder="9087654321" disabled>
                    <span class="input-group-btn">
                    <div type="button"  class="btn btn-primary change "  >Change</div>
                    </span> </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..."  spellcheck="false"></textarea>
                </div>
              </div>
            </div>
            <div class="col-md-12 mr-top-20" id="name1" style="display:none;">
              <div class="col-md-6 ">
                <div class="form-group">
                  <label for="">Contact Person </label>
                  <select class="form-control select2" style="width: 100%;" data-placeholder="Select">
                    <option selected="selected">Select</option>
                    <option value="Name1">Name1 </option>
                    <option value="Name2">Name2</option>
                    <option value="Name3">Name3</option>
                    <option value="Name4">Name1</option>
                    <option value="Name5">Name5</option>
                    <option value="Name6">Name6</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <label for="">Email</label>
                  <div class="input-group">
                    <input type="text" class="form-control disabledCheckboxestwo" placeholder="abc@gmail.com" disabled="">
                    <span class="input-group-btn">
                    <div type="button"  class="btn btn-primary changetwo">Change</div>
                    </span> </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..."  spellcheck="false"></textarea>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>


<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        lengthChange: true,
		autoWidth : true,
       /*  buttons: [   'csv', 'excel', 'pdf', 'print' ], */
		
		
    } );
 
  /*   table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' ); */
		
} );

$(document).ready(function() {
    var table = $('#example1').DataTable( {
        lengthChange: true,
		autoWidth : true,
       /*  buttons: [   'csv', 'excel', 'pdf', 'print' ], */
		
		
    } );

		
} );

$(document).ready(function() {
    var table = $('#example2').DataTable( {
        lengthChange: true,
		autoWidth : true,
       /*  buttons: [   'csv', 'excel', 'pdf', 'print' ], */
		
		
    } );
 
  
} );
$(document).ready(function() {
    var table = $('#example3').DataTable( {
        lengthChange: true,
		autoWidth : true,
       /*  buttons: [   'csv', 'excel', 'pdf', 'print' ], */
		
		
    } );
 
  
} );
</script> 
<script>


$(document).ready(function () {
    $('#selectall').click(function () {
        $('.selectedId').prop('checked', this.checked);
    });

    $('.selectedId').change(function () {
        var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
        $('#selectall').prop("checked", check);
    });
});

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
	 //Datemask3 mm/dd/yyyy
    
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('.datepicker').datepicker({
	format: 'yyyy-mm-dd',
	   autoclose: true,

    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    
    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })

  $(document).ready(function(){
      $('.review_data').click(function(){  
       var enquiry_id = $(this).attr("id");
        $.ajax({  
              url:"<?php echo base_url('review'); ?>",  
              method:"post",  
              data:{enquiry_id:enquiry_id},  
              success:function(data){
                 console.log(data);
                $('#Mymodal_view').html(data);
              }                             
         }); 
      });
    });
</script> 
