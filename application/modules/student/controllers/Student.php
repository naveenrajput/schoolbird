<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends MY_Controller {

	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Student_model');
      }

    // Controller for Student List
	public function Master_List(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('master_list');
		$this->load->view('../../inc/footer');
	}

	// Controller for Student Fees
	public function Fees(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for Student Attendance
	public function Attendance(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['classgroup'] = $this->Student_model->classgroup();
		$data['Acadmic']    = $this->Student_model->AcademicYear();
		$this->load->view('attendance',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for Student Remarks
	public function Remarks(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['classgroup'] = $this->Student_model->classgroup();
		$data['Acadmic']    = $this->Student_model->AcademicYear();
		$this->load->view('remarks',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for Get student Remarks
	public function GetRemark(){

		$class           = $this->input->post('clsaa');
		$acadmic         = $this->input->post('acadmic');
		$data['student'] = $this->Student_model->getRemarkStudent($class,$acadmic);
        $this->load->view('remarktable',$data);
		
	}
	// Controller for Get student Remarks
	public function GetRemarkcomment(){
       
		$id              = $this->input->post('id');
		$class           = $this->input->post('clsaa');
		$acadmic         = $this->input->post('acadmic');
		$data['comment'] = $this->Student_model->getStudentcomment($id,$class,$acadmic);
        $this->load->view('studentcomment',$data);
		
	}

	// Controller for save student Remarks
	public function saveComment(){

		$id              = $this->input->post('id');
		$class           = $this->input->post('class');
		$acadmic         = $this->input->post('acadmic');
        $comment         = $this->input->post('comment');

        $data = array(
			     'stid'          =>$id,
		         'academicyearid'=> $acadmic,
		         'classid'       => $class,
		         'remark'        => $comment,
		         'createdate'    =>date("Y-m-d")
                );
		$data['comment'] = $this->Student_model->SaveStudentcomment($data);

	
		$resp = '<li class="commentLineBox">'.$comment; if($data["classid"]==1){echo "techers";}else "Admin";'</li>';
		echo $resp;
        
		
	}

	// Controller for Student Leave
	public function Leave(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for Student Parent
	public function Parent(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for Student Transfer
	public function Transfer(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['classgroup'] = $this->Student_model->classgroup();
		$data['Acadmic']    = $this->Student_model->AcademicYear();
		$this->load->view('sessiontransfer',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for Get Attendance Form
	public function GetAttendanceForm(){

		$class= $this->input->post('clsaa');
		$month= $this->input->post('month');
		$data['student']    = $this->Student_model->getStudent($class);
		$this->load->view('attendanceform',$data);
		
	}


   // Controller for get data to transfer new session 
	public function GetSessionData()  {
		$class= $this->input->post('clsaa');
		$acadmic= $this->input->post('acadmic');
		$data['classgroup'] = $this->Student_model->classgroup();
		$data['Acadmic']    = $this->Student_model->AcademicYear();
		$data['house']    = $this->Student_model->gethouse();
        $data['studentdata']    = $this->Student_model->GetsessionStudent($class,$acadmic);
        $this->load->view('sessiondata',$data);
      }

     // Controller for student data update  
	public function classUpdate()  {

		$id = $this->input->post('id');
		$class= $this->input->post('class');
		$house= $this->input->post('house');
		$rowname= $this->input->post('rowname');
		$data['dataupdate'] = $this->Student_model->updatestudentrecord($id,$class,$house,$rowname);
      }


	// Controller for Student Category
	public function Category(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['category']    = $this->Student_model->GetStudentcategory();
		$this->load->view('category',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for Add New Student Category
	public function AddCategory(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('addcategory');
		$this->load->view('../../inc/footer');
	}

	// Controller for Add New Student Category
	public function SaveCategory(){

		$Subject_Name = $this->input->post('c_name');
		$Subject_code = $this->input->post('c_code');
		$Notes        = $this->input->post('c_notes');
		$data = array(
			        'notes'     => $Notes,
                    'code'      => $Subject_code,
			        'name'      => $Subject_Name,
			        'createdate'=>date("Y-m-d")
                );

		$this->form_validation->set_rules('c_name', 'code', 'required');
        $this->form_validation->set_rules('c_code', 'Name', 'required');
        $this->form_validation->set_rules('c_notes', 'Notes', 'required');
        if( 
        	$this->form_validation->run() == FALSE){
   
        }
        else
        {
          $this->Student_model->SaveCategoryData($data);
		  redirect(base_url('Category'));
        }
	}


    public function sessionUpdate(){

		$student= $_POST['id'];
    	$class= $_POST['class'];
    	$acadmic= $_POST['acadmic'];

    	$student= explode(',', $student);

    foreach ($student as $value) {
    	
     $data= $this->Student_model->getStudentrecord($value);

     $data= $this->Student_model->updatestudent($data,$class,$acadmic);
    }

	
	}
	// Controller for Delet category
	public function Deletcategory(){

        $id = $this->input->post('employee_id');
		$this->Student_model->Categorydelete($id);
		redirect('Category');
	}

	// Controller for Student No_Dues
	public function No_Dues(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for Student TC
	public function TC(){

		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}
	
}
