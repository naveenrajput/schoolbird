<?php  
   class Student_model extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }  
     
      //Function To Fetch  classgroup  Record
      public function classgroup()  {  
        $this->db->where('delete', 1); 
        $query = $this->db->get('classes'); 
        return $query;  
      }

      //Function To Fetch  academic year Record 
      public function AcademicYear()  {  
        $query = $this->db->get('academicyear'); 
         return $query;  
      }

      //Function To Fetch  house year Record 
      public function gethouse()  {  
        $query = $this->db->get('houses');
         return $query;  
      }

      //Function To Fetch  Student Record 
      public function getStudentrecord($value)  { 
                 $this->db->where('stid',$value); 
        $query = $this->db->get('students')->row();
         return $query;  
      }


      // Update Query For chainge student session 
      public function updatestudent($data,$class,$acadmic){
       $data->academicyear=$acadmic;
       $data->admittedclass=$class;
       unset($data->id);
        $this->db->insert('students', $data);
      }


      //Function To Fetch  Student category Record 
      public function GetStudentcategory()  { 
                 $this->db->where('delete', 0); 
        $query = $this->db->get('studentcategory');
                 return $query;  
      }

      //Function To insert New category Record 
      public function Categorydelete($id)  { 
               $u['delete']='2';
               $query= $this->db->where('id',$id)->update('studentcategory',$u);
               return $query;  
      }

      //Function To insert New category Record 
      public function SaveCategoryData($data)  { 
                $this->db->insert('studentcategory',$data); 
               return $query;  
      }

      //Function To Fetch  house year Record 
      public function getRemarkStudent($class,$acadmic){
          $this->db->select('sts.studentid,sts.id,st.name');
          $this->db->from('studentsession as sts');
          $this->db->join('students as st', 'st.id = sts.studentid');  
          $this->db->where('sts.classid',$class);
          $this->db->where('sts.academicyear',$acadmic);
          $query = $this->db->get();
          return $query;  
      }

      //Function To Fetch  house year Record 
      public function getStudent($class)  {  
        $this->db->select('st.name,st.stid,st.admissiondate');
        $this->db->from('students as st');
        $this->db->join('studentsession as stse', 'stse.studentid = st.id');
        $this->db->where('stse.classid='.$class);
        $query = $this->db->get();
         return $query;  

           
      } 

      //Function To Fetch  house year Record 
      public function getStudentcomment($id,$class,$acadmic)  { 

        $this->db->select('*');
        $this->db->from('studentremark');
        $this->db->where('stid',$id);
        $this->db->where('academicyearid',$acadmic);
        $this->db->order_by("createdate", "asc");
        $query = $this->db->get();
       // $query = $this->db->last_query($query);print_r($query);die;
        return $query;  
      }

      public function SaveStudentcomment($data)  { 

        $query = $this->db->insert('studentremark',$data);
        return $query;  
      }

     // update a student record
      public function updatestudentrecord($id,$class,$house,$rowname)  { 

        $data=array(
                'admittedclass'=>$class,
                'house_id'=>$house,
                'stid'=>$rowname
               );
                $this->db->where('id', $id);
        $query=  $this->db->update('students', $data);
        return $query;  
      }

      //Function To Fetch  old session student 
      public function GetsessionStudent($class,$acadmic)  {

        $this->db->select('st.fname,st.id,st.house_id,st.stid,hs.name,cl.class_id,acy.name');
        $this->db->from('students as st');
        $this->db->join('academicyear as acy', 'acy.id = st.academicyear');
        $this->db->join('classes as cl', 'cl.class_id = st.admittedclass');  
        $this->db->join('houses as hs', 'hs.id = st.house_id');
        $this->db->where('cl.class_id='.$class);
        $this->db->where('st.academicyear='.$acadmic);
        $query = $this->db->get();
        return $query;

         
      }
      
   }  
?>  