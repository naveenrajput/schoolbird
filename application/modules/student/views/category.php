<style>



 </style>
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
	<div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Category
     
      </h1>
	  <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Student</a></li>
        <li><a href="#">Category</a></li>
      </ol>
	  </div>
	  <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;">
	  
        <a  href="<?php echo base_url('addCategory'); ?>" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp;Category</a>
		 
	  </div>
    </section>

    <!-- Main content -->
     <section class="content">
      <div class="row">
        <div class="col-xs-12">
		

		 <div class="box">
		  <div class="box-header with-border mr-top-20 text-center">
			<div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
			<div class="col-md-2" >
			  <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
			
			
			
			  </div>
   
            <div class="box-body table-responsive">
              <table id="example" class="table table-bordered " >
			  <div class="txt-dis">Export in Below Format</div>
        <thead>
            <tr>
		
                <th><input type="checkbox" id="selectall"> All</input></th>
                <td>S NO.</td>
                <td>NAME</td>
                <td>CODES</td>
                <td>NOTES</td>
                <th>CREATEDATE</th>
                <th>Action</th>
        
        
            </tr>
        </thead>
        <tbody>
    <?php foreach ($category->result() as $row) {?>
            <tr>
      <td>
    <input type="checkbox" class="selectedId" name="selectedId" />
      </td>
              
                <td>S NO.</td>
                <td><?php echo $row->name;?></td>
                <td><?php echo $row->code;?></td>
                <td><?php echo $row->notes;?></td>
                <td><?php echo $row->createdate;?></td>
        
      
        <td>
          <ul class="table-icons">
               <li><a href=" <?php echo base_url('categoryedit/')?>" class="table-icon" title="Edit"> 
    
    <span class="glyphicon glyphicon-edit display-icon"></span>
    </a></li>
          
     
    <li>
    <span class="glyphicon glyphicon-trash display-icon view_data" id="<?php echo $row->id;?>"></span>
    </li>
          </ul>
        </td>
              
        
            </tr>
           
    <?php }?>
        </tbody>
        <tfoot>
            <tr>
              <th><input type="checkbox" id="selectall"> All</input></th>
                <td>S NO.</td>
                <td>NAME</td>
                <td>CODES</td>
                <td>NOTES</td>
                <th>CREATEDATE</th>
                <th>Action</th>

            </tr>
        </tfoot>
    </table>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

 
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        lengthChange: false,
		autoWidth : true,
        buttons: [   'csv', 'excel', 'pdf', 'print' ],
		
		
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
		
} );




</script>


<script>  
 $(document).ready(function(){
      $('.view_data').click(function(){  
           var employee_id = $(this).attr("id");
           //alert(employee_id);
           var x=confirm("Are you sure to delete record?");//alert(employee_id);
          if (x) {
            $.ajax({  
                        url:"<?php echo base_url('Student/Deletcategory'); ?>",  
                        method:"post",  
                        data:{employee_id:employee_id},  
                        success:function(data){  
                               location.reload();
                        }  
                   }); 
          } else {
            return false;
          } 
              });  
 });  
 </script>