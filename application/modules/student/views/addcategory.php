
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Add category
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Student</a></li>
        <li><a href="#">category</a></li>
        <li class="active"> Add category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <!-- /.box-header -->
          
            <form method="post" action="<?php echo base_url('addnecategory');?>" data-toggle="validator" role="form">

              <div class="box-body">
        
        
          <div class="row">
          <div class="col-md-12 "> <p class="lead">Student category Details  </p></div>
         
        <div class="col-md-6">
        <div class="form-group">
                  <label for=""> Name  </label>
                  <input type="text" class="form-control"name="c_name" placeholder="Group Name " required>
                </div>
        </div>
          <div class="col-md-6">
        <div class="form-group">
                  <label for="">Code</label>
                  <input type="text" class="form-control"name="c_code" placeholder="Group Name " required>
                </div>
        </div>
       
              
         <div class="col-md-12">
        <div class="form-group">
                  <label>Notes</label>
                  <textarea class="form-control" name="c_notes" rows="3" placeholder="Notes" required></textarea>
                </div>
        </div>
         
        
          
       
         </div>
         
   
        </div>
  </div>
             </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
           
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
        


        </div>
      <!-- /.box -->

     </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript">
  
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

   
    

  })

</script>
