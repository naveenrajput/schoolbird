<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Examination extends MY_Controller {

	
	public function __construct(){
        parent::__construct();
        $this->load->model('Examination_model');
        $this->load->helper('url');
      }

    // Controller for exam_type
	public function exam_type()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['exam'] = $this->Examination_model->getExaminationtypes(); 
		$this->load->view('examtype',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for save Exam Types
	public function saveExamTypes()
	{
		$name = $this->input->post('name');
		$code = $this->input->post('code');
		$type = $this->input->post('type');
		$Notes = $this->input->post('notes');
		$data = array(
			'exams'  => $name,
			'examtype'  => $type,
			'code'  => $code,
			'notes' => $Notes,
			'createdate'  =>date("Y-m-d")
		);

		$this->form_validation->set_rules('name', 'Subject Name', 'required');
		$this->form_validation->set_rules('code', 'Subject code', 'required');
		$this->form_validation->set_rules('notes', 'Notes', 'required');
		if( 
			$this->form_validation->run() == FALSE){

		}
		else
		{
			$this->Examination_model->AddnewExam($data);
			redirect(base_url('exam'));
		}
	}

   // Add new Grading Scale
	public function Savenewgrading()
	{
		$name = $this->input->post('name');
		$Score = $this->input->post('Score');
		$code = $this->input->post('code');
		$Notes = $this->input->post('notes');
		$data = array(
			'name'  => $name,
			'minscore' =>$Score,
			'creditpoints'  => $code,
			'notes' => $Notes,
			'createdate'  =>date("Y-m-d")
		);

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('Score', 'Score', 'required');
		$this->form_validation->set_rules('code', 'code', 'required');
		$this->form_validation->set_rules('notes', 'Notes', 'required');
		if( 
			$this->form_validation->run() == FALSE){

		}
		else
		{  
			$this->Examination_model->Addnewgrading($data);
			redirect(base_url('grading_scale'));
		}
	}

	// Update Grading Scale
	public function Updategrading()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$Score = $this->input->post('Score');
		$code = $this->input->post('code');
		$Notes = $this->input->post('notes');
		$data = array(
			'name'  => $name,
			'minscore' =>$Score,
			'creditpoints'  => $code,
			'notes' => $Notes,
			'modifieddate'  =>date("Y-m-d")
		);

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('Score', 'Score', 'required');
		$this->form_validation->set_rules('code', 'code', 'required');
		$this->form_validation->set_rules('notes', 'Notes', 'required');
		if( 
			$this->form_validation->run() == FALSE){

		}
		else
		{  
			
			$this->Examination_model->Updategrading($data,$id);
			redirect(base_url('grading_scale'));
		}
	}

	// Controller for save Exam Update Types
	public function ExamUpdate()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$type = $this->input->post('type');
		$code = $this->input->post('code');
		$Notes = $this->input->post('notes');
		$data = array(
			'exams'  => $name,
			'examtype'  => $type,
			'code'  => $code,
			'notes' => $Notes,
			'modifieddate'  =>date("Y-m-d")
		);

		$this->form_validation->set_rules('name', 'Subject Name', 'required');
		$this->form_validation->set_rules('code', 'Subject code', 'required');
		$this->form_validation->set_rules('notes', 'Notes', 'required');
		if( 
			$this->form_validation->run() == FALSE){

		}
		else
		{
			$this->Examination_model->UpdateExam($data,$id);
			redirect(base_url('exam'));
		}
	}

	 // Controller for exam_type
	public function AddExamTypes()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['type']=$this->Examination_model->getExaminationtypes();
		$this->load->view('addexamtypes',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for exam
	public function exam()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['exam']=$this->Examination_model->ShowExam();
        $this->load->view('exam',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for exam
	public function examEdit($id)
	{   
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['exam'] = $this->Examination_model->getExam($id);
		$data['type']=$this->Examination_model->getExaminationtypes();
		$this->load->view('edit-exam',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for GragingEdit
	public function GragingEdit($id)
	{   
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['Graging'] = $this->Examination_model->getGraging($id);
		$this->load->view('GragingEdit',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for Delet Exam 
	public function DeletExam()
	{   
		$id = $this->input->post('id');

		$data = array(
			'delete'  =>2,
		);
		$data['exam'] = $this->Examination_model->deletExam($data,$id);
		redirect(base_url('exam'));
	}

	// Controller for Delet Exam 
	public function DeletGrading()
	{   
		$id = $this->input->post('id');

		$data = array(
			'delete'  =>2,
		);
		$data['exam'] = $this->Examination_model->deletGrading($data,$id);
		redirect(base_url('exam'));
	}
	
	// Controller for grading_scale
	public function grading_scale()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['grading'] = $this->Examination_model->grading_scale();
		$this->load->view('grading_scale',$data);
		$this->load->view('../../inc/footer');
	}


    // Controller for grading_scale
	public function AddGradingScale()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('Add_grading_scale');
		$this->load->view('../../inc/footer');
	}

	// Controller for exam_allotment
	public function exam_allotment()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['Examallotment'] = $this->Examination_model->Examallotment();
		$this->load->view('exam_allotment',$data);
		$this->load->view('../../inc/footer');
	}

	// Controller for exam_allotment
	public function AddExamAllotment()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['Academic'] = $this->Examination_model->getAcademic();
		$data['class'] = $this->Examination_model->getClass();
		$data['Exam'] = $this->Examination_model->ShowExam();
		$this->load->view('Add_allotment',$data);
		$this->load->view('../../inc/footer');
	}

	//Controller for time_table
	public function time_table()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for update_marks
	public function update_marks()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for exam_reports
	public function exam_reports()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for classwise_report
	public function classwise_report()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// Controller for export_report
	public function export_report()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}


}
