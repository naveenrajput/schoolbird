<?php  
  class Examination_model extends CI_Model{  
    function __construct()  {  
      parent::__construct();  
    } 

      // get classes 
      public function getExaminationtypes()  {  
        $query = $this->db->get('examtype');
         return $query;  
      }

       // get grading_scale 
      public function grading_scale()  { 
                 $this->db->where('delete',0); 
        $query = $this->db->get('gradingscale');
         return $query;  
      } 

      // get classes 
      public function getExam($id){  
                 $this->db->where('id',$id);
        $query = $this->db->get('exams')->result();
                 return $query;  
      }

      // get classes 
      public function getGraging($id){  
                 $this->db->where('id',$id);
        $query = $this->db->get('gradingscale');
                 return $query;  
      } 

      // get exam data
      public function ShowExam()  { 
                 $this->db->where('delete',1); 
        $query = $this->db->get('exams');
         return $query;  
      }

      // get Academic data
      public function getAcademic()  { 
                 $this->db->where('delete',0); 
        $query = $this->db->get('academicyear');
         return $query;  
      }

      // get class data
      public function getClass()  { 
                 $this->db->where('delete',1); 
        $query = $this->db->get('classes');
         return $query;  
      }

      // get Acadmic Year 
      public function Examallotment()  { 

              $this->db->select('ex.id,ex.exams,ex.createdate,cl.class_title,cl.section,ac.name');
              $this->db->from('examallotment as e');
              $this->db->join('exams as ex', 'ex.id = e.examid');
              $this->db->join('classes as cl', 'cl.class_id = e.classid');
              $this->db->join('academicyear as Ac', 'Ac.id = e.academicyear');
              $query = $this->db->get(); 
               return $query;  
      }

      // Update Exam  data
      public function UpdateExam($data,$id)  { 
                $this->db->where('id', $id);
        $query =$this->db->update('exams', $data); 
        
         return $query;  
      }

       // Update Exam  data
      public function Updategrading($data,$id)  { 
                $this->db->where('id', $id);
        $query =$this->db->update('gradingscale', $data); 
        
         return $query;  
      }

      // Update Exam  data
      public function deletExam($data,$id)  { 
                $this->db->where('id', $id);
        $query =$this->db->update('exams', $data); 
        
         return $query;  
      } 

      // Delet grading scale data
      public function deletGrading($data,$id)  { 
                $this->db->where('id', $id);
        $query =$this->db->update('gradingscale', $data); 
        
         return $query;  
      }

       // Add new Examtype
      public function AddnewExamtype($data)  {  
         $this->db->insert('examtype', $data);  
      } 

      // Add new Examtype
      public function AddnewExam($data)  {  
         $this->db->insert('exams', $data);  
      }

      // Add new Examtype
      public function Addnewgrading($data)  {  
         $this->db->insert('gradingscale',$data);  
      }

       
  }  
?>  