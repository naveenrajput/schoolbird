<style>



 </style>
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
  <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Exam 
     
      </h1>
    <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Examination</a></li>
        <li><a href="#"> Exam</a></li>
      </ol>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;">
    
        <a  href="<?php echo base_url('addexamtypes')?>" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp; Add Exam</a>
    </div>
    </section>

    <!-- Main content -->
     <section class="content">
      <div class="row">
        <div class="col-xs-12">
    

     <div class="box">
     
         
    <div class="box-body table-responsive">
      
             <!-- <table id="example" class="display nowrap" style="width:100%">--->
              <table id="example" class="table table-bordered " >
        <div class="txt-dis">Export in Below Format</div>
        <thead>
            <tr>
    
                <th><input type="checkbox" id="selectall"> All</th>
                <th>S.no</th>
                <th>Name</th>
                <th>Code</th>
                <th>NOTES</th>
                <th>Action</th>
                
        
        
            </tr>
        </thead>
        <tbody>
    <?php  foreach ($exam->result() as $row)  {  ?>
            <tr>
      <td>
    <input type="checkbox" class="selectedId" name="selectedId" />
      </td>
              
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->exams;?></td>
                <td><?php echo $row->code;?></td>
                <td><?php echo $row->notes;?></td>
         
         
               
        <td>
              <ul class="table-icons">
      
          
      
    <li><a href=" <?php echo base_url('examedit/').$row->id;?>" class="table-icon" title="Edit"> 
    
    <span class="glyphicon glyphicon-edit display-icon"></span>
    </a></li>
          
     
    <li>
    <span class="glyphicon glyphicon-trash display-icon view_data" id="<?php echo $row->id;?>"></span>
    </li>
    </ul>
          </td>
              
        
            </tr>
           
    <?php }?>
        </tbody>
        <tfoot>
            <tr>
              <th> All</th>
                 <th>S.no</th>
                <th>SESSION</th>
                <th>STATUS</th>
                <th>NOTES</th>
                <th>Forward</th>
               

            </tr>
        </tfoot>
    </table>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

   
<script>  
 $(document).ready(function(){
      $('.view_data').click(function(){  
           var id = $(this).attr("id");
           //alert(employee_id);
           var x=confirm("Are you sure to delete record?");//alert(employee_id);
          if (x) {
            $.ajax({  
                        url:"<?php echo base_url('Examination/Examination/DeletExam'); ?>",  
                        method:"post",  
                        data:{id:id},  
                        success:function(data){  
                               location.reload();
                        }  
                   }); 
          } else {
            return false;
          } 
              });  
 });  
 </script>