<style>



 </style>
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
  <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Add New Allotment
     
      </h1>
   <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Examination</a></li>
        <li><a href="#">Exam types</a></li>
        <li class="active"> Add Exam Allotment</li>
      </ol>
    </div>
  
    </section>

    <!-- Main content -->
     <section class="content">
      <div class="row">
        <div class="col-xs-12">
    

     <div class="box">
      
        
      
            <div class="box-body table-responsive">
      
             <!-- <table id="example" class="display nowrap" style="width:100%">--->
             <form method="post" action="<?php echo base_url('saveexamtype');?>" data-toggle="validator" role="form">

              <div class="box-body">
        
        
          <div class="row">
         
         
        <div class="col-md-12">
        <div class="form-group">
                  <label for="">Academic Year</label>
                  <select type="text" class="form-control"name="acdmic" placeholder="Academic Year " required>
                    <option>--Select Acadmic--</option>
                    <?php foreach ($Academic->result() as$value) {?>
                     <option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
                    <?php }?>
                  </select>
                </div>
        </div>
       <div class="col-md-12">
        <div class="form-group">
                  <label>Exam Name </label>
                  <select type="text" class="form-control"name="exam" placeholder="Exam Name" required>
                    <option>--Select Exam--</option>
                    <?php foreach ($Exam->result() as  $value) {?>
                     <option value="<?php echo $value->id;?>"><?php echo $value->exams;?></option>
                    <?php }?>
                  </select>
                  
                </div>
        </div>      
         <div class="col-md-12">
        <div class="form-group">
                  <label>Class </label>
                  <select type="text"  id="class" class="form-control" name="Class" placeholder="Notes"  required>
                    <option>--Select Class--</option>
                    <?php foreach ($class->result() as $value) {?>
                     <option value="<?php echo $value->class_id;?>"><?php echo $value->class_title;?></option>
                    <?php }?>
                  </select>
                </div>
        </div>
         
<div class="col-md-12 table-responsive" id="subject" style="display: none;">

  <table class="table fetchdata table-hover table-bordered">
   <thead><tr>
     <th>Subject</th>
     <th>Date</th>
     <th>From time</th>
     <th>To Time</th>
     <th>Hall</th>
     <th>Building</th>  
     <th> Instructor</th>
     <th>Max Marks/<br> Highest Grade</th> 
      <th>Passing Marks/<br> Lowest Grade</th>
     
   </tr>
 </thead>
 <tbody id="test-body-sub"><tr  id="row0">
   <td>
     
      <select class="form-control select1" name="dep_id" style="width: 100%;" data-placeholder="Select" required="">
        <option selected="selected" value="">Select</option>
          <?php  $data = json_decode($value->subject); 
              foreach ($data as $row) {?> 
               <option selected="selected" value=""><?php echo $row->sub_title; ?></option>
              <?php } ?>
                 
      </select>      
   </td>
   <td>
     <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" name="joiningdate" class="form-control pull-right datepicker" required="">
        </div>

   </td>
   <td><div class="input-group">
     <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" class="form-control timepicker">

                   
                  </div></td>
   <td><div class="input-group">
     <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" class="form-control timepicker">

                   
                  </div></td>
   <td><input type="text"  class="form-control" required=""></td>
   <td><input type="text"  class="form-control" required=""></td>
   <td> 
    <select class="form-control select1" name="dep_id" style="width: 100%;"data-placeholder="Select" required="">
      <option selected="selected" value="">Select</option>
      <option value="1">DDDFDDFF</option>         
      <option value="2">FFFF</option>
      <option value="1">FFDDDFFF</option>         
      <option value="2">BBBB</option>
      <option value="1">GGGDDD</option>         
      <option value="2">BBDDDBB</option>         
    </select>   
  </td>
   <td><input type="text"  class="form-control" required=""></td>
   <td><input type="text"  class="form-control" required=""></td>

   </tr>
  </tbody></table>
  <div class="form-group">
          <button id="add-row-sub" class="btn btn-primary btn-sm" type="button" value="Add"> Add </button>             
        </div>
</div>
          
       
         </div>
        
   
        </div>
 
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
<script type="text/javascript">
  
$(document).ready(function(){
  $("#class").change(function(){
  var selectBox = document.getElementById("class");
    var selectedclass = selectBox.options[selectBox.selectedIndex].value; 
    alert(selectedclass);
    $("#subject").show();
  });
 
});

$(document).ready(function(){
// ADD ROW
var row=1;
  $(document).on("click", "#add-row-sub", function () {
  var new_row = '<tr id="row"><td> <select class="form-control select1" name="dep_id" style="width: 100%;" data-placeholder="Select" required=""> <option selected="selected" value="">Select</option><option value="1">FFFGFFFFAAA</option> <option value="2">SDDFF</option><option value="1">GFFFFDFFF</option><option value="2">VFRFRR</option><option value="1">DFFFDDFF</option><option value="2">VFGRR</option></select></td><td><div class="input-group date"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" name="joiningdate" class="form-control pull-right datepicker" required=""></div></td><td><div class="input-group"><div class="input-group-addon"><i class="fa fa-clock-o"></i></div><input type="text" class="form-control timepicker"></div></td><td><div class="input-group"><div class="input-group-addon"><i class="fa fa-clock-o"></i></div><input type="text" class="form-control timepicker"></div></td><td><input type="text"  class="form-control" required=""></td><td><input type="text"  class="form-control" required=""></td><td> <select class="form-control select1" name="dep_id" style="width: 100%;" data-placeholder="Select" required=""><option selected="selected" value="">Select</option><option value="1">DDDFDDFF</option><option value="2">FFFF</option><option value="1">FFDDDFFF</option><option value="2">BBBB</option><option value="1">GGGDDD</option><option value="2">BBDDDBB</option></select></td><td><input type="text"  class="form-control" required=""></td><td><input type="text"  class="form-control" required=""></td></tr>';

  $('#test-body-sub').append(new_row);
  row++;
  return false;
  });
  });
</script>
