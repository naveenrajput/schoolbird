
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit exam Form
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dashboard</a></li>
        <li class="active">Editexam</li>
      </ol>
    </section>
    <!-- Add class content section -->
    <section class="content">
      <div class="row">
        <!-- SELECT2 EXAMPLE -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Edit exam</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <div class="box-body">
                <div class="row">
                 <form method="post" action="<?php echo base_url('examupdate'); ?>" data-toggle="validator" role="form">
                      
             <div class="col-md-6">
              <div class="form-group">

                <input type="hidden" name="id" value="<?php  echo $exam[0]->id;?>" >
                      <label>exam Name*</label>
                      <input type="text" name="name" class="form-control" value=" <?php echo $exam[0]->exams;?>" required><?php echo form_error('class'); ?>
                    </div>

                    <div class="form-group">

                <div class="col-md-12">
                  <div class="form-group">
                    <label>Exam Type </label>
                    <select type="text" class="form-control"name="type" placeholder="Code" required>
                      <option>--Select Type--</option>
                      <?php foreach ($type->result() as $val) {?>
                       <option value="<?php echo $val->id;?>"><?php echo $val->name;?></option>
                      <?php }?>
                    </select>
                  </div>
               </div>
                    </div>

            </div>
             <div class="col-md-6">
              <div class="form-group">
                <label>code*</label>
                  <input type="text" name="code" class="form-control" value="<?php  echo $exam[0]->code;  ?>" required>
               </div>
            </div>
           
            <div class="col-md-12">
             <div class="form-group">
                      <label>Note </label>
                      <textarea class="form-control" name="notes" rows="3" placeholder="Counselor’s Remarks " required><?php echo $exam[0]->notes; ?></textarea>
                    </div>

            </div>
            <div class="clearfix"></div>
            <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

          </form>
      </div>

                
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  