<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport extends MY_Controller {

	
	public function __construct(){
        parent::__construct();
        //$this->load->library('APIAccessToken');
        $this->load->helper('url');
        $this->load->model('Transport_model');
      }

	// For frontdask buses
	public function buses()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['buses']=$this->Transport_model->getbuses();
		$this->load->view('buses',$data);
		$this->load->view('../../inc/footer');
	}

	// For Add new Bus
	public function AddnewBus()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('addbus');
		$this->load->view('../../inc/footer');
	}

    // For Acadmic routes
	public function routes()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['routes']=$this->Transport_model->getRoutes();//print_r($data);die;
		$this->load->view('Routes',$data);
		$this->load->view('../../inc/footer');
	}

	// For load  Add New routes form
	public function AddNewroutes()
	{
		$data['getStops'] =$this->db->query('SELECT * FROM `stops` ORDER BY `name` ASC');//print_r($getStops);die;
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('Add_Routes',$data);
		$this->load->view('../../inc/footer');
	}
	// For load   Save new Router
	public function SavenewRouter()
	{
		$this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('route1', 'First Stop', 'required');
        $this->form_validation->set_rules('route2', 'Last Stop', 'required');
        if($this->form_validation->run() == FALSE){
       echo validation_errors();
        }
        else
        {
	        $name = $this->input->post('name');
			$route1 = $this->input->post('route1');
			$route2 = $this->input->post('route2');
			$waypoints = $this->input->post('waypts');
			$stop = count($waypoints)-1;
			$waypoints = implode(':',$waypoints);
			$First_Stop = $this->input->post('First_Stop');
			$Last_Stop = $this->input->post('Last_Stop');
			
			
			$data = array(
	                 'name'     => $name,
			         'start'    => $route1,
			         'end'      =>$route2, 
			         'waypoints'=>$waypoints,
			         'stopcount'=>$stop+count($route1)+count($route1),
			         'createdate'      => date("Y-m-d h:i:sa")
	                );
	        $data['routes'] = $this->Transport_model->AddNewRouts($data);
			redirect(base_url('routes'));
        }
		
	}

    // For Save new Bus
    public function SavenewBus(){

		$busno = $this->input->post('busno');
		$driver = $this->input->post('driver');
		$dmobile = $this->input->post('dmobile');
		$conductor = $this->input->post('conductor');
		$cmobile = $this->input->post('cmobile');

		$this->form_validation->set_rules('busno', 'bus no', 'required');
		$this->form_validation->set_rules('driver', 'driver name', 'required');
        $this->form_validation->set_rules('dmobile', 'Driver mobile', 'required');
        $this->form_validation->set_rules('conductor', 'conductor name', 'required');
        $this->form_validation->set_rules('cmobile', 'conductor mobile', 'required');

		$data = array(
                 'bus'            => $busno,
		         'drivername'     => $driver,
		         'drivernumber'   => $dmobile,
		         'conductorname'  => $conductor,
		         'conductornumber'=> $cmobile,
		         'createdate'     =>date("Y-m-d h:i:sa")
                );
        if( 
        	$this->form_validation->run() == FALSE){
   
        }
        else
        {
          $data['savebus'] = $this->Transport_model->addNewBus($data);
		  redirect(base_url('buses'));
        }
		
	}

    // For frontdask stops
	public function stops()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['stops'] = $this->Transport_model->getStops();
		$this->load->view('stops',$data);
		$this->load->view('../../inc/footer');
	} 


    // For Transport Add new stop Data
	public function SaveNewstop()
	{
		$name       = $this->input->post('name');
		$address    = $this->input->post('address');
		$identifier = $this->input->post('identifier');
		$latitude   = $this->input->post('latitude');
		$longitude  = $this->input->post('longitude');
		//$stopcount = $this->input->post('First_Stop');
		$data = array(
                 'name'      => $name,
		         'address'   => $address,
		         'identifier'=> $identifier,
		         'lat'       => $latitude,
		         'lng'       => $longitude,
		         'createdate'=>date("Y-m-d h:i:sa")
                );
		$this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('address', 'Addressp', 'required');
        $this->form_validation->set_rules('identifier', 'Identifier', 'required');
        $this->form_validation->set_rules('latitude', 'latitude', 'required');
        $this->form_validation->set_rules('longitude', 'longitude', 'required');
        if( 
        	$this->form_validation->run() == FALSE){
   
        }
        else
        {//print_r($data);die;
          $this->Transport_model->addNewstop($data);
		  redirect(base_url('stops'));
        }
		
	}

	// For load  Add new Stop Form
	public function AddStopForm()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('Add_Stop');
		$this->load->view('../../inc/footer');
	}

	// For load  Stop view on map  
	public function StopView($id)
	{ 
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['stop'] = $this->Transport_model->GetstopData($id);
		$this->load->view('Stop_View',$data);
		$this->load->view('../../inc/footer');
	}

	// For update Routs   
	public function UpdateStopsData()
	{   $id= $this->input->post("id");
	    $name       = $this->input->post('name');
		$address    = $this->input->post('address');
		$identifier = $this->input->post('identifier');
		$latitude   = $this->input->post('latitude');
		$longitude  = $this->input->post('longitude');
		$data = array(
                 'name'      => $name,
		         'address'   => $address,
		         'identifier'=> $identifier,
		         'lat'       => $latitude,
		         'lng'       => $longitude,
		         'modifieddate'=>date("Y-m-d h:i:sa")
                );
		$this->Transport_model->UpdateStopsData($id,$data);
		//print_r($data);die;
		
	}

    // For frontdask reporting
	public function reporting()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // For frontdask live_tracking
	public function live_tracking()
	{  
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['export']  = $this->Transport_model->ExportBusRoute();
		$this->load->view('live-tracking',$data);
		$this->load->view('../../inc/footer');
	}

	

   // For Transport speed_chart
	public function speed_chart()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['export']  = $this->Transport_model->ExportBusRoute();
		$this->load->view('speedchart',$data);
		$this->load->view('../../inc/footer');
	}

	// For Transport speed_chart
	public function getSpeedChart()
	{
		$bus     =   $this->input->post('BUS');
		$fromdate    =   $this->input->post('date');
		$timeone = $this->input->post('timeone');
		$timetwo = $this->input->post('timetwo');
		
		$data['getdata'] = $this->db->query("SELECT * FROM `buses` WHERE `bus` = '$bus'");

		  
		$data['getList'] = $this->db->query("SELECT * FROM `busgps` WHERE `busno` = '$bus' AND `devicetime` BETWEEN '$fromdate.$timeone' AND '$fromdate.$timetwo'");

		$this->load->view('get_speedchart',$data);
	}

	// For Transport halt_summary
	public function halt_summary()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['export']  = $this->Transport_model->ExportBusRoute();
		$this->load->view('haltChart',$data);
		$this->load->view('../../inc/footer');
	}

	// For Transport halt_summary chart
	public function Getchart()
	{
		$bus = $this->input->post('BUS');
		$halttime = $this->input->post('halttime');
		$fromdate = $this->input->post('fromdate');
		$todate = $this->input->post('todate');

		$data['getdata'] = $this->db->query("SELECT * FROM `buses` WHERE `bus` = '$bus'");

		$data['getList'] = $this->db->query("SELECT * FROM `busgps` WHERE `busid` = '$bus' AND `devicetime` BETWEEN '$fromdate' AND '$todate'");

		$this->load->view('halt_summarychart',$data);
		
	}	

	// For Transport halt_summary chart
	public function Getdirection()
	{
		$bus     =   $this->input->post('BUS');
		$date    =   $this->input->post('date');
		$timeone = $this->input->post('timeone');
		$timetwo = $this->input->post('timetwo');

		$data['getdata'] = $this->db->query("SELECT * FROM `buses` WHERE `bus` = '$bus'");
          
		$data['getList'] = $this->db->query("SELECT * FROM `busgps` WHERE `busno` = '$bus' AND `devicetime` BETWEEN '$date.$timeone' AND '$date.$timetwo'");
		  $this->load->view('getreplay-route',$data);
		
	}

	// For Transport Bus_Route
	public function Bus_Route()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}
	
	// For Transport in_process
	public function in_process()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// For Mis Reports
	public function MisReports()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('mis_reports');
		$this->load->view('../../inc/footer');
	}

	

     // For bus Bus Route Export
	public function BusRoute()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['export'] = $this->Transport_model->ExportBusRoute();
		$this->load->view('bus-route',$data);
		$this->load->view('../../inc/footer');
		
	}

	  // ForNotification Mis Report Export
	public function NotificationMisReport()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['export'] = $this->Transport_model->ExportBusRoute();
		$this->load->view('notification-mis',$data);
		$this->load->view('../../inc/footer');
		
	} 

	// ForNotification Mis Report Export
	public function notificationMisRresult()
	{
		//print_r($_POST);die;

		$date = $_POST['req'];
		$from = $date." 00:00:00";
		$to = $date." 23:59:59";

		$getData=$this->Transport_model->notificationMis();

		$name = "Notification-MIS.xls";
    }

    // For PortalLogin Mis Report Export
	public function PortalLoginMis()
	{   
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['export'] = $this->Transport_model->PortalnMis();
		$this->load->view('notification-mis',$data);
		$this->load->view('../../inc/footer');
    }


	

	 // For bus Bus Route Export
	public function BusRouteResult()
	{
		if($_POST['bus'] == 'ALL'){

            $data['Result'] = $this->Transport_model->BusRoute();
			
			}
	    else{

			$bus = $_POST['bus'];
			$data['Result'] = $this->Transport_model->BusRouteselected($bus);
				
		}
			$name = "Buz-vs-Route.xls";
			header("Content-Disposition: attachment; filename=\"$name\"");
			header("Content-Type: application/vnd.ms-excel");
			
	}
	


    // For bus Replay Tracking
	public function ReplayTracking()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$data['export'] = $this->Transport_model->ReplayTracking();
		$this->load->view('replay-tracking',$data);
		$this->load->view('../../inc/footer');
	}


    



}
