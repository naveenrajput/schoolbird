<?php  
   class Transport_model extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }  
      //Function To Fetch  Routes data Record 
      public function getRoutes()  {  
        $this->db->where('delete', 0); 
        $data = $this->db->get('routes');
         return $data; 
      }

      //Function To Fetch  Routes data Record 
      public function getbuses()  {  
        $this->db->where('delete', 0); 
        $data = $this->db->get('buses');
         return $data; 
      } 


      //Function To Fetch  Routes data Record 
      public function GetstopData($id)  {  
        $this->db->where('delete', 0);
        $this->db->where('id', $id); 
        $data = $this->db->get('stops');
         return $data; 
      }


      //Function To Insert New addNewstop  data  
      public function addNewstop($data)  {  

        $this->db->insert('stops', $data);

      }

      //Function To Insert New Routes  data  
      public function AddNewRouts($data)  {  

       $query = $this->db->insert('routes', $data);
       //$query = $this->db->last_query();print_r($query);die;

      } 



      //Function To Insert New Routes  data  
      public function addNewBus($data)  {  

        $this->db->insert('buses', $data);
      }


      //Function To update Routs data  data  
      public function UpdateStopsData($id,$data)  {  
        $this->db->where('id', $id);
        $this->db->update('stops', $data);
      }

       //Function To Fetch  Stop data Record 
      public function getStops()  {  
             $this->db->where('delete', 0); 
             $data = $this->db->get('stops');
         return $data; 
      } 


      //Function To Fetch  BusRoutet data Record 
      public function ExportBusRoute()  { 
              $data = $this->db->get('buses');
              return $data; 
      } 


      //Function To Fetch  BusRoutet data Record 
      public function notificationMis()  { 
             $this->db->select('no.notification,gua.father,gua.username,student.name,class.name,notifications.createdate,student.sms');
             $this->db->from('students as st');
             $this->db->join('guardians as gua', 'gua.studentid = st.id');
             $this->db->join('classes as cl', 'cl.id = st.admittedclass');
             $this->db->where('gua.logged=1');
             $this->db->where('gua.pushtoken=1');
             $query = $this->db->get(); 
         //$query = $this->db->last_query();print_r($query);die;
          return $query; 

             
      }

      //Function To Fetch  PortalnMis data Record 
      public function PortalnMis()  { 
        $getData = mysqli_query($con,"SELECT * FROM `userlog` ORDER BY `id` DESC") or die(mysqli_error($con));
             $this->db->select('*');
             $this->db->from('students as st');
             $this->db->join('guardians as gua', 'gua.studentid = st.id');
             $this->db->join('classes as cl', 'cl.id = st.admittedclass');
             $this->db->where('gua.logged=1');
             $this->db->where('gua.pushtoken=1');
             $query = $this->db->get(); 
         //$query = $this->db->last_query();print_r($query);die;
          return $query; 

             
      } 




      public function getexplodstudent()  { 
              $this->db->order_by("name", "asc");
              $data = $this->db->get('routes');
             return $data; 
      }


      // get data for realy traking
      public function ReplayTracking()  { 
              
              $data = $this->db->get('buses');
             return $data; 
      }


      // get export data for parants login Table
      public function getmodeldata()  { 

             $this->db->select('st.stid,st.name, gua.mobile,gua.g_name, cl.name');
             $this->db->from('students as st');
             $this->db->join('guardians as gua', 'gua.studentid = st.id');
             $this->db->join('classes as cl', 'cl.id = st.admittedclass');
             $this->db->where('gua.logged=1');
             $this->db->where('gua.pushtoken=1');
             $query = $this->db->get(); 
             //$query = $this->db->last_query();print_r($query);die;
              return $query; 
      }

      // get data for get data BusRoute Table
      public function BusRoute()  { 

           $this->db->select('buses.bus,routes.name,busroutemap.from, busroutemap.to, busroutemap.day');
           $this->db->from('buses,routes,busroutemap'); 
           $this->db->where('buses.id = busroutemap.busid');
           $this->db->where('routes.id = busroutemap.routeid');
           $this->db->order_by('buses.bus','asc'); 
           $query = $this->db->get(); 
            return $query; 
      } 
      // get data for get data BusRoute selected Table
      public function BusRouteselected($bus)  { 

         $this->db->select('buses.bus,routes.name,busroutemap.from, busroutemap.to, busroutemap.day');
         $this->db->from('buses,routes,busroutemap'); 
         $this->db->where('buses.id = busroutemap.busid');
         $this->db->where('routes.id = busroutemap.routeid');
         $this->db->where('buses.id = busroutemap.routeid');
         $this->db->order_by('buses.bus',$bus); 
         $query = $this->db->get(); 
          return $query; 
      }
      

      // get data for get data RouteStudent Table
      public function RouteStudent()  {


         $this->db->select('students.stid,students.name,students.mobile,classes.name,stops.name,classes.section,studenttransport.morningroute,studenttransport.eveningroute,studenttransport.saturdaymorningroute,studenttransport.saturdayeveningroute');
         $this->db->from('students,classes,stops,studentsession,studenttransport'); 
         $this->db->where('studenttransport.studentid = students.id');
         $this->db->where('studentsession.studentid = students.id');
         $this->db->where('studentsession.classid = classes.id'); 
         $this->db->where('studenttransport.stop = stops.id');
         $this->db->order_by('students.name','asc');         
         $query = $this->db->get(); 
          return $query; 
      }

      

      // get data for get data StopsStudent Table
      public function RouteStudentselected($stop)  { 

         $this->db->select('students.stid,students.name,students.mobile,classes.name,stops.name,classes.section,studenttransport.morningroute,studenttransport.eveningroute,studenttransport.saturdaymorningroute,studenttransport.saturdayeveningroute');
         $this->db->from('students,classes,stops,studentsession,studenttransport'); 
         $this->db->where('studenttransport.studentid = students.id');
         $this->db->where('studentsession.studentid = students.id');
         $this->db->where('studentsession.classid = classes.id'); 
         $this->db->where('studenttransport.stop = stops.id'); 
         $this->db->where('studenttransport.stop = stops.id');
         $this->db->order_by('stops.id',$stop);         
         $query = $this->db->get();
         
          return $query; 
         
      }

      

     


      
   }  
?>  