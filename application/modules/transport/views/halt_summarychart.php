<?php 


$busName = $this->input->post('BUS');
$fromdate = $this->input->post('fromdate');
$todate = $this->input->post('todate');
$minhalt  = $this->input->post('halttime');
//print_r($minhalt);die;
if($minhalt == '')
{
    $minhalt = '0';
}
?>

 <meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link href="https://www.schoolbirdapp.com/dps/client/css/w3.css" rel="stylesheet">

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="../scripts/misc.js"></script> -->

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/dc4f36d12e.js"></script>

<style type="text/css"></style>


<script type="text/javascript"> 
 var xAxisArray = [];
var seriesArray = []; 
    <?php
    $j=0;
    $deviceArray = Array();
    $haltArray = Array();
    $timeArray = Array();
    $ltlng = Array();

    foreach ($getdata->result() as $row) {

    	$busName = $row->bus;

    }

    foreach ($getList->result() as $row) {
    
    
	    $devicetime = $row->devicetime;
	    $halttime = $row->halttime;
	    $halt = strtotime($row->halttime) - strtotime($row->devicetime);
	    $halt = round(($halt/60),2);

	    if($halt >= $minhalt)
	    {
		    $temp = date("d M h:i A",strtotime($row->devicetime))." ~ ".date("d M h:i A",strtotime($row->halttime));
		    $haltArray[$j] = $halt;
		    $timeArray[$j] = $temp;
		    $ltlng[$j] = $row->lat.",".$row->lng;

	    
			?>
			seriesArray[<?php echo $j;?>] = <?php echo $halt;?>;
			xAxisArray[<?php echo $j;?>] = '<?php echo $temp;?>';
			<?php
			$j++;
		}

	}

  
    ?>

</script>






<script type='text/javascript'>//<![CDATA[

var chart;
// Data retrieved from http://vikjavev.no/ver/index.php?spenn=2d&sluttid=16.06.2015.
$(function () {
    chart = Highcharts.chart('container', {
        chart: {
            type: 'column',
            style: {
            fontFamily: 'Rubik'
        }
        },
        title: {
            text: 'Halt Chart For <?php echo $busName;?>'
        },
        subtitle: {
            text: 'Data for <?php echo date("d, M y",strtotime($fromdate));?> To <?php echo date("d, M y",strtotime($todate));?>'
        },
        xAxis: {
            categories: xAxisArray,
            labels:{
                enabled:false
            }
        },
        yAxis: {
            title: {
                text: 'Speed Kmph'
            },
            minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
            plotBands: [{ 
                from: 4,
                to: 500,
                color: 'rgba(231, 76, 60, 0.4)',
                label: {
                    text: 'LONG HALT',
                    style: {
                        color: '#606060'
                    }
                }
            }]
        },
        tooltip: {
            valueSuffix: ' Kmph',
            enabled: true,
    formatter: function() {
                return '<b>'+ this.y +' Minutes</b><br/>'+this.x;

                  }
        },
        plotOptions: {
            spline: {
                lineWidth: 4,
                states: {
                    hover: {
                        lineWidth: 5
                    }
                },
                marker: {
                    enabled: true
                },
            }
        },
        series: [{
            name: '<?php echo $busName;?>',
            data: seriesArray

        }],
        navigation: {
            menuItemStyle: {
                fontSize: '10px'
            }
        }
    });
});
//]]> 

</script>
  

 <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://cdn.jsdelivr.net/gh/Kevnz/stats.js/build/stats.min.js"></script>
<div style="float:right">
<div class="btn-group">
    <button class="btn btn-sm btn-default" onclick="$('#container').show();$('#containerTab').hide();changeType('column')">
                <i class="fa fa-bar-chart"></i>&nbsp;BAR
    </button>
    <button class="btn btn-sm btn-default" onclick="$('#container').show();$('#containerTab').hide();changeType('spline')">
                <i class="fa fa-line-chart"></i>&nbsp;SPLINE
    </button><button class="btn btn-sm btn-default" onclick="$('#container').hide();$('#containerTab').show();">
                <i class="fa fa-th-large"></i>&nbsp;DATA TABLE
    </button>
</div>
</div>
<br/><br/>
<div id="container" style="width: 100%; height: 400px; margin: 0 auto"></div>
<div id="containerTab" style="width: 100%; height: 400px; margin: 0 auto;overflow-y:auto;display:none">
    <table class="table table-striped table-hovered fetch">
        <tr>
            <th>#</th><th>Halt Duration</th><th>During Time</th><td>Address</td>
        </tr>
        <?php

        foreach($haltArray as $key => $val)
        {
            ?>
            <tr class="<?php if($val > 4) echo "bg-danger" ;?>">
                <td><?php echo $key+1;?></td>
                <td><?php echo $val;?> Minutes</td>
                <td><?php echo $timeArray[$key];?></td>
                <td class="text-default"  lang="<?php echo $ltlng[$key];?>" onclick="getAddress(this)">View Address</td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>

<script type="text/javascript">
    window.top.window.hideProcessing();

    function changeType(newType) {
        chart.update({
            chart: {
                type:newType
            } 
        });
    }
</script>
