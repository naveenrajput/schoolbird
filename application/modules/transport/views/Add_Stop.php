<style>

.controls {
    margin-top: 10px;
    border: 1px solid transparent;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    height: 32px;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

 </style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Add Stop 
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Trasport</a></li>
        <li><a href="#">Stop</a></li>
        <li class="active"> Add Stop</li>
      </ol>
    </section>
<section class="content">
      <div class="row">
      <!-- SELECT2 EXAMPLE -->
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box ">
            <div class="box-header with-border">
              <h3 class="box-title">Add Stop</h3>
            </div>
            <!-- /.box-header -->
          
            <form method="post" action="<?php echo base_url('Savestop');?>" data-toggle="validator" role="form" novalidate="true">

              <div class="box-body">
        
        
          <div class="row">
         
        <div class="col-md-6">
        <div class="form-group has-error has-danger">
                 
          <input class="form-control" type="text" style="width:100%" id="stop0" name="name" required="">
         <label class="w3-label w3-label-custom" style="font-size:11px !important;">
          Name</label>
                </div>
        </div>
          <div class="col-md-6">
        <div class="form-group"> 
          <input class="form-control" type="text"  id="stop1" name="address" required="">
          <label class="" style="font-size:11px !important;">Address</label>
    </div> </div>
         <div class="col-md-12">
   <div class="form-group">      
      <input class="form-control" type="text" id="stop4" name="identifier" required="">
      <label class="" >
    Identifier (For future reference to this stop)</label>
    </div>
<div class="col-md-6">
        <div class="form-group"> 
          <input class="form-control" type="text"  id="stop2" name="latitude" readonly="readonly"  lang onkeyup="this.value = this.lang">
          <label class="">Latitude</label>
    </div> </div>
    <div class="col-md-6">
        <div class="form-group"> 
          <input class="form-control" type="text" id="stop3" name="longitude" readonly="readonly" lang  onkeyup="this.value = this.lang">
          <label class="" >Longitude</label>
    </div> </div>
        </div>
         </div>
        
                <button type="submit" class="btn btn-primary">Submit</button>
          
         </div>
         
   
        </form></div>
  </div>
             </div>
              <!-- /.box-body -->

              <div class="box-footer">
       <div style="border:10px #fff solid;border-radiius:5px;" id="infoMap">
<br>
  Please click on the right spot on the map for this location.
  <br>

<input id="pac-input" class="controls" type="text" placeholder="Search Your Stop Here">
  <div style="width:100%;height:550px" id="map"></div>
</div>
              </div>
            
           
          </section>
    <!-- Main content -->
 


  </div>



<!-- zoo -->

</html>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places&callback=initMap">
    
</script>

<script type="text/javascript">
  

var searchBox;
var map;
var input;
var markers;
var places;
var bounds;
var icon;
var myposition;

var mycity;
var preinfowindow;
var preLat = '0';

var myLat = 22.717623859241886;
myLat = parseFloat(myLat);

var myLang = 75.85792502429194;//alert(myLang);
myLang = parseFloat(myLang);
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: myLat, lng: myLang},
          zoom: 13,
          scrollwheel: false,
        });

         google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(map, event.latLng);
  });


if(preLat == '1')
{    image = {
  url: '../pin.svg',
  size: new google.maps.Size(24, 24 ),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(40,40),
  scaledSize: new google.maps.Size(24, 24)
};




    myposition = new google.maps.Marker({
    position: {lat: myLat, lng: myLang},
    map: map,
    optimized: false,
    icon:image
  });     




 preinfowindow = new google.maps.InfoWindow({
    content: '<button class="btn btn-sm btn-success">THIS IS THE CURRENT SPOT</button>'
  });
  preinfowindow.open(map,myposition);
}



//  animateRadius();

        // Create the search box and link it to the UI element.
        input = document.getElementById('pac-input');
        searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location,
              optimized: false
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

 var infowindow;
 var marker;
function myMap() {
  var mapCanvas = document.getElementById("map");
  var myCenter=new google.maps.LatLng(51.508742,-0.120850);
  var mapOptions = {center: myCenter, zoom: 5};
  var map = new google.maps.Map(mapCanvas, mapOptions);

  google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(map, event.latLng);
  });
}


  var direction = 1;
    var rMin = 500, rMax = 700;

var animCity;
function animateRadius()
{
       animCity = setInterval(function() {
            var radius = myCity.getRadius();
            if ((radius > rMax) || (radius < rMin)) {
                direction *= -1;
            }
            myCity.setRadius(radius + direction * 100);
        }, 100);
}

var image;

function placeMarker(map, location) {
  if(marker)
  {
  marker.setMap(null);    
  }

     image = {
  url: '../pin.svg',
  size: new google.maps.Size(12, 12 ),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(10,10),
  scaledSize: new google.maps.Size(12, 12)
};



marker = new google.maps.Marker({
    position: location,
    map: map,
    optimized: false,
    icon:image
  });

google.maps.event.addListener(marker, 'click', function() {
  infowindow.open(map,marker);
  });
 infowindow = new google.maps.InfoWindow({
    content: '<button class="btn btn-sm btn-danger" onclick="sendToTop('+location.lat()+','+location.lng()+');this.innerHTML = \'SPOT USED\';this.className = \'btn btn-sm btn-success\'">USE THIS LOCATION</button>',
    pixelOffset: new google.maps.Size(0,0)
  });
  infowindow.open(map,marker);
}


function sendToTop(lat,lng)
{
 window.top.window.document.getElementById('stop2').readOnly = false;
 window.top.window.document.getElementById('stop3').readOnly = false;


 window.top.window.document.getElementById('stop2').lang = lat;  
 window.top.window.document.getElementById('stop3').lang = lng;
 window.top.window.document.getElementById('stop2').value = lat;  
 window.top.window.document.getElementById('stop3').value = lng;


 }
</script>
<!-- <script src="https://code.jquery.com/jquery.js"></script> 
<script async defer src="https://maps.googleapis.com/maps/api/js??key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places&callback=initMap"></script> -->