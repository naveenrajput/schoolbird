
 <style type="text/css">
  #map {
        height: 100%;
        width: 100%;
        margin-top:20px;
       
      }

    
.waypoints {
  min-height: 50px;
 
  margin: 10px 10px 20px 0px;
  padding: 8px 8px 4px 8px;
  background-color: #ECEFF1;
 
  display: grid: ;
}


.item {
  cursor: pointer;  
  padding: 15px;
  margin-bottom: 8px;
  background-color: white;
  border-radius: 2px;
  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);
  transition: margin 0.1s;
}

.item-selected {
  box-shadow: 0 5px 5px -3px rgba(0,0,0,.2), 0 8px 10px 1px rgba(0,0,0,.14), 0 3px 14px 2px rgba(0,0,0,.12);
  margin: 5px -15px 15px -15px;
}

    </style> 
      <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
        Transport
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Transport</a></li>
        <li class="active">Report</li><li class="active">MIS REPORTS</li><li class="active">MIS REPORTS EXPORT</li>
      </ol>
    </section>

    <!-- Main content -->
     <section class="content">
      <div class="row">
        <div class="col-xs-12">
    

     <div class="box">
      
        
      
            <div class="box-body">
      
        
          <div class="row">
         
     <div class="col-md-12">
        <div class="col-md-6">    
        <div class="form-group">
                  <label for="">Academic Year </label>
                   <select class="form-control waypoints select1"  id="actBus" name="bus" required=""  multiple="multiple">
    
     <?php foreach ($export->result() as $export){ ?>

            <option class="item " value="<?php echo $export->bus; ?>"><?php echo $export->bus; ?></option>
          <?php } ?>
          </select>  
                </div>
        </div>



                <div class="col-md-12" >
              <button class="btn btn-primary" id="TRACK">TRACK BUSES <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
      </div>
        <div class="clearfix" ></div>
 
     </div>
   </div>

       
          <!-- /.box-body --> 
        </div>
    

        </div>
        <!-- /.col -->

      </div>
         <div class="col-sm-12" style="height:600px;overflow:hidden;">
            <div id="map"></div>
      
          </div>

     </div>
    </section>

 
    <!-- /.content -->
  </div>
   



<script type="text/javascript">
  $('#TRACK').click(function changeFunc() {
   
   var selectBox = document.getElementById("actBus");
    var bus = selectBox.options[selectBox.selectedIndex].value;
   var fruits = [];
   var marker = null;
   var newLatLng = new google.maps.LatLng(22.7196,75.8577);
   $.ajax({

        type:"post",//or POST
        url:'https://locate.trackinggenie.com/trackingapi/Get_vehicle_status.php?token=VEdfMTI1OTM',
                          
         data:{bus:bus},
        dataType: 'text',
        success:function(responce){
      

       var jo = $.parseJSON(responce); 

        $.each(jo, function (index, value) {
          if (bus == index) {
              console.log(value[0].NLangitude);  
               var lat = value[0].NLangitude;
               var lng = value[0].ELangitude;
              var uluru = {lat: lat, lng: lng};
              //alert(uluru);
              var newLatLng = new google.maps.LatLng(lat, lng);
              initMap(newLatLng,bus)
              
              
          }
        });
                
                }  
     })
   })

 function initMap(newLatLng,bus) {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: {lat: 26.2183, lng: 78.1828},
          mapTypeControl: true,
         });

        var icon = {
    url: "<?php echo base_url("assets/dist/img/map-bus.png") ?>", // url
    scaledSize: new google.maps.Size(60, 60), // scaled size
    origin: new google.maps.Point(0,0), // origin
    anchor: new google.maps.Point(0, 0) // anchor
};

        setInterval(function() {     
          marker = new google.maps.Marker({
          map: map,
          draggable: false,
          icon:icon,
             title: bus,
          position: newLatLng,
        });
   }, 3000);
    
  
      }


 </script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places&callback=initMap">
    
</script>
