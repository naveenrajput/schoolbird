  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Speed Chart
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Transport</a></li>
        <li class="active">Speed Chart</li><li class="active">Speed Chart</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
   
   
     <div id="tableDiv" style="padding: 10px; height: 412px; overflow-y: auto;"><div style="padding:20px;">

<form  method="post"id="formid" >
<div class="row">

<div class="col-sm-6" style="padding:0px ;">
<div class="w3-group margin10" style="width:100%">  
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         Select Bus</label>
    <select class="form-control select1"  id="route1" name="bus" required="">
      <option value="0">Select</option>
      <?php foreach ($export->result() as $export){ ?>
    <option value="<?php echo $export->bus; ?>"><?php echo $export->bus; ?></option>
  <?php } ?>
         </select>  
    </div>
</div>
<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%">      
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         For Date</label>
          <input class="form-control" type="date" style="width:100%" id="date" name="date" required="">
    </div>
</div>

<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%">      
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         From Time</label>
          <input class="form-control" type="time" style="width:100%" id="timeone" name="fromtime" required="">
    </div>
</div>


<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%">      
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         To Time</label>
          <input class="form-control" type="time" style="width:100%" id="timetwo" name="totime" required="">
    </div>
</div><br><br>
<div class="col-sm-6">
  <button class="btn btn-primary" id="TRACK"type="button">View Result&nbsp;&nbsp;<i class="fa fa-play" aria-hidden="true"></i></button>
</div>
<div id="Table_id"></div>
  </div>

</form>

</div>
  
</div>
   
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<script type="text/javascript">
     $(document).ready(function(){
   $("#TRACK").click(function(){

    var BUS  = $('#route1').find('option:selected').val(); 
    var date = $('#date').val();
    var timeone= $('#timeone').val();
    var timetwo= $('#timetwo').val();

   
        $.ajax({

              type:"post",//or POST
              url:'getspeed',
                                    
              data:{BUS:BUS,date:date,timeone:timeone,timetwo:timetwo},
                
                success:function(data){
                     
                      $('#Table_id').html(data);  
                  //location.reload();
                       
                } 
               });
          });
   });
</script>