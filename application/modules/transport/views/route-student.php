<?php //include 'inc/header.php'; ?>
 <?php //include 'inc/sidebar.php'; ?>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transport
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Transport</a></li>
        <li class="active">Report</li><li class="active">MIS REPORTS</li><li class="active">MIS REPORTS EXPORT</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
   
   <div class="col-sm-10" style="padding:0px !important" id="rightContainer">
     <div id="tableDiv" style="padding: 10px; height: 412px; overflow-y: auto;"><div style="padding:20px;">

<form action="Transport/RouteStudentResult" method="post">
<div class="row">

<div class="col-sm-12" style="padding:0px ;">

<div class="w3-group margin10">  

    <select class="form-control select1" style="width:100%;height:47px" id="actBus" name="route" required="">
    <option value="ALL">ALL STOPS</option>
    <?php foreach ($export->result() as $export){ ?>

            <option value="<?php echo $export->id; ?>"><?php echo $export->name; ?></option>
          <?php } ?>
          </select>  
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         Select Stop</label>
    </div>
</div>
<div class="col-sm-12">
  <button class="btn btn-primary" onclick="">EXPORT&nbsp;&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button>
</div>
  </div>

  


</form></div></div>
    <div id="formDiv" style="display: none; padding: 10px; height: 412px; overflow-y: auto;"></div>
    </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
 <?php //include 'inc/footer.php'; ?>
