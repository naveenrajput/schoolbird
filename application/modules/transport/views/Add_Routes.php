 <style>
      /* Always set the map height explicitly to define the size of the div
      * element that contains the map. */
      #map {
        height: 100%;
        width: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        width: 100%;
        padding: 0;
      }
      #floating-panel {
        /*position: absolute;*/ 
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
      #warnings-panel {
        width: 100%;
        height:10%;
        text-align: center;
      }
      .warnbox-content,.adp-warnbox{
        visibility:visible;
        height:0;
        width:0;

      }
      .drop {
  width: 300px;
  height: 100px;
  border: 1px solid blue;
  box-sizing: border-box;
}.bin {
  min-height: 50px;
  max-width: 500px;
  margin: 10px 10px 20px 0px;
  padding: 8px 8px 4px 8px;
  background-color: #ECEFF1;
}
.waypoints {
  min-height: 50px;
  max-width: 500px;
  margin: 10px 10px 20px 0px;
  padding: 8px 8px 4px 8px;
  background-color: #ECEFF1;
  width: 536px;
  display: grid: ;
}

/* Maybe you don't want to wrap the destination labels in <p> tags...*/
.bin>p {
  margin: 4px;
  font-weight: 600;
  pointer-events: none;
}

.item {
  cursor: pointer;  
  padding: 15px;
  margin-bottom: 8px;
  background-color: white;
  border-radius: 2px;
  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);
  transition: margin 0.1s;
}

.item-selected {
  box-shadow: 0 5px 5px -3px rgba(0,0,0,.2), 0 8px 10px 1px rgba(0,0,0,.14), 0 3px 14px 2px rgba(0,0,0,.12);
  margin: 5px -15px 15px -15px;
}

.bin-selectable {
  cursor: pointer;

}



/* Styles for use in the demo only */

body {
  font-family: 'Open Sans', sans-serif;
  background: #eeeeee;
  margin: 0px;
  color: #333333;
  overflow: auto;
}




.donebtn {
  margin: 10px;
  padding: 20px;
  background-color: #00c853;
  color: white;
  cursor: pointer;
  display: inline-block;
}



    </style>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <?php


    $lat = Array();
    $lngs = Array();
    $ids = Array();
    $addresses = Array();
    $identifiers = Array();
    $j=0;
    $waypoints = array();
    $centerpoint = array();

    $json = json_encode($waypoints,JSON_NUMERIC_CHECK);
    $wpJson =$json = str_replace('"','', (string) $json);
    $cpJson = json_encode($centerpoint,JSON_NUMERIC_CHECK);

    foreach ($getStops->result() as $row)
    {
      $stops[]= $row->name;
      $lat    =    $row->lat;
      $lngs[] = $row->lng;
      $ids[] = $row->id;
      $addresses[] .= $row->address;
      $identifiers[] .= $row->identifier;

      if($row->lat != '')
      {
    // $stopName[$j]= $row->name;
    // $stopLat[$j]= $row->lat;
    // $stopLong[$j] = $row->lng;
        if(empty($centerpoint)){
          $centerpoint = array('lat' => $row->lat, 'lng'=>$row->lng );
        }
        $temp = array('lat' => $row->lat, 'lng'=>$row->lng );
        array_push($waypoints, $temp);
        $temp = array();

        $j++;
      }
}//print_r($stops);die;



//echo $wpJson; exit;
//echo $cpJson; exit;
?>


<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Add Routes

   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Acadmic</a></li>
    <li class="active"> Add Routes</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

  <div class="row">
    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box ">

        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post"  id="formoid" data-toggle="validator" role="form" >
          <div class="box-body">


            <div class="row">
              <div class="col-md-6">

              </div>
              <div class="col-md-4">

              </div>

              <div class="col-md-4">
               <div class="form-group">

                <label>name </label>

                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-celender"></i></span>
                  <input type="text" id="inputName"  name= "name"class="form-control" placeholder="name" required>
                </div>
                <!-- /.input group -->
              </div><br/><br/>
              First Stop

              <select class="input form-control" name= "First_Stop" style="width:100%;" id="route1"  required="" >
                <option value="indore">Select Start Stop</option>
                <?php
                foreach($getStops->result() as $value)
                {
                  ?>
                  <option id="<?php echo $value->id;?>"  value="<?php echo $value->lat;?>,<?php echo $value->lng;?>"><?php echo $value->name;?></option>
                  <?php     
                }
                ?>
              </select>   <br/><br/>

              Last Stop
              <select class="input form-control" style="width:100%;"  name= "Last_Stop" id="route2" required >
                <option value="bhopal">Select Final Stop</option>
                <?php
                foreach($getStops->result() as $value)
                {
                  ?>
                  <option id="<?php echo $value->id;?>" value="<?php echo $value->lat;?>,<?php echo $value->lng;?>"><?php echo $value->name;?></option>
                  <?php     
                }
                ?>
              </select>  <br/><br/>

                
            </div>
            <div class="col-sm-3" style="height:600px;overflow:hidden">
              <p><strong>Order</strong></p>

             <!-- Destination bin -->

              <div class="bin"></div>
            </div>
          <div class="col-sm-5" style="height:600px;overflow:hidden">
            
             <span class="" style="font-size:11px !important;color:#999">
             Way Points (Click to select/deselect)</span>

             <br/>

             <div class="list-group-item filterTabLi">
              <br/>
              <input type="" class="input" name="" placeholder="Search Here" style="width:95%;" onkeyup="filterRouteSelect('')" id="stopSearcher">
              <br/>
              <br/>
              <table style="width:100%" class="filterTabs">
                <tr>
                  <td class="active"  id="allTab">ALL</td>
                  <td  id="selectedTab">SELECTED</td>
                  <td  id="not-selectedTab">NOT-SELECTED</td>
                </tr>
              </table>

            </div>
      
      <p><strong>Skills</strong></p>
      
        <select multiple="multiple" class="waypoints" value="" id="waypoints">
          <?php $i=1; foreach($getStops->result() as $value) { ?>
          <option id="<?php echo $value->id;?>" class="item "id="item<?php echo $i;?>" value='<?php echo $value->lat;?>,<?php echo $value->lng;?>'><span style="font-size:20px;"><?php echo $value->address;?></span></option>
          <?php $i++; } ?>

        </select> 
          </div>

    </div>
            
          </div>
          <div class="col-sm-12" style="height:600px;overflow:hidden">
            <div id="map"></div> 
            <div id="warnings-panel"></div>
          </div>
        </div>

          <div id="directions-panel"></div>
          <button type="button" id="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>

  </div>
 </div>
</section>
</div>



<script type="text/javascript">
  var waypoints=[];

      function initMap() {

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({
    draggable: true,
    map: map,
    panel: document.getElementById('right-panel')
  });
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: {lat: 22.7196, lng: 75.8577}
        });
        directionsDisplay.setMap(map);
      document.getElementById('waypoints').addEventListener('click', function() {
          calculateAndDisplayRoute(directionsService, directionsDisplay);
        });
      }
      
     
      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var waypts = [];
    var checkboxArray = document.getElementById('waypoints');
    $(".bin option").remove();
     waypoints.splice(0,waypoints.length);
    for (var i = 0; i < checkboxArray.length; i++) {
      if (checkboxArray.options[i].selected) {
        
        waypts.push({
          location: checkboxArray[i].value,
          stopover: true
        });
     waypoints.push(checkboxArray[i].id,);
         $(checkboxArray[i])
                .clone()
                .appendTo(".bin");

      }
    }
    alert(waypts);
    directionsService.route({
          origin: document.getElementById('route1').value,
          destination: document.getElementById('route2').value,
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {

            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var summaryPanel = document.getElementById('directions-panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
              var routeSegment = i + 1;
              summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                  '</b><br>';
              summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
              summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
              summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
      
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&callback=initMap">
    </script> 

<script type="text/javascript">
   $("#submit").click(function(){
   var route1 = $('#route1').find('option:selected').attr('id'); 
   var route2 = $('#route2').find('option:selected').attr('id');
   var name   = $('#inputName').val();
        $.ajax({

              type:"post",//or POST
              url:'Add_new_routes',
                                    
              data:{name:name,route1:route1,route2:route2,waypts:waypoints},
                
                success:function(){

                  location.reload();
                       
                } 
               });
          });
</script> 
