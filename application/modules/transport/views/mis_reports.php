
<style type="text/css">
  .grid-container {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: 50px auto;
  align-content: center;
  grid-gap: 1em;
}

.grid-container > div {
  background-color: #ffffff;
  text-align: center;
  border: 1px solid black;
}
.midBox:hover {
    background: #c9302c;
    color: #fff;
}
.midBox {
    border: 1px #eee solid;
    padding: 20px;
    text-align: center;
    cursor: pointer;
}
</style>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         MIS REPORTS
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Reports</a></li>
        <li class="active"> MIS REPORTS</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
   
      <!-- /.row -->
   <div class="grid-container">
  <div class="midBox" onclick="window.open('Transport/BusRoute')">
  Bus Vs Route Report
  </div>
  <div class="midBox" onclick="window.open('Transport/ParentsLoggedIn')">
  Parents Logged In
  </div>
  <div class="midBox" onclick="window.open('Transport/NotificationMisReport')">
  Notification Mis Report
  </div><br>
  <div class="midBox" onclick="window.open('Transport/PortalLoginMis')">
  Portal Login Mis
  </div>
  
  <!-- <div class="midBox" onclick="window.open('Transport/StopStudent')">
  Stop vs Student Report
  </div>
 <div class="midBox" onclick="window.open('Transport/RouteStudent')">
  Route vs Student Report
  </div> -->
  <!-- <div class="midBox" onclick="window.open('Transport/NotificationSMS')">
  Notification vs SMS Report
  </div> -->
  
<!--   <div class="midBox" onclick="window.open('Transport/StudentMisReport')">
  Student Mis Report
  </div><br> -->
  
  
</div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
 <?php //include 'inc/footer.php'; ?>
