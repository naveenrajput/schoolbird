<style>



 </style>
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" ">
  <div class="col-md-6 col-xs-12 col-sm-8 content-header">
      <h1 class="">
        Stop
     
      </h1>
    <ol class="breadcrumb" style="background:none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"> Transport</a></li>
        <li class="active">Stop</li>
      </ol>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-4 content-header" style="text-align:right;">
    
        <a  href="<?php echo base_url('Add_Stop')?>" class="btn btn-primary"> <i class="fa fa-plus"></i> &nbsp; Add Stop</a>
     <a  href="#" class="btn  btn-primary sp-10"  data-toggle="modal" data-target="#modal-enquiry"> <i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp; Send Enquiry</a>
    </div>
    </section>

    <!-- Main content -->
     <section class="content">
      <div class="row">
        <div class="col-xs-12">
    

     <div class="box">
      <div class="box-header with-border mr-top-20 text-center">
      <div class="form-group col-md-5">
            
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1" placeholder="Start Date">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
                <div class="form-group col-md-5">
               
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="End Date">
                </div>
                <!-- /.input group -->
              </div>
      <div class="col-md-2" >
        <button type="submit" class="btn btn-primary"> &nbsp;&nbsp;Go&nbsp;&nbsp;</button>
              
              </div>
      
      
      
        </div>
            <!-- /.box-header -->
      <!----<div class="box-body">
      <div class="dropdown pull-right">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> Default Reminder
  <span class="caret"></span></button>
  <ul class="dropdown-menu">
    <li><a href="#">HTML</a></li>
    <li><a href="#">CSS</a></li>
    <li><a href="#">JavaScript</a></li>
  </ul>
</div>




      <!----<div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example
    <span class="caret"></span></button>
    <ul class="dropdown-menu" style=" max-height: 150px; overflow-y: auto;">
      <input class="form-control" id="myInput" type="text" placeholder="Search..">
      <li><a href="#">1 Day</a></li>
      <li><a href="#">2 Days</a></li>
    <li><a href="#">3 Day</a></li>
      <li><a href="#">4 Days</a></li>
      <li><a href="#">5 Day</a></li>
      <li><a href="#">6 Days</a></li>
    <li><a href="#">7 Day</a></li>
      <li><a href="#">8 Days</a></li>
   
    </ul>
  </div>
      
      
      
      </div>----->
      
            <div class="box-body table-responsive">
      
             <!-- <table id="example" class="display nowrap" style="width:100%">--->
              <table id="example" class="table table-bordered " >
        <div class="txt-dis">Export in Below Format</div>
        <thead>
            <tr>
    
                <th><input type="checkbox" id="selectall"> All</input></th>
                <th>S.no</th>
                <th>Name</th>
                <th>Address</th>
                <th>createdate</th>
                <th>Action</th>
                
        
        
            </tr>
        </thead>
        <tbody>
   
            <tr>
      <td>
    <input type="checkbox" class="selectedId" name="selectedId" />
      </td>
              
                <td></td>
                <td></td>
                <td></td>
                <td></td>
         
         
               
        <td>
              <ul class="table-icons">
      
          <li><a href="" class="table-icon" title="VIEW ON MAP">
    <span class="label label-primary"><i class="fa fa-map-marker" aria-hidden="true"></i> VIEW ON MAP</span>
    </a></li>
    </ul>
          </td>
              
        
            </tr>
           
   
        </tbody>
        <tfoot>
            <tr>
              <th> All</input></th>
                 <th>S.no</th>
                <th>Name</th>
                <th>Address</th>
                <th>createdate</th>
                <th>Action</th>
               

            </tr>
        </tfoot>
    </table>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

 <div class="modal fade" id="modal-reminder">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Reminder</h4>
              </div>
              <div class="modal-body">
              <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2019
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                      <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                      <div class="timeline-body">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                        quora plaxo ideeli hulu weebly balihoo...
                      </div>
                     
                    </div>
                  </li>
                  <!-- END timeline item -->
                 
                  <li class="time-label">
                        <span class="bg-green">
                          3 Mar. 2019
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-yellow"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                      <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                      <div class="timeline-body">
                        Take me to your leader!
                        Switzerland is small and neutral!
                        We are more like Germany, ambitious and misunderstood!
                      </div>
                      
                    </div>
                  </li>
                  <!-- END timeline item -->
                 
                </ul>
        
        <div class="row">
        
        <div class="col-md-6">
                  <div class="form-group">
                    <label> Date </label>
                    <div class="input-group date">
                      <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div>
                      <input type="text" class="form-control pull-right" id="datepicker">
                    </div>
                    <!-- /.input group --> 
                  </div>
                </div>
        <div class="col-md-6">
                  <div class="form-group">
                    <label> Time </label>
                    <div class="input-group">
          <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" class="form-control timepicker">

                    
                  </div>
                    <!-- /.input group --> 
                  </div>
                </div>
            <div class="col-md-12">
      <textarea class="form-control mr-top-10" rows="3" placeholder="Type a comment  ..." spellcheck="false"></textarea>
        
       </div> 
        </div>
        
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
    