<?php

        
$busid = $this->input->post('BUS');
$fordate = $this->input->post('date');
$fromdate = $this->input->post('timeone');
$todate = $this->input->post('timetwo');
 
 


    
foreach ($getdata->result() as $row ) {
    
    $busName = $row->bus;

}


$data = Array();
$thisData = Array();
$k=0;

?>

<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/style.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/replay-tracking/w3.css'); ?>"/>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../scripts/misc.js"></script>

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/dc4f36d12e.js"></script>
 <script src="http://code.jquery.com/jquery.min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<style type="text/css"></style>
<title></title>

<script type="text/javascript">
var xAxisArray = [];
var seriesArray = [];
    <?php
    $j=0;
    $deviceArray = Array();
    $speedArray = Array();
    $ltlng = Array();
foreach ($getList->result() as $row ) {
    
    $temp = $row->devicetime;
    $temp = explode(" ",$temp);
    $temp1 = explode(":",$temp[1]);
    $temp2 = $temp1[0].":".$temp1[1];
    $deviceArray[] .= $row->devicetime;
    $speedArray[] .= $row->speed; 
        $ltlng[$j] = $row->lat.",".$row->lng;
?>
xAxisArray[<?php echo $j;?>] = '<?php echo $temp2;?>'; 
seriesArray[<?php echo $j;?>] = <?php echo $row->speed;?>;
<?php
$j++;
}

  
    ?>

</script>






<script type='text/javascript'>//<![CDATA[

// Data retrieved from http://vikjavev.no/ver/index.php?spenn=2d&sluttid=16.06.2015.

var chart;

$(function () {
   chart =  Highcharts.chart('container', {
        chart: {
            type: 'spline',
                        style: {
            fontFamily: 'Rubik'
        }

        },
        title: {
            text: 'Speed Chart For <?php echo $busName;?>'
        },
        subtitle: {
            text: 'Data for <?php echo date("d, M y",strtotime($fordate));?>'
        },
        xAxis: {
            categories: xAxisArray,
            labels:{
                enabled:false
            }
        },
        yAxis: {
            title: {
                text: 'Speed Kmph'
            },
            minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
            plotBands: [{ 
                from: 45,
                to: 500,
                color: 'rgba(231, 76, 60, 0.4)',
                label: {
                    text: 'SPEED ALERT',
                    style: {
                        color: '#606060'
                    }
                }
            }]
        },
        tooltip: {
            valueSuffix: ' Kmph',
            enabled: true,
    formatter: function() {
                return '<b>'+ this.y +' Kmph</b><br/>'+this.x;

                  }
        },
        plotOptions: {
            spline: {
                lineWidth: 4,
                states: {
                    hover: {
                        lineWidth: 5
                    }
                },
                marker: {
                    enabled: true
                },
            }
        },
        series: [{
            name: '<?php echo $busName;?>',
            data: seriesArray

        }],
        navigation: {
            menuItemStyle: {
                fontSize: '10px'
            }
        }
    });
});
//]]> 

</script>

  <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div style="float:right">
<div class="btn-group">
  <button class="btn btn-sm btn-default" onclick="$('#container').show();$('#containerTab').hide();changeType('column')">
                <i class="fa fa-bar-chart"></i>&nbsp;BAR
    </button>
    <button class="btn btn-sm btn-default" onclick="$('#container').show();$('#containerTab').hide();changeType('spline')">
                <i class="fa fa-line-chart"></i>&nbsp;SPLINE
    </button><button class="btn btn-sm btn-default" onclick="$('#container').hide();$('#containerTab').show();">
                <i class="fa fa-th-large"></i>&nbsp;DATA TABLE
    </button>
</div>
</div>
<br/><br/>
<div id="container" style="width: 100%; height: 400px; margin: 0 auto"></div>
<div id="containerTab" style="width: 100%; height: 400px; margin: 0 auto;overflow-y:auto;display:none">
    <table class="table table-striped table-hovered fetch">
        <tr>
            <th>#</th><th>Speed</th><th>Time</th><td>Address</td>
        </tr>
        <?php
        foreach($speedArray as $key => $val)
        {
            ?>
            <tr class="<?php if($val >= 50) echo "bg-danger" ;?>">
                <td><?php echo $key+1;?></td>
                <td><?php echo $val;?></td>
                <td><?php echo $deviceArray[$key];?></td>
                <td class="text-default"  lang="<?php echo $ltlng[$key];?>" onclick="getAddress(this)">View Address</td>

            </tr>
            <?php
        }
        ?>
    </table>
</div>
  <script type="text/javascript">
      window.top.window.hideProcessing();
          function changeType(newType) {
        chart.update({
            chart: {
                type:newType
            } 
        });


    }
  </script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places&callback=initMap">
    
</script>
<script src="<?php echo base_url('assets/js/stats.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/replay-tracking/misc.js'); ?>"></script>




