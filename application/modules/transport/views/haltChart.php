

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halt-Chart
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Transport</a></li>
        <li class="active">Report</li><li class="active">MIS REPORTS</li><li class="active">Halt-Chart</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
   
   
     <div id="tableDiv" style="padding: 10px; height: 412px; overflow-y: auto;"><div style="padding:20px;">

<form  method="post"id="formid" >
<div class="row">

<div class="col-sm-6" style="padding:0px ;">
<div class="w3-group margin10" style="width:100%">
    <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         Select Bus</label>  
    <select class="form-control select1"  id="route1" name="bus" required="">
      <option value="0">Select</option>
      <?php foreach ($export->result() as $export){ ?>
    <option value="<?php echo $export->bus; ?>"><?php echo $export->bus; ?></option>
  <?php } ?>
         </select>  
          
    </div>
</div>
<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%">    
         <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         Minimum Halt Time (Mins)</label>  
          <input class="form-control" type="number"value="0" style="width:100%" id="halttime" name="halttime" required="">
          
    </div>
</div>

<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%"> 
        <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         From Date</label>     
          <input class="form-control" type="date" style="width:100%" id="fromdate" name="fromdate" required="">
         
    </div>
</div>


<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%">
        <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         To Date</label>      
          <input class="form-control" type="date" style="width:100%" id="todate" name="todate" required="">
          
    </div>
</div>


<div class="col-sm-6" style="margin-top:10px;">
  <button class="btn btn-primary" id="TRACK"type="button">REPLAY TRACK&nbsp;&nbsp;<i class="fa fa-play" aria-hidden="true"></i></button>
</div>

  </div>

</form>


  <div id="Table_id"></div>
</div>
   </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<script type="text/javascript">
     $(document).ready(function(){
   $("#TRACK").click(function(){

   var BUS = $('#route1').find('option:selected').val(); 
    var halttime   = $('#halttime').val();
    var fromdate   = $('#fromdate').val();
    var todate   = $('#todate').val();
        $.ajax({

              type:"post",//or POST
              url:'getchart',
                                    
              data:{BUS:BUS,halttime:halttime,fromdate:fromdate,todate:todate},
                
                success:function(data){

                  $('#Table_id').html(data);
                       
                } 
               });
          });
   });
</script>