<style type="text/css">
 #map {
        height: 100%;
        width: 100%;
        margin-top:20px;
       
      }
    
.waypoints {
  min-height: 50px;
 
  margin: 10px 10px 20px 0px;
  padding: 8px 8px 4px 8px;
  background-color: #ECEFF1;
 
  display: grid: ;
}


.item {
  cursor: pointer;  
  padding: 15px;
  margin-bottom: 8px;
  background-color: white;
  border-radius: 2px;
  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);
  transition: margin 0.1s;
}

.item-selected {
  box-shadow: 0 5px 5px -3px rgba(0,0,0,.2), 0 8px 10px 1px rgba(0,0,0,.14), 0 3px 14px 2px rgba(0,0,0,.12);
  margin: 5px -15px 15px -15px;
}

    </style>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transport
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Transport</a></li>
        <li class="active">Report</li><li class="active">MIS REPORTS</li><li class="active">MIS REPORTS EXPORT</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
   
   
     <div id="tableDiv" style="padding: 10px; height: 412px; overflow-y: auto;"><div style="padding:20px;">

<form  method="post"id="formid" >
<div class="row">

<div class="col-sm-6" style="padding:0px ;">
<div class="w3-group margin10" style="width:100%">  
    <select class="form-control select1"  id="route1" name="bus" required="">
      <option value="0">Select</option>
      <?php foreach ($export->result() as $export){ ?>
    <option value="<?php echo $export->bus; ?>"><?php echo $export->bus; ?></option>
  <?php } ?>
         </select>  
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         Select Bus</label>
    </div>
</div>
<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%">      
          <input class="form-control" type="date" style="width:100%" id="date" name="date" required="">
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         For Date</label>
    </div>
</div>

<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%">      
          <input class="form-control" type="time" style="width:100%" id="timeone" name="fromtime" required="">
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         From Time</label>
    </div>
</div>


<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%">      
          <input class="form-control" type="time" style="width:100%" id="timetwo" name="totime" required="">
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         To Time</label>
    </div>
</div><br><br>
<div class="col-sm-6">
  <button class="btn btn-primary" id="TRACK"type="button">REPLAY TRACK&nbsp;&nbsp;<i class="fa fa-play" aria-hidden="true"></i></button>
</div>
  </div>

</form>

</div>
  
</div>
    <div id="formDiv" style="display: none; padding: 10px; height: 412px; overflow-y: auto;"></div>
    
      <!-- /.row (main row) -->

      <div class="col-sm-12" style="height:600px;overflow:hidden;">
            <div id="map"></div>
      
          </div>       
      
    <div id="Table_id"></div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<script type="text/javascript">
     $(document).ready(function(){
   $("#TRACK").click(function(){

   var BUS = $('#route1').find('option:selected').val(); 
    var date   = $('#date').val();
    var timeone   = $('#timeone').val();
    var timetwo   = $('#timetwo').val();

   
        $.ajax({

              type:"post",//or POST
              url:'getdirection',
                                    
              data:{BUS:BUS,date:date,timeone:timeone,timetwo:timetwo},
                
                success:function(data){
                     
                      $('#Table_id').html(data);  
                  //location.reload();
                       
                } 
               });
          });
   });
</script>