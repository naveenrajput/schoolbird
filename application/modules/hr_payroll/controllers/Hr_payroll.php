<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hr_payroll extends MY_Controller {

	
	public function __construct(){
        parent::__construct();
        //$this->load->model('contact_model', 'contact');
        $this->load->helper('url');
      }

	// For frontdask employees
	public function employees()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // For frontdask employee_category
	public function employee_category()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // For frontdask designations
	public function designations()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // For frontdask departments
	public function departments()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // For frontdask employee_attendance
	public function employee_attendance()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

    // For frontdask leave_management
	public function leave_management()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

   // For frontdask leave_type
	public function leave_type()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// For frontdask payheads
	public function payheads()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}

	// For frontdask employee_access
	public function employee_access()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}
	
	// For frontdask dashboard_access
	public function dashboard_access()
	{
		$this->load->view('../../inc/header');
		$this->load->view('../../inc/sidebar');
		$this->load->view('index');
		$this->load->view('../../inc/footer');
	}


}
